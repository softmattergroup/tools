ль
ЬЄ
8
Const
output"dtype"
valuetensor"
dtypetype
l
LookupTableExportV2
table_handle
keys"Tkeys
values"Tvalues"
Tkeystype"
TvaluestypeИ
и
MutableHashTableV2
table_handle"
	containerstring "
shared_namestring "!
use_node_name_sharingbool( "
	key_dtypetype"
value_dtypetypeИ

NoOp
│
PartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetypeИ
╛
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring И
Ц
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 И"serve*2.3.12v2.3.0-54-gfcc4b966f18Ю░
v
dense/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
▄А*
shared_namedense/kernel
o
 dense/kernel/Read/ReadVariableOpReadVariableOpdense/kernel* 
_output_shapes
:
▄А*
dtype0
m

dense/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:А*
shared_name
dense/bias
f
dense/bias/Read/ReadVariableOpReadVariableOp
dense/bias*
_output_shapes	
:А*
dtype0
z
dense_1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
АА*
shared_namedense_1/kernel
s
"dense_1/kernel/Read/ReadVariableOpReadVariableOpdense_1/kernel* 
_output_shapes
:
АА*
dtype0
q
dense_1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:А*
shared_namedense_1/bias
j
 dense_1/bias/Read/ReadVariableOpReadVariableOpdense_1/bias*
_output_shapes	
:А*
dtype0
y
dense_2/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	А@*
shared_namedense_2/kernel
r
"dense_2/kernel/Read/ReadVariableOpReadVariableOpdense_2/kernel*
_output_shapes
:	А@*
dtype0
p
dense_2/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*
shared_namedense_2/bias
i
 dense_2/bias/Read/ReadVariableOpReadVariableOpdense_2/bias*
_output_shapes
:@*
dtype0
`
meanVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_namemean
Y
mean/Read/ReadVariableOpReadVariableOpmean*
_output_shapes
:*
dtype0
h
varianceVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_name
variance
a
variance/Read/ReadVariableOpReadVariableOpvariance*
_output_shapes
:*
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0	
x
dense_3/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*
shared_namedense_3/kernel
q
"dense_3/kernel/Read/ReadVariableOpReadVariableOpdense_3/kernel*
_output_shapes

:@@*
dtype0
p
dense_3/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*
shared_namedense_3/bias
i
 dense_3/bias/Read/ReadVariableOpReadVariableOpdense_3/bias*
_output_shapes
:@*
dtype0
x
dense_4/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:E@*
shared_namedense_4/kernel
q
"dense_4/kernel/Read/ReadVariableOpReadVariableOpdense_4/kernel*
_output_shapes

:E@*
dtype0
p
dense_4/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*
shared_namedense_4/bias
i
 dense_4/bias/Read/ReadVariableOpReadVariableOpdense_4/bias*
_output_shapes
:@*
dtype0
x
dense_5/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*
shared_namedense_5/kernel
q
"dense_5/kernel/Read/ReadVariableOpReadVariableOpdense_5/kernel*
_output_shapes

:@@*
dtype0
p
dense_5/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*
shared_namedense_5/bias
i
 dense_5/bias/Read/ReadVariableOpReadVariableOpdense_5/bias*
_output_shapes
:@*
dtype0
x
dense_6/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*
shared_namedense_6/kernel
q
"dense_6/kernel/Read/ReadVariableOpReadVariableOpdense_6/kernel*
_output_shapes

:@@*
dtype0
p
dense_6/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*
shared_namedense_6/bias
i
 dense_6/bias/Read/ReadVariableOpReadVariableOpdense_6/bias*
_output_shapes
:@*
dtype0
x
dense_7/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*
shared_namedense_7/kernel
q
"dense_7/kernel/Read/ReadVariableOpReadVariableOpdense_7/kernel*
_output_shapes

:@@*
dtype0
p
dense_7/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*
shared_namedense_7/bias
i
 dense_7/bias/Read/ReadVariableOpReadVariableOpdense_7/bias*
_output_shapes
:@*
dtype0
x
dense_8/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*
shared_namedense_8/kernel
q
"dense_8/kernel/Read/ReadVariableOpReadVariableOpdense_8/kernel*
_output_shapes

:@@*
dtype0
p
dense_8/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*
shared_namedense_8/bias
i
 dense_8/bias/Read/ReadVariableOpReadVariableOpdense_8/bias*
_output_shapes
:@*
dtype0
x
dense_9/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*
shared_namedense_9/kernel
q
"dense_9/kernel/Read/ReadVariableOpReadVariableOpdense_9/kernel*
_output_shapes

:@@*
dtype0
p
dense_9/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*
shared_namedense_9/bias
i
 dense_9/bias/Read/ReadVariableOpReadVariableOpdense_9/bias*
_output_shapes
:@*
dtype0
z
dense_10/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@* 
shared_namedense_10/kernel
s
#dense_10/kernel/Read/ReadVariableOpReadVariableOpdense_10/kernel*
_output_shapes

:@*
dtype0
r
dense_10/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_namedense_10/bias
k
!dense_10/bias/Read/ReadVariableOpReadVariableOpdense_10/bias*
_output_shapes
:*
dtype0
f
	Adam/iterVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	Adam/iter
_
Adam/iter/Read/ReadVariableOpReadVariableOp	Adam/iter*
_output_shapes
: *
dtype0	
j
Adam/beta_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_1
c
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_output_shapes
: *
dtype0
j
Adam/beta_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_2
c
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
_output_shapes
: *
dtype0
h

Adam/decayVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name
Adam/decay
a
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_output_shapes
: *
dtype0
x
Adam/learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameAdam/learning_rate
q
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*
_output_shapes
: *
dtype0
Ж
string_lookup_index_tableMutableHashTableV2*
_output_shapes
: *
	key_dtype0*
shared_name
table_26*
value_dtype0	
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
b
count_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	count_1
[
count_1/Read/ReadVariableOpReadVariableOpcount_1*
_output_shapes
: *
dtype0
Д
Adam/dense/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:
▄А*$
shared_nameAdam/dense/kernel/m
}
'Adam/dense/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense/kernel/m* 
_output_shapes
:
▄А*
dtype0
{
Adam/dense/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:А*"
shared_nameAdam/dense/bias/m
t
%Adam/dense/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense/bias/m*
_output_shapes	
:А*
dtype0
И
Adam/dense_1/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:
АА*&
shared_nameAdam/dense_1/kernel/m
Б
)Adam/dense_1/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_1/kernel/m* 
_output_shapes
:
АА*
dtype0

Adam/dense_1/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:А*$
shared_nameAdam/dense_1/bias/m
x
'Adam/dense_1/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_1/bias/m*
_output_shapes	
:А*
dtype0
З
Adam/dense_2/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:	А@*&
shared_nameAdam/dense_2/kernel/m
А
)Adam/dense_2/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_2/kernel/m*
_output_shapes
:	А@*
dtype0
~
Adam/dense_2/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_2/bias/m
w
'Adam/dense_2/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_2/bias/m*
_output_shapes
:@*
dtype0
Ж
Adam/dense_3/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*&
shared_nameAdam/dense_3/kernel/m

)Adam/dense_3/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_3/kernel/m*
_output_shapes

:@@*
dtype0
~
Adam/dense_3/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_3/bias/m
w
'Adam/dense_3/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_3/bias/m*
_output_shapes
:@*
dtype0
Ж
Adam/dense_4/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:E@*&
shared_nameAdam/dense_4/kernel/m

)Adam/dense_4/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_4/kernel/m*
_output_shapes

:E@*
dtype0
~
Adam/dense_4/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_4/bias/m
w
'Adam/dense_4/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_4/bias/m*
_output_shapes
:@*
dtype0
Ж
Adam/dense_5/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*&
shared_nameAdam/dense_5/kernel/m

)Adam/dense_5/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_5/kernel/m*
_output_shapes

:@@*
dtype0
~
Adam/dense_5/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_5/bias/m
w
'Adam/dense_5/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_5/bias/m*
_output_shapes
:@*
dtype0
Ж
Adam/dense_6/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*&
shared_nameAdam/dense_6/kernel/m

)Adam/dense_6/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_6/kernel/m*
_output_shapes

:@@*
dtype0
~
Adam/dense_6/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_6/bias/m
w
'Adam/dense_6/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_6/bias/m*
_output_shapes
:@*
dtype0
Ж
Adam/dense_7/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*&
shared_nameAdam/dense_7/kernel/m

)Adam/dense_7/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_7/kernel/m*
_output_shapes

:@@*
dtype0
~
Adam/dense_7/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_7/bias/m
w
'Adam/dense_7/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_7/bias/m*
_output_shapes
:@*
dtype0
Ж
Adam/dense_8/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*&
shared_nameAdam/dense_8/kernel/m

)Adam/dense_8/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_8/kernel/m*
_output_shapes

:@@*
dtype0
~
Adam/dense_8/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_8/bias/m
w
'Adam/dense_8/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_8/bias/m*
_output_shapes
:@*
dtype0
Ж
Adam/dense_9/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*&
shared_nameAdam/dense_9/kernel/m

)Adam/dense_9/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_9/kernel/m*
_output_shapes

:@@*
dtype0
~
Adam/dense_9/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_9/bias/m
w
'Adam/dense_9/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_9/bias/m*
_output_shapes
:@*
dtype0
И
Adam/dense_10/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@*'
shared_nameAdam/dense_10/kernel/m
Б
*Adam/dense_10/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_10/kernel/m*
_output_shapes

:@*
dtype0
А
Adam/dense_10/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*%
shared_nameAdam/dense_10/bias/m
y
(Adam/dense_10/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_10/bias/m*
_output_shapes
:*
dtype0
Д
Adam/dense/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:
▄А*$
shared_nameAdam/dense/kernel/v
}
'Adam/dense/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense/kernel/v* 
_output_shapes
:
▄А*
dtype0
{
Adam/dense/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:А*"
shared_nameAdam/dense/bias/v
t
%Adam/dense/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense/bias/v*
_output_shapes	
:А*
dtype0
И
Adam/dense_1/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:
АА*&
shared_nameAdam/dense_1/kernel/v
Б
)Adam/dense_1/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_1/kernel/v* 
_output_shapes
:
АА*
dtype0

Adam/dense_1/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:А*$
shared_nameAdam/dense_1/bias/v
x
'Adam/dense_1/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_1/bias/v*
_output_shapes	
:А*
dtype0
З
Adam/dense_2/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:	А@*&
shared_nameAdam/dense_2/kernel/v
А
)Adam/dense_2/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_2/kernel/v*
_output_shapes
:	А@*
dtype0
~
Adam/dense_2/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_2/bias/v
w
'Adam/dense_2/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_2/bias/v*
_output_shapes
:@*
dtype0
Ж
Adam/dense_3/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*&
shared_nameAdam/dense_3/kernel/v

)Adam/dense_3/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_3/kernel/v*
_output_shapes

:@@*
dtype0
~
Adam/dense_3/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_3/bias/v
w
'Adam/dense_3/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_3/bias/v*
_output_shapes
:@*
dtype0
Ж
Adam/dense_4/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:E@*&
shared_nameAdam/dense_4/kernel/v

)Adam/dense_4/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_4/kernel/v*
_output_shapes

:E@*
dtype0
~
Adam/dense_4/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_4/bias/v
w
'Adam/dense_4/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_4/bias/v*
_output_shapes
:@*
dtype0
Ж
Adam/dense_5/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*&
shared_nameAdam/dense_5/kernel/v

)Adam/dense_5/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_5/kernel/v*
_output_shapes

:@@*
dtype0
~
Adam/dense_5/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_5/bias/v
w
'Adam/dense_5/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_5/bias/v*
_output_shapes
:@*
dtype0
Ж
Adam/dense_6/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*&
shared_nameAdam/dense_6/kernel/v

)Adam/dense_6/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_6/kernel/v*
_output_shapes

:@@*
dtype0
~
Adam/dense_6/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_6/bias/v
w
'Adam/dense_6/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_6/bias/v*
_output_shapes
:@*
dtype0
Ж
Adam/dense_7/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*&
shared_nameAdam/dense_7/kernel/v

)Adam/dense_7/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_7/kernel/v*
_output_shapes

:@@*
dtype0
~
Adam/dense_7/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_7/bias/v
w
'Adam/dense_7/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_7/bias/v*
_output_shapes
:@*
dtype0
Ж
Adam/dense_8/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*&
shared_nameAdam/dense_8/kernel/v

)Adam/dense_8/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_8/kernel/v*
_output_shapes

:@@*
dtype0
~
Adam/dense_8/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_8/bias/v
w
'Adam/dense_8/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_8/bias/v*
_output_shapes
:@*
dtype0
Ж
Adam/dense_9/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*&
shared_nameAdam/dense_9/kernel/v

)Adam/dense_9/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_9/kernel/v*
_output_shapes

:@@*
dtype0
~
Adam/dense_9/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_9/bias/v
w
'Adam/dense_9/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_9/bias/v*
_output_shapes
:@*
dtype0
И
Adam/dense_10/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@*'
shared_nameAdam/dense_10/kernel/v
Б
*Adam/dense_10/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_10/kernel/v*
_output_shapes

:@*
dtype0
А
Adam/dense_10/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*%
shared_nameAdam/dense_10/bias/v
y
(Adam/dense_10/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_10/bias/v*
_output_shapes
:*
dtype0
G
ConstConst*
_output_shapes
: *
dtype0	*
value	B	 R
ш
PartitionedCallPartitionedCall*	
Tin
 *
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8В *"
fR
__inference_<lambda>_8084

NoOpNoOp^PartitionedCall
°
Hstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2LookupTableExportV2string_lookup_index_table*
Tkeys0*
Tvalues0	*,
_class"
 loc:@string_lookup_index_table*2
_output_shapes 
:         :         
Чr
Const_1Const"/device:CPU:0*
_output_shapes
: *
dtype0*╨q
value╞qB├q B╝q
Ь
layer-0
layer_with_weights-0
layer-1
layer_with_weights-1
layer-2
layer_with_weights-2
layer-3
layer-4
layer_with_weights-3
layer-5
layer_with_weights-4
layer-6
layer_with_weights-5
layer-7
	layer-8

layer_with_weights-6

layer-9
layer_with_weights-7
layer-10
layer_with_weights-8
layer-11
layer_with_weights-9
layer-12
layer_with_weights-10
layer-13
layer_with_weights-11
layer-14
layer_with_weights-12
layer-15
	optimizer
	variables
regularization_losses
trainable_variables
	keras_api

signatures
 
=
state_variables
_index_lookup_layer
	keras_api
h

kernel
bias
	variables
regularization_losses
trainable_variables
	keras_api
h

 kernel
!bias
"	variables
#regularization_losses
$trainable_variables
%	keras_api
 
h

&kernel
'bias
(	variables
)regularization_losses
*trainable_variables
+	keras_api
]
,state_variables
-_broadcast_shape
.mean
/variance
	0count
1	keras_api
h

2kernel
3bias
4	variables
5regularization_losses
6trainable_variables
7	keras_api
R
8	variables
9regularization_losses
:trainable_variables
;	keras_api
h

<kernel
=bias
>	variables
?regularization_losses
@trainable_variables
A	keras_api
h

Bkernel
Cbias
D	variables
Eregularization_losses
Ftrainable_variables
G	keras_api
h

Hkernel
Ibias
J	variables
Kregularization_losses
Ltrainable_variables
M	keras_api
h

Nkernel
Obias
P	variables
Qregularization_losses
Rtrainable_variables
S	keras_api
h

Tkernel
Ubias
V	variables
Wregularization_losses
Xtrainable_variables
Y	keras_api
h

Zkernel
[bias
\	variables
]regularization_losses
^trainable_variables
_	keras_api
h

`kernel
abias
b	variables
cregularization_losses
dtrainable_variables
e	keras_api
°
fiter

gbeta_1

hbeta_2
	idecay
jlearning_ratem┤m╡ m╢!m╖&m╕'m╣2m║3m╗<m╝=m╜Bm╛Cm┐Hm└Im┴Nm┬Om├Tm─Um┼Zm╞[m╟`m╚am╔v╩v╦ v╠!v═&v╬'v╧2v╨3v╤<v╥=v╙Bv╘Cv╒Hv╓Iv╫Nv╪Ov┘Tv┌Uv█Zv▄[v▌`v▐av▀
┐
1
2
 3
!4
&5
'6
.7
/8
09
210
311
<12
=13
B14
C15
H16
I17
N18
O19
T20
U21
Z22
[23
`24
a25
 
ж
0
1
 2
!3
&4
'5
26
37
<8
=9
B10
C11
H12
I13
N14
O15
T16
U17
Z18
[19
`20
a21
н
	variables
regularization_losses
knon_trainable_variables
llayer_metrics
mmetrics

nlayers
trainable_variables
olayer_regularization_losses
 
 
0
pstate_variables

q_table
r	keras_api
 
XV
VARIABLE_VALUEdense/kernel6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUE
TR
VARIABLE_VALUE
dense/bias4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1
 

0
1
н
	variables
regularization_losses
snon_trainable_variables
tlayer_metrics
umetrics

vlayers
trainable_variables
wlayer_regularization_losses
ZX
VARIABLE_VALUEdense_1/kernel6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEdense_1/bias4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUE

 0
!1
 

 0
!1
н
"	variables
#regularization_losses
xnon_trainable_variables
ylayer_metrics
zmetrics

{layers
$trainable_variables
|layer_regularization_losses
ZX
VARIABLE_VALUEdense_2/kernel6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEdense_2/bias4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUE

&0
'1
 

&0
'1
п
(	variables
)regularization_losses
}non_trainable_variables
~layer_metrics
metrics
Аlayers
*trainable_variables
 Бlayer_regularization_losses
#
.mean
/variance
	0count
 
NL
VARIABLE_VALUEmean4layer_with_weights-4/mean/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEvariance8layer_with_weights-4/variance/.ATTRIBUTES/VARIABLE_VALUE
PN
VARIABLE_VALUEcount5layer_with_weights-4/count/.ATTRIBUTES/VARIABLE_VALUE
 
ZX
VARIABLE_VALUEdense_3/kernel6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEdense_3/bias4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUE

20
31
 

20
31
▓
4	variables
5regularization_losses
Вnon_trainable_variables
Гlayer_metrics
Дmetrics
Еlayers
6trainable_variables
 Жlayer_regularization_losses
 
 
 
▓
8	variables
9regularization_losses
Зnon_trainable_variables
Иlayer_metrics
Йmetrics
Кlayers
:trainable_variables
 Лlayer_regularization_losses
ZX
VARIABLE_VALUEdense_4/kernel6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEdense_4/bias4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUE

<0
=1
 

<0
=1
▓
>	variables
?regularization_losses
Мnon_trainable_variables
Нlayer_metrics
Оmetrics
Пlayers
@trainable_variables
 Рlayer_regularization_losses
ZX
VARIABLE_VALUEdense_5/kernel6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEdense_5/bias4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUE

B0
C1
 

B0
C1
▓
D	variables
Eregularization_losses
Сnon_trainable_variables
Тlayer_metrics
Уmetrics
Фlayers
Ftrainable_variables
 Хlayer_regularization_losses
ZX
VARIABLE_VALUEdense_6/kernel6layer_with_weights-8/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEdense_6/bias4layer_with_weights-8/bias/.ATTRIBUTES/VARIABLE_VALUE

H0
I1
 

H0
I1
▓
J	variables
Kregularization_losses
Цnon_trainable_variables
Чlayer_metrics
Шmetrics
Щlayers
Ltrainable_variables
 Ъlayer_regularization_losses
ZX
VARIABLE_VALUEdense_7/kernel6layer_with_weights-9/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEdense_7/bias4layer_with_weights-9/bias/.ATTRIBUTES/VARIABLE_VALUE

N0
O1
 

N0
O1
▓
P	variables
Qregularization_losses
Ыnon_trainable_variables
Ьlayer_metrics
Эmetrics
Юlayers
Rtrainable_variables
 Яlayer_regularization_losses
[Y
VARIABLE_VALUEdense_8/kernel7layer_with_weights-10/kernel/.ATTRIBUTES/VARIABLE_VALUE
WU
VARIABLE_VALUEdense_8/bias5layer_with_weights-10/bias/.ATTRIBUTES/VARIABLE_VALUE

T0
U1
 

T0
U1
▓
V	variables
Wregularization_losses
аnon_trainable_variables
бlayer_metrics
вmetrics
гlayers
Xtrainable_variables
 дlayer_regularization_losses
[Y
VARIABLE_VALUEdense_9/kernel7layer_with_weights-11/kernel/.ATTRIBUTES/VARIABLE_VALUE
WU
VARIABLE_VALUEdense_9/bias5layer_with_weights-11/bias/.ATTRIBUTES/VARIABLE_VALUE

Z0
[1
 

Z0
[1
▓
\	variables
]regularization_losses
еnon_trainable_variables
жlayer_metrics
зmetrics
иlayers
^trainable_variables
 йlayer_regularization_losses
\Z
VARIABLE_VALUEdense_10/kernel7layer_with_weights-12/kernel/.ATTRIBUTES/VARIABLE_VALUE
XV
VARIABLE_VALUEdense_10/bias5layer_with_weights-12/bias/.ATTRIBUTES/VARIABLE_VALUE

`0
a1
 

`0
a1
▓
b	variables
cregularization_losses
кnon_trainable_variables
лlayer_metrics
мmetrics
нlayers
dtrainable_variables
 оlayer_regularization_losses
HF
VARIABLE_VALUE	Adam/iter)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_1+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_2+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUE
JH
VARIABLE_VALUE
Adam/decay*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEAdam/learning_rate2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUE

.1
/2
03
 

п0
v
0
1
2
3
4
5
6
7
	8

9
10
11
12
13
14
15
 
 
LJ
tableAlayer_with_weights-0/_index_lookup_layer/_table/.ATTRIBUTES/table
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
8

░total

▒count
▓	variables
│	keras_api
OM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE
QO
VARIABLE_VALUEcount_14keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE

░0
▒1

▓	variables
{y
VARIABLE_VALUEAdam/dense/kernel/mRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
wu
VARIABLE_VALUEAdam/dense/bias/mPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_1/kernel/mRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_1/bias/mPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_2/kernel/mRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_2/bias/mPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_3/kernel/mRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_3/bias/mPlayer_with_weights-5/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_4/kernel/mRlayer_with_weights-6/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_4/bias/mPlayer_with_weights-6/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_5/kernel/mRlayer_with_weights-7/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_5/bias/mPlayer_with_weights-7/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_6/kernel/mRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_6/bias/mPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_7/kernel/mRlayer_with_weights-9/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_7/bias/mPlayer_with_weights-9/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_8/kernel/mSlayer_with_weights-10/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_8/bias/mQlayer_with_weights-10/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_9/kernel/mSlayer_with_weights-11/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_9/bias/mQlayer_with_weights-11/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}
VARIABLE_VALUEAdam/dense_10/kernel/mSlayer_with_weights-12/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
{y
VARIABLE_VALUEAdam/dense_10/bias/mQlayer_with_weights-12/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
{y
VARIABLE_VALUEAdam/dense/kernel/vRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
wu
VARIABLE_VALUEAdam/dense/bias/vPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_1/kernel/vRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_1/bias/vPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_2/kernel/vRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_2/bias/vPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_3/kernel/vRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_3/bias/vPlayer_with_weights-5/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_4/kernel/vRlayer_with_weights-6/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_4/bias/vPlayer_with_weights-6/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_5/kernel/vRlayer_with_weights-7/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_5/bias/vPlayer_with_weights-7/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_6/kernel/vRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_6/bias/vPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_7/kernel/vRlayer_with_weights-9/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_7/bias/vPlayer_with_weights-9/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_8/kernel/vSlayer_with_weights-10/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_8/bias/vQlayer_with_weights-10/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_9/kernel/vSlayer_with_weights-11/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_9/bias/vQlayer_with_weights-11/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}
VARIABLE_VALUEAdam/dense_10/kernel/vSlayer_with_weights-12/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
{y
VARIABLE_VALUEAdam/dense_10/bias/vQlayer_with_weights-12/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
z
serving_default_input_1Placeholder*'
_output_shapes
:         *
dtype0*
shape:         
z
serving_default_input_2Placeholder*'
_output_shapes
:         *
dtype0*
shape:         
Ў
StatefulPartitionedCallStatefulPartitionedCallserving_default_input_1serving_default_input_2string_lookup_index_tableConstdense/kernel
dense/biasdense_1/kerneldense_1/biasdense_2/kerneldense_2/biasmeanvariancedense_3/kerneldense_3/biasdense_4/kerneldense_4/biasdense_5/kerneldense_5/biasdense_6/kerneldense_6/biasdense_7/kerneldense_7/biasdense_8/kerneldense_8/biasdense_9/kerneldense_9/biasdense_10/kerneldense_10/bias*'
Tin 
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *:
_read_only_resource_inputs
	
*-
config_proto

CPU

GPU 2J 8В *+
f&R$
"__inference_signature_wrapper_7321
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
ч
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename dense/kernel/Read/ReadVariableOpdense/bias/Read/ReadVariableOp"dense_1/kernel/Read/ReadVariableOp dense_1/bias/Read/ReadVariableOp"dense_2/kernel/Read/ReadVariableOp dense_2/bias/Read/ReadVariableOpmean/Read/ReadVariableOpvariance/Read/ReadVariableOpcount/Read/ReadVariableOp"dense_3/kernel/Read/ReadVariableOp dense_3/bias/Read/ReadVariableOp"dense_4/kernel/Read/ReadVariableOp dense_4/bias/Read/ReadVariableOp"dense_5/kernel/Read/ReadVariableOp dense_5/bias/Read/ReadVariableOp"dense_6/kernel/Read/ReadVariableOp dense_6/bias/Read/ReadVariableOp"dense_7/kernel/Read/ReadVariableOp dense_7/bias/Read/ReadVariableOp"dense_8/kernel/Read/ReadVariableOp dense_8/bias/Read/ReadVariableOp"dense_9/kernel/Read/ReadVariableOp dense_9/bias/Read/ReadVariableOp#dense_10/kernel/Read/ReadVariableOp!dense_10/bias/Read/ReadVariableOpAdam/iter/Read/ReadVariableOpAdam/beta_1/Read/ReadVariableOpAdam/beta_2/Read/ReadVariableOpAdam/decay/Read/ReadVariableOp&Adam/learning_rate/Read/ReadVariableOpHstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2Jstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2:1total/Read/ReadVariableOpcount_1/Read/ReadVariableOp'Adam/dense/kernel/m/Read/ReadVariableOp%Adam/dense/bias/m/Read/ReadVariableOp)Adam/dense_1/kernel/m/Read/ReadVariableOp'Adam/dense_1/bias/m/Read/ReadVariableOp)Adam/dense_2/kernel/m/Read/ReadVariableOp'Adam/dense_2/bias/m/Read/ReadVariableOp)Adam/dense_3/kernel/m/Read/ReadVariableOp'Adam/dense_3/bias/m/Read/ReadVariableOp)Adam/dense_4/kernel/m/Read/ReadVariableOp'Adam/dense_4/bias/m/Read/ReadVariableOp)Adam/dense_5/kernel/m/Read/ReadVariableOp'Adam/dense_5/bias/m/Read/ReadVariableOp)Adam/dense_6/kernel/m/Read/ReadVariableOp'Adam/dense_6/bias/m/Read/ReadVariableOp)Adam/dense_7/kernel/m/Read/ReadVariableOp'Adam/dense_7/bias/m/Read/ReadVariableOp)Adam/dense_8/kernel/m/Read/ReadVariableOp'Adam/dense_8/bias/m/Read/ReadVariableOp)Adam/dense_9/kernel/m/Read/ReadVariableOp'Adam/dense_9/bias/m/Read/ReadVariableOp*Adam/dense_10/kernel/m/Read/ReadVariableOp(Adam/dense_10/bias/m/Read/ReadVariableOp'Adam/dense/kernel/v/Read/ReadVariableOp%Adam/dense/bias/v/Read/ReadVariableOp)Adam/dense_1/kernel/v/Read/ReadVariableOp'Adam/dense_1/bias/v/Read/ReadVariableOp)Adam/dense_2/kernel/v/Read/ReadVariableOp'Adam/dense_2/bias/v/Read/ReadVariableOp)Adam/dense_3/kernel/v/Read/ReadVariableOp'Adam/dense_3/bias/v/Read/ReadVariableOp)Adam/dense_4/kernel/v/Read/ReadVariableOp'Adam/dense_4/bias/v/Read/ReadVariableOp)Adam/dense_5/kernel/v/Read/ReadVariableOp'Adam/dense_5/bias/v/Read/ReadVariableOp)Adam/dense_6/kernel/v/Read/ReadVariableOp'Adam/dense_6/bias/v/Read/ReadVariableOp)Adam/dense_7/kernel/v/Read/ReadVariableOp'Adam/dense_7/bias/v/Read/ReadVariableOp)Adam/dense_8/kernel/v/Read/ReadVariableOp'Adam/dense_8/bias/v/Read/ReadVariableOp)Adam/dense_9/kernel/v/Read/ReadVariableOp'Adam/dense_9/bias/v/Read/ReadVariableOp*Adam/dense_10/kernel/v/Read/ReadVariableOp(Adam/dense_10/bias/v/Read/ReadVariableOpConst_1*[
TinT
R2P			*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8В *&
f!R
__inference__traced_save_8343
Ї
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenamedense/kernel
dense/biasdense_1/kerneldense_1/biasdense_2/kerneldense_2/biasmeanvariancecountdense_3/kerneldense_3/biasdense_4/kerneldense_4/biasdense_5/kerneldense_5/biasdense_6/kerneldense_6/biasdense_7/kerneldense_7/biasdense_8/kerneldense_8/biasdense_9/kerneldense_9/biasdense_10/kerneldense_10/bias	Adam/iterAdam/beta_1Adam/beta_2
Adam/decayAdam/learning_ratestring_lookup_index_tabletotalcount_1Adam/dense/kernel/mAdam/dense/bias/mAdam/dense_1/kernel/mAdam/dense_1/bias/mAdam/dense_2/kernel/mAdam/dense_2/bias/mAdam/dense_3/kernel/mAdam/dense_3/bias/mAdam/dense_4/kernel/mAdam/dense_4/bias/mAdam/dense_5/kernel/mAdam/dense_5/bias/mAdam/dense_6/kernel/mAdam/dense_6/bias/mAdam/dense_7/kernel/mAdam/dense_7/bias/mAdam/dense_8/kernel/mAdam/dense_8/bias/mAdam/dense_9/kernel/mAdam/dense_9/bias/mAdam/dense_10/kernel/mAdam/dense_10/bias/mAdam/dense/kernel/vAdam/dense/bias/vAdam/dense_1/kernel/vAdam/dense_1/bias/vAdam/dense_2/kernel/vAdam/dense_2/bias/vAdam/dense_3/kernel/vAdam/dense_3/bias/vAdam/dense_4/kernel/vAdam/dense_4/bias/vAdam/dense_5/kernel/vAdam/dense_5/bias/vAdam/dense_6/kernel/vAdam/dense_6/bias/vAdam/dense_7/kernel/vAdam/dense_7/bias/vAdam/dense_8/kernel/vAdam/dense_8/bias/vAdam/dense_9/kernel/vAdam/dense_9/bias/vAdam/dense_10/kernel/vAdam/dense_10/bias/v*Y
TinR
P2N*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8В *)
f$R"
 __inference__traced_restore_8584Тў
╧
╒
"text_vectorization_cond_false_6905'
#text_vectorization_cond_placeholder`
\text_vectorization_cond_strided_slice_text_vectorization_raggedtotensor_raggedtensortotensor	$
 text_vectorization_cond_identity	л
+text_vectorization/cond/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2-
+text_vectorization/cond/strided_slice/stackп
-text_vectorization/cond/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ▄  2/
-text_vectorization/cond/strided_slice/stack_1п
-text_vectorization/cond/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2/
-text_vectorization/cond/strided_slice/stack_2╠
%text_vectorization/cond/strided_sliceStridedSlice\text_vectorization_cond_strided_slice_text_vectorization_raggedtotensor_raggedtensortotensor4text_vectorization/cond/strided_slice/stack:output:06text_vectorization/cond/strided_slice/stack_1:output:06text_vectorization/cond/strided_slice/stack_2:output:0*
Index0*
T0	*0
_output_shapes
:                  *

begin_mask*
end_mask2'
%text_vectorization/cond/strided_slice╗
 text_vectorization/cond/IdentityIdentity.text_vectorization/cond/strided_slice:output:0*
T0	*0
_output_shapes
:                  2"
 text_vectorization/cond/Identity"M
 text_vectorization_cond_identity)text_vectorization/cond/Identity:output:0*1
_input_shapes 
: :                  : 

_output_shapes
: :62
0
_output_shapes
:                  
╓
{
&__inference_dense_8_layer_call_fn_7998

inputs
unknown
	unknown_0
identityИвStatefulPartitionedCallё
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_8_layer_call_and_return_conditional_losses_66252
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
Д
+
__inference__destroyer_8052
identityP
ConstConst*
_output_shapes
: *
dtype0*
value	B :2
ConstQ
IdentityIdentityConst:output:0*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*
_input_shapes 
Щ
V
*__inference_concatenate_layer_call_fn_7898
inputs_0
inputs_1
identity╨
PartitionedCallPartitionedCallinputs_0inputs_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         E* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8В *N
fIRG
E__inference_concatenate_layer_call_and_return_conditional_losses_64972
PartitionedCalll
IdentityIdentityPartitionedCall:output:0*
T0*'
_output_shapes
:         E2

Identity"
identityIdentity:output:0*9
_input_shapes(
&:         :         @:Q M
'
_output_shapes
:         
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:         @
"
_user_specified_name
inputs/1
╒
╡
__inference_save_fn_8071
checkpoint_keyY
Ustring_lookup_index_table_lookup_table_export_values_lookuptableexportv2_table_handle
identity

identity_1

identity_2

identity_3

identity_4

identity_5	ИвHstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2ї
Hstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2LookupTableExportV2Ustring_lookup_index_table_lookup_table_export_values_lookuptableexportv2_table_handle",/job:localhost/replica:0/task:0/device:CPU:0*
Tkeys0*
Tvalues0	*'
_output_shapes
:         :2J
Hstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2T
add/yConst*
_output_shapes
: *
dtype0*
valueB B-keys2
add/yR
addAddcheckpoint_keyadd/y:output:0*
T0*
_output_shapes
: 2
addZ
add_1/yConst*
_output_shapes
: *
dtype0*
valueB B-values2	
add_1/yX
add_1Addcheckpoint_keyadd_1/y:output:0*
T0*
_output_shapes
: 2
add_1Х
IdentityIdentityadd:z:0I^string_lookup_index_table_lookup_table_export_values/LookupTableExportV2*
T0*
_output_shapes
: 2

IdentityO
ConstConst*
_output_shapes
: *
dtype0*
valueB B 2
Constа

Identity_1IdentityConst:output:0I^string_lookup_index_table_lookup_table_export_values/LookupTableExportV2*
T0*
_output_shapes
: 2

Identity_1ю

Identity_2IdentityOstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2:keys:0I^string_lookup_index_table_lookup_table_export_values/LookupTableExportV2*
T0*#
_output_shapes
:         2

Identity_2Ы

Identity_3Identity	add_1:z:0I^string_lookup_index_table_lookup_table_export_values/LookupTableExportV2*
T0*
_output_shapes
: 2

Identity_3S
Const_1Const*
_output_shapes
: *
dtype0*
valueB B 2	
Const_1в

Identity_4IdentityConst_1:output:0I^string_lookup_index_table_lookup_table_export_values/LookupTableExportV2*
T0*
_output_shapes
: 2

Identity_4х

Identity_5IdentityQstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2:values:0I^string_lookup_index_table_lookup_table_export_values/LookupTableExportV2*
T0	*
_output_shapes
:2

Identity_5"
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0"!

identity_3Identity_3:output:0"!

identity_4Identity_4:output:0"!

identity_5Identity_5:output:0*
_input_shapes
: :2Ф
Hstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2Hstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2:F B

_output_shapes
: 
(
_user_specified_namecheckpoint_key
╪
{
&__inference_dense_2_layer_call_fn_7865

inputs
unknown
	unknown_0
identityИвStatefulPartitionedCallё
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_2_layer_call_and_return_conditional_losses_64362
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*/
_input_shapes
:         А::22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:         А
 
_user_specified_nameinputs
ж
й
A__inference_dense_5_layer_call_and_return_conditional_losses_7929

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИН
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpБ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @:::O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
Г
З
+__inference_functional_1_layer_call_fn_7804
inputs_0
inputs_1
unknown
	unknown_0	
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
	unknown_7
	unknown_8
	unknown_9

unknown_10

unknown_11

unknown_12

unknown_13

unknown_14

unknown_15

unknown_16

unknown_17

unknown_18

unknown_19

unknown_20

unknown_21

unknown_22

unknown_23

unknown_24
identityИвStatefulPartitionedCall╚
StatefulPartitionedCallStatefulPartitionedCallinputs_0inputs_1unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24*'
Tin 
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *:
_read_only_resource_inputs
	
*-
config_proto

CPU

GPU 2J 8В *O
fJRH
F__inference_functional_1_layer_call_and_return_conditional_losses_71982
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*б
_input_shapesП
М:         :         :: ::::::::::::::::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:Q M
'
_output_shapes
:         
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:         
"
_user_specified_name
inputs/1
ж
й
A__inference_dense_3_layer_call_and_return_conditional_losses_6474

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИН
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpБ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @:::O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
╧
╒
"text_vectorization_cond_false_7110'
#text_vectorization_cond_placeholder`
\text_vectorization_cond_strided_slice_text_vectorization_raggedtotensor_raggedtensortotensor	$
 text_vectorization_cond_identity	л
+text_vectorization/cond/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2-
+text_vectorization/cond/strided_slice/stackп
-text_vectorization/cond/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ▄  2/
-text_vectorization/cond/strided_slice/stack_1п
-text_vectorization/cond/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2/
-text_vectorization/cond/strided_slice/stack_2╠
%text_vectorization/cond/strided_sliceStridedSlice\text_vectorization_cond_strided_slice_text_vectorization_raggedtotensor_raggedtensortotensor4text_vectorization/cond/strided_slice/stack:output:06text_vectorization/cond/strided_slice/stack_1:output:06text_vectorization/cond/strided_slice/stack_2:output:0*
Index0*
T0	*0
_output_shapes
:                  *

begin_mask*
end_mask2'
%text_vectorization/cond/strided_slice╗
 text_vectorization/cond/IdentityIdentity.text_vectorization/cond/strided_slice:output:0*
T0	*0
_output_shapes
:                  2"
 text_vectorization/cond/Identity"M
 text_vectorization_cond_identity)text_vectorization/cond/Identity:output:0*1
_input_shapes 
: :                  : 

_output_shapes
: :62
0
_output_shapes
:                  
Е
)
__inference_<lambda>_8084
identityS
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  А?2
ConstQ
IdentityIdentityConst:output:0*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*
_input_shapes 
╓
{
&__inference_dense_6_layer_call_fn_7958

inputs
unknown
	unknown_0
identityИвStatefulPartitionedCallё
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_6_layer_call_and_return_conditional_losses_65712
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
╒╟
▀	
F__inference_functional_1_layer_call_and_return_conditional_losses_7198

inputs
inputs_1Z
Vtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handle[
Wtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value	

dense_7130

dense_7132
dense_1_7135
dense_1_7137
dense_2_7140
dense_2_71421
-normalization_reshape_readvariableop_resource3
/normalization_reshape_1_readvariableop_resource
dense_3_7156
dense_3_7158
dense_4_7162
dense_4_7164
dense_5_7167
dense_5_7169
dense_6_7172
dense_6_7174
dense_7_7177
dense_7_7179
dense_8_7182
dense_8_7184
dense_9_7187
dense_9_7189
dense_10_7192
dense_10_7194
identityИвdense/StatefulPartitionedCallвdense_1/StatefulPartitionedCallв dense_10/StatefulPartitionedCallвdense_2/StatefulPartitionedCallвdense_3/StatefulPartitionedCallвdense_4/StatefulPartitionedCallвdense_5/StatefulPartitionedCallвdense_6/StatefulPartitionedCallвdense_7/StatefulPartitionedCallвdense_8/StatefulPartitionedCallвdense_9/StatefulPartitionedCallвItext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2В
text_vectorization/StringLowerStringLowerinputs_1*'
_output_shapes
:         2 
text_vectorization/StringLower¤
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*'
_output_shapes
:         *6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite 2'
%text_vectorization/StaticRegexReplace┴
text_vectorization/SqueezeSqueeze.text_vectorization/StaticRegexReplace:output:0*
T0*#
_output_shapes
:         *
squeeze_dims

         2
text_vectorization/Squeeze╖
9text_vectorization/StringsByteSplit/StringSplit/delimiterConst*
_output_shapes
: *
dtype0*
valueB B 2;
9text_vectorization/StringsByteSplit/StringSplit/delimiterк
/text_vectorization/StringsByteSplit/StringSplitStringSplit#text_vectorization/Squeeze:output:0Btext_vectorization/StringsByteSplit/StringSplit/delimiter:output:0*<
_output_shapes*
(:         :         :*

skip_empty( 21
/text_vectorization/StringsByteSplit/StringSplit├
7text_vectorization/StringsByteSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        29
7text_vectorization/StringsByteSplit/strided_slice/stack╟
9text_vectorization/StringsByteSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       2;
9text_vectorization/StringsByteSplit/strided_slice/stack_1╟
9text_vectorization/StringsByteSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2;
9text_vectorization/StringsByteSplit/strided_slice/stack_2Ё
1text_vectorization/StringsByteSplit/strided_sliceStridedSlice9text_vectorization/StringsByteSplit/StringSplit:indices:0@text_vectorization/StringsByteSplit/strided_slice/stack:output:0Btext_vectorization/StringsByteSplit/strided_slice/stack_1:output:0Btext_vectorization/StringsByteSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:         *

begin_mask*
end_mask*
shrink_axis_mask23
1text_vectorization/StringsByteSplit/strided_slice└
9text_vectorization/StringsByteSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2;
9text_vectorization/StringsByteSplit/strided_slice_1/stack─
;text_vectorization/StringsByteSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2=
;text_vectorization/StringsByteSplit/strided_slice_1/stack_1─
;text_vectorization/StringsByteSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2=
;text_vectorization/StringsByteSplit/strided_slice_1/stack_2╔
3text_vectorization/StringsByteSplit/strided_slice_1StridedSlice7text_vectorization/StringsByteSplit/StringSplit:shape:0Btext_vectorization/StringsByteSplit/strided_slice_1/stack:output:0Dtext_vectorization/StringsByteSplit/strided_slice_1/stack_1:output:0Dtext_vectorization/StringsByteSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask25
3text_vectorization/StringsByteSplit/strided_slice_1╣
Ztext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast:text_vectorization/StringsByteSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:         2\
Ztext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast▓
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast<text_vectorization/StringsByteSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: 2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1┌
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShape^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeЦ
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const╤
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdmtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0mtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: 2e
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdЦ
htext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : 2j
htext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y▌
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreaterltext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0qtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greaterю
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastjtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: 2e
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastЪ
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1┴
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMax^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0otext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxО
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y╬
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ktext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0mtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add┴
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulgtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul╞
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum`text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum╩
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum`text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumУ
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2╔
gtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincount^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0otext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:         2i
gtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountИ
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : 2c
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis╨
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumntext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:         2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumШ
etext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R 2g
etext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0И
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2c
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis┐
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2ntext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:         2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatА
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Vtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handle8text_vectorization/StringsByteSplit/StringSplit:values:0Wtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*#
_output_shapes
:         2K
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2Ж
2text_vectorization/string_lookup/assert_equal/NoOpNoOp*
_output_shapes
 24
2text_vectorization/string_lookup/assert_equal/NoOpф
)text_vectorization/string_lookup/IdentityIdentityRtext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:         2+
)text_vectorization/string_lookup/Identity√
+text_vectorization/string_lookup/Identity_1Identityetext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat:output:0*
T0	*#
_output_shapes
:         2-
+text_vectorization/string_lookup/Identity_1д
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R 21
/text_vectorization/RaggedToTensor/default_valueЭ
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
: *
dtype0	*
valueB	 R
         2)
'text_vectorization/RaggedToTensor/Const¤
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:02text_vectorization/string_lookup/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:04text_vectorization/string_lookup/Identity_1:output:0*
T0	*
Tindex0	*
Tshape0	*0
_output_shapes
:                  *
num_row_partition_tensors*%
row_partition_types

ROW_SPLITS28
6text_vectorization/RaggedToTensor/RaggedTensorToTensorг
text_vectorization/ShapeShape?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
T0	*
_output_shapes
:2
text_vectorization/ShapeЪ
&text_vectorization/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&text_vectorization/strided_slice/stackЮ
(text_vectorization/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(text_vectorization/strided_slice/stack_1Ю
(text_vectorization/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(text_vectorization/strided_slice/stack_2╘
 text_vectorization/strided_sliceStridedSlice!text_vectorization/Shape:output:0/text_vectorization/strided_slice/stack:output:01text_vectorization/strided_slice/stack_1:output:01text_vectorization/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 text_vectorization/strided_slicew
text_vectorization/sub/xConst*
_output_shapes
: *
dtype0*
value
B :▄2
text_vectorization/sub/xж
text_vectorization/subSub!text_vectorization/sub/x:output:0)text_vectorization/strided_slice:output:0*
T0*
_output_shapes
: 2
text_vectorization/suby
text_vectorization/Less/yConst*
_output_shapes
: *
dtype0*
value
B :▄2
text_vectorization/Less/yк
text_vectorization/LessLess)text_vectorization/strided_slice:output:0"text_vectorization/Less/y:output:0*
T0*
_output_shapes
: 2
text_vectorization/Less∙
text_vectorization/condStatelessIftext_vectorization/Less:z:0text_vectorization/sub:z:0?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
Tcond0
*
Tin
2	*
Tout
2	*
_lower_using_switch_merge(*0
_output_shapes
:                  * 
_read_only_resource_inputs
 *5
else_branch&R$
"text_vectorization_cond_false_7110*/
output_shapes
:                  *4
then_branch%R#
!text_vectorization_cond_true_71092
text_vectorization/condе
 text_vectorization/cond/IdentityIdentity text_vectorization/cond:output:0*
T0	*(
_output_shapes
:         ▄2"
 text_vectorization/cond/Identityг
dense/StatefulPartitionedCallStatefulPartitionedCall)text_vectorization/cond/Identity:output:0
dense_7130
dense_7132*
Tin
2	*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:         А*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *H
fCRA
?__inference_dense_layer_call_and_return_conditional_losses_63822
dense/StatefulPartitionedCallк
dense_1/StatefulPartitionedCallStatefulPartitionedCall&dense/StatefulPartitionedCall:output:0dense_1_7135dense_1_7137*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:         А*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_1_layer_call_and_return_conditional_losses_64092!
dense_1/StatefulPartitionedCallл
dense_2/StatefulPartitionedCallStatefulPartitionedCall(dense_1/StatefulPartitionedCall:output:0dense_2_7140dense_2_7142*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_2_layer_call_and_return_conditional_losses_64362!
dense_2/StatefulPartitionedCall╢
$normalization/Reshape/ReadVariableOpReadVariableOp-normalization_reshape_readvariableop_resource*
_output_shapes
:*
dtype02&
$normalization/Reshape/ReadVariableOpЛ
normalization/Reshape/shapeConst*
_output_shapes
:*
dtype0*
valueB"      2
normalization/Reshape/shape╢
normalization/ReshapeReshape,normalization/Reshape/ReadVariableOp:value:0$normalization/Reshape/shape:output:0*
T0*
_output_shapes

:2
normalization/Reshape╝
&normalization/Reshape_1/ReadVariableOpReadVariableOp/normalization_reshape_1_readvariableop_resource*
_output_shapes
:*
dtype02(
&normalization/Reshape_1/ReadVariableOpП
normalization/Reshape_1/shapeConst*
_output_shapes
:*
dtype0*
valueB"      2
normalization/Reshape_1/shape╛
normalization/Reshape_1Reshape.normalization/Reshape_1/ReadVariableOp:value:0&normalization/Reshape_1/shape:output:0*
T0*
_output_shapes

:2
normalization/Reshape_1З
normalization/subSubinputsnormalization/Reshape:output:0*
T0*'
_output_shapes
:         2
normalization/sub{
normalization/SqrtSqrt normalization/Reshape_1:output:0*
T0*
_output_shapes

:2
normalization/SqrtЪ
normalization/truedivRealDivnormalization/sub:z:0normalization/Sqrt:y:0*
T0*'
_output_shapes
:         2
normalization/truedivл
dense_3/StatefulPartitionedCallStatefulPartitionedCall(dense_2/StatefulPartitionedCall:output:0dense_3_7156dense_3_7158*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_3_layer_call_and_return_conditional_losses_64742!
dense_3/StatefulPartitionedCallЩ
concatenate/PartitionedCallPartitionedCallnormalization/truediv:z:0(dense_3/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         E* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8В *N
fIRG
E__inference_concatenate_layer_call_and_return_conditional_losses_64972
concatenate/PartitionedCallз
dense_4/StatefulPartitionedCallStatefulPartitionedCall$concatenate/PartitionedCall:output:0dense_4_7162dense_4_7164*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_4_layer_call_and_return_conditional_losses_65172!
dense_4/StatefulPartitionedCallл
dense_5/StatefulPartitionedCallStatefulPartitionedCall(dense_4/StatefulPartitionedCall:output:0dense_5_7167dense_5_7169*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_5_layer_call_and_return_conditional_losses_65442!
dense_5/StatefulPartitionedCallл
dense_6/StatefulPartitionedCallStatefulPartitionedCall(dense_5/StatefulPartitionedCall:output:0dense_6_7172dense_6_7174*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_6_layer_call_and_return_conditional_losses_65712!
dense_6/StatefulPartitionedCallл
dense_7/StatefulPartitionedCallStatefulPartitionedCall(dense_6/StatefulPartitionedCall:output:0dense_7_7177dense_7_7179*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_7_layer_call_and_return_conditional_losses_65982!
dense_7/StatefulPartitionedCallл
dense_8/StatefulPartitionedCallStatefulPartitionedCall(dense_7/StatefulPartitionedCall:output:0dense_8_7182dense_8_7184*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_8_layer_call_and_return_conditional_losses_66252!
dense_8/StatefulPartitionedCallл
dense_9/StatefulPartitionedCallStatefulPartitionedCall(dense_8/StatefulPartitionedCall:output:0dense_9_7187dense_9_7189*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_9_layer_call_and_return_conditional_losses_66522!
dense_9/StatefulPartitionedCall░
 dense_10/StatefulPartitionedCallStatefulPartitionedCall(dense_9/StatefulPartitionedCall:output:0dense_10_7192dense_10_7194*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *K
fFRD
B__inference_dense_10_layer_call_and_return_conditional_losses_66782"
 dense_10/StatefulPartitionedCall╛
IdentityIdentity)dense_10/StatefulPartitionedCall:output:0^dense/StatefulPartitionedCall ^dense_1/StatefulPartitionedCall!^dense_10/StatefulPartitionedCall ^dense_2/StatefulPartitionedCall ^dense_3/StatefulPartitionedCall ^dense_4/StatefulPartitionedCall ^dense_5/StatefulPartitionedCall ^dense_6/StatefulPartitionedCall ^dense_7/StatefulPartitionedCall ^dense_8/StatefulPartitionedCall ^dense_9/StatefulPartitionedCallJ^text_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*б
_input_shapesП
М:         :         :: ::::::::::::::::::::::::2>
dense/StatefulPartitionedCalldense/StatefulPartitionedCall2B
dense_1/StatefulPartitionedCalldense_1/StatefulPartitionedCall2D
 dense_10/StatefulPartitionedCall dense_10/StatefulPartitionedCall2B
dense_2/StatefulPartitionedCalldense_2/StatefulPartitionedCall2B
dense_3/StatefulPartitionedCalldense_3/StatefulPartitionedCall2B
dense_4/StatefulPartitionedCalldense_4/StatefulPartitionedCall2B
dense_5/StatefulPartitionedCalldense_5/StatefulPartitionedCall2B
dense_6/StatefulPartitionedCalldense_6/StatefulPartitionedCall2B
dense_7/StatefulPartitionedCalldense_7/StatefulPartitionedCall2B
dense_8/StatefulPartitionedCalldense_8/StatefulPartitionedCall2B
dense_9/StatefulPartitionedCalldense_9/StatefulPartitionedCall2Ц
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2:O K
'
_output_shapes
:         
 
_user_specified_nameinputs:OK
'
_output_shapes
:         
 
_user_specified_nameinputs
ж
й
A__inference_dense_9_layer_call_and_return_conditional_losses_8009

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИН
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpБ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @:::O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
╫╟
▀	
F__inference_functional_1_layer_call_and_return_conditional_losses_6842
input_2
input_1Z
Vtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handle[
Wtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value	

dense_6774

dense_6776
dense_1_6779
dense_1_6781
dense_2_6784
dense_2_67861
-normalization_reshape_readvariableop_resource3
/normalization_reshape_1_readvariableop_resource
dense_3_6800
dense_3_6802
dense_4_6806
dense_4_6808
dense_5_6811
dense_5_6813
dense_6_6816
dense_6_6818
dense_7_6821
dense_7_6823
dense_8_6826
dense_8_6828
dense_9_6831
dense_9_6833
dense_10_6836
dense_10_6838
identityИвdense/StatefulPartitionedCallвdense_1/StatefulPartitionedCallв dense_10/StatefulPartitionedCallвdense_2/StatefulPartitionedCallвdense_3/StatefulPartitionedCallвdense_4/StatefulPartitionedCallвdense_5/StatefulPartitionedCallвdense_6/StatefulPartitionedCallвdense_7/StatefulPartitionedCallвdense_8/StatefulPartitionedCallвdense_9/StatefulPartitionedCallвItext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2Б
text_vectorization/StringLowerStringLowerinput_1*'
_output_shapes
:         2 
text_vectorization/StringLower¤
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*'
_output_shapes
:         *6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite 2'
%text_vectorization/StaticRegexReplace┴
text_vectorization/SqueezeSqueeze.text_vectorization/StaticRegexReplace:output:0*
T0*#
_output_shapes
:         *
squeeze_dims

         2
text_vectorization/Squeeze╖
9text_vectorization/StringsByteSplit/StringSplit/delimiterConst*
_output_shapes
: *
dtype0*
valueB B 2;
9text_vectorization/StringsByteSplit/StringSplit/delimiterк
/text_vectorization/StringsByteSplit/StringSplitStringSplit#text_vectorization/Squeeze:output:0Btext_vectorization/StringsByteSplit/StringSplit/delimiter:output:0*<
_output_shapes*
(:         :         :*

skip_empty( 21
/text_vectorization/StringsByteSplit/StringSplit├
7text_vectorization/StringsByteSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        29
7text_vectorization/StringsByteSplit/strided_slice/stack╟
9text_vectorization/StringsByteSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       2;
9text_vectorization/StringsByteSplit/strided_slice/stack_1╟
9text_vectorization/StringsByteSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2;
9text_vectorization/StringsByteSplit/strided_slice/stack_2Ё
1text_vectorization/StringsByteSplit/strided_sliceStridedSlice9text_vectorization/StringsByteSplit/StringSplit:indices:0@text_vectorization/StringsByteSplit/strided_slice/stack:output:0Btext_vectorization/StringsByteSplit/strided_slice/stack_1:output:0Btext_vectorization/StringsByteSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:         *

begin_mask*
end_mask*
shrink_axis_mask23
1text_vectorization/StringsByteSplit/strided_slice└
9text_vectorization/StringsByteSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2;
9text_vectorization/StringsByteSplit/strided_slice_1/stack─
;text_vectorization/StringsByteSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2=
;text_vectorization/StringsByteSplit/strided_slice_1/stack_1─
;text_vectorization/StringsByteSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2=
;text_vectorization/StringsByteSplit/strided_slice_1/stack_2╔
3text_vectorization/StringsByteSplit/strided_slice_1StridedSlice7text_vectorization/StringsByteSplit/StringSplit:shape:0Btext_vectorization/StringsByteSplit/strided_slice_1/stack:output:0Dtext_vectorization/StringsByteSplit/strided_slice_1/stack_1:output:0Dtext_vectorization/StringsByteSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask25
3text_vectorization/StringsByteSplit/strided_slice_1╣
Ztext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast:text_vectorization/StringsByteSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:         2\
Ztext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast▓
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast<text_vectorization/StringsByteSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: 2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1┌
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShape^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeЦ
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const╤
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdmtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0mtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: 2e
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdЦ
htext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : 2j
htext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y▌
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreaterltext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0qtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greaterю
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastjtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: 2e
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastЪ
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1┴
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMax^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0otext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxО
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y╬
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ktext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0mtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add┴
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulgtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul╞
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum`text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum╩
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum`text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumУ
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2╔
gtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincount^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0otext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:         2i
gtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountИ
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : 2c
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis╨
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumntext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:         2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumШ
etext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R 2g
etext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0И
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2c
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis┐
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2ntext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:         2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatА
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Vtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handle8text_vectorization/StringsByteSplit/StringSplit:values:0Wtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*#
_output_shapes
:         2K
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2Ж
2text_vectorization/string_lookup/assert_equal/NoOpNoOp*
_output_shapes
 24
2text_vectorization/string_lookup/assert_equal/NoOpф
)text_vectorization/string_lookup/IdentityIdentityRtext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:         2+
)text_vectorization/string_lookup/Identity√
+text_vectorization/string_lookup/Identity_1Identityetext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat:output:0*
T0	*#
_output_shapes
:         2-
+text_vectorization/string_lookup/Identity_1д
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R 21
/text_vectorization/RaggedToTensor/default_valueЭ
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
: *
dtype0	*
valueB	 R
         2)
'text_vectorization/RaggedToTensor/Const¤
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:02text_vectorization/string_lookup/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:04text_vectorization/string_lookup/Identity_1:output:0*
T0	*
Tindex0	*
Tshape0	*0
_output_shapes
:                  *
num_row_partition_tensors*%
row_partition_types

ROW_SPLITS28
6text_vectorization/RaggedToTensor/RaggedTensorToTensorг
text_vectorization/ShapeShape?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
T0	*
_output_shapes
:2
text_vectorization/ShapeЪ
&text_vectorization/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&text_vectorization/strided_slice/stackЮ
(text_vectorization/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(text_vectorization/strided_slice/stack_1Ю
(text_vectorization/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(text_vectorization/strided_slice/stack_2╘
 text_vectorization/strided_sliceStridedSlice!text_vectorization/Shape:output:0/text_vectorization/strided_slice/stack:output:01text_vectorization/strided_slice/stack_1:output:01text_vectorization/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 text_vectorization/strided_slicew
text_vectorization/sub/xConst*
_output_shapes
: *
dtype0*
value
B :▄2
text_vectorization/sub/xж
text_vectorization/subSub!text_vectorization/sub/x:output:0)text_vectorization/strided_slice:output:0*
T0*
_output_shapes
: 2
text_vectorization/suby
text_vectorization/Less/yConst*
_output_shapes
: *
dtype0*
value
B :▄2
text_vectorization/Less/yк
text_vectorization/LessLess)text_vectorization/strided_slice:output:0"text_vectorization/Less/y:output:0*
T0*
_output_shapes
: 2
text_vectorization/Less∙
text_vectorization/condStatelessIftext_vectorization/Less:z:0text_vectorization/sub:z:0?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
Tcond0
*
Tin
2	*
Tout
2	*
_lower_using_switch_merge(*0
_output_shapes
:                  * 
_read_only_resource_inputs
 *5
else_branch&R$
"text_vectorization_cond_false_6754*/
output_shapes
:                  *4
then_branch%R#
!text_vectorization_cond_true_67532
text_vectorization/condе
 text_vectorization/cond/IdentityIdentity text_vectorization/cond:output:0*
T0	*(
_output_shapes
:         ▄2"
 text_vectorization/cond/Identityг
dense/StatefulPartitionedCallStatefulPartitionedCall)text_vectorization/cond/Identity:output:0
dense_6774
dense_6776*
Tin
2	*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:         А*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *H
fCRA
?__inference_dense_layer_call_and_return_conditional_losses_63822
dense/StatefulPartitionedCallк
dense_1/StatefulPartitionedCallStatefulPartitionedCall&dense/StatefulPartitionedCall:output:0dense_1_6779dense_1_6781*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:         А*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_1_layer_call_and_return_conditional_losses_64092!
dense_1/StatefulPartitionedCallл
dense_2/StatefulPartitionedCallStatefulPartitionedCall(dense_1/StatefulPartitionedCall:output:0dense_2_6784dense_2_6786*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_2_layer_call_and_return_conditional_losses_64362!
dense_2/StatefulPartitionedCall╢
$normalization/Reshape/ReadVariableOpReadVariableOp-normalization_reshape_readvariableop_resource*
_output_shapes
:*
dtype02&
$normalization/Reshape/ReadVariableOpЛ
normalization/Reshape/shapeConst*
_output_shapes
:*
dtype0*
valueB"      2
normalization/Reshape/shape╢
normalization/ReshapeReshape,normalization/Reshape/ReadVariableOp:value:0$normalization/Reshape/shape:output:0*
T0*
_output_shapes

:2
normalization/Reshape╝
&normalization/Reshape_1/ReadVariableOpReadVariableOp/normalization_reshape_1_readvariableop_resource*
_output_shapes
:*
dtype02(
&normalization/Reshape_1/ReadVariableOpП
normalization/Reshape_1/shapeConst*
_output_shapes
:*
dtype0*
valueB"      2
normalization/Reshape_1/shape╛
normalization/Reshape_1Reshape.normalization/Reshape_1/ReadVariableOp:value:0&normalization/Reshape_1/shape:output:0*
T0*
_output_shapes

:2
normalization/Reshape_1И
normalization/subSubinput_2normalization/Reshape:output:0*
T0*'
_output_shapes
:         2
normalization/sub{
normalization/SqrtSqrt normalization/Reshape_1:output:0*
T0*
_output_shapes

:2
normalization/SqrtЪ
normalization/truedivRealDivnormalization/sub:z:0normalization/Sqrt:y:0*
T0*'
_output_shapes
:         2
normalization/truedivл
dense_3/StatefulPartitionedCallStatefulPartitionedCall(dense_2/StatefulPartitionedCall:output:0dense_3_6800dense_3_6802*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_3_layer_call_and_return_conditional_losses_64742!
dense_3/StatefulPartitionedCallЩ
concatenate/PartitionedCallPartitionedCallnormalization/truediv:z:0(dense_3/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         E* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8В *N
fIRG
E__inference_concatenate_layer_call_and_return_conditional_losses_64972
concatenate/PartitionedCallз
dense_4/StatefulPartitionedCallStatefulPartitionedCall$concatenate/PartitionedCall:output:0dense_4_6806dense_4_6808*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_4_layer_call_and_return_conditional_losses_65172!
dense_4/StatefulPartitionedCallл
dense_5/StatefulPartitionedCallStatefulPartitionedCall(dense_4/StatefulPartitionedCall:output:0dense_5_6811dense_5_6813*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_5_layer_call_and_return_conditional_losses_65442!
dense_5/StatefulPartitionedCallл
dense_6/StatefulPartitionedCallStatefulPartitionedCall(dense_5/StatefulPartitionedCall:output:0dense_6_6816dense_6_6818*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_6_layer_call_and_return_conditional_losses_65712!
dense_6/StatefulPartitionedCallл
dense_7/StatefulPartitionedCallStatefulPartitionedCall(dense_6/StatefulPartitionedCall:output:0dense_7_6821dense_7_6823*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_7_layer_call_and_return_conditional_losses_65982!
dense_7/StatefulPartitionedCallл
dense_8/StatefulPartitionedCallStatefulPartitionedCall(dense_7/StatefulPartitionedCall:output:0dense_8_6826dense_8_6828*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_8_layer_call_and_return_conditional_losses_66252!
dense_8/StatefulPartitionedCallл
dense_9/StatefulPartitionedCallStatefulPartitionedCall(dense_8/StatefulPartitionedCall:output:0dense_9_6831dense_9_6833*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_9_layer_call_and_return_conditional_losses_66522!
dense_9/StatefulPartitionedCall░
 dense_10/StatefulPartitionedCallStatefulPartitionedCall(dense_9/StatefulPartitionedCall:output:0dense_10_6836dense_10_6838*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *K
fFRD
B__inference_dense_10_layer_call_and_return_conditional_losses_66782"
 dense_10/StatefulPartitionedCall╛
IdentityIdentity)dense_10/StatefulPartitionedCall:output:0^dense/StatefulPartitionedCall ^dense_1/StatefulPartitionedCall!^dense_10/StatefulPartitionedCall ^dense_2/StatefulPartitionedCall ^dense_3/StatefulPartitionedCall ^dense_4/StatefulPartitionedCall ^dense_5/StatefulPartitionedCall ^dense_6/StatefulPartitionedCall ^dense_7/StatefulPartitionedCall ^dense_8/StatefulPartitionedCall ^dense_9/StatefulPartitionedCallJ^text_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*б
_input_shapesП
М:         :         :: ::::::::::::::::::::::::2>
dense/StatefulPartitionedCalldense/StatefulPartitionedCall2B
dense_1/StatefulPartitionedCalldense_1/StatefulPartitionedCall2D
 dense_10/StatefulPartitionedCall dense_10/StatefulPartitionedCall2B
dense_2/StatefulPartitionedCalldense_2/StatefulPartitionedCall2B
dense_3/StatefulPartitionedCalldense_3/StatefulPartitionedCall2B
dense_4/StatefulPartitionedCalldense_4/StatefulPartitionedCall2B
dense_5/StatefulPartitionedCalldense_5/StatefulPartitionedCall2B
dense_6/StatefulPartitionedCalldense_6/StatefulPartitionedCall2B
dense_7/StatefulPartitionedCalldense_7/StatefulPartitionedCall2B
dense_8/StatefulPartitionedCalldense_8/StatefulPartitionedCall2B
dense_9/StatefulPartitionedCalldense_9/StatefulPartitionedCall2Ц
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2:P L
'
_output_shapes
:         
!
_user_specified_name	input_2:PL
'
_output_shapes
:         
!
_user_specified_name	input_1
┼Х
─
__inference__traced_save_8343
file_prefix+
'savev2_dense_kernel_read_readvariableop)
%savev2_dense_bias_read_readvariableop-
)savev2_dense_1_kernel_read_readvariableop+
'savev2_dense_1_bias_read_readvariableop-
)savev2_dense_2_kernel_read_readvariableop+
'savev2_dense_2_bias_read_readvariableop#
savev2_mean_read_readvariableop'
#savev2_variance_read_readvariableop$
 savev2_count_read_readvariableop	-
)savev2_dense_3_kernel_read_readvariableop+
'savev2_dense_3_bias_read_readvariableop-
)savev2_dense_4_kernel_read_readvariableop+
'savev2_dense_4_bias_read_readvariableop-
)savev2_dense_5_kernel_read_readvariableop+
'savev2_dense_5_bias_read_readvariableop-
)savev2_dense_6_kernel_read_readvariableop+
'savev2_dense_6_bias_read_readvariableop-
)savev2_dense_7_kernel_read_readvariableop+
'savev2_dense_7_bias_read_readvariableop-
)savev2_dense_8_kernel_read_readvariableop+
'savev2_dense_8_bias_read_readvariableop-
)savev2_dense_9_kernel_read_readvariableop+
'savev2_dense_9_bias_read_readvariableop.
*savev2_dense_10_kernel_read_readvariableop,
(savev2_dense_10_bias_read_readvariableop(
$savev2_adam_iter_read_readvariableop	*
&savev2_adam_beta_1_read_readvariableop*
&savev2_adam_beta_2_read_readvariableop)
%savev2_adam_decay_read_readvariableop1
-savev2_adam_learning_rate_read_readvariableopS
Osavev2_string_lookup_index_table_lookup_table_export_values_lookuptableexportv2U
Qsavev2_string_lookup_index_table_lookup_table_export_values_lookuptableexportv2_1	$
 savev2_total_read_readvariableop&
"savev2_count_1_read_readvariableop2
.savev2_adam_dense_kernel_m_read_readvariableop0
,savev2_adam_dense_bias_m_read_readvariableop4
0savev2_adam_dense_1_kernel_m_read_readvariableop2
.savev2_adam_dense_1_bias_m_read_readvariableop4
0savev2_adam_dense_2_kernel_m_read_readvariableop2
.savev2_adam_dense_2_bias_m_read_readvariableop4
0savev2_adam_dense_3_kernel_m_read_readvariableop2
.savev2_adam_dense_3_bias_m_read_readvariableop4
0savev2_adam_dense_4_kernel_m_read_readvariableop2
.savev2_adam_dense_4_bias_m_read_readvariableop4
0savev2_adam_dense_5_kernel_m_read_readvariableop2
.savev2_adam_dense_5_bias_m_read_readvariableop4
0savev2_adam_dense_6_kernel_m_read_readvariableop2
.savev2_adam_dense_6_bias_m_read_readvariableop4
0savev2_adam_dense_7_kernel_m_read_readvariableop2
.savev2_adam_dense_7_bias_m_read_readvariableop4
0savev2_adam_dense_8_kernel_m_read_readvariableop2
.savev2_adam_dense_8_bias_m_read_readvariableop4
0savev2_adam_dense_9_kernel_m_read_readvariableop2
.savev2_adam_dense_9_bias_m_read_readvariableop5
1savev2_adam_dense_10_kernel_m_read_readvariableop3
/savev2_adam_dense_10_bias_m_read_readvariableop2
.savev2_adam_dense_kernel_v_read_readvariableop0
,savev2_adam_dense_bias_v_read_readvariableop4
0savev2_adam_dense_1_kernel_v_read_readvariableop2
.savev2_adam_dense_1_bias_v_read_readvariableop4
0savev2_adam_dense_2_kernel_v_read_readvariableop2
.savev2_adam_dense_2_bias_v_read_readvariableop4
0savev2_adam_dense_3_kernel_v_read_readvariableop2
.savev2_adam_dense_3_bias_v_read_readvariableop4
0savev2_adam_dense_4_kernel_v_read_readvariableop2
.savev2_adam_dense_4_bias_v_read_readvariableop4
0savev2_adam_dense_5_kernel_v_read_readvariableop2
.savev2_adam_dense_5_bias_v_read_readvariableop4
0savev2_adam_dense_6_kernel_v_read_readvariableop2
.savev2_adam_dense_6_bias_v_read_readvariableop4
0savev2_adam_dense_7_kernel_v_read_readvariableop2
.savev2_adam_dense_7_bias_v_read_readvariableop4
0savev2_adam_dense_8_kernel_v_read_readvariableop2
.savev2_adam_dense_8_bias_v_read_readvariableop4
0savev2_adam_dense_9_kernel_v_read_readvariableop2
.savev2_adam_dense_9_bias_v_read_readvariableop5
1savev2_adam_dense_10_kernel_v_read_readvariableop3
/savev2_adam_dense_10_bias_v_read_readvariableop
savev2_const_1

identity_1ИвMergeV2CheckpointsП
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*2
StaticRegexFullMatchc
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.part2
ConstН
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*<
value3B1 B+_temp_06d283b287894649bae7e9743d662739/part2	
Const_1Л
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: 2
Selectt

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shardж
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename╗,
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:O*
dtype0*═+
value├+B└+OB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/mean/.ATTRIBUTES/VARIABLE_VALUEB8layer_with_weights-4/variance/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-4/count/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-8/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-8/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-9/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-9/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-10/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-10/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-11/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-11/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-12/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-12/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEBFlayer_with_weights-0/_index_lookup_layer/_table/.ATTRIBUTES/table-keysBHlayer_with_weights-0/_index_lookup_layer/_table/.ATTRIBUTES/table-valuesB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-5/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-6/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-6/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-7/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-7/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-9/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-9/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-10/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-10/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-11/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-11/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-12/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-12/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-5/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-6/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-6/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-7/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-7/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-9/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-9/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-10/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-10/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-11/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-11/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-12/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-12/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2/tensor_namesй
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:O*
dtype0*│
valueйBжOB B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
SaveV2/shape_and_slicesк
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0'savev2_dense_kernel_read_readvariableop%savev2_dense_bias_read_readvariableop)savev2_dense_1_kernel_read_readvariableop'savev2_dense_1_bias_read_readvariableop)savev2_dense_2_kernel_read_readvariableop'savev2_dense_2_bias_read_readvariableopsavev2_mean_read_readvariableop#savev2_variance_read_readvariableop savev2_count_read_readvariableop)savev2_dense_3_kernel_read_readvariableop'savev2_dense_3_bias_read_readvariableop)savev2_dense_4_kernel_read_readvariableop'savev2_dense_4_bias_read_readvariableop)savev2_dense_5_kernel_read_readvariableop'savev2_dense_5_bias_read_readvariableop)savev2_dense_6_kernel_read_readvariableop'savev2_dense_6_bias_read_readvariableop)savev2_dense_7_kernel_read_readvariableop'savev2_dense_7_bias_read_readvariableop)savev2_dense_8_kernel_read_readvariableop'savev2_dense_8_bias_read_readvariableop)savev2_dense_9_kernel_read_readvariableop'savev2_dense_9_bias_read_readvariableop*savev2_dense_10_kernel_read_readvariableop(savev2_dense_10_bias_read_readvariableop$savev2_adam_iter_read_readvariableop&savev2_adam_beta_1_read_readvariableop&savev2_adam_beta_2_read_readvariableop%savev2_adam_decay_read_readvariableop-savev2_adam_learning_rate_read_readvariableopOsavev2_string_lookup_index_table_lookup_table_export_values_lookuptableexportv2Qsavev2_string_lookup_index_table_lookup_table_export_values_lookuptableexportv2_1 savev2_total_read_readvariableop"savev2_count_1_read_readvariableop.savev2_adam_dense_kernel_m_read_readvariableop,savev2_adam_dense_bias_m_read_readvariableop0savev2_adam_dense_1_kernel_m_read_readvariableop.savev2_adam_dense_1_bias_m_read_readvariableop0savev2_adam_dense_2_kernel_m_read_readvariableop.savev2_adam_dense_2_bias_m_read_readvariableop0savev2_adam_dense_3_kernel_m_read_readvariableop.savev2_adam_dense_3_bias_m_read_readvariableop0savev2_adam_dense_4_kernel_m_read_readvariableop.savev2_adam_dense_4_bias_m_read_readvariableop0savev2_adam_dense_5_kernel_m_read_readvariableop.savev2_adam_dense_5_bias_m_read_readvariableop0savev2_adam_dense_6_kernel_m_read_readvariableop.savev2_adam_dense_6_bias_m_read_readvariableop0savev2_adam_dense_7_kernel_m_read_readvariableop.savev2_adam_dense_7_bias_m_read_readvariableop0savev2_adam_dense_8_kernel_m_read_readvariableop.savev2_adam_dense_8_bias_m_read_readvariableop0savev2_adam_dense_9_kernel_m_read_readvariableop.savev2_adam_dense_9_bias_m_read_readvariableop1savev2_adam_dense_10_kernel_m_read_readvariableop/savev2_adam_dense_10_bias_m_read_readvariableop.savev2_adam_dense_kernel_v_read_readvariableop,savev2_adam_dense_bias_v_read_readvariableop0savev2_adam_dense_1_kernel_v_read_readvariableop.savev2_adam_dense_1_bias_v_read_readvariableop0savev2_adam_dense_2_kernel_v_read_readvariableop.savev2_adam_dense_2_bias_v_read_readvariableop0savev2_adam_dense_3_kernel_v_read_readvariableop.savev2_adam_dense_3_bias_v_read_readvariableop0savev2_adam_dense_4_kernel_v_read_readvariableop.savev2_adam_dense_4_bias_v_read_readvariableop0savev2_adam_dense_5_kernel_v_read_readvariableop.savev2_adam_dense_5_bias_v_read_readvariableop0savev2_adam_dense_6_kernel_v_read_readvariableop.savev2_adam_dense_6_bias_v_read_readvariableop0savev2_adam_dense_7_kernel_v_read_readvariableop.savev2_adam_dense_7_bias_v_read_readvariableop0savev2_adam_dense_8_kernel_v_read_readvariableop.savev2_adam_dense_8_bias_v_read_readvariableop0savev2_adam_dense_9_kernel_v_read_readvariableop.savev2_adam_dense_9_bias_v_read_readvariableop1savev2_adam_dense_10_kernel_v_read_readvariableop/savev2_adam_dense_10_bias_v_read_readvariableopsavev2_const_1"/device:CPU:0*
_output_shapes
 *]
dtypesS
Q2O			2
SaveV2║
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixesб
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identitym

Identity_1IdentityIdentity:output:0^MergeV2Checkpoints*
T0*
_output_shapes
: 2

Identity_1"!

identity_1Identity_1:output:0*°
_input_shapesц
у: :
▄А:А:
АА:А:	А@:@::: :@@:@:E@:@:@@:@:@@:@:@@:@:@@:@:@@:@:@:: : : : : :         :         : : :
▄А:А:
АА:А:	А@:@:@@:@:E@:@:@@:@:@@:@:@@:@:@@:@:@@:@:@::
▄А:А:
АА:А:	А@:@:@@:@:E@:@:@@:@:@@:@:@@:@:@@:@:@@:@:@:: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:&"
 
_output_shapes
:
▄А:!

_output_shapes	
:А:&"
 
_output_shapes
:
АА:!

_output_shapes	
:А:%!

_output_shapes
:	А@: 

_output_shapes
:@: 

_output_shapes
:: 

_output_shapes
::	

_output_shapes
: :$
 

_output_shapes

:@@: 

_output_shapes
:@:$ 

_output_shapes

:E@: 

_output_shapes
:@:$ 

_output_shapes

:@@: 

_output_shapes
:@:$ 

_output_shapes

:@@: 

_output_shapes
:@:$ 

_output_shapes

:@@: 

_output_shapes
:@:$ 

_output_shapes

:@@: 

_output_shapes
:@:$ 

_output_shapes

:@@: 

_output_shapes
:@:$ 

_output_shapes

:@: 

_output_shapes
::

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :)%
#
_output_shapes
:         :) %
#
_output_shapes
:         :!

_output_shapes
: :"

_output_shapes
: :&#"
 
_output_shapes
:
▄А:!$

_output_shapes	
:А:&%"
 
_output_shapes
:
АА:!&

_output_shapes	
:А:%'!

_output_shapes
:	А@: (

_output_shapes
:@:$) 

_output_shapes

:@@: *

_output_shapes
:@:$+ 

_output_shapes

:E@: ,

_output_shapes
:@:$- 

_output_shapes

:@@: .

_output_shapes
:@:$/ 

_output_shapes

:@@: 0

_output_shapes
:@:$1 

_output_shapes

:@@: 2

_output_shapes
:@:$3 

_output_shapes

:@@: 4

_output_shapes
:@:$5 

_output_shapes

:@@: 6

_output_shapes
:@:$7 

_output_shapes

:@: 8

_output_shapes
::&9"
 
_output_shapes
:
▄А:!:

_output_shapes	
:А:&;"
 
_output_shapes
:
АА:!<

_output_shapes	
:А:%=!

_output_shapes
:	А@: >

_output_shapes
:@:$? 

_output_shapes

:@@: @

_output_shapes
:@:$A 

_output_shapes

:E@: B

_output_shapes
:@:$C 

_output_shapes

:@@: D

_output_shapes
:@:$E 

_output_shapes

:@@: F

_output_shapes
:@:$G 

_output_shapes

:@@: H

_output_shapes
:@:$I 

_output_shapes

:@@: J

_output_shapes
:@:$K 

_output_shapes

:@@: L

_output_shapes
:@:$M 

_output_shapes

:@: N

_output_shapes
::O

_output_shapes
: 
э├
ш'
 __inference__traced_restore_8584
file_prefix!
assignvariableop_dense_kernel!
assignvariableop_1_dense_bias%
!assignvariableop_2_dense_1_kernel#
assignvariableop_3_dense_1_bias%
!assignvariableop_4_dense_2_kernel#
assignvariableop_5_dense_2_bias
assignvariableop_6_mean
assignvariableop_7_variance
assignvariableop_8_count%
!assignvariableop_9_dense_3_kernel$
 assignvariableop_10_dense_3_bias&
"assignvariableop_11_dense_4_kernel$
 assignvariableop_12_dense_4_bias&
"assignvariableop_13_dense_5_kernel$
 assignvariableop_14_dense_5_bias&
"assignvariableop_15_dense_6_kernel$
 assignvariableop_16_dense_6_bias&
"assignvariableop_17_dense_7_kernel$
 assignvariableop_18_dense_7_bias&
"assignvariableop_19_dense_8_kernel$
 assignvariableop_20_dense_8_bias&
"assignvariableop_21_dense_9_kernel$
 assignvariableop_22_dense_9_bias'
#assignvariableop_23_dense_10_kernel%
!assignvariableop_24_dense_10_bias!
assignvariableop_25_adam_iter#
assignvariableop_26_adam_beta_1#
assignvariableop_27_adam_beta_2"
assignvariableop_28_adam_decay*
&assignvariableop_29_adam_learning_rateY
Ustring_lookup_index_table_table_restore_lookuptableimportv2_string_lookup_index_table
assignvariableop_30_total
assignvariableop_31_count_1+
'assignvariableop_32_adam_dense_kernel_m)
%assignvariableop_33_adam_dense_bias_m-
)assignvariableop_34_adam_dense_1_kernel_m+
'assignvariableop_35_adam_dense_1_bias_m-
)assignvariableop_36_adam_dense_2_kernel_m+
'assignvariableop_37_adam_dense_2_bias_m-
)assignvariableop_38_adam_dense_3_kernel_m+
'assignvariableop_39_adam_dense_3_bias_m-
)assignvariableop_40_adam_dense_4_kernel_m+
'assignvariableop_41_adam_dense_4_bias_m-
)assignvariableop_42_adam_dense_5_kernel_m+
'assignvariableop_43_adam_dense_5_bias_m-
)assignvariableop_44_adam_dense_6_kernel_m+
'assignvariableop_45_adam_dense_6_bias_m-
)assignvariableop_46_adam_dense_7_kernel_m+
'assignvariableop_47_adam_dense_7_bias_m-
)assignvariableop_48_adam_dense_8_kernel_m+
'assignvariableop_49_adam_dense_8_bias_m-
)assignvariableop_50_adam_dense_9_kernel_m+
'assignvariableop_51_adam_dense_9_bias_m.
*assignvariableop_52_adam_dense_10_kernel_m,
(assignvariableop_53_adam_dense_10_bias_m+
'assignvariableop_54_adam_dense_kernel_v)
%assignvariableop_55_adam_dense_bias_v-
)assignvariableop_56_adam_dense_1_kernel_v+
'assignvariableop_57_adam_dense_1_bias_v-
)assignvariableop_58_adam_dense_2_kernel_v+
'assignvariableop_59_adam_dense_2_bias_v-
)assignvariableop_60_adam_dense_3_kernel_v+
'assignvariableop_61_adam_dense_3_bias_v-
)assignvariableop_62_adam_dense_4_kernel_v+
'assignvariableop_63_adam_dense_4_bias_v-
)assignvariableop_64_adam_dense_5_kernel_v+
'assignvariableop_65_adam_dense_5_bias_v-
)assignvariableop_66_adam_dense_6_kernel_v+
'assignvariableop_67_adam_dense_6_bias_v-
)assignvariableop_68_adam_dense_7_kernel_v+
'assignvariableop_69_adam_dense_7_bias_v-
)assignvariableop_70_adam_dense_8_kernel_v+
'assignvariableop_71_adam_dense_8_bias_v-
)assignvariableop_72_adam_dense_9_kernel_v+
'assignvariableop_73_adam_dense_9_bias_v.
*assignvariableop_74_adam_dense_10_kernel_v,
(assignvariableop_75_adam_dense_10_bias_v
identity_77ИвAssignVariableOpвAssignVariableOp_1вAssignVariableOp_10вAssignVariableOp_11вAssignVariableOp_12вAssignVariableOp_13вAssignVariableOp_14вAssignVariableOp_15вAssignVariableOp_16вAssignVariableOp_17вAssignVariableOp_18вAssignVariableOp_19вAssignVariableOp_2вAssignVariableOp_20вAssignVariableOp_21вAssignVariableOp_22вAssignVariableOp_23вAssignVariableOp_24вAssignVariableOp_25вAssignVariableOp_26вAssignVariableOp_27вAssignVariableOp_28вAssignVariableOp_29вAssignVariableOp_3вAssignVariableOp_30вAssignVariableOp_31вAssignVariableOp_32вAssignVariableOp_33вAssignVariableOp_34вAssignVariableOp_35вAssignVariableOp_36вAssignVariableOp_37вAssignVariableOp_38вAssignVariableOp_39вAssignVariableOp_4вAssignVariableOp_40вAssignVariableOp_41вAssignVariableOp_42вAssignVariableOp_43вAssignVariableOp_44вAssignVariableOp_45вAssignVariableOp_46вAssignVariableOp_47вAssignVariableOp_48вAssignVariableOp_49вAssignVariableOp_5вAssignVariableOp_50вAssignVariableOp_51вAssignVariableOp_52вAssignVariableOp_53вAssignVariableOp_54вAssignVariableOp_55вAssignVariableOp_56вAssignVariableOp_57вAssignVariableOp_58вAssignVariableOp_59вAssignVariableOp_6вAssignVariableOp_60вAssignVariableOp_61вAssignVariableOp_62вAssignVariableOp_63вAssignVariableOp_64вAssignVariableOp_65вAssignVariableOp_66вAssignVariableOp_67вAssignVariableOp_68вAssignVariableOp_69вAssignVariableOp_7вAssignVariableOp_70вAssignVariableOp_71вAssignVariableOp_72вAssignVariableOp_73вAssignVariableOp_74вAssignVariableOp_75вAssignVariableOp_8вAssignVariableOp_9в;string_lookup_index_table_table_restore/LookupTableImportV2┴,
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:O*
dtype0*═+
value├+B└+OB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/mean/.ATTRIBUTES/VARIABLE_VALUEB8layer_with_weights-4/variance/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-4/count/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-8/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-8/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-9/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-9/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-10/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-10/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-11/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-11/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-12/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-12/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEBFlayer_with_weights-0/_index_lookup_layer/_table/.ATTRIBUTES/table-keysBHlayer_with_weights-0/_index_lookup_layer/_table/.ATTRIBUTES/table-valuesB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-5/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-6/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-6/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-7/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-7/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-9/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-9/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-10/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-10/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-11/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-11/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-12/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-12/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-5/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-6/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-6/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-7/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-7/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-9/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-9/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-10/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-10/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-11/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-11/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-12/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-12/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2/tensor_namesп
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:O*
dtype0*│
valueйBжOB B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
RestoreV2/shape_and_slices╣
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*╥
_output_shapes┐
╝:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*]
dtypesS
Q2O			2
	RestoreV2g
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:2

IdentityЬ
AssignVariableOpAssignVariableOpassignvariableop_dense_kernelIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOpk

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:2

Identity_1в
AssignVariableOp_1AssignVariableOpassignvariableop_1_dense_biasIdentity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_1k

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:2

Identity_2ж
AssignVariableOp_2AssignVariableOp!assignvariableop_2_dense_1_kernelIdentity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_2k

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:2

Identity_3д
AssignVariableOp_3AssignVariableOpassignvariableop_3_dense_1_biasIdentity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_3k

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0*
_output_shapes
:2

Identity_4ж
AssignVariableOp_4AssignVariableOp!assignvariableop_4_dense_2_kernelIdentity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_4k

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:2

Identity_5д
AssignVariableOp_5AssignVariableOpassignvariableop_5_dense_2_biasIdentity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_5k

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:2

Identity_6Ь
AssignVariableOp_6AssignVariableOpassignvariableop_6_meanIdentity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_6k

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:2

Identity_7а
AssignVariableOp_7AssignVariableOpassignvariableop_7_varianceIdentity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_7k

Identity_8IdentityRestoreV2:tensors:8"/device:CPU:0*
T0	*
_output_shapes
:2

Identity_8Э
AssignVariableOp_8AssignVariableOpassignvariableop_8_countIdentity_8:output:0"/device:CPU:0*
_output_shapes
 *
dtype0	2
AssignVariableOp_8k

Identity_9IdentityRestoreV2:tensors:9"/device:CPU:0*
T0*
_output_shapes
:2

Identity_9ж
AssignVariableOp_9AssignVariableOp!assignvariableop_9_dense_3_kernelIdentity_9:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_9n
Identity_10IdentityRestoreV2:tensors:10"/device:CPU:0*
T0*
_output_shapes
:2
Identity_10и
AssignVariableOp_10AssignVariableOp assignvariableop_10_dense_3_biasIdentity_10:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_10n
Identity_11IdentityRestoreV2:tensors:11"/device:CPU:0*
T0*
_output_shapes
:2
Identity_11к
AssignVariableOp_11AssignVariableOp"assignvariableop_11_dense_4_kernelIdentity_11:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_11n
Identity_12IdentityRestoreV2:tensors:12"/device:CPU:0*
T0*
_output_shapes
:2
Identity_12и
AssignVariableOp_12AssignVariableOp assignvariableop_12_dense_4_biasIdentity_12:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_12n
Identity_13IdentityRestoreV2:tensors:13"/device:CPU:0*
T0*
_output_shapes
:2
Identity_13к
AssignVariableOp_13AssignVariableOp"assignvariableop_13_dense_5_kernelIdentity_13:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_13n
Identity_14IdentityRestoreV2:tensors:14"/device:CPU:0*
T0*
_output_shapes
:2
Identity_14и
AssignVariableOp_14AssignVariableOp assignvariableop_14_dense_5_biasIdentity_14:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_14n
Identity_15IdentityRestoreV2:tensors:15"/device:CPU:0*
T0*
_output_shapes
:2
Identity_15к
AssignVariableOp_15AssignVariableOp"assignvariableop_15_dense_6_kernelIdentity_15:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_15n
Identity_16IdentityRestoreV2:tensors:16"/device:CPU:0*
T0*
_output_shapes
:2
Identity_16и
AssignVariableOp_16AssignVariableOp assignvariableop_16_dense_6_biasIdentity_16:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_16n
Identity_17IdentityRestoreV2:tensors:17"/device:CPU:0*
T0*
_output_shapes
:2
Identity_17к
AssignVariableOp_17AssignVariableOp"assignvariableop_17_dense_7_kernelIdentity_17:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_17n
Identity_18IdentityRestoreV2:tensors:18"/device:CPU:0*
T0*
_output_shapes
:2
Identity_18и
AssignVariableOp_18AssignVariableOp assignvariableop_18_dense_7_biasIdentity_18:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_18n
Identity_19IdentityRestoreV2:tensors:19"/device:CPU:0*
T0*
_output_shapes
:2
Identity_19к
AssignVariableOp_19AssignVariableOp"assignvariableop_19_dense_8_kernelIdentity_19:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_19n
Identity_20IdentityRestoreV2:tensors:20"/device:CPU:0*
T0*
_output_shapes
:2
Identity_20и
AssignVariableOp_20AssignVariableOp assignvariableop_20_dense_8_biasIdentity_20:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_20n
Identity_21IdentityRestoreV2:tensors:21"/device:CPU:0*
T0*
_output_shapes
:2
Identity_21к
AssignVariableOp_21AssignVariableOp"assignvariableop_21_dense_9_kernelIdentity_21:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_21n
Identity_22IdentityRestoreV2:tensors:22"/device:CPU:0*
T0*
_output_shapes
:2
Identity_22и
AssignVariableOp_22AssignVariableOp assignvariableop_22_dense_9_biasIdentity_22:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_22n
Identity_23IdentityRestoreV2:tensors:23"/device:CPU:0*
T0*
_output_shapes
:2
Identity_23л
AssignVariableOp_23AssignVariableOp#assignvariableop_23_dense_10_kernelIdentity_23:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_23n
Identity_24IdentityRestoreV2:tensors:24"/device:CPU:0*
T0*
_output_shapes
:2
Identity_24й
AssignVariableOp_24AssignVariableOp!assignvariableop_24_dense_10_biasIdentity_24:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_24n
Identity_25IdentityRestoreV2:tensors:25"/device:CPU:0*
T0	*
_output_shapes
:2
Identity_25е
AssignVariableOp_25AssignVariableOpassignvariableop_25_adam_iterIdentity_25:output:0"/device:CPU:0*
_output_shapes
 *
dtype0	2
AssignVariableOp_25n
Identity_26IdentityRestoreV2:tensors:26"/device:CPU:0*
T0*
_output_shapes
:2
Identity_26з
AssignVariableOp_26AssignVariableOpassignvariableop_26_adam_beta_1Identity_26:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_26n
Identity_27IdentityRestoreV2:tensors:27"/device:CPU:0*
T0*
_output_shapes
:2
Identity_27з
AssignVariableOp_27AssignVariableOpassignvariableop_27_adam_beta_2Identity_27:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_27n
Identity_28IdentityRestoreV2:tensors:28"/device:CPU:0*
T0*
_output_shapes
:2
Identity_28ж
AssignVariableOp_28AssignVariableOpassignvariableop_28_adam_decayIdentity_28:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_28n
Identity_29IdentityRestoreV2:tensors:29"/device:CPU:0*
T0*
_output_shapes
:2
Identity_29о
AssignVariableOp_29AssignVariableOp&assignvariableop_29_adam_learning_rateIdentity_29:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_29я
;string_lookup_index_table_table_restore/LookupTableImportV2LookupTableImportV2Ustring_lookup_index_table_table_restore_lookuptableimportv2_string_lookup_index_tableRestoreV2:tensors:30RestoreV2:tensors:31*	
Tin0*

Tout0	*,
_class"
 loc:@string_lookup_index_table*
_output_shapes
 2=
;string_lookup_index_table_table_restore/LookupTableImportV2n
Identity_30IdentityRestoreV2:tensors:32"/device:CPU:0*
T0*
_output_shapes
:2
Identity_30б
AssignVariableOp_30AssignVariableOpassignvariableop_30_totalIdentity_30:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_30n
Identity_31IdentityRestoreV2:tensors:33"/device:CPU:0*
T0*
_output_shapes
:2
Identity_31г
AssignVariableOp_31AssignVariableOpassignvariableop_31_count_1Identity_31:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_31n
Identity_32IdentityRestoreV2:tensors:34"/device:CPU:0*
T0*
_output_shapes
:2
Identity_32п
AssignVariableOp_32AssignVariableOp'assignvariableop_32_adam_dense_kernel_mIdentity_32:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_32n
Identity_33IdentityRestoreV2:tensors:35"/device:CPU:0*
T0*
_output_shapes
:2
Identity_33н
AssignVariableOp_33AssignVariableOp%assignvariableop_33_adam_dense_bias_mIdentity_33:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_33n
Identity_34IdentityRestoreV2:tensors:36"/device:CPU:0*
T0*
_output_shapes
:2
Identity_34▒
AssignVariableOp_34AssignVariableOp)assignvariableop_34_adam_dense_1_kernel_mIdentity_34:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_34n
Identity_35IdentityRestoreV2:tensors:37"/device:CPU:0*
T0*
_output_shapes
:2
Identity_35п
AssignVariableOp_35AssignVariableOp'assignvariableop_35_adam_dense_1_bias_mIdentity_35:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_35n
Identity_36IdentityRestoreV2:tensors:38"/device:CPU:0*
T0*
_output_shapes
:2
Identity_36▒
AssignVariableOp_36AssignVariableOp)assignvariableop_36_adam_dense_2_kernel_mIdentity_36:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_36n
Identity_37IdentityRestoreV2:tensors:39"/device:CPU:0*
T0*
_output_shapes
:2
Identity_37п
AssignVariableOp_37AssignVariableOp'assignvariableop_37_adam_dense_2_bias_mIdentity_37:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_37n
Identity_38IdentityRestoreV2:tensors:40"/device:CPU:0*
T0*
_output_shapes
:2
Identity_38▒
AssignVariableOp_38AssignVariableOp)assignvariableop_38_adam_dense_3_kernel_mIdentity_38:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_38n
Identity_39IdentityRestoreV2:tensors:41"/device:CPU:0*
T0*
_output_shapes
:2
Identity_39п
AssignVariableOp_39AssignVariableOp'assignvariableop_39_adam_dense_3_bias_mIdentity_39:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_39n
Identity_40IdentityRestoreV2:tensors:42"/device:CPU:0*
T0*
_output_shapes
:2
Identity_40▒
AssignVariableOp_40AssignVariableOp)assignvariableop_40_adam_dense_4_kernel_mIdentity_40:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_40n
Identity_41IdentityRestoreV2:tensors:43"/device:CPU:0*
T0*
_output_shapes
:2
Identity_41п
AssignVariableOp_41AssignVariableOp'assignvariableop_41_adam_dense_4_bias_mIdentity_41:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_41n
Identity_42IdentityRestoreV2:tensors:44"/device:CPU:0*
T0*
_output_shapes
:2
Identity_42▒
AssignVariableOp_42AssignVariableOp)assignvariableop_42_adam_dense_5_kernel_mIdentity_42:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_42n
Identity_43IdentityRestoreV2:tensors:45"/device:CPU:0*
T0*
_output_shapes
:2
Identity_43п
AssignVariableOp_43AssignVariableOp'assignvariableop_43_adam_dense_5_bias_mIdentity_43:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_43n
Identity_44IdentityRestoreV2:tensors:46"/device:CPU:0*
T0*
_output_shapes
:2
Identity_44▒
AssignVariableOp_44AssignVariableOp)assignvariableop_44_adam_dense_6_kernel_mIdentity_44:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_44n
Identity_45IdentityRestoreV2:tensors:47"/device:CPU:0*
T0*
_output_shapes
:2
Identity_45п
AssignVariableOp_45AssignVariableOp'assignvariableop_45_adam_dense_6_bias_mIdentity_45:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_45n
Identity_46IdentityRestoreV2:tensors:48"/device:CPU:0*
T0*
_output_shapes
:2
Identity_46▒
AssignVariableOp_46AssignVariableOp)assignvariableop_46_adam_dense_7_kernel_mIdentity_46:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_46n
Identity_47IdentityRestoreV2:tensors:49"/device:CPU:0*
T0*
_output_shapes
:2
Identity_47п
AssignVariableOp_47AssignVariableOp'assignvariableop_47_adam_dense_7_bias_mIdentity_47:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_47n
Identity_48IdentityRestoreV2:tensors:50"/device:CPU:0*
T0*
_output_shapes
:2
Identity_48▒
AssignVariableOp_48AssignVariableOp)assignvariableop_48_adam_dense_8_kernel_mIdentity_48:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_48n
Identity_49IdentityRestoreV2:tensors:51"/device:CPU:0*
T0*
_output_shapes
:2
Identity_49п
AssignVariableOp_49AssignVariableOp'assignvariableop_49_adam_dense_8_bias_mIdentity_49:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_49n
Identity_50IdentityRestoreV2:tensors:52"/device:CPU:0*
T0*
_output_shapes
:2
Identity_50▒
AssignVariableOp_50AssignVariableOp)assignvariableop_50_adam_dense_9_kernel_mIdentity_50:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_50n
Identity_51IdentityRestoreV2:tensors:53"/device:CPU:0*
T0*
_output_shapes
:2
Identity_51п
AssignVariableOp_51AssignVariableOp'assignvariableop_51_adam_dense_9_bias_mIdentity_51:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_51n
Identity_52IdentityRestoreV2:tensors:54"/device:CPU:0*
T0*
_output_shapes
:2
Identity_52▓
AssignVariableOp_52AssignVariableOp*assignvariableop_52_adam_dense_10_kernel_mIdentity_52:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_52n
Identity_53IdentityRestoreV2:tensors:55"/device:CPU:0*
T0*
_output_shapes
:2
Identity_53░
AssignVariableOp_53AssignVariableOp(assignvariableop_53_adam_dense_10_bias_mIdentity_53:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_53n
Identity_54IdentityRestoreV2:tensors:56"/device:CPU:0*
T0*
_output_shapes
:2
Identity_54п
AssignVariableOp_54AssignVariableOp'assignvariableop_54_adam_dense_kernel_vIdentity_54:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_54n
Identity_55IdentityRestoreV2:tensors:57"/device:CPU:0*
T0*
_output_shapes
:2
Identity_55н
AssignVariableOp_55AssignVariableOp%assignvariableop_55_adam_dense_bias_vIdentity_55:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_55n
Identity_56IdentityRestoreV2:tensors:58"/device:CPU:0*
T0*
_output_shapes
:2
Identity_56▒
AssignVariableOp_56AssignVariableOp)assignvariableop_56_adam_dense_1_kernel_vIdentity_56:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_56n
Identity_57IdentityRestoreV2:tensors:59"/device:CPU:0*
T0*
_output_shapes
:2
Identity_57п
AssignVariableOp_57AssignVariableOp'assignvariableop_57_adam_dense_1_bias_vIdentity_57:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_57n
Identity_58IdentityRestoreV2:tensors:60"/device:CPU:0*
T0*
_output_shapes
:2
Identity_58▒
AssignVariableOp_58AssignVariableOp)assignvariableop_58_adam_dense_2_kernel_vIdentity_58:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_58n
Identity_59IdentityRestoreV2:tensors:61"/device:CPU:0*
T0*
_output_shapes
:2
Identity_59п
AssignVariableOp_59AssignVariableOp'assignvariableop_59_adam_dense_2_bias_vIdentity_59:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_59n
Identity_60IdentityRestoreV2:tensors:62"/device:CPU:0*
T0*
_output_shapes
:2
Identity_60▒
AssignVariableOp_60AssignVariableOp)assignvariableop_60_adam_dense_3_kernel_vIdentity_60:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_60n
Identity_61IdentityRestoreV2:tensors:63"/device:CPU:0*
T0*
_output_shapes
:2
Identity_61п
AssignVariableOp_61AssignVariableOp'assignvariableop_61_adam_dense_3_bias_vIdentity_61:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_61n
Identity_62IdentityRestoreV2:tensors:64"/device:CPU:0*
T0*
_output_shapes
:2
Identity_62▒
AssignVariableOp_62AssignVariableOp)assignvariableop_62_adam_dense_4_kernel_vIdentity_62:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_62n
Identity_63IdentityRestoreV2:tensors:65"/device:CPU:0*
T0*
_output_shapes
:2
Identity_63п
AssignVariableOp_63AssignVariableOp'assignvariableop_63_adam_dense_4_bias_vIdentity_63:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_63n
Identity_64IdentityRestoreV2:tensors:66"/device:CPU:0*
T0*
_output_shapes
:2
Identity_64▒
AssignVariableOp_64AssignVariableOp)assignvariableop_64_adam_dense_5_kernel_vIdentity_64:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_64n
Identity_65IdentityRestoreV2:tensors:67"/device:CPU:0*
T0*
_output_shapes
:2
Identity_65п
AssignVariableOp_65AssignVariableOp'assignvariableop_65_adam_dense_5_bias_vIdentity_65:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_65n
Identity_66IdentityRestoreV2:tensors:68"/device:CPU:0*
T0*
_output_shapes
:2
Identity_66▒
AssignVariableOp_66AssignVariableOp)assignvariableop_66_adam_dense_6_kernel_vIdentity_66:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_66n
Identity_67IdentityRestoreV2:tensors:69"/device:CPU:0*
T0*
_output_shapes
:2
Identity_67п
AssignVariableOp_67AssignVariableOp'assignvariableop_67_adam_dense_6_bias_vIdentity_67:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_67n
Identity_68IdentityRestoreV2:tensors:70"/device:CPU:0*
T0*
_output_shapes
:2
Identity_68▒
AssignVariableOp_68AssignVariableOp)assignvariableop_68_adam_dense_7_kernel_vIdentity_68:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_68n
Identity_69IdentityRestoreV2:tensors:71"/device:CPU:0*
T0*
_output_shapes
:2
Identity_69п
AssignVariableOp_69AssignVariableOp'assignvariableop_69_adam_dense_7_bias_vIdentity_69:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_69n
Identity_70IdentityRestoreV2:tensors:72"/device:CPU:0*
T0*
_output_shapes
:2
Identity_70▒
AssignVariableOp_70AssignVariableOp)assignvariableop_70_adam_dense_8_kernel_vIdentity_70:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_70n
Identity_71IdentityRestoreV2:tensors:73"/device:CPU:0*
T0*
_output_shapes
:2
Identity_71п
AssignVariableOp_71AssignVariableOp'assignvariableop_71_adam_dense_8_bias_vIdentity_71:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_71n
Identity_72IdentityRestoreV2:tensors:74"/device:CPU:0*
T0*
_output_shapes
:2
Identity_72▒
AssignVariableOp_72AssignVariableOp)assignvariableop_72_adam_dense_9_kernel_vIdentity_72:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_72n
Identity_73IdentityRestoreV2:tensors:75"/device:CPU:0*
T0*
_output_shapes
:2
Identity_73п
AssignVariableOp_73AssignVariableOp'assignvariableop_73_adam_dense_9_bias_vIdentity_73:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_73n
Identity_74IdentityRestoreV2:tensors:76"/device:CPU:0*
T0*
_output_shapes
:2
Identity_74▓
AssignVariableOp_74AssignVariableOp*assignvariableop_74_adam_dense_10_kernel_vIdentity_74:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_74n
Identity_75IdentityRestoreV2:tensors:77"/device:CPU:0*
T0*
_output_shapes
:2
Identity_75░
AssignVariableOp_75AssignVariableOp(assignvariableop_75_adam_dense_10_bias_vIdentity_75:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_759
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOpд
Identity_76Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_40^AssignVariableOp_41^AssignVariableOp_42^AssignVariableOp_43^AssignVariableOp_44^AssignVariableOp_45^AssignVariableOp_46^AssignVariableOp_47^AssignVariableOp_48^AssignVariableOp_49^AssignVariableOp_5^AssignVariableOp_50^AssignVariableOp_51^AssignVariableOp_52^AssignVariableOp_53^AssignVariableOp_54^AssignVariableOp_55^AssignVariableOp_56^AssignVariableOp_57^AssignVariableOp_58^AssignVariableOp_59^AssignVariableOp_6^AssignVariableOp_60^AssignVariableOp_61^AssignVariableOp_62^AssignVariableOp_63^AssignVariableOp_64^AssignVariableOp_65^AssignVariableOp_66^AssignVariableOp_67^AssignVariableOp_68^AssignVariableOp_69^AssignVariableOp_7^AssignVariableOp_70^AssignVariableOp_71^AssignVariableOp_72^AssignVariableOp_73^AssignVariableOp_74^AssignVariableOp_75^AssignVariableOp_8^AssignVariableOp_9^NoOp<^string_lookup_index_table_table_restore/LookupTableImportV2"/device:CPU:0*
T0*
_output_shapes
: 2
Identity_76Ч
Identity_77IdentityIdentity_76:output:0^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_40^AssignVariableOp_41^AssignVariableOp_42^AssignVariableOp_43^AssignVariableOp_44^AssignVariableOp_45^AssignVariableOp_46^AssignVariableOp_47^AssignVariableOp_48^AssignVariableOp_49^AssignVariableOp_5^AssignVariableOp_50^AssignVariableOp_51^AssignVariableOp_52^AssignVariableOp_53^AssignVariableOp_54^AssignVariableOp_55^AssignVariableOp_56^AssignVariableOp_57^AssignVariableOp_58^AssignVariableOp_59^AssignVariableOp_6^AssignVariableOp_60^AssignVariableOp_61^AssignVariableOp_62^AssignVariableOp_63^AssignVariableOp_64^AssignVariableOp_65^AssignVariableOp_66^AssignVariableOp_67^AssignVariableOp_68^AssignVariableOp_69^AssignVariableOp_7^AssignVariableOp_70^AssignVariableOp_71^AssignVariableOp_72^AssignVariableOp_73^AssignVariableOp_74^AssignVariableOp_75^AssignVariableOp_8^AssignVariableOp_9<^string_lookup_index_table_table_restore/LookupTableImportV2*
T0*
_output_shapes
: 2
Identity_77"#
identity_77Identity_77:output:0*╦
_input_shapes╣
╢: :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212*
AssignVariableOp_22AssignVariableOp_222*
AssignVariableOp_23AssignVariableOp_232*
AssignVariableOp_24AssignVariableOp_242*
AssignVariableOp_25AssignVariableOp_252*
AssignVariableOp_26AssignVariableOp_262*
AssignVariableOp_27AssignVariableOp_272*
AssignVariableOp_28AssignVariableOp_282*
AssignVariableOp_29AssignVariableOp_292(
AssignVariableOp_3AssignVariableOp_32*
AssignVariableOp_30AssignVariableOp_302*
AssignVariableOp_31AssignVariableOp_312*
AssignVariableOp_32AssignVariableOp_322*
AssignVariableOp_33AssignVariableOp_332*
AssignVariableOp_34AssignVariableOp_342*
AssignVariableOp_35AssignVariableOp_352*
AssignVariableOp_36AssignVariableOp_362*
AssignVariableOp_37AssignVariableOp_372*
AssignVariableOp_38AssignVariableOp_382*
AssignVariableOp_39AssignVariableOp_392(
AssignVariableOp_4AssignVariableOp_42*
AssignVariableOp_40AssignVariableOp_402*
AssignVariableOp_41AssignVariableOp_412*
AssignVariableOp_42AssignVariableOp_422*
AssignVariableOp_43AssignVariableOp_432*
AssignVariableOp_44AssignVariableOp_442*
AssignVariableOp_45AssignVariableOp_452*
AssignVariableOp_46AssignVariableOp_462*
AssignVariableOp_47AssignVariableOp_472*
AssignVariableOp_48AssignVariableOp_482*
AssignVariableOp_49AssignVariableOp_492(
AssignVariableOp_5AssignVariableOp_52*
AssignVariableOp_50AssignVariableOp_502*
AssignVariableOp_51AssignVariableOp_512*
AssignVariableOp_52AssignVariableOp_522*
AssignVariableOp_53AssignVariableOp_532*
AssignVariableOp_54AssignVariableOp_542*
AssignVariableOp_55AssignVariableOp_552*
AssignVariableOp_56AssignVariableOp_562*
AssignVariableOp_57AssignVariableOp_572*
AssignVariableOp_58AssignVariableOp_582*
AssignVariableOp_59AssignVariableOp_592(
AssignVariableOp_6AssignVariableOp_62*
AssignVariableOp_60AssignVariableOp_602*
AssignVariableOp_61AssignVariableOp_612*
AssignVariableOp_62AssignVariableOp_622*
AssignVariableOp_63AssignVariableOp_632*
AssignVariableOp_64AssignVariableOp_642*
AssignVariableOp_65AssignVariableOp_652*
AssignVariableOp_66AssignVariableOp_662*
AssignVariableOp_67AssignVariableOp_672*
AssignVariableOp_68AssignVariableOp_682*
AssignVariableOp_69AssignVariableOp_692(
AssignVariableOp_7AssignVariableOp_72*
AssignVariableOp_70AssignVariableOp_702*
AssignVariableOp_71AssignVariableOp_712*
AssignVariableOp_72AssignVariableOp_722*
AssignVariableOp_73AssignVariableOp_732*
AssignVariableOp_74AssignVariableOp_742*
AssignVariableOp_75AssignVariableOp_752(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_92z
;string_lookup_index_table_table_restore/LookupTableImportV2;string_lookup_index_table_table_restore/LookupTableImportV2:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:2.
,
_class"
 loc:@string_lookup_index_table
ф
H
__inference__creator_8042
identityИвstring_lookup_index_tableг
string_lookup_index_tableMutableHashTableV2*
_output_shapes
: *
	key_dtype0*
shared_name
table_26*
value_dtype0	2
string_lookup_index_tableЗ
IdentityIdentity(string_lookup_index_table:table_handle:0^string_lookup_index_table*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*
_input_shapes 26
string_lookup_index_tablestring_lookup_index_table
й
й
A__inference_dense_2_layer_call_and_return_conditional_losses_6436

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИО
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	А@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpБ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*/
_input_shapes
:         А:::P L
(
_output_shapes
:         А
 
_user_specified_nameinputs
╓
y
$__inference_dense_layer_call_fn_7825

inputs	
unknown
	unknown_0
identityИвStatefulPartitionedCallЁ
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2	*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:         А*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *H
fCRA
?__inference_dense_layer_call_and_return_conditional_losses_63822
StatefulPartitionedCallП
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*(
_output_shapes
:         А2

Identity"
identityIdentity:output:0*/
_input_shapes
:         ▄::22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:         ▄
 
_user_specified_nameinputs
╧
╒
"text_vectorization_cond_false_7407'
#text_vectorization_cond_placeholder`
\text_vectorization_cond_strided_slice_text_vectorization_raggedtotensor_raggedtensortotensor	$
 text_vectorization_cond_identity	л
+text_vectorization/cond/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2-
+text_vectorization/cond/strided_slice/stackп
-text_vectorization/cond/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ▄  2/
-text_vectorization/cond/strided_slice/stack_1п
-text_vectorization/cond/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2/
-text_vectorization/cond/strided_slice/stack_2╠
%text_vectorization/cond/strided_sliceStridedSlice\text_vectorization_cond_strided_slice_text_vectorization_raggedtotensor_raggedtensortotensor4text_vectorization/cond/strided_slice/stack:output:06text_vectorization/cond/strided_slice/stack_1:output:06text_vectorization/cond/strided_slice/stack_2:output:0*
Index0*
T0	*0
_output_shapes
:                  *

begin_mask*
end_mask2'
%text_vectorization/cond/strided_slice╗
 text_vectorization/cond/IdentityIdentity.text_vectorization/cond/strided_slice:output:0*
T0	*0
_output_shapes
:                  2"
 text_vectorization/cond/Identity"M
 text_vectorization_cond_identity)text_vectorization/cond/Identity:output:0*1
_input_shapes 
: :                  : 

_output_shapes
: :62
0
_output_shapes
:                  
ж
й
A__inference_dense_8_layer_call_and_return_conditional_losses_6625

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИН
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpБ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @:::O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
╒╟
▀	
F__inference_functional_1_layer_call_and_return_conditional_losses_6993

inputs
inputs_1Z
Vtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handle[
Wtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value	

dense_6925

dense_6927
dense_1_6930
dense_1_6932
dense_2_6935
dense_2_69371
-normalization_reshape_readvariableop_resource3
/normalization_reshape_1_readvariableop_resource
dense_3_6951
dense_3_6953
dense_4_6957
dense_4_6959
dense_5_6962
dense_5_6964
dense_6_6967
dense_6_6969
dense_7_6972
dense_7_6974
dense_8_6977
dense_8_6979
dense_9_6982
dense_9_6984
dense_10_6987
dense_10_6989
identityИвdense/StatefulPartitionedCallвdense_1/StatefulPartitionedCallв dense_10/StatefulPartitionedCallвdense_2/StatefulPartitionedCallвdense_3/StatefulPartitionedCallвdense_4/StatefulPartitionedCallвdense_5/StatefulPartitionedCallвdense_6/StatefulPartitionedCallвdense_7/StatefulPartitionedCallвdense_8/StatefulPartitionedCallвdense_9/StatefulPartitionedCallвItext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2В
text_vectorization/StringLowerStringLowerinputs_1*'
_output_shapes
:         2 
text_vectorization/StringLower¤
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*'
_output_shapes
:         *6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite 2'
%text_vectorization/StaticRegexReplace┴
text_vectorization/SqueezeSqueeze.text_vectorization/StaticRegexReplace:output:0*
T0*#
_output_shapes
:         *
squeeze_dims

         2
text_vectorization/Squeeze╖
9text_vectorization/StringsByteSplit/StringSplit/delimiterConst*
_output_shapes
: *
dtype0*
valueB B 2;
9text_vectorization/StringsByteSplit/StringSplit/delimiterк
/text_vectorization/StringsByteSplit/StringSplitStringSplit#text_vectorization/Squeeze:output:0Btext_vectorization/StringsByteSplit/StringSplit/delimiter:output:0*<
_output_shapes*
(:         :         :*

skip_empty( 21
/text_vectorization/StringsByteSplit/StringSplit├
7text_vectorization/StringsByteSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        29
7text_vectorization/StringsByteSplit/strided_slice/stack╟
9text_vectorization/StringsByteSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       2;
9text_vectorization/StringsByteSplit/strided_slice/stack_1╟
9text_vectorization/StringsByteSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2;
9text_vectorization/StringsByteSplit/strided_slice/stack_2Ё
1text_vectorization/StringsByteSplit/strided_sliceStridedSlice9text_vectorization/StringsByteSplit/StringSplit:indices:0@text_vectorization/StringsByteSplit/strided_slice/stack:output:0Btext_vectorization/StringsByteSplit/strided_slice/stack_1:output:0Btext_vectorization/StringsByteSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:         *

begin_mask*
end_mask*
shrink_axis_mask23
1text_vectorization/StringsByteSplit/strided_slice└
9text_vectorization/StringsByteSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2;
9text_vectorization/StringsByteSplit/strided_slice_1/stack─
;text_vectorization/StringsByteSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2=
;text_vectorization/StringsByteSplit/strided_slice_1/stack_1─
;text_vectorization/StringsByteSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2=
;text_vectorization/StringsByteSplit/strided_slice_1/stack_2╔
3text_vectorization/StringsByteSplit/strided_slice_1StridedSlice7text_vectorization/StringsByteSplit/StringSplit:shape:0Btext_vectorization/StringsByteSplit/strided_slice_1/stack:output:0Dtext_vectorization/StringsByteSplit/strided_slice_1/stack_1:output:0Dtext_vectorization/StringsByteSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask25
3text_vectorization/StringsByteSplit/strided_slice_1╣
Ztext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast:text_vectorization/StringsByteSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:         2\
Ztext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast▓
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast<text_vectorization/StringsByteSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: 2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1┌
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShape^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeЦ
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const╤
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdmtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0mtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: 2e
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdЦ
htext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : 2j
htext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y▌
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreaterltext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0qtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greaterю
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastjtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: 2e
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastЪ
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1┴
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMax^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0otext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxО
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y╬
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ktext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0mtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add┴
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulgtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul╞
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum`text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum╩
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum`text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumУ
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2╔
gtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincount^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0otext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:         2i
gtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountИ
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : 2c
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis╨
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumntext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:         2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumШ
etext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R 2g
etext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0И
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2c
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis┐
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2ntext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:         2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatА
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Vtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handle8text_vectorization/StringsByteSplit/StringSplit:values:0Wtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*#
_output_shapes
:         2K
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2Ж
2text_vectorization/string_lookup/assert_equal/NoOpNoOp*
_output_shapes
 24
2text_vectorization/string_lookup/assert_equal/NoOpф
)text_vectorization/string_lookup/IdentityIdentityRtext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:         2+
)text_vectorization/string_lookup/Identity√
+text_vectorization/string_lookup/Identity_1Identityetext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat:output:0*
T0	*#
_output_shapes
:         2-
+text_vectorization/string_lookup/Identity_1д
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R 21
/text_vectorization/RaggedToTensor/default_valueЭ
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
: *
dtype0	*
valueB	 R
         2)
'text_vectorization/RaggedToTensor/Const¤
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:02text_vectorization/string_lookup/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:04text_vectorization/string_lookup/Identity_1:output:0*
T0	*
Tindex0	*
Tshape0	*0
_output_shapes
:                  *
num_row_partition_tensors*%
row_partition_types

ROW_SPLITS28
6text_vectorization/RaggedToTensor/RaggedTensorToTensorг
text_vectorization/ShapeShape?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
T0	*
_output_shapes
:2
text_vectorization/ShapeЪ
&text_vectorization/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&text_vectorization/strided_slice/stackЮ
(text_vectorization/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(text_vectorization/strided_slice/stack_1Ю
(text_vectorization/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(text_vectorization/strided_slice/stack_2╘
 text_vectorization/strided_sliceStridedSlice!text_vectorization/Shape:output:0/text_vectorization/strided_slice/stack:output:01text_vectorization/strided_slice/stack_1:output:01text_vectorization/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 text_vectorization/strided_slicew
text_vectorization/sub/xConst*
_output_shapes
: *
dtype0*
value
B :▄2
text_vectorization/sub/xж
text_vectorization/subSub!text_vectorization/sub/x:output:0)text_vectorization/strided_slice:output:0*
T0*
_output_shapes
: 2
text_vectorization/suby
text_vectorization/Less/yConst*
_output_shapes
: *
dtype0*
value
B :▄2
text_vectorization/Less/yк
text_vectorization/LessLess)text_vectorization/strided_slice:output:0"text_vectorization/Less/y:output:0*
T0*
_output_shapes
: 2
text_vectorization/Less∙
text_vectorization/condStatelessIftext_vectorization/Less:z:0text_vectorization/sub:z:0?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
Tcond0
*
Tin
2	*
Tout
2	*
_lower_using_switch_merge(*0
_output_shapes
:                  * 
_read_only_resource_inputs
 *5
else_branch&R$
"text_vectorization_cond_false_6905*/
output_shapes
:                  *4
then_branch%R#
!text_vectorization_cond_true_69042
text_vectorization/condе
 text_vectorization/cond/IdentityIdentity text_vectorization/cond:output:0*
T0	*(
_output_shapes
:         ▄2"
 text_vectorization/cond/Identityг
dense/StatefulPartitionedCallStatefulPartitionedCall)text_vectorization/cond/Identity:output:0
dense_6925
dense_6927*
Tin
2	*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:         А*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *H
fCRA
?__inference_dense_layer_call_and_return_conditional_losses_63822
dense/StatefulPartitionedCallк
dense_1/StatefulPartitionedCallStatefulPartitionedCall&dense/StatefulPartitionedCall:output:0dense_1_6930dense_1_6932*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:         А*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_1_layer_call_and_return_conditional_losses_64092!
dense_1/StatefulPartitionedCallл
dense_2/StatefulPartitionedCallStatefulPartitionedCall(dense_1/StatefulPartitionedCall:output:0dense_2_6935dense_2_6937*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_2_layer_call_and_return_conditional_losses_64362!
dense_2/StatefulPartitionedCall╢
$normalization/Reshape/ReadVariableOpReadVariableOp-normalization_reshape_readvariableop_resource*
_output_shapes
:*
dtype02&
$normalization/Reshape/ReadVariableOpЛ
normalization/Reshape/shapeConst*
_output_shapes
:*
dtype0*
valueB"      2
normalization/Reshape/shape╢
normalization/ReshapeReshape,normalization/Reshape/ReadVariableOp:value:0$normalization/Reshape/shape:output:0*
T0*
_output_shapes

:2
normalization/Reshape╝
&normalization/Reshape_1/ReadVariableOpReadVariableOp/normalization_reshape_1_readvariableop_resource*
_output_shapes
:*
dtype02(
&normalization/Reshape_1/ReadVariableOpП
normalization/Reshape_1/shapeConst*
_output_shapes
:*
dtype0*
valueB"      2
normalization/Reshape_1/shape╛
normalization/Reshape_1Reshape.normalization/Reshape_1/ReadVariableOp:value:0&normalization/Reshape_1/shape:output:0*
T0*
_output_shapes

:2
normalization/Reshape_1З
normalization/subSubinputsnormalization/Reshape:output:0*
T0*'
_output_shapes
:         2
normalization/sub{
normalization/SqrtSqrt normalization/Reshape_1:output:0*
T0*
_output_shapes

:2
normalization/SqrtЪ
normalization/truedivRealDivnormalization/sub:z:0normalization/Sqrt:y:0*
T0*'
_output_shapes
:         2
normalization/truedivл
dense_3/StatefulPartitionedCallStatefulPartitionedCall(dense_2/StatefulPartitionedCall:output:0dense_3_6951dense_3_6953*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_3_layer_call_and_return_conditional_losses_64742!
dense_3/StatefulPartitionedCallЩ
concatenate/PartitionedCallPartitionedCallnormalization/truediv:z:0(dense_3/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         E* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8В *N
fIRG
E__inference_concatenate_layer_call_and_return_conditional_losses_64972
concatenate/PartitionedCallз
dense_4/StatefulPartitionedCallStatefulPartitionedCall$concatenate/PartitionedCall:output:0dense_4_6957dense_4_6959*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_4_layer_call_and_return_conditional_losses_65172!
dense_4/StatefulPartitionedCallл
dense_5/StatefulPartitionedCallStatefulPartitionedCall(dense_4/StatefulPartitionedCall:output:0dense_5_6962dense_5_6964*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_5_layer_call_and_return_conditional_losses_65442!
dense_5/StatefulPartitionedCallл
dense_6/StatefulPartitionedCallStatefulPartitionedCall(dense_5/StatefulPartitionedCall:output:0dense_6_6967dense_6_6969*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_6_layer_call_and_return_conditional_losses_65712!
dense_6/StatefulPartitionedCallл
dense_7/StatefulPartitionedCallStatefulPartitionedCall(dense_6/StatefulPartitionedCall:output:0dense_7_6972dense_7_6974*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_7_layer_call_and_return_conditional_losses_65982!
dense_7/StatefulPartitionedCallл
dense_8/StatefulPartitionedCallStatefulPartitionedCall(dense_7/StatefulPartitionedCall:output:0dense_8_6977dense_8_6979*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_8_layer_call_and_return_conditional_losses_66252!
dense_8/StatefulPartitionedCallл
dense_9/StatefulPartitionedCallStatefulPartitionedCall(dense_8/StatefulPartitionedCall:output:0dense_9_6982dense_9_6984*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_9_layer_call_and_return_conditional_losses_66522!
dense_9/StatefulPartitionedCall░
 dense_10/StatefulPartitionedCallStatefulPartitionedCall(dense_9/StatefulPartitionedCall:output:0dense_10_6987dense_10_6989*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *K
fFRD
B__inference_dense_10_layer_call_and_return_conditional_losses_66782"
 dense_10/StatefulPartitionedCall╛
IdentityIdentity)dense_10/StatefulPartitionedCall:output:0^dense/StatefulPartitionedCall ^dense_1/StatefulPartitionedCall!^dense_10/StatefulPartitionedCall ^dense_2/StatefulPartitionedCall ^dense_3/StatefulPartitionedCall ^dense_4/StatefulPartitionedCall ^dense_5/StatefulPartitionedCall ^dense_6/StatefulPartitionedCall ^dense_7/StatefulPartitionedCall ^dense_8/StatefulPartitionedCall ^dense_9/StatefulPartitionedCallJ^text_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*б
_input_shapesП
М:         :         :: ::::::::::::::::::::::::2>
dense/StatefulPartitionedCalldense/StatefulPartitionedCall2B
dense_1/StatefulPartitionedCalldense_1/StatefulPartitionedCall2D
 dense_10/StatefulPartitionedCall dense_10/StatefulPartitionedCall2B
dense_2/StatefulPartitionedCalldense_2/StatefulPartitionedCall2B
dense_3/StatefulPartitionedCalldense_3/StatefulPartitionedCall2B
dense_4/StatefulPartitionedCalldense_4/StatefulPartitionedCall2B
dense_5/StatefulPartitionedCalldense_5/StatefulPartitionedCall2B
dense_6/StatefulPartitionedCalldense_6/StatefulPartitionedCall2B
dense_7/StatefulPartitionedCalldense_7/StatefulPartitionedCall2B
dense_8/StatefulPartitionedCalldense_8/StatefulPartitionedCall2B
dense_9/StatefulPartitionedCalldense_9/StatefulPartitionedCall2Ц
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2:O K
'
_output_shapes
:         
 
_user_specified_nameinputs:OK
'
_output_shapes
:         
 
_user_specified_nameinputs
╪
|
'__inference_dense_10_layer_call_fn_8037

inputs
unknown
	unknown_0
identityИвStatefulPartitionedCallЄ
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *K
fFRD
B__inference_dense_10_layer_call_and_return_conditional_losses_66782
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
п
й
A__inference_dense_1_layer_call_and_return_conditional_losses_7836

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИП
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
АА*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:         А2
MatMulН
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:А*
dtype02
BiasAdd/ReadVariableOpВ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:         А2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:         А2
Relug
IdentityIdentityRelu:activations:0*
T0*(
_output_shapes
:         А2

Identity"
identityIdentity:output:0*/
_input_shapes
:         А:::P L
(
_output_shapes
:         А
 
_user_specified_nameinputs
ж
й
A__inference_dense_4_layer_call_and_return_conditional_losses_7909

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИН
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:E@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpБ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         E:::O K
'
_output_shapes
:         E
 
_user_specified_nameinputs
╓
{
&__inference_dense_9_layer_call_fn_8018

inputs
unknown
	unknown_0
identityИвStatefulPartitionedCallё
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_9_layer_call_and_return_conditional_losses_66522
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
Ж
-
__inference__initializer_8047
identityP
ConstConst*
_output_shapes
: *
dtype0*
value	B :2
ConstQ
IdentityIdentityConst:output:0*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*
_input_shapes 
ж
й
A__inference_dense_6_layer_call_and_return_conditional_losses_7949

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИН
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpБ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @:::O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
ж
й
A__inference_dense_6_layer_call_and_return_conditional_losses_6571

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИН
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpБ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @:::O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
П	
з
?__inference_dense_layer_call_and_return_conditional_losses_6382

inputs	"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИ^
CastCastinputs*

DstT0*

SrcT0	*(
_output_shapes
:         ▄2
CastП
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
▄А*
dtype02
MatMul/ReadVariableOpv
MatMulMatMulCast:y:0MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:         А2
MatMulН
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:А*
dtype02
BiasAdd/ReadVariableOpВ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:         А2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:         А2
Relug
IdentityIdentityRelu:activations:0*
T0*(
_output_shapes
:         А2

Identity"
identityIdentity:output:0*/
_input_shapes
:         ▄:::P L
(
_output_shapes
:         ▄
 
_user_specified_nameinputs
ж
й
A__inference_dense_9_layer_call_and_return_conditional_losses_6652

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИН
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpБ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @:::O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
╧
╒
"text_vectorization_cond_false_7577'
#text_vectorization_cond_placeholder`
\text_vectorization_cond_strided_slice_text_vectorization_raggedtotensor_raggedtensortotensor	$
 text_vectorization_cond_identity	л
+text_vectorization/cond/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2-
+text_vectorization/cond/strided_slice/stackп
-text_vectorization/cond/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ▄  2/
-text_vectorization/cond/strided_slice/stack_1п
-text_vectorization/cond/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2/
-text_vectorization/cond/strided_slice/stack_2╠
%text_vectorization/cond/strided_sliceStridedSlice\text_vectorization_cond_strided_slice_text_vectorization_raggedtotensor_raggedtensortotensor4text_vectorization/cond/strided_slice/stack:output:06text_vectorization/cond/strided_slice/stack_1:output:06text_vectorization/cond/strided_slice/stack_2:output:0*
Index0*
T0	*0
_output_shapes
:                  *

begin_mask*
end_mask2'
%text_vectorization/cond/strided_slice╗
 text_vectorization/cond/IdentityIdentity.text_vectorization/cond/strided_slice:output:0*
T0	*0
_output_shapes
:                  2"
 text_vectorization/cond/Identity"M
 text_vectorization_cond_identity)text_vectorization/cond/Identity:output:0*1
_input_shapes 
: :                  : 

_output_shapes
: :62
0
_output_shapes
:                  
╧
╒
"text_vectorization_cond_false_6754'
#text_vectorization_cond_placeholder`
\text_vectorization_cond_strided_slice_text_vectorization_raggedtotensor_raggedtensortotensor	$
 text_vectorization_cond_identity	л
+text_vectorization/cond/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2-
+text_vectorization/cond/strided_slice/stackп
-text_vectorization/cond/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ▄  2/
-text_vectorization/cond/strided_slice/stack_1п
-text_vectorization/cond/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2/
-text_vectorization/cond/strided_slice/stack_2╠
%text_vectorization/cond/strided_sliceStridedSlice\text_vectorization_cond_strided_slice_text_vectorization_raggedtotensor_raggedtensortotensor4text_vectorization/cond/strided_slice/stack:output:06text_vectorization/cond/strided_slice/stack_1:output:06text_vectorization/cond/strided_slice/stack_2:output:0*
Index0*
T0	*0
_output_shapes
:                  *

begin_mask*
end_mask2'
%text_vectorization/cond/strided_slice╗
 text_vectorization/cond/IdentityIdentity.text_vectorization/cond/strided_slice:output:0*
T0	*0
_output_shapes
:                  2"
 text_vectorization/cond/Identity"M
 text_vectorization_cond_identity)text_vectorization/cond/Identity:output:0*1
_input_shapes 
: :                  : 

_output_shapes
: :62
0
_output_shapes
:                  
ж
й
A__inference_dense_4_layer_call_and_return_conditional_losses_6517

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИН
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:E@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpБ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         E:::O K
'
_output_shapes
:         E
 
_user_specified_nameinputs
¤
Е
+__inference_functional_1_layer_call_fn_7048
input_2
input_1
unknown
	unknown_0	
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
	unknown_7
	unknown_8
	unknown_9

unknown_10

unknown_11

unknown_12

unknown_13

unknown_14

unknown_15

unknown_16

unknown_17

unknown_18

unknown_19

unknown_20

unknown_21

unknown_22

unknown_23

unknown_24
identityИвStatefulPartitionedCall╞
StatefulPartitionedCallStatefulPartitionedCallinput_2input_1unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24*'
Tin 
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *:
_read_only_resource_inputs
	
*-
config_proto

CPU

GPU 2J 8В *O
fJRH
F__inference_functional_1_layer_call_and_return_conditional_losses_69932
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*б
_input_shapesП
М:         :         :: ::::::::::::::::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:P L
'
_output_shapes
:         
!
_user_specified_name	input_2:PL
'
_output_shapes
:         
!
_user_specified_name	input_1
╓
{
&__inference_dense_5_layer_call_fn_7938

inputs
unknown
	unknown_0
identityИвStatefulPartitionedCallё
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_5_layer_call_and_return_conditional_losses_65442
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
а	
ъ
__inference_restore_fn_8079
restored_tensors_0
restored_tensors_1	L
Hstring_lookup_index_table_table_restore_lookuptableimportv2_table_handle
identityИв;string_lookup_index_table_table_restore/LookupTableImportV2▐
;string_lookup_index_table_table_restore/LookupTableImportV2LookupTableImportV2Hstring_lookup_index_table_table_restore_lookuptableimportv2_table_handlerestored_tensors_0restored_tensors_1",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*
_output_shapes
 2=
;string_lookup_index_table_table_restore/LookupTableImportV2P
ConstConst*
_output_shapes
: *
dtype0*
value	B :2
ConstП
IdentityIdentityConst:output:0<^string_lookup_index_table_table_restore/LookupTableImportV2*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0**
_input_shapes
:         ::2z
;string_lookup_index_table_table_restore/LookupTableImportV2;string_lookup_index_table_table_restore/LookupTableImportV2:W S
#
_output_shapes
:         
,
_user_specified_namerestored_tensors_0:LH

_output_shapes
:
,
_user_specified_namerestored_tensors_1
П	
з
?__inference_dense_layer_call_and_return_conditional_losses_7816

inputs	"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИ^
CastCastinputs*

DstT0*

SrcT0	*(
_output_shapes
:         ▄2
CastП
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
▄А*
dtype02
MatMul/ReadVariableOpv
MatMulMatMulCast:y:0MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:         А2
MatMulН
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:А*
dtype02
BiasAdd/ReadVariableOpВ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:         А2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:         А2
Relug
IdentityIdentityRelu:activations:0*
T0*(
_output_shapes
:         А2

Identity"
identityIdentity:output:0*/
_input_shapes
:         ▄:::P L
(
_output_shapes
:         ▄
 
_user_specified_nameinputs
╦
к
B__inference_dense_10_layer_call_and_return_conditional_losses_6678

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИН
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2
MatMulМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOpБ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2	
BiasAddd
IdentityIdentityBiasAdd:output:0*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @:::O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
НИ
щ
__inference__wrapped_model_6290
input_2
input_1g
cfunctional_1_text_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handleh
dfunctional_1_text_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value	5
1functional_1_dense_matmul_readvariableop_resource6
2functional_1_dense_biasadd_readvariableop_resource7
3functional_1_dense_1_matmul_readvariableop_resource8
4functional_1_dense_1_biasadd_readvariableop_resource7
3functional_1_dense_2_matmul_readvariableop_resource8
4functional_1_dense_2_biasadd_readvariableop_resource>
:functional_1_normalization_reshape_readvariableop_resource@
<functional_1_normalization_reshape_1_readvariableop_resource7
3functional_1_dense_3_matmul_readvariableop_resource8
4functional_1_dense_3_biasadd_readvariableop_resource7
3functional_1_dense_4_matmul_readvariableop_resource8
4functional_1_dense_4_biasadd_readvariableop_resource7
3functional_1_dense_5_matmul_readvariableop_resource8
4functional_1_dense_5_biasadd_readvariableop_resource7
3functional_1_dense_6_matmul_readvariableop_resource8
4functional_1_dense_6_biasadd_readvariableop_resource7
3functional_1_dense_7_matmul_readvariableop_resource8
4functional_1_dense_7_biasadd_readvariableop_resource7
3functional_1_dense_8_matmul_readvariableop_resource8
4functional_1_dense_8_biasadd_readvariableop_resource7
3functional_1_dense_9_matmul_readvariableop_resource8
4functional_1_dense_9_biasadd_readvariableop_resource8
4functional_1_dense_10_matmul_readvariableop_resource9
5functional_1_dense_10_biasadd_readvariableop_resource
identityИвVfunctional_1/text_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2Ы
+functional_1/text_vectorization/StringLowerStringLowerinput_1*'
_output_shapes
:         2-
+functional_1/text_vectorization/StringLowerд
2functional_1/text_vectorization/StaticRegexReplaceStaticRegexReplace4functional_1/text_vectorization/StringLower:output:0*'
_output_shapes
:         *6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite 24
2functional_1/text_vectorization/StaticRegexReplaceш
'functional_1/text_vectorization/SqueezeSqueeze;functional_1/text_vectorization/StaticRegexReplace:output:0*
T0*#
_output_shapes
:         *
squeeze_dims

         2)
'functional_1/text_vectorization/Squeeze╤
Ffunctional_1/text_vectorization/StringsByteSplit/StringSplit/delimiterConst*
_output_shapes
: *
dtype0*
valueB B 2H
Ffunctional_1/text_vectorization/StringsByteSplit/StringSplit/delimiter▐
<functional_1/text_vectorization/StringsByteSplit/StringSplitStringSplit0functional_1/text_vectorization/Squeeze:output:0Ofunctional_1/text_vectorization/StringsByteSplit/StringSplit/delimiter:output:0*<
_output_shapes*
(:         :         :*

skip_empty( 2>
<functional_1/text_vectorization/StringsByteSplit/StringSplit▌
Dfunctional_1/text_vectorization/StringsByteSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2F
Dfunctional_1/text_vectorization/StringsByteSplit/strided_slice/stackс
Ffunctional_1/text_vectorization/StringsByteSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       2H
Ffunctional_1/text_vectorization/StringsByteSplit/strided_slice/stack_1с
Ffunctional_1/text_vectorization/StringsByteSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2H
Ffunctional_1/text_vectorization/StringsByteSplit/strided_slice/stack_2╛
>functional_1/text_vectorization/StringsByteSplit/strided_sliceStridedSliceFfunctional_1/text_vectorization/StringsByteSplit/StringSplit:indices:0Mfunctional_1/text_vectorization/StringsByteSplit/strided_slice/stack:output:0Ofunctional_1/text_vectorization/StringsByteSplit/strided_slice/stack_1:output:0Ofunctional_1/text_vectorization/StringsByteSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:         *

begin_mask*
end_mask*
shrink_axis_mask2@
>functional_1/text_vectorization/StringsByteSplit/strided_slice┌
Ffunctional_1/text_vectorization/StringsByteSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2H
Ffunctional_1/text_vectorization/StringsByteSplit/strided_slice_1/stack▐
Hfunctional_1/text_vectorization/StringsByteSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2J
Hfunctional_1/text_vectorization/StringsByteSplit/strided_slice_1/stack_1▐
Hfunctional_1/text_vectorization/StringsByteSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2J
Hfunctional_1/text_vectorization/StringsByteSplit/strided_slice_1/stack_2Ч
@functional_1/text_vectorization/StringsByteSplit/strided_slice_1StridedSliceDfunctional_1/text_vectorization/StringsByteSplit/StringSplit:shape:0Ofunctional_1/text_vectorization/StringsByteSplit/strided_slice_1/stack:output:0Qfunctional_1/text_vectorization/StringsByteSplit/strided_slice_1/stack_1:output:0Qfunctional_1/text_vectorization/StringsByteSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask2B
@functional_1/text_vectorization/StringsByteSplit/strided_slice_1р
gfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCastGfunctional_1/text_vectorization/StringsByteSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:         2i
gfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast┘
ifunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1CastIfunctional_1/text_vectorization/StringsByteSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: 2k
ifunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Б
qfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShapekfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:2s
qfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape░
qfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2s
qfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstЕ
pfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdzfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0zfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: 2r
pfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod░
ufunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : 2w
ufunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yС
sfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreateryfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0~functional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: 2u
sfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterХ
pfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastwfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: 2r
pfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast┤
sfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2u
sfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1ї
ofunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMaxkfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0|functional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: 2q
ofunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maxи
qfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :2s
qfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yВ
ofunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2xfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0zfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: 2q
ofunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addї
ofunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMultfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0sfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: 2q
ofunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul·
sfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximummfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0sfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: 2u
sfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum■
sfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimummfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0wfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: 2u
sfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimumн
sfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 2u
sfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2К
tfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincountkfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0wfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0|functional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:         2v
tfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincountв
nfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : 2p
nfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisД
ifunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsum{functional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0wfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:         2k
ifunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum▓
rfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R 2t
rfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0в
nfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2p
nfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisА
ifunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2{functional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0ofunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0wfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:         2k
ifunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat┴
Vfunctional_1/text_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2LookupTableFindV2cfunctional_1_text_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handleEfunctional_1/text_vectorization/StringsByteSplit/StringSplit:values:0dfunctional_1_text_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*#
_output_shapes
:         2X
Vfunctional_1/text_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2а
?functional_1/text_vectorization/string_lookup/assert_equal/NoOpNoOp*
_output_shapes
 2A
?functional_1/text_vectorization/string_lookup/assert_equal/NoOpЛ
6functional_1/text_vectorization/string_lookup/IdentityIdentity_functional_1/text_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:         28
6functional_1/text_vectorization/string_lookup/Identityв
8functional_1/text_vectorization/string_lookup/Identity_1Identityrfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat:output:0*
T0	*#
_output_shapes
:         2:
8functional_1/text_vectorization/string_lookup/Identity_1╛
<functional_1/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R 2>
<functional_1/text_vectorization/RaggedToTensor/default_value╖
4functional_1/text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
: *
dtype0	*
valueB	 R
         26
4functional_1/text_vectorization/RaggedToTensor/Const╦
Cfunctional_1/text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor=functional_1/text_vectorization/RaggedToTensor/Const:output:0?functional_1/text_vectorization/string_lookup/Identity:output:0Efunctional_1/text_vectorization/RaggedToTensor/default_value:output:0Afunctional_1/text_vectorization/string_lookup/Identity_1:output:0*
T0	*
Tindex0	*
Tshape0	*0
_output_shapes
:                  *
num_row_partition_tensors*%
row_partition_types

ROW_SPLITS2E
Cfunctional_1/text_vectorization/RaggedToTensor/RaggedTensorToTensor╩
%functional_1/text_vectorization/ShapeShapeLfunctional_1/text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
T0	*
_output_shapes
:2'
%functional_1/text_vectorization/Shape┤
3functional_1/text_vectorization/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:25
3functional_1/text_vectorization/strided_slice/stack╕
5functional_1/text_vectorization/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:27
5functional_1/text_vectorization/strided_slice/stack_1╕
5functional_1/text_vectorization/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:27
5functional_1/text_vectorization/strided_slice/stack_2в
-functional_1/text_vectorization/strided_sliceStridedSlice.functional_1/text_vectorization/Shape:output:0<functional_1/text_vectorization/strided_slice/stack:output:0>functional_1/text_vectorization/strided_slice/stack_1:output:0>functional_1/text_vectorization/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2/
-functional_1/text_vectorization/strided_sliceС
%functional_1/text_vectorization/sub/xConst*
_output_shapes
: *
dtype0*
value
B :▄2'
%functional_1/text_vectorization/sub/x┌
#functional_1/text_vectorization/subSub.functional_1/text_vectorization/sub/x:output:06functional_1/text_vectorization/strided_slice:output:0*
T0*
_output_shapes
: 2%
#functional_1/text_vectorization/subУ
&functional_1/text_vectorization/Less/yConst*
_output_shapes
: *
dtype0*
value
B :▄2(
&functional_1/text_vectorization/Less/y▐
$functional_1/text_vectorization/LessLess6functional_1/text_vectorization/strided_slice:output:0/functional_1/text_vectorization/Less/y:output:0*
T0*
_output_shapes
: 2&
$functional_1/text_vectorization/Less╘
$functional_1/text_vectorization/condStatelessIf(functional_1/text_vectorization/Less:z:0'functional_1/text_vectorization/sub:z:0Lfunctional_1/text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
Tcond0
*
Tin
2	*
Tout
2	*
_lower_using_switch_merge(*0
_output_shapes
:                  * 
_read_only_resource_inputs
 *B
else_branch3R1
/functional_1_text_vectorization_cond_false_6179*/
output_shapes
:                  *A
then_branch2R0
.functional_1_text_vectorization_cond_true_61782&
$functional_1/text_vectorization/cond╠
-functional_1/text_vectorization/cond/IdentityIdentity-functional_1/text_vectorization/cond:output:0*
T0	*(
_output_shapes
:         ▄2/
-functional_1/text_vectorization/cond/Identity┤
functional_1/dense/CastCast6functional_1/text_vectorization/cond/Identity:output:0*

DstT0*

SrcT0	*(
_output_shapes
:         ▄2
functional_1/dense/Cast╚
(functional_1/dense/MatMul/ReadVariableOpReadVariableOp1functional_1_dense_matmul_readvariableop_resource* 
_output_shapes
:
▄А*
dtype02*
(functional_1/dense/MatMul/ReadVariableOp┬
functional_1/dense/MatMulMatMulfunctional_1/dense/Cast:y:00functional_1/dense/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:         А2
functional_1/dense/MatMul╞
)functional_1/dense/BiasAdd/ReadVariableOpReadVariableOp2functional_1_dense_biasadd_readvariableop_resource*
_output_shapes	
:А*
dtype02+
)functional_1/dense/BiasAdd/ReadVariableOp╬
functional_1/dense/BiasAddBiasAdd#functional_1/dense/MatMul:product:01functional_1/dense/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:         А2
functional_1/dense/BiasAddТ
functional_1/dense/ReluRelu#functional_1/dense/BiasAdd:output:0*
T0*(
_output_shapes
:         А2
functional_1/dense/Relu╬
*functional_1/dense_1/MatMul/ReadVariableOpReadVariableOp3functional_1_dense_1_matmul_readvariableop_resource* 
_output_shapes
:
АА*
dtype02,
*functional_1/dense_1/MatMul/ReadVariableOp╥
functional_1/dense_1/MatMulMatMul%functional_1/dense/Relu:activations:02functional_1/dense_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:         А2
functional_1/dense_1/MatMul╠
+functional_1/dense_1/BiasAdd/ReadVariableOpReadVariableOp4functional_1_dense_1_biasadd_readvariableop_resource*
_output_shapes	
:А*
dtype02-
+functional_1/dense_1/BiasAdd/ReadVariableOp╓
functional_1/dense_1/BiasAddBiasAdd%functional_1/dense_1/MatMul:product:03functional_1/dense_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:         А2
functional_1/dense_1/BiasAddШ
functional_1/dense_1/ReluRelu%functional_1/dense_1/BiasAdd:output:0*
T0*(
_output_shapes
:         А2
functional_1/dense_1/Relu═
*functional_1/dense_2/MatMul/ReadVariableOpReadVariableOp3functional_1_dense_2_matmul_readvariableop_resource*
_output_shapes
:	А@*
dtype02,
*functional_1/dense_2/MatMul/ReadVariableOp╙
functional_1/dense_2/MatMulMatMul'functional_1/dense_1/Relu:activations:02functional_1/dense_2/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_2/MatMul╦
+functional_1/dense_2/BiasAdd/ReadVariableOpReadVariableOp4functional_1_dense_2_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02-
+functional_1/dense_2/BiasAdd/ReadVariableOp╒
functional_1/dense_2/BiasAddBiasAdd%functional_1/dense_2/MatMul:product:03functional_1/dense_2/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_2/BiasAddЧ
functional_1/dense_2/ReluRelu%functional_1/dense_2/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
functional_1/dense_2/Relu▌
1functional_1/normalization/Reshape/ReadVariableOpReadVariableOp:functional_1_normalization_reshape_readvariableop_resource*
_output_shapes
:*
dtype023
1functional_1/normalization/Reshape/ReadVariableOpе
(functional_1/normalization/Reshape/shapeConst*
_output_shapes
:*
dtype0*
valueB"      2*
(functional_1/normalization/Reshape/shapeъ
"functional_1/normalization/ReshapeReshape9functional_1/normalization/Reshape/ReadVariableOp:value:01functional_1/normalization/Reshape/shape:output:0*
T0*
_output_shapes

:2$
"functional_1/normalization/Reshapeу
3functional_1/normalization/Reshape_1/ReadVariableOpReadVariableOp<functional_1_normalization_reshape_1_readvariableop_resource*
_output_shapes
:*
dtype025
3functional_1/normalization/Reshape_1/ReadVariableOpй
*functional_1/normalization/Reshape_1/shapeConst*
_output_shapes
:*
dtype0*
valueB"      2,
*functional_1/normalization/Reshape_1/shapeЄ
$functional_1/normalization/Reshape_1Reshape;functional_1/normalization/Reshape_1/ReadVariableOp:value:03functional_1/normalization/Reshape_1/shape:output:0*
T0*
_output_shapes

:2&
$functional_1/normalization/Reshape_1п
functional_1/normalization/subSubinput_2+functional_1/normalization/Reshape:output:0*
T0*'
_output_shapes
:         2 
functional_1/normalization/subв
functional_1/normalization/SqrtSqrt-functional_1/normalization/Reshape_1:output:0*
T0*
_output_shapes

:2!
functional_1/normalization/Sqrt╬
"functional_1/normalization/truedivRealDiv"functional_1/normalization/sub:z:0#functional_1/normalization/Sqrt:y:0*
T0*'
_output_shapes
:         2$
"functional_1/normalization/truediv╠
*functional_1/dense_3/MatMul/ReadVariableOpReadVariableOp3functional_1_dense_3_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02,
*functional_1/dense_3/MatMul/ReadVariableOp╙
functional_1/dense_3/MatMulMatMul'functional_1/dense_2/Relu:activations:02functional_1/dense_3/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_3/MatMul╦
+functional_1/dense_3/BiasAdd/ReadVariableOpReadVariableOp4functional_1_dense_3_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02-
+functional_1/dense_3/BiasAdd/ReadVariableOp╒
functional_1/dense_3/BiasAddBiasAdd%functional_1/dense_3/MatMul:product:03functional_1/dense_3/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_3/BiasAddЧ
functional_1/dense_3/ReluRelu%functional_1/dense_3/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
functional_1/dense_3/ReluО
$functional_1/concatenate/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2&
$functional_1/concatenate/concat/axisЙ
functional_1/concatenate/concatConcatV2&functional_1/normalization/truediv:z:0'functional_1/dense_3/Relu:activations:0-functional_1/concatenate/concat/axis:output:0*
N*
T0*'
_output_shapes
:         E2!
functional_1/concatenate/concat╠
*functional_1/dense_4/MatMul/ReadVariableOpReadVariableOp3functional_1_dense_4_matmul_readvariableop_resource*
_output_shapes

:E@*
dtype02,
*functional_1/dense_4/MatMul/ReadVariableOp╘
functional_1/dense_4/MatMulMatMul(functional_1/concatenate/concat:output:02functional_1/dense_4/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_4/MatMul╦
+functional_1/dense_4/BiasAdd/ReadVariableOpReadVariableOp4functional_1_dense_4_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02-
+functional_1/dense_4/BiasAdd/ReadVariableOp╒
functional_1/dense_4/BiasAddBiasAdd%functional_1/dense_4/MatMul:product:03functional_1/dense_4/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_4/BiasAddЧ
functional_1/dense_4/ReluRelu%functional_1/dense_4/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
functional_1/dense_4/Relu╠
*functional_1/dense_5/MatMul/ReadVariableOpReadVariableOp3functional_1_dense_5_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02,
*functional_1/dense_5/MatMul/ReadVariableOp╙
functional_1/dense_5/MatMulMatMul'functional_1/dense_4/Relu:activations:02functional_1/dense_5/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_5/MatMul╦
+functional_1/dense_5/BiasAdd/ReadVariableOpReadVariableOp4functional_1_dense_5_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02-
+functional_1/dense_5/BiasAdd/ReadVariableOp╒
functional_1/dense_5/BiasAddBiasAdd%functional_1/dense_5/MatMul:product:03functional_1/dense_5/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_5/BiasAddЧ
functional_1/dense_5/ReluRelu%functional_1/dense_5/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
functional_1/dense_5/Relu╠
*functional_1/dense_6/MatMul/ReadVariableOpReadVariableOp3functional_1_dense_6_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02,
*functional_1/dense_6/MatMul/ReadVariableOp╙
functional_1/dense_6/MatMulMatMul'functional_1/dense_5/Relu:activations:02functional_1/dense_6/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_6/MatMul╦
+functional_1/dense_6/BiasAdd/ReadVariableOpReadVariableOp4functional_1_dense_6_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02-
+functional_1/dense_6/BiasAdd/ReadVariableOp╒
functional_1/dense_6/BiasAddBiasAdd%functional_1/dense_6/MatMul:product:03functional_1/dense_6/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_6/BiasAddЧ
functional_1/dense_6/ReluRelu%functional_1/dense_6/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
functional_1/dense_6/Relu╠
*functional_1/dense_7/MatMul/ReadVariableOpReadVariableOp3functional_1_dense_7_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02,
*functional_1/dense_7/MatMul/ReadVariableOp╙
functional_1/dense_7/MatMulMatMul'functional_1/dense_6/Relu:activations:02functional_1/dense_7/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_7/MatMul╦
+functional_1/dense_7/BiasAdd/ReadVariableOpReadVariableOp4functional_1_dense_7_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02-
+functional_1/dense_7/BiasAdd/ReadVariableOp╒
functional_1/dense_7/BiasAddBiasAdd%functional_1/dense_7/MatMul:product:03functional_1/dense_7/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_7/BiasAddЧ
functional_1/dense_7/ReluRelu%functional_1/dense_7/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
functional_1/dense_7/Relu╠
*functional_1/dense_8/MatMul/ReadVariableOpReadVariableOp3functional_1_dense_8_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02,
*functional_1/dense_8/MatMul/ReadVariableOp╙
functional_1/dense_8/MatMulMatMul'functional_1/dense_7/Relu:activations:02functional_1/dense_8/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_8/MatMul╦
+functional_1/dense_8/BiasAdd/ReadVariableOpReadVariableOp4functional_1_dense_8_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02-
+functional_1/dense_8/BiasAdd/ReadVariableOp╒
functional_1/dense_8/BiasAddBiasAdd%functional_1/dense_8/MatMul:product:03functional_1/dense_8/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_8/BiasAddЧ
functional_1/dense_8/ReluRelu%functional_1/dense_8/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
functional_1/dense_8/Relu╠
*functional_1/dense_9/MatMul/ReadVariableOpReadVariableOp3functional_1_dense_9_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02,
*functional_1/dense_9/MatMul/ReadVariableOp╙
functional_1/dense_9/MatMulMatMul'functional_1/dense_8/Relu:activations:02functional_1/dense_9/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_9/MatMul╦
+functional_1/dense_9/BiasAdd/ReadVariableOpReadVariableOp4functional_1_dense_9_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02-
+functional_1/dense_9/BiasAdd/ReadVariableOp╒
functional_1/dense_9/BiasAddBiasAdd%functional_1/dense_9/MatMul:product:03functional_1/dense_9/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_9/BiasAddЧ
functional_1/dense_9/ReluRelu%functional_1/dense_9/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
functional_1/dense_9/Relu╧
+functional_1/dense_10/MatMul/ReadVariableOpReadVariableOp4functional_1_dense_10_matmul_readvariableop_resource*
_output_shapes

:@*
dtype02-
+functional_1/dense_10/MatMul/ReadVariableOp╓
functional_1/dense_10/MatMulMatMul'functional_1/dense_9/Relu:activations:03functional_1/dense_10/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2
functional_1/dense_10/MatMul╬
,functional_1/dense_10/BiasAdd/ReadVariableOpReadVariableOp5functional_1_dense_10_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02.
,functional_1/dense_10/BiasAdd/ReadVariableOp┘
functional_1/dense_10/BiasAddBiasAdd&functional_1/dense_10/MatMul:product:04functional_1/dense_10/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2
functional_1/dense_10/BiasAdd╙
IdentityIdentity&functional_1/dense_10/BiasAdd:output:0W^functional_1/text_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*б
_input_shapesП
М:         :         :: ::::::::::::::::::::::::2░
Vfunctional_1/text_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2Vfunctional_1/text_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2:P L
'
_output_shapes
:         
!
_user_specified_name	input_2:PL
'
_output_shapes
:         
!
_user_specified_name	input_1
п
й
A__inference_dense_1_layer_call_and_return_conditional_losses_6409

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИП
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
АА*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:         А2
MatMulН
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:А*
dtype02
BiasAdd/ReadVariableOpВ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:         А2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:         А2
Relug
IdentityIdentityRelu:activations:0*
T0*(
_output_shapes
:         А2

Identity"
identityIdentity:output:0*/
_input_shapes
:         А:::P L
(
_output_shapes
:         А
 
_user_specified_nameinputs
║
q
E__inference_concatenate_layer_call_and_return_conditional_losses_7892
inputs_0
inputs_1
identity\
concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concat/axisБ
concatConcatV2inputs_0inputs_1concat/axis:output:0*
N*
T0*'
_output_shapes
:         E2
concatc
IdentityIdentityconcat:output:0*
T0*'
_output_shapes
:         E2

Identity"
identityIdentity:output:0*9
_input_shapes(
&:         :         @:Q M
'
_output_shapes
:         
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:         @
"
_user_specified_name
inputs/1
╫╟
▀	
F__inference_functional_1_layer_call_and_return_conditional_losses_6695
input_2
input_1Z
Vtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handle[
Wtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value	

dense_6393

dense_6395
dense_1_6420
dense_1_6422
dense_2_6447
dense_2_64491
-normalization_reshape_readvariableop_resource3
/normalization_reshape_1_readvariableop_resource
dense_3_6485
dense_3_6487
dense_4_6528
dense_4_6530
dense_5_6555
dense_5_6557
dense_6_6582
dense_6_6584
dense_7_6609
dense_7_6611
dense_8_6636
dense_8_6638
dense_9_6663
dense_9_6665
dense_10_6689
dense_10_6691
identityИвdense/StatefulPartitionedCallвdense_1/StatefulPartitionedCallв dense_10/StatefulPartitionedCallвdense_2/StatefulPartitionedCallвdense_3/StatefulPartitionedCallвdense_4/StatefulPartitionedCallвdense_5/StatefulPartitionedCallвdense_6/StatefulPartitionedCallвdense_7/StatefulPartitionedCallвdense_8/StatefulPartitionedCallвdense_9/StatefulPartitionedCallвItext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2Б
text_vectorization/StringLowerStringLowerinput_1*'
_output_shapes
:         2 
text_vectorization/StringLower¤
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*'
_output_shapes
:         *6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite 2'
%text_vectorization/StaticRegexReplace┴
text_vectorization/SqueezeSqueeze.text_vectorization/StaticRegexReplace:output:0*
T0*#
_output_shapes
:         *
squeeze_dims

         2
text_vectorization/Squeeze╖
9text_vectorization/StringsByteSplit/StringSplit/delimiterConst*
_output_shapes
: *
dtype0*
valueB B 2;
9text_vectorization/StringsByteSplit/StringSplit/delimiterк
/text_vectorization/StringsByteSplit/StringSplitStringSplit#text_vectorization/Squeeze:output:0Btext_vectorization/StringsByteSplit/StringSplit/delimiter:output:0*<
_output_shapes*
(:         :         :*

skip_empty( 21
/text_vectorization/StringsByteSplit/StringSplit├
7text_vectorization/StringsByteSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        29
7text_vectorization/StringsByteSplit/strided_slice/stack╟
9text_vectorization/StringsByteSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       2;
9text_vectorization/StringsByteSplit/strided_slice/stack_1╟
9text_vectorization/StringsByteSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2;
9text_vectorization/StringsByteSplit/strided_slice/stack_2Ё
1text_vectorization/StringsByteSplit/strided_sliceStridedSlice9text_vectorization/StringsByteSplit/StringSplit:indices:0@text_vectorization/StringsByteSplit/strided_slice/stack:output:0Btext_vectorization/StringsByteSplit/strided_slice/stack_1:output:0Btext_vectorization/StringsByteSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:         *

begin_mask*
end_mask*
shrink_axis_mask23
1text_vectorization/StringsByteSplit/strided_slice└
9text_vectorization/StringsByteSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2;
9text_vectorization/StringsByteSplit/strided_slice_1/stack─
;text_vectorization/StringsByteSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2=
;text_vectorization/StringsByteSplit/strided_slice_1/stack_1─
;text_vectorization/StringsByteSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2=
;text_vectorization/StringsByteSplit/strided_slice_1/stack_2╔
3text_vectorization/StringsByteSplit/strided_slice_1StridedSlice7text_vectorization/StringsByteSplit/StringSplit:shape:0Btext_vectorization/StringsByteSplit/strided_slice_1/stack:output:0Dtext_vectorization/StringsByteSplit/strided_slice_1/stack_1:output:0Dtext_vectorization/StringsByteSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask25
3text_vectorization/StringsByteSplit/strided_slice_1╣
Ztext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast:text_vectorization/StringsByteSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:         2\
Ztext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast▓
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast<text_vectorization/StringsByteSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: 2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1┌
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShape^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeЦ
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const╤
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdmtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0mtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: 2e
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdЦ
htext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : 2j
htext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y▌
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreaterltext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0qtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greaterю
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastjtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: 2e
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastЪ
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1┴
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMax^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0otext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxО
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y╬
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ktext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0mtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add┴
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulgtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul╞
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum`text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum╩
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum`text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumУ
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2╔
gtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincount^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0otext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:         2i
gtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountИ
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : 2c
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis╨
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumntext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:         2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumШ
etext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R 2g
etext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0И
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2c
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis┐
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2ntext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:         2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatА
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Vtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handle8text_vectorization/StringsByteSplit/StringSplit:values:0Wtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*#
_output_shapes
:         2K
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2Ж
2text_vectorization/string_lookup/assert_equal/NoOpNoOp*
_output_shapes
 24
2text_vectorization/string_lookup/assert_equal/NoOpф
)text_vectorization/string_lookup/IdentityIdentityRtext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:         2+
)text_vectorization/string_lookup/Identity√
+text_vectorization/string_lookup/Identity_1Identityetext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat:output:0*
T0	*#
_output_shapes
:         2-
+text_vectorization/string_lookup/Identity_1д
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R 21
/text_vectorization/RaggedToTensor/default_valueЭ
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
: *
dtype0	*
valueB	 R
         2)
'text_vectorization/RaggedToTensor/Const¤
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:02text_vectorization/string_lookup/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:04text_vectorization/string_lookup/Identity_1:output:0*
T0	*
Tindex0	*
Tshape0	*0
_output_shapes
:                  *
num_row_partition_tensors*%
row_partition_types

ROW_SPLITS28
6text_vectorization/RaggedToTensor/RaggedTensorToTensorг
text_vectorization/ShapeShape?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
T0	*
_output_shapes
:2
text_vectorization/ShapeЪ
&text_vectorization/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&text_vectorization/strided_slice/stackЮ
(text_vectorization/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(text_vectorization/strided_slice/stack_1Ю
(text_vectorization/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(text_vectorization/strided_slice/stack_2╘
 text_vectorization/strided_sliceStridedSlice!text_vectorization/Shape:output:0/text_vectorization/strided_slice/stack:output:01text_vectorization/strided_slice/stack_1:output:01text_vectorization/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 text_vectorization/strided_slicew
text_vectorization/sub/xConst*
_output_shapes
: *
dtype0*
value
B :▄2
text_vectorization/sub/xж
text_vectorization/subSub!text_vectorization/sub/x:output:0)text_vectorization/strided_slice:output:0*
T0*
_output_shapes
: 2
text_vectorization/suby
text_vectorization/Less/yConst*
_output_shapes
: *
dtype0*
value
B :▄2
text_vectorization/Less/yк
text_vectorization/LessLess)text_vectorization/strided_slice:output:0"text_vectorization/Less/y:output:0*
T0*
_output_shapes
: 2
text_vectorization/Less∙
text_vectorization/condStatelessIftext_vectorization/Less:z:0text_vectorization/sub:z:0?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
Tcond0
*
Tin
2	*
Tout
2	*
_lower_using_switch_merge(*0
_output_shapes
:                  * 
_read_only_resource_inputs
 *5
else_branch&R$
"text_vectorization_cond_false_6350*/
output_shapes
:                  *4
then_branch%R#
!text_vectorization_cond_true_63492
text_vectorization/condе
 text_vectorization/cond/IdentityIdentity text_vectorization/cond:output:0*
T0	*(
_output_shapes
:         ▄2"
 text_vectorization/cond/Identityг
dense/StatefulPartitionedCallStatefulPartitionedCall)text_vectorization/cond/Identity:output:0
dense_6393
dense_6395*
Tin
2	*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:         А*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *H
fCRA
?__inference_dense_layer_call_and_return_conditional_losses_63822
dense/StatefulPartitionedCallк
dense_1/StatefulPartitionedCallStatefulPartitionedCall&dense/StatefulPartitionedCall:output:0dense_1_6420dense_1_6422*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:         А*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_1_layer_call_and_return_conditional_losses_64092!
dense_1/StatefulPartitionedCallл
dense_2/StatefulPartitionedCallStatefulPartitionedCall(dense_1/StatefulPartitionedCall:output:0dense_2_6447dense_2_6449*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_2_layer_call_and_return_conditional_losses_64362!
dense_2/StatefulPartitionedCall╢
$normalization/Reshape/ReadVariableOpReadVariableOp-normalization_reshape_readvariableop_resource*
_output_shapes
:*
dtype02&
$normalization/Reshape/ReadVariableOpЛ
normalization/Reshape/shapeConst*
_output_shapes
:*
dtype0*
valueB"      2
normalization/Reshape/shape╢
normalization/ReshapeReshape,normalization/Reshape/ReadVariableOp:value:0$normalization/Reshape/shape:output:0*
T0*
_output_shapes

:2
normalization/Reshape╝
&normalization/Reshape_1/ReadVariableOpReadVariableOp/normalization_reshape_1_readvariableop_resource*
_output_shapes
:*
dtype02(
&normalization/Reshape_1/ReadVariableOpП
normalization/Reshape_1/shapeConst*
_output_shapes
:*
dtype0*
valueB"      2
normalization/Reshape_1/shape╛
normalization/Reshape_1Reshape.normalization/Reshape_1/ReadVariableOp:value:0&normalization/Reshape_1/shape:output:0*
T0*
_output_shapes

:2
normalization/Reshape_1И
normalization/subSubinput_2normalization/Reshape:output:0*
T0*'
_output_shapes
:         2
normalization/sub{
normalization/SqrtSqrt normalization/Reshape_1:output:0*
T0*
_output_shapes

:2
normalization/SqrtЪ
normalization/truedivRealDivnormalization/sub:z:0normalization/Sqrt:y:0*
T0*'
_output_shapes
:         2
normalization/truedivл
dense_3/StatefulPartitionedCallStatefulPartitionedCall(dense_2/StatefulPartitionedCall:output:0dense_3_6485dense_3_6487*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_3_layer_call_and_return_conditional_losses_64742!
dense_3/StatefulPartitionedCallЩ
concatenate/PartitionedCallPartitionedCallnormalization/truediv:z:0(dense_3/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         E* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8В *N
fIRG
E__inference_concatenate_layer_call_and_return_conditional_losses_64972
concatenate/PartitionedCallз
dense_4/StatefulPartitionedCallStatefulPartitionedCall$concatenate/PartitionedCall:output:0dense_4_6528dense_4_6530*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_4_layer_call_and_return_conditional_losses_65172!
dense_4/StatefulPartitionedCallл
dense_5/StatefulPartitionedCallStatefulPartitionedCall(dense_4/StatefulPartitionedCall:output:0dense_5_6555dense_5_6557*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_5_layer_call_and_return_conditional_losses_65442!
dense_5/StatefulPartitionedCallл
dense_6/StatefulPartitionedCallStatefulPartitionedCall(dense_5/StatefulPartitionedCall:output:0dense_6_6582dense_6_6584*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_6_layer_call_and_return_conditional_losses_65712!
dense_6/StatefulPartitionedCallл
dense_7/StatefulPartitionedCallStatefulPartitionedCall(dense_6/StatefulPartitionedCall:output:0dense_7_6609dense_7_6611*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_7_layer_call_and_return_conditional_losses_65982!
dense_7/StatefulPartitionedCallл
dense_8/StatefulPartitionedCallStatefulPartitionedCall(dense_7/StatefulPartitionedCall:output:0dense_8_6636dense_8_6638*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_8_layer_call_and_return_conditional_losses_66252!
dense_8/StatefulPartitionedCallл
dense_9/StatefulPartitionedCallStatefulPartitionedCall(dense_8/StatefulPartitionedCall:output:0dense_9_6663dense_9_6665*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_9_layer_call_and_return_conditional_losses_66522!
dense_9/StatefulPartitionedCall░
 dense_10/StatefulPartitionedCallStatefulPartitionedCall(dense_9/StatefulPartitionedCall:output:0dense_10_6689dense_10_6691*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *K
fFRD
B__inference_dense_10_layer_call_and_return_conditional_losses_66782"
 dense_10/StatefulPartitionedCall╛
IdentityIdentity)dense_10/StatefulPartitionedCall:output:0^dense/StatefulPartitionedCall ^dense_1/StatefulPartitionedCall!^dense_10/StatefulPartitionedCall ^dense_2/StatefulPartitionedCall ^dense_3/StatefulPartitionedCall ^dense_4/StatefulPartitionedCall ^dense_5/StatefulPartitionedCall ^dense_6/StatefulPartitionedCall ^dense_7/StatefulPartitionedCall ^dense_8/StatefulPartitionedCall ^dense_9/StatefulPartitionedCallJ^text_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*б
_input_shapesП
М:         :         :: ::::::::::::::::::::::::2>
dense/StatefulPartitionedCalldense/StatefulPartitionedCall2B
dense_1/StatefulPartitionedCalldense_1/StatefulPartitionedCall2D
 dense_10/StatefulPartitionedCall dense_10/StatefulPartitionedCall2B
dense_2/StatefulPartitionedCalldense_2/StatefulPartitionedCall2B
dense_3/StatefulPartitionedCalldense_3/StatefulPartitionedCall2B
dense_4/StatefulPartitionedCalldense_4/StatefulPartitionedCall2B
dense_5/StatefulPartitionedCalldense_5/StatefulPartitionedCall2B
dense_6/StatefulPartitionedCalldense_6/StatefulPartitionedCall2B
dense_7/StatefulPartitionedCalldense_7/StatefulPartitionedCall2B
dense_8/StatefulPartitionedCalldense_8/StatefulPartitionedCall2B
dense_9/StatefulPartitionedCalldense_9/StatefulPartitionedCall2Ц
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2:P L
'
_output_shapes
:         
!
_user_specified_name	input_2:PL
'
_output_shapes
:         
!
_user_specified_name	input_1
═
№
"__inference_signature_wrapper_7321
input_1
input_2
unknown
	unknown_0	
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
	unknown_7
	unknown_8
	unknown_9

unknown_10

unknown_11

unknown_12

unknown_13

unknown_14

unknown_15

unknown_16

unknown_17

unknown_18

unknown_19

unknown_20

unknown_21

unknown_22

unknown_23

unknown_24
identityИвStatefulPartitionedCallЯ
StatefulPartitionedCallStatefulPartitionedCallinput_2input_1unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24*'
Tin 
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *:
_read_only_resource_inputs
	
*-
config_proto

CPU

GPU 2J 8В *(
f#R!
__inference__wrapped_model_62902
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*б
_input_shapesП
М:         :         :: ::::::::::::::::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:P L
'
_output_shapes
:         
!
_user_specified_name	input_1:PL
'
_output_shapes
:         
!
_user_specified_name	input_2
Г
З
+__inference_functional_1_layer_call_fn_7746
inputs_0
inputs_1
unknown
	unknown_0	
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
	unknown_7
	unknown_8
	unknown_9

unknown_10

unknown_11

unknown_12

unknown_13

unknown_14

unknown_15

unknown_16

unknown_17

unknown_18

unknown_19

unknown_20

unknown_21

unknown_22

unknown_23

unknown_24
identityИвStatefulPartitionedCall╚
StatefulPartitionedCallStatefulPartitionedCallinputs_0inputs_1unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24*'
Tin 
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *:
_read_only_resource_inputs
	
*-
config_proto

CPU

GPU 2J 8В *O
fJRH
F__inference_functional_1_layer_call_and_return_conditional_losses_69932
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*б
_input_shapesП
М:         :         :: ::::::::::::::::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:Q M
'
_output_shapes
:         
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:         
"
_user_specified_name
inputs/1
Ч
▓
.functional_1_text_vectorization_cond_true_6178[
Wfunctional_1_text_vectorization_cond_pad_paddings_1_functional_1_text_vectorization_subp
lfunctional_1_text_vectorization_cond_pad_functional_1_text_vectorization_raggedtotensor_raggedtensortotensor	1
-functional_1_text_vectorization_cond_identity	░
5functional_1/text_vectorization/cond/Pad/paddings/1/0Const*
_output_shapes
: *
dtype0*
value	B : 27
5functional_1/text_vectorization/cond/Pad/paddings/1/0╣
3functional_1/text_vectorization/cond/Pad/paddings/1Pack>functional_1/text_vectorization/cond/Pad/paddings/1/0:output:0Wfunctional_1_text_vectorization_cond_pad_paddings_1_functional_1_text_vectorization_sub*
N*
T0*
_output_shapes
:25
3functional_1/text_vectorization/cond/Pad/paddings/1┐
5functional_1/text_vectorization/cond/Pad/paddings/0_1Const*
_output_shapes
:*
dtype0*
valueB"        27
5functional_1/text_vectorization/cond/Pad/paddings/0_1Ю
1functional_1/text_vectorization/cond/Pad/paddingsPack>functional_1/text_vectorization/cond/Pad/paddings/0_1:output:0<functional_1/text_vectorization/cond/Pad/paddings/1:output:0*
N*
T0*
_output_shapes

:23
1functional_1/text_vectorization/cond/Pad/paddings└
(functional_1/text_vectorization/cond/PadPadlfunctional_1_text_vectorization_cond_pad_functional_1_text_vectorization_raggedtotensor_raggedtensortotensor:functional_1/text_vectorization/cond/Pad/paddings:output:0*
T0	*0
_output_shapes
:                  2*
(functional_1/text_vectorization/cond/Pad╪
-functional_1/text_vectorization/cond/IdentityIdentity1functional_1/text_vectorization/cond/Pad:output:0*
T0	*0
_output_shapes
:                  2/
-functional_1/text_vectorization/cond/Identity"g
-functional_1_text_vectorization_cond_identity6functional_1/text_vectorization/cond/Identity:output:0*1
_input_shapes 
: :                  : 

_output_shapes
: :62
0
_output_shapes
:                  
·
Ц
/functional_1_text_vectorization_cond_false_61794
0functional_1_text_vectorization_cond_placeholderz
vfunctional_1_text_vectorization_cond_strided_slice_functional_1_text_vectorization_raggedtotensor_raggedtensortotensor	1
-functional_1_text_vectorization_cond_identity	┼
8functional_1/text_vectorization/cond/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2:
8functional_1/text_vectorization/cond/strided_slice/stack╔
:functional_1/text_vectorization/cond/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ▄  2<
:functional_1/text_vectorization/cond/strided_slice/stack_1╔
:functional_1/text_vectorization/cond/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2<
:functional_1/text_vectorization/cond/strided_slice/stack_2з
2functional_1/text_vectorization/cond/strided_sliceStridedSlicevfunctional_1_text_vectorization_cond_strided_slice_functional_1_text_vectorization_raggedtotensor_raggedtensortotensorAfunctional_1/text_vectorization/cond/strided_slice/stack:output:0Cfunctional_1/text_vectorization/cond/strided_slice/stack_1:output:0Cfunctional_1/text_vectorization/cond/strided_slice/stack_2:output:0*
Index0*
T0	*0
_output_shapes
:                  *

begin_mask*
end_mask24
2functional_1/text_vectorization/cond/strided_sliceт
-functional_1/text_vectorization/cond/IdentityIdentity;functional_1/text_vectorization/cond/strided_slice:output:0*
T0	*0
_output_shapes
:                  2/
-functional_1/text_vectorization/cond/Identity"g
-functional_1_text_vectorization_cond_identity6functional_1/text_vectorization/cond/Identity:output:0*1
_input_shapes 
: :                  : 

_output_shapes
: :62
0
_output_shapes
:                  
Ю
ф
!text_vectorization_cond_true_7406A
=text_vectorization_cond_pad_paddings_1_text_vectorization_subV
Rtext_vectorization_cond_pad_text_vectorization_raggedtotensor_raggedtensortotensor	$
 text_vectorization_cond_identity	Ц
(text_vectorization/cond/Pad/paddings/1/0Const*
_output_shapes
: *
dtype0*
value	B : 2*
(text_vectorization/cond/Pad/paddings/1/0°
&text_vectorization/cond/Pad/paddings/1Pack1text_vectorization/cond/Pad/paddings/1/0:output:0=text_vectorization_cond_pad_paddings_1_text_vectorization_sub*
N*
T0*
_output_shapes
:2(
&text_vectorization/cond/Pad/paddings/1е
(text_vectorization/cond/Pad/paddings/0_1Const*
_output_shapes
:*
dtype0*
valueB"        2*
(text_vectorization/cond/Pad/paddings/0_1ъ
$text_vectorization/cond/Pad/paddingsPack1text_vectorization/cond/Pad/paddings/0_1:output:0/text_vectorization/cond/Pad/paddings/1:output:0*
N*
T0*
_output_shapes

:2&
$text_vectorization/cond/Pad/paddings 
text_vectorization/cond/PadPadRtext_vectorization_cond_pad_text_vectorization_raggedtotensor_raggedtensortotensor-text_vectorization/cond/Pad/paddings:output:0*
T0	*0
_output_shapes
:                  2
text_vectorization/cond/Pad▒
 text_vectorization/cond/IdentityIdentity$text_vectorization/cond/Pad:output:0*
T0	*0
_output_shapes
:                  2"
 text_vectorization/cond/Identity"M
 text_vectorization_cond_identity)text_vectorization/cond/Identity:output:0*1
_input_shapes 
: :                  : 

_output_shapes
: :62
0
_output_shapes
:                  
╦▌
│
F__inference_functional_1_layer_call_and_return_conditional_losses_7518
inputs_0
inputs_1Z
Vtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handle[
Wtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value	(
$dense_matmul_readvariableop_resource)
%dense_biasadd_readvariableop_resource*
&dense_1_matmul_readvariableop_resource+
'dense_1_biasadd_readvariableop_resource*
&dense_2_matmul_readvariableop_resource+
'dense_2_biasadd_readvariableop_resource1
-normalization_reshape_readvariableop_resource3
/normalization_reshape_1_readvariableop_resource*
&dense_3_matmul_readvariableop_resource+
'dense_3_biasadd_readvariableop_resource*
&dense_4_matmul_readvariableop_resource+
'dense_4_biasadd_readvariableop_resource*
&dense_5_matmul_readvariableop_resource+
'dense_5_biasadd_readvariableop_resource*
&dense_6_matmul_readvariableop_resource+
'dense_6_biasadd_readvariableop_resource*
&dense_7_matmul_readvariableop_resource+
'dense_7_biasadd_readvariableop_resource*
&dense_8_matmul_readvariableop_resource+
'dense_8_biasadd_readvariableop_resource*
&dense_9_matmul_readvariableop_resource+
'dense_9_biasadd_readvariableop_resource+
'dense_10_matmul_readvariableop_resource,
(dense_10_biasadd_readvariableop_resource
identityИвItext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2В
text_vectorization/StringLowerStringLowerinputs_1*'
_output_shapes
:         2 
text_vectorization/StringLower¤
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*'
_output_shapes
:         *6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite 2'
%text_vectorization/StaticRegexReplace┴
text_vectorization/SqueezeSqueeze.text_vectorization/StaticRegexReplace:output:0*
T0*#
_output_shapes
:         *
squeeze_dims

         2
text_vectorization/Squeeze╖
9text_vectorization/StringsByteSplit/StringSplit/delimiterConst*
_output_shapes
: *
dtype0*
valueB B 2;
9text_vectorization/StringsByteSplit/StringSplit/delimiterк
/text_vectorization/StringsByteSplit/StringSplitStringSplit#text_vectorization/Squeeze:output:0Btext_vectorization/StringsByteSplit/StringSplit/delimiter:output:0*<
_output_shapes*
(:         :         :*

skip_empty( 21
/text_vectorization/StringsByteSplit/StringSplit├
7text_vectorization/StringsByteSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        29
7text_vectorization/StringsByteSplit/strided_slice/stack╟
9text_vectorization/StringsByteSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       2;
9text_vectorization/StringsByteSplit/strided_slice/stack_1╟
9text_vectorization/StringsByteSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2;
9text_vectorization/StringsByteSplit/strided_slice/stack_2Ё
1text_vectorization/StringsByteSplit/strided_sliceStridedSlice9text_vectorization/StringsByteSplit/StringSplit:indices:0@text_vectorization/StringsByteSplit/strided_slice/stack:output:0Btext_vectorization/StringsByteSplit/strided_slice/stack_1:output:0Btext_vectorization/StringsByteSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:         *

begin_mask*
end_mask*
shrink_axis_mask23
1text_vectorization/StringsByteSplit/strided_slice└
9text_vectorization/StringsByteSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2;
9text_vectorization/StringsByteSplit/strided_slice_1/stack─
;text_vectorization/StringsByteSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2=
;text_vectorization/StringsByteSplit/strided_slice_1/stack_1─
;text_vectorization/StringsByteSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2=
;text_vectorization/StringsByteSplit/strided_slice_1/stack_2╔
3text_vectorization/StringsByteSplit/strided_slice_1StridedSlice7text_vectorization/StringsByteSplit/StringSplit:shape:0Btext_vectorization/StringsByteSplit/strided_slice_1/stack:output:0Dtext_vectorization/StringsByteSplit/strided_slice_1/stack_1:output:0Dtext_vectorization/StringsByteSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask25
3text_vectorization/StringsByteSplit/strided_slice_1╣
Ztext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast:text_vectorization/StringsByteSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:         2\
Ztext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast▓
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast<text_vectorization/StringsByteSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: 2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1┌
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShape^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeЦ
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const╤
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdmtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0mtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: 2e
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdЦ
htext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : 2j
htext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y▌
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreaterltext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0qtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greaterю
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastjtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: 2e
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastЪ
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1┴
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMax^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0otext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxО
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y╬
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ktext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0mtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add┴
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulgtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul╞
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum`text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum╩
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum`text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumУ
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2╔
gtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincount^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0otext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:         2i
gtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountИ
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : 2c
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis╨
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumntext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:         2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumШ
etext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R 2g
etext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0И
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2c
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis┐
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2ntext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:         2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatА
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Vtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handle8text_vectorization/StringsByteSplit/StringSplit:values:0Wtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*#
_output_shapes
:         2K
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2Ж
2text_vectorization/string_lookup/assert_equal/NoOpNoOp*
_output_shapes
 24
2text_vectorization/string_lookup/assert_equal/NoOpф
)text_vectorization/string_lookup/IdentityIdentityRtext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:         2+
)text_vectorization/string_lookup/Identity√
+text_vectorization/string_lookup/Identity_1Identityetext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat:output:0*
T0	*#
_output_shapes
:         2-
+text_vectorization/string_lookup/Identity_1д
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R 21
/text_vectorization/RaggedToTensor/default_valueЭ
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
: *
dtype0	*
valueB	 R
         2)
'text_vectorization/RaggedToTensor/Const¤
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:02text_vectorization/string_lookup/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:04text_vectorization/string_lookup/Identity_1:output:0*
T0	*
Tindex0	*
Tshape0	*0
_output_shapes
:                  *
num_row_partition_tensors*%
row_partition_types

ROW_SPLITS28
6text_vectorization/RaggedToTensor/RaggedTensorToTensorг
text_vectorization/ShapeShape?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
T0	*
_output_shapes
:2
text_vectorization/ShapeЪ
&text_vectorization/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&text_vectorization/strided_slice/stackЮ
(text_vectorization/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(text_vectorization/strided_slice/stack_1Ю
(text_vectorization/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(text_vectorization/strided_slice/stack_2╘
 text_vectorization/strided_sliceStridedSlice!text_vectorization/Shape:output:0/text_vectorization/strided_slice/stack:output:01text_vectorization/strided_slice/stack_1:output:01text_vectorization/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 text_vectorization/strided_slicew
text_vectorization/sub/xConst*
_output_shapes
: *
dtype0*
value
B :▄2
text_vectorization/sub/xж
text_vectorization/subSub!text_vectorization/sub/x:output:0)text_vectorization/strided_slice:output:0*
T0*
_output_shapes
: 2
text_vectorization/suby
text_vectorization/Less/yConst*
_output_shapes
: *
dtype0*
value
B :▄2
text_vectorization/Less/yк
text_vectorization/LessLess)text_vectorization/strided_slice:output:0"text_vectorization/Less/y:output:0*
T0*
_output_shapes
: 2
text_vectorization/Less∙
text_vectorization/condStatelessIftext_vectorization/Less:z:0text_vectorization/sub:z:0?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
Tcond0
*
Tin
2	*
Tout
2	*
_lower_using_switch_merge(*0
_output_shapes
:                  * 
_read_only_resource_inputs
 *5
else_branch&R$
"text_vectorization_cond_false_7407*/
output_shapes
:                  *4
then_branch%R#
!text_vectorization_cond_true_74062
text_vectorization/condе
 text_vectorization/cond/IdentityIdentity text_vectorization/cond:output:0*
T0	*(
_output_shapes
:         ▄2"
 text_vectorization/cond/IdentityН

dense/CastCast)text_vectorization/cond/Identity:output:0*

DstT0*

SrcT0	*(
_output_shapes
:         ▄2

dense/Castб
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource* 
_output_shapes
:
▄А*
dtype02
dense/MatMul/ReadVariableOpО
dense/MatMulMatMuldense/Cast:y:0#dense/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:         А2
dense/MatMulЯ
dense/BiasAdd/ReadVariableOpReadVariableOp%dense_biasadd_readvariableop_resource*
_output_shapes	
:А*
dtype02
dense/BiasAdd/ReadVariableOpЪ
dense/BiasAddBiasAdddense/MatMul:product:0$dense/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:         А2
dense/BiasAddk

dense/ReluReludense/BiasAdd:output:0*
T0*(
_output_shapes
:         А2

dense/Reluз
dense_1/MatMul/ReadVariableOpReadVariableOp&dense_1_matmul_readvariableop_resource* 
_output_shapes
:
АА*
dtype02
dense_1/MatMul/ReadVariableOpЮ
dense_1/MatMulMatMuldense/Relu:activations:0%dense_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:         А2
dense_1/MatMulе
dense_1/BiasAdd/ReadVariableOpReadVariableOp'dense_1_biasadd_readvariableop_resource*
_output_shapes	
:А*
dtype02 
dense_1/BiasAdd/ReadVariableOpв
dense_1/BiasAddBiasAdddense_1/MatMul:product:0&dense_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:         А2
dense_1/BiasAddq
dense_1/ReluReludense_1/BiasAdd:output:0*
T0*(
_output_shapes
:         А2
dense_1/Reluж
dense_2/MatMul/ReadVariableOpReadVariableOp&dense_2_matmul_readvariableop_resource*
_output_shapes
:	А@*
dtype02
dense_2/MatMul/ReadVariableOpЯ
dense_2/MatMulMatMuldense_1/Relu:activations:0%dense_2/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_2/MatMulд
dense_2/BiasAdd/ReadVariableOpReadVariableOp'dense_2_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_2/BiasAdd/ReadVariableOpб
dense_2/BiasAddBiasAdddense_2/MatMul:product:0&dense_2/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_2/BiasAddp
dense_2/ReluReludense_2/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_2/Relu╢
$normalization/Reshape/ReadVariableOpReadVariableOp-normalization_reshape_readvariableop_resource*
_output_shapes
:*
dtype02&
$normalization/Reshape/ReadVariableOpЛ
normalization/Reshape/shapeConst*
_output_shapes
:*
dtype0*
valueB"      2
normalization/Reshape/shape╢
normalization/ReshapeReshape,normalization/Reshape/ReadVariableOp:value:0$normalization/Reshape/shape:output:0*
T0*
_output_shapes

:2
normalization/Reshape╝
&normalization/Reshape_1/ReadVariableOpReadVariableOp/normalization_reshape_1_readvariableop_resource*
_output_shapes
:*
dtype02(
&normalization/Reshape_1/ReadVariableOpП
normalization/Reshape_1/shapeConst*
_output_shapes
:*
dtype0*
valueB"      2
normalization/Reshape_1/shape╛
normalization/Reshape_1Reshape.normalization/Reshape_1/ReadVariableOp:value:0&normalization/Reshape_1/shape:output:0*
T0*
_output_shapes

:2
normalization/Reshape_1Й
normalization/subSubinputs_0normalization/Reshape:output:0*
T0*'
_output_shapes
:         2
normalization/sub{
normalization/SqrtSqrt normalization/Reshape_1:output:0*
T0*
_output_shapes

:2
normalization/SqrtЪ
normalization/truedivRealDivnormalization/sub:z:0normalization/Sqrt:y:0*
T0*'
_output_shapes
:         2
normalization/truedivе
dense_3/MatMul/ReadVariableOpReadVariableOp&dense_3_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
dense_3/MatMul/ReadVariableOpЯ
dense_3/MatMulMatMuldense_2/Relu:activations:0%dense_3/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_3/MatMulд
dense_3/BiasAdd/ReadVariableOpReadVariableOp'dense_3_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_3/BiasAdd/ReadVariableOpб
dense_3/BiasAddBiasAdddense_3/MatMul:product:0&dense_3/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_3/BiasAddp
dense_3/ReluReludense_3/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_3/Relut
concatenate/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concatenate/concat/axis╚
concatenate/concatConcatV2normalization/truediv:z:0dense_3/Relu:activations:0 concatenate/concat/axis:output:0*
N*
T0*'
_output_shapes
:         E2
concatenate/concatе
dense_4/MatMul/ReadVariableOpReadVariableOp&dense_4_matmul_readvariableop_resource*
_output_shapes

:E@*
dtype02
dense_4/MatMul/ReadVariableOpа
dense_4/MatMulMatMulconcatenate/concat:output:0%dense_4/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_4/MatMulд
dense_4/BiasAdd/ReadVariableOpReadVariableOp'dense_4_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_4/BiasAdd/ReadVariableOpб
dense_4/BiasAddBiasAdddense_4/MatMul:product:0&dense_4/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_4/BiasAddp
dense_4/ReluReludense_4/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_4/Reluе
dense_5/MatMul/ReadVariableOpReadVariableOp&dense_5_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
dense_5/MatMul/ReadVariableOpЯ
dense_5/MatMulMatMuldense_4/Relu:activations:0%dense_5/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_5/MatMulд
dense_5/BiasAdd/ReadVariableOpReadVariableOp'dense_5_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_5/BiasAdd/ReadVariableOpб
dense_5/BiasAddBiasAdddense_5/MatMul:product:0&dense_5/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_5/BiasAddp
dense_5/ReluReludense_5/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_5/Reluе
dense_6/MatMul/ReadVariableOpReadVariableOp&dense_6_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
dense_6/MatMul/ReadVariableOpЯ
dense_6/MatMulMatMuldense_5/Relu:activations:0%dense_6/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_6/MatMulд
dense_6/BiasAdd/ReadVariableOpReadVariableOp'dense_6_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_6/BiasAdd/ReadVariableOpб
dense_6/BiasAddBiasAdddense_6/MatMul:product:0&dense_6/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_6/BiasAddp
dense_6/ReluReludense_6/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_6/Reluе
dense_7/MatMul/ReadVariableOpReadVariableOp&dense_7_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
dense_7/MatMul/ReadVariableOpЯ
dense_7/MatMulMatMuldense_6/Relu:activations:0%dense_7/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_7/MatMulд
dense_7/BiasAdd/ReadVariableOpReadVariableOp'dense_7_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_7/BiasAdd/ReadVariableOpб
dense_7/BiasAddBiasAdddense_7/MatMul:product:0&dense_7/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_7/BiasAddp
dense_7/ReluReludense_7/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_7/Reluе
dense_8/MatMul/ReadVariableOpReadVariableOp&dense_8_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
dense_8/MatMul/ReadVariableOpЯ
dense_8/MatMulMatMuldense_7/Relu:activations:0%dense_8/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_8/MatMulд
dense_8/BiasAdd/ReadVariableOpReadVariableOp'dense_8_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_8/BiasAdd/ReadVariableOpб
dense_8/BiasAddBiasAdddense_8/MatMul:product:0&dense_8/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_8/BiasAddp
dense_8/ReluReludense_8/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_8/Reluе
dense_9/MatMul/ReadVariableOpReadVariableOp&dense_9_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
dense_9/MatMul/ReadVariableOpЯ
dense_9/MatMulMatMuldense_8/Relu:activations:0%dense_9/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_9/MatMulд
dense_9/BiasAdd/ReadVariableOpReadVariableOp'dense_9_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_9/BiasAdd/ReadVariableOpб
dense_9/BiasAddBiasAdddense_9/MatMul:product:0&dense_9/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_9/BiasAddp
dense_9/ReluReludense_9/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_9/Reluи
dense_10/MatMul/ReadVariableOpReadVariableOp'dense_10_matmul_readvariableop_resource*
_output_shapes

:@*
dtype02 
dense_10/MatMul/ReadVariableOpв
dense_10/MatMulMatMuldense_9/Relu:activations:0&dense_10/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2
dense_10/MatMulз
dense_10/BiasAdd/ReadVariableOpReadVariableOp(dense_10_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02!
dense_10/BiasAdd/ReadVariableOpе
dense_10/BiasAddBiasAdddense_10/MatMul:product:0'dense_10/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2
dense_10/BiasAdd╣
IdentityIdentitydense_10/BiasAdd:output:0J^text_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*б
_input_shapesП
М:         :         :: ::::::::::::::::::::::::2Ц
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2:Q M
'
_output_shapes
:         
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:         
"
_user_specified_name
inputs/1
Ю
ф
!text_vectorization_cond_true_6904A
=text_vectorization_cond_pad_paddings_1_text_vectorization_subV
Rtext_vectorization_cond_pad_text_vectorization_raggedtotensor_raggedtensortotensor	$
 text_vectorization_cond_identity	Ц
(text_vectorization/cond/Pad/paddings/1/0Const*
_output_shapes
: *
dtype0*
value	B : 2*
(text_vectorization/cond/Pad/paddings/1/0°
&text_vectorization/cond/Pad/paddings/1Pack1text_vectorization/cond/Pad/paddings/1/0:output:0=text_vectorization_cond_pad_paddings_1_text_vectorization_sub*
N*
T0*
_output_shapes
:2(
&text_vectorization/cond/Pad/paddings/1е
(text_vectorization/cond/Pad/paddings/0_1Const*
_output_shapes
:*
dtype0*
valueB"        2*
(text_vectorization/cond/Pad/paddings/0_1ъ
$text_vectorization/cond/Pad/paddingsPack1text_vectorization/cond/Pad/paddings/0_1:output:0/text_vectorization/cond/Pad/paddings/1:output:0*
N*
T0*
_output_shapes

:2&
$text_vectorization/cond/Pad/paddings 
text_vectorization/cond/PadPadRtext_vectorization_cond_pad_text_vectorization_raggedtotensor_raggedtensortotensor-text_vectorization/cond/Pad/paddings:output:0*
T0	*0
_output_shapes
:                  2
text_vectorization/cond/Pad▒
 text_vectorization/cond/IdentityIdentity$text_vectorization/cond/Pad:output:0*
T0	*0
_output_shapes
:                  2"
 text_vectorization/cond/Identity"M
 text_vectorization_cond_identity)text_vectorization/cond/Identity:output:0*1
_input_shapes 
: :                  : 

_output_shapes
: :62
0
_output_shapes
:                  
╦▌
│
F__inference_functional_1_layer_call_and_return_conditional_losses_7688
inputs_0
inputs_1Z
Vtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handle[
Wtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value	(
$dense_matmul_readvariableop_resource)
%dense_biasadd_readvariableop_resource*
&dense_1_matmul_readvariableop_resource+
'dense_1_biasadd_readvariableop_resource*
&dense_2_matmul_readvariableop_resource+
'dense_2_biasadd_readvariableop_resource1
-normalization_reshape_readvariableop_resource3
/normalization_reshape_1_readvariableop_resource*
&dense_3_matmul_readvariableop_resource+
'dense_3_biasadd_readvariableop_resource*
&dense_4_matmul_readvariableop_resource+
'dense_4_biasadd_readvariableop_resource*
&dense_5_matmul_readvariableop_resource+
'dense_5_biasadd_readvariableop_resource*
&dense_6_matmul_readvariableop_resource+
'dense_6_biasadd_readvariableop_resource*
&dense_7_matmul_readvariableop_resource+
'dense_7_biasadd_readvariableop_resource*
&dense_8_matmul_readvariableop_resource+
'dense_8_biasadd_readvariableop_resource*
&dense_9_matmul_readvariableop_resource+
'dense_9_biasadd_readvariableop_resource+
'dense_10_matmul_readvariableop_resource,
(dense_10_biasadd_readvariableop_resource
identityИвItext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2В
text_vectorization/StringLowerStringLowerinputs_1*'
_output_shapes
:         2 
text_vectorization/StringLower¤
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*'
_output_shapes
:         *6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite 2'
%text_vectorization/StaticRegexReplace┴
text_vectorization/SqueezeSqueeze.text_vectorization/StaticRegexReplace:output:0*
T0*#
_output_shapes
:         *
squeeze_dims

         2
text_vectorization/Squeeze╖
9text_vectorization/StringsByteSplit/StringSplit/delimiterConst*
_output_shapes
: *
dtype0*
valueB B 2;
9text_vectorization/StringsByteSplit/StringSplit/delimiterк
/text_vectorization/StringsByteSplit/StringSplitStringSplit#text_vectorization/Squeeze:output:0Btext_vectorization/StringsByteSplit/StringSplit/delimiter:output:0*<
_output_shapes*
(:         :         :*

skip_empty( 21
/text_vectorization/StringsByteSplit/StringSplit├
7text_vectorization/StringsByteSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        29
7text_vectorization/StringsByteSplit/strided_slice/stack╟
9text_vectorization/StringsByteSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       2;
9text_vectorization/StringsByteSplit/strided_slice/stack_1╟
9text_vectorization/StringsByteSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2;
9text_vectorization/StringsByteSplit/strided_slice/stack_2Ё
1text_vectorization/StringsByteSplit/strided_sliceStridedSlice9text_vectorization/StringsByteSplit/StringSplit:indices:0@text_vectorization/StringsByteSplit/strided_slice/stack:output:0Btext_vectorization/StringsByteSplit/strided_slice/stack_1:output:0Btext_vectorization/StringsByteSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:         *

begin_mask*
end_mask*
shrink_axis_mask23
1text_vectorization/StringsByteSplit/strided_slice└
9text_vectorization/StringsByteSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2;
9text_vectorization/StringsByteSplit/strided_slice_1/stack─
;text_vectorization/StringsByteSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2=
;text_vectorization/StringsByteSplit/strided_slice_1/stack_1─
;text_vectorization/StringsByteSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2=
;text_vectorization/StringsByteSplit/strided_slice_1/stack_2╔
3text_vectorization/StringsByteSplit/strided_slice_1StridedSlice7text_vectorization/StringsByteSplit/StringSplit:shape:0Btext_vectorization/StringsByteSplit/strided_slice_1/stack:output:0Dtext_vectorization/StringsByteSplit/strided_slice_1/stack_1:output:0Dtext_vectorization/StringsByteSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask25
3text_vectorization/StringsByteSplit/strided_slice_1╣
Ztext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast:text_vectorization/StringsByteSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:         2\
Ztext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast▓
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast<text_vectorization/StringsByteSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: 2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1┌
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShape^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeЦ
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const╤
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdmtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0mtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: 2e
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdЦ
htext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : 2j
htext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y▌
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreaterltext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0qtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greaterю
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastjtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: 2e
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastЪ
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1┴
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMax^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0otext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxО
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y╬
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ktext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0mtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add┴
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulgtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul╞
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum`text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum╩
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum`text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumУ
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2╔
gtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincount^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0otext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:         2i
gtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountИ
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : 2c
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis╨
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumntext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:         2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumШ
etext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R 2g
etext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0И
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2c
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis┐
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2ntext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:         2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatА
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Vtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handle8text_vectorization/StringsByteSplit/StringSplit:values:0Wtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*#
_output_shapes
:         2K
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2Ж
2text_vectorization/string_lookup/assert_equal/NoOpNoOp*
_output_shapes
 24
2text_vectorization/string_lookup/assert_equal/NoOpф
)text_vectorization/string_lookup/IdentityIdentityRtext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:         2+
)text_vectorization/string_lookup/Identity√
+text_vectorization/string_lookup/Identity_1Identityetext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat:output:0*
T0	*#
_output_shapes
:         2-
+text_vectorization/string_lookup/Identity_1д
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R 21
/text_vectorization/RaggedToTensor/default_valueЭ
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
: *
dtype0	*
valueB	 R
         2)
'text_vectorization/RaggedToTensor/Const¤
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:02text_vectorization/string_lookup/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:04text_vectorization/string_lookup/Identity_1:output:0*
T0	*
Tindex0	*
Tshape0	*0
_output_shapes
:                  *
num_row_partition_tensors*%
row_partition_types

ROW_SPLITS28
6text_vectorization/RaggedToTensor/RaggedTensorToTensorг
text_vectorization/ShapeShape?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
T0	*
_output_shapes
:2
text_vectorization/ShapeЪ
&text_vectorization/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&text_vectorization/strided_slice/stackЮ
(text_vectorization/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(text_vectorization/strided_slice/stack_1Ю
(text_vectorization/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(text_vectorization/strided_slice/stack_2╘
 text_vectorization/strided_sliceStridedSlice!text_vectorization/Shape:output:0/text_vectorization/strided_slice/stack:output:01text_vectorization/strided_slice/stack_1:output:01text_vectorization/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 text_vectorization/strided_slicew
text_vectorization/sub/xConst*
_output_shapes
: *
dtype0*
value
B :▄2
text_vectorization/sub/xж
text_vectorization/subSub!text_vectorization/sub/x:output:0)text_vectorization/strided_slice:output:0*
T0*
_output_shapes
: 2
text_vectorization/suby
text_vectorization/Less/yConst*
_output_shapes
: *
dtype0*
value
B :▄2
text_vectorization/Less/yк
text_vectorization/LessLess)text_vectorization/strided_slice:output:0"text_vectorization/Less/y:output:0*
T0*
_output_shapes
: 2
text_vectorization/Less∙
text_vectorization/condStatelessIftext_vectorization/Less:z:0text_vectorization/sub:z:0?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
Tcond0
*
Tin
2	*
Tout
2	*
_lower_using_switch_merge(*0
_output_shapes
:                  * 
_read_only_resource_inputs
 *5
else_branch&R$
"text_vectorization_cond_false_7577*/
output_shapes
:                  *4
then_branch%R#
!text_vectorization_cond_true_75762
text_vectorization/condе
 text_vectorization/cond/IdentityIdentity text_vectorization/cond:output:0*
T0	*(
_output_shapes
:         ▄2"
 text_vectorization/cond/IdentityН

dense/CastCast)text_vectorization/cond/Identity:output:0*

DstT0*

SrcT0	*(
_output_shapes
:         ▄2

dense/Castб
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource* 
_output_shapes
:
▄А*
dtype02
dense/MatMul/ReadVariableOpО
dense/MatMulMatMuldense/Cast:y:0#dense/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:         А2
dense/MatMulЯ
dense/BiasAdd/ReadVariableOpReadVariableOp%dense_biasadd_readvariableop_resource*
_output_shapes	
:А*
dtype02
dense/BiasAdd/ReadVariableOpЪ
dense/BiasAddBiasAdddense/MatMul:product:0$dense/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:         А2
dense/BiasAddk

dense/ReluReludense/BiasAdd:output:0*
T0*(
_output_shapes
:         А2

dense/Reluз
dense_1/MatMul/ReadVariableOpReadVariableOp&dense_1_matmul_readvariableop_resource* 
_output_shapes
:
АА*
dtype02
dense_1/MatMul/ReadVariableOpЮ
dense_1/MatMulMatMuldense/Relu:activations:0%dense_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:         А2
dense_1/MatMulе
dense_1/BiasAdd/ReadVariableOpReadVariableOp'dense_1_biasadd_readvariableop_resource*
_output_shapes	
:А*
dtype02 
dense_1/BiasAdd/ReadVariableOpв
dense_1/BiasAddBiasAdddense_1/MatMul:product:0&dense_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:         А2
dense_1/BiasAddq
dense_1/ReluReludense_1/BiasAdd:output:0*
T0*(
_output_shapes
:         А2
dense_1/Reluж
dense_2/MatMul/ReadVariableOpReadVariableOp&dense_2_matmul_readvariableop_resource*
_output_shapes
:	А@*
dtype02
dense_2/MatMul/ReadVariableOpЯ
dense_2/MatMulMatMuldense_1/Relu:activations:0%dense_2/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_2/MatMulд
dense_2/BiasAdd/ReadVariableOpReadVariableOp'dense_2_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_2/BiasAdd/ReadVariableOpб
dense_2/BiasAddBiasAdddense_2/MatMul:product:0&dense_2/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_2/BiasAddp
dense_2/ReluReludense_2/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_2/Relu╢
$normalization/Reshape/ReadVariableOpReadVariableOp-normalization_reshape_readvariableop_resource*
_output_shapes
:*
dtype02&
$normalization/Reshape/ReadVariableOpЛ
normalization/Reshape/shapeConst*
_output_shapes
:*
dtype0*
valueB"      2
normalization/Reshape/shape╢
normalization/ReshapeReshape,normalization/Reshape/ReadVariableOp:value:0$normalization/Reshape/shape:output:0*
T0*
_output_shapes

:2
normalization/Reshape╝
&normalization/Reshape_1/ReadVariableOpReadVariableOp/normalization_reshape_1_readvariableop_resource*
_output_shapes
:*
dtype02(
&normalization/Reshape_1/ReadVariableOpП
normalization/Reshape_1/shapeConst*
_output_shapes
:*
dtype0*
valueB"      2
normalization/Reshape_1/shape╛
normalization/Reshape_1Reshape.normalization/Reshape_1/ReadVariableOp:value:0&normalization/Reshape_1/shape:output:0*
T0*
_output_shapes

:2
normalization/Reshape_1Й
normalization/subSubinputs_0normalization/Reshape:output:0*
T0*'
_output_shapes
:         2
normalization/sub{
normalization/SqrtSqrt normalization/Reshape_1:output:0*
T0*
_output_shapes

:2
normalization/SqrtЪ
normalization/truedivRealDivnormalization/sub:z:0normalization/Sqrt:y:0*
T0*'
_output_shapes
:         2
normalization/truedivе
dense_3/MatMul/ReadVariableOpReadVariableOp&dense_3_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
dense_3/MatMul/ReadVariableOpЯ
dense_3/MatMulMatMuldense_2/Relu:activations:0%dense_3/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_3/MatMulд
dense_3/BiasAdd/ReadVariableOpReadVariableOp'dense_3_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_3/BiasAdd/ReadVariableOpб
dense_3/BiasAddBiasAdddense_3/MatMul:product:0&dense_3/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_3/BiasAddp
dense_3/ReluReludense_3/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_3/Relut
concatenate/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concatenate/concat/axis╚
concatenate/concatConcatV2normalization/truediv:z:0dense_3/Relu:activations:0 concatenate/concat/axis:output:0*
N*
T0*'
_output_shapes
:         E2
concatenate/concatе
dense_4/MatMul/ReadVariableOpReadVariableOp&dense_4_matmul_readvariableop_resource*
_output_shapes

:E@*
dtype02
dense_4/MatMul/ReadVariableOpа
dense_4/MatMulMatMulconcatenate/concat:output:0%dense_4/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_4/MatMulд
dense_4/BiasAdd/ReadVariableOpReadVariableOp'dense_4_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_4/BiasAdd/ReadVariableOpб
dense_4/BiasAddBiasAdddense_4/MatMul:product:0&dense_4/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_4/BiasAddp
dense_4/ReluReludense_4/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_4/Reluе
dense_5/MatMul/ReadVariableOpReadVariableOp&dense_5_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
dense_5/MatMul/ReadVariableOpЯ
dense_5/MatMulMatMuldense_4/Relu:activations:0%dense_5/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_5/MatMulд
dense_5/BiasAdd/ReadVariableOpReadVariableOp'dense_5_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_5/BiasAdd/ReadVariableOpб
dense_5/BiasAddBiasAdddense_5/MatMul:product:0&dense_5/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_5/BiasAddp
dense_5/ReluReludense_5/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_5/Reluе
dense_6/MatMul/ReadVariableOpReadVariableOp&dense_6_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
dense_6/MatMul/ReadVariableOpЯ
dense_6/MatMulMatMuldense_5/Relu:activations:0%dense_6/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_6/MatMulд
dense_6/BiasAdd/ReadVariableOpReadVariableOp'dense_6_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_6/BiasAdd/ReadVariableOpб
dense_6/BiasAddBiasAdddense_6/MatMul:product:0&dense_6/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_6/BiasAddp
dense_6/ReluReludense_6/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_6/Reluе
dense_7/MatMul/ReadVariableOpReadVariableOp&dense_7_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
dense_7/MatMul/ReadVariableOpЯ
dense_7/MatMulMatMuldense_6/Relu:activations:0%dense_7/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_7/MatMulд
dense_7/BiasAdd/ReadVariableOpReadVariableOp'dense_7_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_7/BiasAdd/ReadVariableOpб
dense_7/BiasAddBiasAdddense_7/MatMul:product:0&dense_7/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_7/BiasAddp
dense_7/ReluReludense_7/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_7/Reluе
dense_8/MatMul/ReadVariableOpReadVariableOp&dense_8_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
dense_8/MatMul/ReadVariableOpЯ
dense_8/MatMulMatMuldense_7/Relu:activations:0%dense_8/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_8/MatMulд
dense_8/BiasAdd/ReadVariableOpReadVariableOp'dense_8_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_8/BiasAdd/ReadVariableOpб
dense_8/BiasAddBiasAdddense_8/MatMul:product:0&dense_8/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_8/BiasAddp
dense_8/ReluReludense_8/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_8/Reluе
dense_9/MatMul/ReadVariableOpReadVariableOp&dense_9_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
dense_9/MatMul/ReadVariableOpЯ
dense_9/MatMulMatMuldense_8/Relu:activations:0%dense_9/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_9/MatMulд
dense_9/BiasAdd/ReadVariableOpReadVariableOp'dense_9_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_9/BiasAdd/ReadVariableOpб
dense_9/BiasAddBiasAdddense_9/MatMul:product:0&dense_9/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_9/BiasAddp
dense_9/ReluReludense_9/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_9/Reluи
dense_10/MatMul/ReadVariableOpReadVariableOp'dense_10_matmul_readvariableop_resource*
_output_shapes

:@*
dtype02 
dense_10/MatMul/ReadVariableOpв
dense_10/MatMulMatMuldense_9/Relu:activations:0&dense_10/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2
dense_10/MatMulз
dense_10/BiasAdd/ReadVariableOpReadVariableOp(dense_10_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02!
dense_10/BiasAdd/ReadVariableOpе
dense_10/BiasAddBiasAdddense_10/MatMul:product:0'dense_10/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2
dense_10/BiasAdd╣
IdentityIdentitydense_10/BiasAdd:output:0J^text_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*б
_input_shapesП
М:         :         :: ::::::::::::::::::::::::2Ц
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2:Q M
'
_output_shapes
:         
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:         
"
_user_specified_name
inputs/1
Ю
ф
!text_vectorization_cond_true_7576A
=text_vectorization_cond_pad_paddings_1_text_vectorization_subV
Rtext_vectorization_cond_pad_text_vectorization_raggedtotensor_raggedtensortotensor	$
 text_vectorization_cond_identity	Ц
(text_vectorization/cond/Pad/paddings/1/0Const*
_output_shapes
: *
dtype0*
value	B : 2*
(text_vectorization/cond/Pad/paddings/1/0°
&text_vectorization/cond/Pad/paddings/1Pack1text_vectorization/cond/Pad/paddings/1/0:output:0=text_vectorization_cond_pad_paddings_1_text_vectorization_sub*
N*
T0*
_output_shapes
:2(
&text_vectorization/cond/Pad/paddings/1е
(text_vectorization/cond/Pad/paddings/0_1Const*
_output_shapes
:*
dtype0*
valueB"        2*
(text_vectorization/cond/Pad/paddings/0_1ъ
$text_vectorization/cond/Pad/paddingsPack1text_vectorization/cond/Pad/paddings/0_1:output:0/text_vectorization/cond/Pad/paddings/1:output:0*
N*
T0*
_output_shapes

:2&
$text_vectorization/cond/Pad/paddings 
text_vectorization/cond/PadPadRtext_vectorization_cond_pad_text_vectorization_raggedtotensor_raggedtensortotensor-text_vectorization/cond/Pad/paddings:output:0*
T0	*0
_output_shapes
:                  2
text_vectorization/cond/Pad▒
 text_vectorization/cond/IdentityIdentity$text_vectorization/cond/Pad:output:0*
T0	*0
_output_shapes
:                  2"
 text_vectorization/cond/Identity"M
 text_vectorization_cond_identity)text_vectorization/cond/Identity:output:0*1
_input_shapes 
: :                  : 

_output_shapes
: :62
0
_output_shapes
:                  
ж
й
A__inference_dense_3_layer_call_and_return_conditional_losses_7876

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИН
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpБ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @:::O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
╦
к
B__inference_dense_10_layer_call_and_return_conditional_losses_8028

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИН
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2
MatMulМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOpБ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2	
BiasAddd
IdentityIdentityBiasAdd:output:0*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @:::O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
Ю
ф
!text_vectorization_cond_true_7109A
=text_vectorization_cond_pad_paddings_1_text_vectorization_subV
Rtext_vectorization_cond_pad_text_vectorization_raggedtotensor_raggedtensortotensor	$
 text_vectorization_cond_identity	Ц
(text_vectorization/cond/Pad/paddings/1/0Const*
_output_shapes
: *
dtype0*
value	B : 2*
(text_vectorization/cond/Pad/paddings/1/0°
&text_vectorization/cond/Pad/paddings/1Pack1text_vectorization/cond/Pad/paddings/1/0:output:0=text_vectorization_cond_pad_paddings_1_text_vectorization_sub*
N*
T0*
_output_shapes
:2(
&text_vectorization/cond/Pad/paddings/1е
(text_vectorization/cond/Pad/paddings/0_1Const*
_output_shapes
:*
dtype0*
valueB"        2*
(text_vectorization/cond/Pad/paddings/0_1ъ
$text_vectorization/cond/Pad/paddingsPack1text_vectorization/cond/Pad/paddings/0_1:output:0/text_vectorization/cond/Pad/paddings/1:output:0*
N*
T0*
_output_shapes

:2&
$text_vectorization/cond/Pad/paddings 
text_vectorization/cond/PadPadRtext_vectorization_cond_pad_text_vectorization_raggedtotensor_raggedtensortotensor-text_vectorization/cond/Pad/paddings:output:0*
T0	*0
_output_shapes
:                  2
text_vectorization/cond/Pad▒
 text_vectorization/cond/IdentityIdentity$text_vectorization/cond/Pad:output:0*
T0	*0
_output_shapes
:                  2"
 text_vectorization/cond/Identity"M
 text_vectorization_cond_identity)text_vectorization/cond/Identity:output:0*1
_input_shapes 
: :                  : 

_output_shapes
: :62
0
_output_shapes
:                  
ж
й
A__inference_dense_7_layer_call_and_return_conditional_losses_7969

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИН
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpБ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @:::O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
Ю
ф
!text_vectorization_cond_true_6349A
=text_vectorization_cond_pad_paddings_1_text_vectorization_subV
Rtext_vectorization_cond_pad_text_vectorization_raggedtotensor_raggedtensortotensor	$
 text_vectorization_cond_identity	Ц
(text_vectorization/cond/Pad/paddings/1/0Const*
_output_shapes
: *
dtype0*
value	B : 2*
(text_vectorization/cond/Pad/paddings/1/0°
&text_vectorization/cond/Pad/paddings/1Pack1text_vectorization/cond/Pad/paddings/1/0:output:0=text_vectorization_cond_pad_paddings_1_text_vectorization_sub*
N*
T0*
_output_shapes
:2(
&text_vectorization/cond/Pad/paddings/1е
(text_vectorization/cond/Pad/paddings/0_1Const*
_output_shapes
:*
dtype0*
valueB"        2*
(text_vectorization/cond/Pad/paddings/0_1ъ
$text_vectorization/cond/Pad/paddingsPack1text_vectorization/cond/Pad/paddings/0_1:output:0/text_vectorization/cond/Pad/paddings/1:output:0*
N*
T0*
_output_shapes

:2&
$text_vectorization/cond/Pad/paddings 
text_vectorization/cond/PadPadRtext_vectorization_cond_pad_text_vectorization_raggedtotensor_raggedtensortotensor-text_vectorization/cond/Pad/paddings:output:0*
T0	*0
_output_shapes
:                  2
text_vectorization/cond/Pad▒
 text_vectorization/cond/IdentityIdentity$text_vectorization/cond/Pad:output:0*
T0	*0
_output_shapes
:                  2"
 text_vectorization/cond/Identity"M
 text_vectorization_cond_identity)text_vectorization/cond/Identity:output:0*1
_input_shapes 
: :                  : 

_output_shapes
: :62
0
_output_shapes
:                  
ж
й
A__inference_dense_7_layer_call_and_return_conditional_losses_6598

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИН
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpБ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @:::O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
▒
o
E__inference_concatenate_layer_call_and_return_conditional_losses_6497

inputs
inputs_1
identity\
concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concat/axis
concatConcatV2inputsinputs_1concat/axis:output:0*
N*
T0*'
_output_shapes
:         E2
concatc
IdentityIdentityconcat:output:0*
T0*'
_output_shapes
:         E2

Identity"
identityIdentity:output:0*9
_input_shapes(
&:         :         @:O K
'
_output_shapes
:         
 
_user_specified_nameinputs:OK
'
_output_shapes
:         @
 
_user_specified_nameinputs
й
й
A__inference_dense_2_layer_call_and_return_conditional_losses_7856

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИО
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	А@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpБ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*/
_input_shapes
:         А:::P L
(
_output_shapes
:         А
 
_user_specified_nameinputs
╧
╒
"text_vectorization_cond_false_6350'
#text_vectorization_cond_placeholder`
\text_vectorization_cond_strided_slice_text_vectorization_raggedtotensor_raggedtensortotensor	$
 text_vectorization_cond_identity	л
+text_vectorization/cond/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2-
+text_vectorization/cond/strided_slice/stackп
-text_vectorization/cond/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ▄  2/
-text_vectorization/cond/strided_slice/stack_1п
-text_vectorization/cond/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2/
-text_vectorization/cond/strided_slice/stack_2╠
%text_vectorization/cond/strided_sliceStridedSlice\text_vectorization_cond_strided_slice_text_vectorization_raggedtotensor_raggedtensortotensor4text_vectorization/cond/strided_slice/stack:output:06text_vectorization/cond/strided_slice/stack_1:output:06text_vectorization/cond/strided_slice/stack_2:output:0*
Index0*
T0	*0
_output_shapes
:                  *

begin_mask*
end_mask2'
%text_vectorization/cond/strided_slice╗
 text_vectorization/cond/IdentityIdentity.text_vectorization/cond/strided_slice:output:0*
T0	*0
_output_shapes
:                  2"
 text_vectorization/cond/Identity"M
 text_vectorization_cond_identity)text_vectorization/cond/Identity:output:0*1
_input_shapes 
: :                  : 

_output_shapes
: :62
0
_output_shapes
:                  
¤
Е
+__inference_functional_1_layer_call_fn_7253
input_2
input_1
unknown
	unknown_0	
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
	unknown_7
	unknown_8
	unknown_9

unknown_10

unknown_11

unknown_12

unknown_13

unknown_14

unknown_15

unknown_16

unknown_17

unknown_18

unknown_19

unknown_20

unknown_21

unknown_22

unknown_23

unknown_24
identityИвStatefulPartitionedCall╞
StatefulPartitionedCallStatefulPartitionedCallinput_2input_1unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24*'
Tin 
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *:
_read_only_resource_inputs
	
*-
config_proto

CPU

GPU 2J 8В *O
fJRH
F__inference_functional_1_layer_call_and_return_conditional_losses_71982
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*б
_input_shapesП
М:         :         :: ::::::::::::::::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:P L
'
_output_shapes
:         
!
_user_specified_name	input_2:PL
'
_output_shapes
:         
!
_user_specified_name	input_1
╓
{
&__inference_dense_3_layer_call_fn_7885

inputs
unknown
	unknown_0
identityИвStatefulPartitionedCallё
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_3_layer_call_and_return_conditional_losses_64742
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
╓
{
&__inference_dense_7_layer_call_fn_7978

inputs
unknown
	unknown_0
identityИвStatefulPartitionedCallё
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_7_layer_call_and_return_conditional_losses_65982
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
╓
{
&__inference_dense_4_layer_call_fn_7918

inputs
unknown
	unknown_0
identityИвStatefulPartitionedCallё
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_4_layer_call_and_return_conditional_losses_65172
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         E::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:         E
 
_user_specified_nameinputs
ж
й
A__inference_dense_8_layer_call_and_return_conditional_losses_7989

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИН
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpБ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @:::O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
┌
{
&__inference_dense_1_layer_call_fn_7845

inputs
unknown
	unknown_0
identityИвStatefulPartitionedCallЄ
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:         А*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_dense_1_layer_call_and_return_conditional_losses_64092
StatefulPartitionedCallП
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*(
_output_shapes
:         А2

Identity"
identityIdentity:output:0*/
_input_shapes
:         А::22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:         А
 
_user_specified_nameinputs
ж
й
A__inference_dense_5_layer_call_and_return_conditional_losses_6544

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИН
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpБ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @:::O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
Ю
ф
!text_vectorization_cond_true_6753A
=text_vectorization_cond_pad_paddings_1_text_vectorization_subV
Rtext_vectorization_cond_pad_text_vectorization_raggedtotensor_raggedtensortotensor	$
 text_vectorization_cond_identity	Ц
(text_vectorization/cond/Pad/paddings/1/0Const*
_output_shapes
: *
dtype0*
value	B : 2*
(text_vectorization/cond/Pad/paddings/1/0°
&text_vectorization/cond/Pad/paddings/1Pack1text_vectorization/cond/Pad/paddings/1/0:output:0=text_vectorization_cond_pad_paddings_1_text_vectorization_sub*
N*
T0*
_output_shapes
:2(
&text_vectorization/cond/Pad/paddings/1е
(text_vectorization/cond/Pad/paddings/0_1Const*
_output_shapes
:*
dtype0*
valueB"        2*
(text_vectorization/cond/Pad/paddings/0_1ъ
$text_vectorization/cond/Pad/paddingsPack1text_vectorization/cond/Pad/paddings/0_1:output:0/text_vectorization/cond/Pad/paddings/1:output:0*
N*
T0*
_output_shapes

:2&
$text_vectorization/cond/Pad/paddings 
text_vectorization/cond/PadPadRtext_vectorization_cond_pad_text_vectorization_raggedtotensor_raggedtensortotensor-text_vectorization/cond/Pad/paddings:output:0*
T0	*0
_output_shapes
:                  2
text_vectorization/cond/Pad▒
 text_vectorization/cond/IdentityIdentity$text_vectorization/cond/Pad:output:0*
T0	*0
_output_shapes
:                  2"
 text_vectorization/cond/Identity"M
 text_vectorization_cond_identity)text_vectorization/cond/Identity:output:0*1
_input_shapes 
: :                  : 

_output_shapes
: :62
0
_output_shapes
:                  "╕L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*ш
serving_default╘
;
input_10
serving_default_input_1:0         
;
input_20
serving_default_input_2:0         <
dense_100
StatefulPartitionedCall:0         tensorflow/serving/predict:╠░
оw
layer-0
layer_with_weights-0
layer-1
layer_with_weights-1
layer-2
layer_with_weights-2
layer-3
layer-4
layer_with_weights-3
layer-5
layer_with_weights-4
layer-6
layer_with_weights-5
layer-7
	layer-8

layer_with_weights-6

layer-9
layer_with_weights-7
layer-10
layer_with_weights-8
layer-11
layer_with_weights-9
layer-12
layer_with_weights-10
layer-13
layer_with_weights-11
layer-14
layer_with_weights-12
layer-15
	optimizer
	variables
regularization_losses
trainable_variables
	keras_api

signatures
р_default_save_signature
+с&call_and_return_all_conditional_losses
т__call__"╡q
_tf_keras_networkЩq{"class_name": "Functional", "name": "functional_1", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "must_restore_from_config": false, "config": {"name": "functional_1", "layers": [{"class_name": "InputLayer", "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 1]}, "dtype": "string", "sparse": false, "ragged": false, "name": "input_1"}, "name": "input_1", "inbound_nodes": []}, {"class_name": "TextVectorization", "config": {"name": "text_vectorization", "trainable": true, "dtype": "string", "max_tokens": 30, "standardize": "lower_and_strip_punctuation", "split": "Custom>charsplit", "ngrams": null, "output_mode": "int", "output_sequence_length": 1500, "pad_to_max_tokens": true}, "name": "text_vectorization", "inbound_nodes": [[["input_1", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense", "trainable": true, "dtype": "float32", "units": 128, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense", "inbound_nodes": [[["text_vectorization", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_1", "trainable": true, "dtype": "float32", "units": 128, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_1", "inbound_nodes": [[["dense", 0, 0, {}]]]}, {"class_name": "InputLayer", "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 5]}, "dtype": "float32", "sparse": false, "ragged": false, "name": "input_2"}, "name": "input_2", "inbound_nodes": []}, {"class_name": "Dense", "config": {"name": "dense_2", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_2", "inbound_nodes": [[["dense_1", 0, 0, {}]]]}, {"class_name": "Normalization", "config": {"name": "normalization", "trainable": true, "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "dtype": "float32", "axis": {"class_name": "__tuple__", "items": [-1]}}, "name": "normalization", "inbound_nodes": [[["input_2", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_3", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_3", "inbound_nodes": [[["dense_2", 0, 0, {}]]]}, {"class_name": "Concatenate", "config": {"name": "concatenate", "trainable": true, "dtype": "float32", "axis": -1}, "name": "concatenate", "inbound_nodes": [[["normalization", 0, 0, {}], ["dense_3", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_4", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_4", "inbound_nodes": [[["concatenate", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_5", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_5", "inbound_nodes": [[["dense_4", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_6", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_6", "inbound_nodes": [[["dense_5", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_7", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_7", "inbound_nodes": [[["dense_6", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_8", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_8", "inbound_nodes": [[["dense_7", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_9", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_9", "inbound_nodes": [[["dense_8", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_10", "trainable": true, "dtype": "float32", "units": 1, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_10", "inbound_nodes": [[["dense_9", 0, 0, {}]]]}], "input_layers": [["input_2", 0, 0], ["input_1", 0, 0]], "output_layers": [["dense_10", 0, 0]]}, "build_input_shape": [{"class_name": "TensorShape", "items": [null, 5]}, {"class_name": "TensorShape", "items": [null, 1]}], "is_graph_network": true, "keras_version": "2.4.0", "backend": "tensorflow", "model_config": {"class_name": "Functional", "config": {"name": "functional_1", "layers": [{"class_name": "InputLayer", "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 1]}, "dtype": "string", "sparse": false, "ragged": false, "name": "input_1"}, "name": "input_1", "inbound_nodes": []}, {"class_name": "TextVectorization", "config": {"name": "text_vectorization", "trainable": true, "dtype": "string", "max_tokens": 30, "standardize": "lower_and_strip_punctuation", "split": "Custom>charsplit", "ngrams": null, "output_mode": "int", "output_sequence_length": 1500, "pad_to_max_tokens": true}, "name": "text_vectorization", "inbound_nodes": [[["input_1", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense", "trainable": true, "dtype": "float32", "units": 128, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense", "inbound_nodes": [[["text_vectorization", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_1", "trainable": true, "dtype": "float32", "units": 128, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_1", "inbound_nodes": [[["dense", 0, 0, {}]]]}, {"class_name": "InputLayer", "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 5]}, "dtype": "float32", "sparse": false, "ragged": false, "name": "input_2"}, "name": "input_2", "inbound_nodes": []}, {"class_name": "Dense", "config": {"name": "dense_2", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_2", "inbound_nodes": [[["dense_1", 0, 0, {}]]]}, {"class_name": "Normalization", "config": {"name": "normalization", "trainable": true, "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "dtype": "float32", "axis": {"class_name": "__tuple__", "items": [-1]}}, "name": "normalization", "inbound_nodes": [[["input_2", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_3", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_3", "inbound_nodes": [[["dense_2", 0, 0, {}]]]}, {"class_name": "Concatenate", "config": {"name": "concatenate", "trainable": true, "dtype": "float32", "axis": -1}, "name": "concatenate", "inbound_nodes": [[["normalization", 0, 0, {}], ["dense_3", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_4", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_4", "inbound_nodes": [[["concatenate", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_5", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_5", "inbound_nodes": [[["dense_4", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_6", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_6", "inbound_nodes": [[["dense_5", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_7", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_7", "inbound_nodes": [[["dense_6", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_8", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_8", "inbound_nodes": [[["dense_7", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_9", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_9", "inbound_nodes": [[["dense_8", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_10", "trainable": true, "dtype": "float32", "units": 1, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_10", "inbound_nodes": [[["dense_9", 0, 0, {}]]]}], "input_layers": [["input_2", 0, 0], ["input_1", 0, 0]], "output_layers": [["dense_10", 0, 0]]}}, "training_config": {"loss": "mean_absolute_error", "metrics": null, "weighted_metrics": null, "loss_weights": null, "optimizer_config": {"class_name": "Adam", "config": {"name": "Adam", "learning_rate": 0.000699999975040555, "decay": 0.0, "beta_1": 0.8999999761581421, "beta_2": 0.9990000128746033, "epsilon": 1e-07, "amsgrad": false}}}}
ч"ф
_tf_keras_input_layer─{"class_name": "InputLayer", "name": "input_1", "dtype": "string", "sparse": false, "ragged": false, "batch_input_shape": {"class_name": "__tuple__", "items": [null, 1]}, "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 1]}, "dtype": "string", "sparse": false, "ragged": false, "name": "input_1"}}
З
state_variables
_index_lookup_layer
	keras_api"╟
_tf_keras_layerн{"class_name": "TextVectorization", "name": "text_vectorization", "trainable": true, "expects_training_arg": false, "dtype": "string", "batch_input_shape": null, "stateful": false, "must_restore_from_config": true, "config": {"name": "text_vectorization", "trainable": true, "dtype": "string", "max_tokens": 30, "standardize": "lower_and_strip_punctuation", "split": "Custom>charsplit", "ngrams": null, "output_mode": "int", "output_sequence_length": 1500, "pad_to_max_tokens": true}, "build_input_shape": {"class_name": "TensorShape", "items": [59125, 1]}}
є

kernel
bias
	variables
regularization_losses
trainable_variables
	keras_api
+х&call_and_return_all_conditional_losses
ц__call__"╠
_tf_keras_layer▓{"class_name": "Dense", "name": "dense", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "dense", "trainable": true, "dtype": "float32", "units": 128, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 1500}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 1500]}}
ї

 kernel
!bias
"	variables
#regularization_losses
$trainable_variables
%	keras_api
+ч&call_and_return_all_conditional_losses
ш__call__"╬
_tf_keras_layer┤{"class_name": "Dense", "name": "dense_1", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "dense_1", "trainable": true, "dtype": "float32", "units": 128, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 128}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 128]}}
щ"ц
_tf_keras_input_layer╞{"class_name": "InputLayer", "name": "input_2", "dtype": "float32", "sparse": false, "ragged": false, "batch_input_shape": {"class_name": "__tuple__", "items": [null, 5]}, "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 5]}, "dtype": "float32", "sparse": false, "ragged": false, "name": "input_2"}}
Ї

&kernel
'bias
(	variables
)regularization_losses
*trainable_variables
+	keras_api
+щ&call_and_return_all_conditional_losses
ъ__call__"═
_tf_keras_layer│{"class_name": "Dense", "name": "dense_2", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "dense_2", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 128}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 128]}}
▐
,state_variables
-_broadcast_shape
.mean
/variance
	0count
1	keras_api"■
_tf_keras_layerф{"class_name": "Normalization", "name": "normalization", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "stateful": false, "must_restore_from_config": true, "config": {"name": "normalization", "trainable": true, "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "dtype": "float32", "axis": {"class_name": "__tuple__", "items": [-1]}}, "build_input_shape": [512, 5]}
Є

2kernel
3bias
4	variables
5regularization_losses
6trainable_variables
7	keras_api
+ы&call_and_return_all_conditional_losses
ь__call__"╦
_tf_keras_layer▒{"class_name": "Dense", "name": "dense_3", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "dense_3", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 64]}}
╩
8	variables
9regularization_losses
:trainable_variables
;	keras_api
+э&call_and_return_all_conditional_losses
ю__call__"╣
_tf_keras_layerЯ{"class_name": "Concatenate", "name": "concatenate", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "concatenate", "trainable": true, "dtype": "float32", "axis": -1}, "build_input_shape": [{"class_name": "TensorShape", "items": [null, 5]}, {"class_name": "TensorShape", "items": [null, 64]}]}
Є

<kernel
=bias
>	variables
?regularization_losses
@trainable_variables
A	keras_api
+я&call_and_return_all_conditional_losses
Ё__call__"╦
_tf_keras_layer▒{"class_name": "Dense", "name": "dense_4", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "dense_4", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 69}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 69]}}
Є

Bkernel
Cbias
D	variables
Eregularization_losses
Ftrainable_variables
G	keras_api
+ё&call_and_return_all_conditional_losses
Є__call__"╦
_tf_keras_layer▒{"class_name": "Dense", "name": "dense_5", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "dense_5", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 64]}}
Є

Hkernel
Ibias
J	variables
Kregularization_losses
Ltrainable_variables
M	keras_api
+є&call_and_return_all_conditional_losses
Ї__call__"╦
_tf_keras_layer▒{"class_name": "Dense", "name": "dense_6", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "dense_6", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 64]}}
Є

Nkernel
Obias
P	variables
Qregularization_losses
Rtrainable_variables
S	keras_api
+ї&call_and_return_all_conditional_losses
Ў__call__"╦
_tf_keras_layer▒{"class_name": "Dense", "name": "dense_7", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "dense_7", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 64]}}
Є

Tkernel
Ubias
V	variables
Wregularization_losses
Xtrainable_variables
Y	keras_api
+ў&call_and_return_all_conditional_losses
°__call__"╦
_tf_keras_layer▒{"class_name": "Dense", "name": "dense_8", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "dense_8", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 64]}}
Є

Zkernel
[bias
\	variables
]regularization_losses
^trainable_variables
_	keras_api
+∙&call_and_return_all_conditional_losses
·__call__"╦
_tf_keras_layer▒{"class_name": "Dense", "name": "dense_9", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "dense_9", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 64]}}
ї

`kernel
abias
b	variables
cregularization_losses
dtrainable_variables
e	keras_api
+√&call_and_return_all_conditional_losses
№__call__"╬
_tf_keras_layer┤{"class_name": "Dense", "name": "dense_10", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "dense_10", "trainable": true, "dtype": "float32", "units": 1, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 64]}}
Л
fiter

gbeta_1

hbeta_2
	idecay
jlearning_ratem┤m╡ m╢!m╖&m╕'m╣2m║3m╗<m╝=m╜Bm╛Cm┐Hm└Im┴Nm┬Om├Tm─Um┼Zm╞[m╟`m╚am╔v╩v╦ v╠!v═&v╬'v╧2v╨3v╤<v╥=v╙Bv╘Cv╒Hv╓Iv╫Nv╪Ov┘Tv┌Uv█Zv▄[v▌`v▐av▀"
	optimizer
▀
1
2
 3
!4
&5
'6
.7
/8
09
210
311
<12
=13
B14
C15
H16
I17
N18
O19
T20
U21
Z22
[23
`24
a25"
trackable_list_wrapper
 "
trackable_list_wrapper
╞
0
1
 2
!3
&4
'5
26
37
<8
=9
B10
C11
H12
I13
N14
O15
T16
U17
Z18
[19
`20
a21"
trackable_list_wrapper
╬
	variables
regularization_losses
knon_trainable_variables
llayer_metrics
mmetrics

nlayers
trainable_variables
olayer_regularization_losses
т__call__
р_default_save_signature
+с&call_and_return_all_conditional_losses
'с"call_and_return_conditional_losses"
_generic_user_object
-
¤serving_default"
signature_map
 "
trackable_dict_wrapper
╤
pstate_variables

q_table
r	keras_api"Ю
_tf_keras_layerД{"class_name": "StringLookup", "name": "string_lookup", "trainable": true, "expects_training_arg": false, "dtype": "string", "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "stateful": false, "must_restore_from_config": true, "config": {"name": "string_lookup", "trainable": true, "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "dtype": "string", "invert": false, "max_tokens": 30, "num_oov_indices": 1, "oov_token": "[UNK]", "mask_token": "", "encoding": "utf-8"}}
"
_generic_user_object
 :
▄А2dense/kernel
:А2
dense/bias
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
░
	variables
regularization_losses
snon_trainable_variables
tlayer_metrics
umetrics

vlayers
trainable_variables
wlayer_regularization_losses
ц__call__
+х&call_and_return_all_conditional_losses
'х"call_and_return_conditional_losses"
_generic_user_object
": 
АА2dense_1/kernel
:А2dense_1/bias
.
 0
!1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
 0
!1"
trackable_list_wrapper
░
"	variables
#regularization_losses
xnon_trainable_variables
ylayer_metrics
zmetrics

{layers
$trainable_variables
|layer_regularization_losses
ш__call__
+ч&call_and_return_all_conditional_losses
'ч"call_and_return_conditional_losses"
_generic_user_object
!:	А@2dense_2/kernel
:@2dense_2/bias
.
&0
'1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
&0
'1"
trackable_list_wrapper
▓
(	variables
)regularization_losses
}non_trainable_variables
~layer_metrics
metrics
Аlayers
*trainable_variables
 Бlayer_regularization_losses
ъ__call__
+щ&call_and_return_all_conditional_losses
'щ"call_and_return_conditional_losses"
_generic_user_object
C
.mean
/variance
	0count"
trackable_dict_wrapper
 "
trackable_list_wrapper
:2mean
:2variance
:	 2count
"
_generic_user_object
 :@@2dense_3/kernel
:@2dense_3/bias
.
20
31"
trackable_list_wrapper
 "
trackable_list_wrapper
.
20
31"
trackable_list_wrapper
╡
4	variables
5regularization_losses
Вnon_trainable_variables
Гlayer_metrics
Дmetrics
Еlayers
6trainable_variables
 Жlayer_regularization_losses
ь__call__
+ы&call_and_return_all_conditional_losses
'ы"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
╡
8	variables
9regularization_losses
Зnon_trainable_variables
Иlayer_metrics
Йmetrics
Кlayers
:trainable_variables
 Лlayer_regularization_losses
ю__call__
+э&call_and_return_all_conditional_losses
'э"call_and_return_conditional_losses"
_generic_user_object
 :E@2dense_4/kernel
:@2dense_4/bias
.
<0
=1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
<0
=1"
trackable_list_wrapper
╡
>	variables
?regularization_losses
Мnon_trainable_variables
Нlayer_metrics
Оmetrics
Пlayers
@trainable_variables
 Рlayer_regularization_losses
Ё__call__
+я&call_and_return_all_conditional_losses
'я"call_and_return_conditional_losses"
_generic_user_object
 :@@2dense_5/kernel
:@2dense_5/bias
.
B0
C1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
B0
C1"
trackable_list_wrapper
╡
D	variables
Eregularization_losses
Сnon_trainable_variables
Тlayer_metrics
Уmetrics
Фlayers
Ftrainable_variables
 Хlayer_regularization_losses
Є__call__
+ё&call_and_return_all_conditional_losses
'ё"call_and_return_conditional_losses"
_generic_user_object
 :@@2dense_6/kernel
:@2dense_6/bias
.
H0
I1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
H0
I1"
trackable_list_wrapper
╡
J	variables
Kregularization_losses
Цnon_trainable_variables
Чlayer_metrics
Шmetrics
Щlayers
Ltrainable_variables
 Ъlayer_regularization_losses
Ї__call__
+є&call_and_return_all_conditional_losses
'є"call_and_return_conditional_losses"
_generic_user_object
 :@@2dense_7/kernel
:@2dense_7/bias
.
N0
O1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
N0
O1"
trackable_list_wrapper
╡
P	variables
Qregularization_losses
Ыnon_trainable_variables
Ьlayer_metrics
Эmetrics
Юlayers
Rtrainable_variables
 Яlayer_regularization_losses
Ў__call__
+ї&call_and_return_all_conditional_losses
'ї"call_and_return_conditional_losses"
_generic_user_object
 :@@2dense_8/kernel
:@2dense_8/bias
.
T0
U1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
T0
U1"
trackable_list_wrapper
╡
V	variables
Wregularization_losses
аnon_trainable_variables
бlayer_metrics
вmetrics
гlayers
Xtrainable_variables
 дlayer_regularization_losses
°__call__
+ў&call_and_return_all_conditional_losses
'ў"call_and_return_conditional_losses"
_generic_user_object
 :@@2dense_9/kernel
:@2dense_9/bias
.
Z0
[1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
Z0
[1"
trackable_list_wrapper
╡
\	variables
]regularization_losses
еnon_trainable_variables
жlayer_metrics
зmetrics
иlayers
^trainable_variables
 йlayer_regularization_losses
·__call__
+∙&call_and_return_all_conditional_losses
'∙"call_and_return_conditional_losses"
_generic_user_object
!:@2dense_10/kernel
:2dense_10/bias
.
`0
a1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
`0
a1"
trackable_list_wrapper
╡
b	variables
cregularization_losses
кnon_trainable_variables
лlayer_metrics
мmetrics
нlayers
dtrainable_variables
 оlayer_regularization_losses
№__call__
+√&call_and_return_all_conditional_losses
'√"call_and_return_conditional_losses"
_generic_user_object
:	 (2	Adam/iter
: (2Adam/beta_1
: (2Adam/beta_2
: (2
Adam/decay
: (2Adam/learning_rate
5
.1
/2
03"
trackable_list_wrapper
 "
trackable_dict_wrapper
(
п0"
trackable_list_wrapper
Ц
0
1
2
3
4
5
6
7
	8

9
10
11
12
13
14
15"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
T
■_create_resource
 _initialize
А_destroy_resourceR Z
tableуф
"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
┐

░total

▒count
▓	variables
│	keras_api"Д
_tf_keras_metricj{"class_name": "Mean", "name": "loss", "dtype": "float32", "config": {"name": "loss", "dtype": "float32"}}
:  (2total
:  (2count
0
░0
▒1"
trackable_list_wrapper
.
▓	variables"
_generic_user_object
%:#
▄А2Adam/dense/kernel/m
:А2Adam/dense/bias/m
':%
АА2Adam/dense_1/kernel/m
 :А2Adam/dense_1/bias/m
&:$	А@2Adam/dense_2/kernel/m
:@2Adam/dense_2/bias/m
%:#@@2Adam/dense_3/kernel/m
:@2Adam/dense_3/bias/m
%:#E@2Adam/dense_4/kernel/m
:@2Adam/dense_4/bias/m
%:#@@2Adam/dense_5/kernel/m
:@2Adam/dense_5/bias/m
%:#@@2Adam/dense_6/kernel/m
:@2Adam/dense_6/bias/m
%:#@@2Adam/dense_7/kernel/m
:@2Adam/dense_7/bias/m
%:#@@2Adam/dense_8/kernel/m
:@2Adam/dense_8/bias/m
%:#@@2Adam/dense_9/kernel/m
:@2Adam/dense_9/bias/m
&:$@2Adam/dense_10/kernel/m
 :2Adam/dense_10/bias/m
%:#
▄А2Adam/dense/kernel/v
:А2Adam/dense/bias/v
':%
АА2Adam/dense_1/kernel/v
 :А2Adam/dense_1/bias/v
&:$	А@2Adam/dense_2/kernel/v
:@2Adam/dense_2/bias/v
%:#@@2Adam/dense_3/kernel/v
:@2Adam/dense_3/bias/v
%:#E@2Adam/dense_4/kernel/v
:@2Adam/dense_4/bias/v
%:#@@2Adam/dense_5/kernel/v
:@2Adam/dense_5/bias/v
%:#@@2Adam/dense_6/kernel/v
:@2Adam/dense_6/bias/v
%:#@@2Adam/dense_7/kernel/v
:@2Adam/dense_7/bias/v
%:#@@2Adam/dense_8/kernel/v
:@2Adam/dense_8/bias/v
%:#@@2Adam/dense_9/kernel/v
:@2Adam/dense_9/bias/v
&:$@2Adam/dense_10/kernel/v
 :2Adam/dense_10/bias/v
Е2В
__inference__wrapped_model_6290▐
Л▓З
FullArgSpec
argsЪ 
varargsjargs
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *NвK
IЪF
!К
input_2         
!К
input_1         
ц2у
F__inference_functional_1_layer_call_and_return_conditional_losses_7688
F__inference_functional_1_layer_call_and_return_conditional_losses_6842
F__inference_functional_1_layer_call_and_return_conditional_losses_7518
F__inference_functional_1_layer_call_and_return_conditional_losses_6695└
╖▓│
FullArgSpec1
args)Ъ&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaultsЪ
p 

 

kwonlyargsЪ 
kwonlydefaultsк 
annotationsк *
 
·2ў
+__inference_functional_1_layer_call_fn_7746
+__inference_functional_1_layer_call_fn_7804
+__inference_functional_1_layer_call_fn_7253
+__inference_functional_1_layer_call_fn_7048└
╖▓│
FullArgSpec1
args)Ъ&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaultsЪ
p 

 

kwonlyargsЪ 
kwonlydefaultsк 
annotationsк *
 
.B,
__inference_save_fn_8071checkpoint_key
IBG
__inference_restore_fn_8079restored_tensors_0restored_tensors_1
щ2ц
?__inference_dense_layer_call_and_return_conditional_losses_7816в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
╬2╦
$__inference_dense_layer_call_fn_7825в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
ы2ш
A__inference_dense_1_layer_call_and_return_conditional_losses_7836в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
╨2═
&__inference_dense_1_layer_call_fn_7845в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
ы2ш
A__inference_dense_2_layer_call_and_return_conditional_losses_7856в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
╨2═
&__inference_dense_2_layer_call_fn_7865в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
ы2ш
A__inference_dense_3_layer_call_and_return_conditional_losses_7876в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
╨2═
&__inference_dense_3_layer_call_fn_7885в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
я2ь
E__inference_concatenate_layer_call_and_return_conditional_losses_7892в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
╘2╤
*__inference_concatenate_layer_call_fn_7898в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
ы2ш
A__inference_dense_4_layer_call_and_return_conditional_losses_7909в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
╨2═
&__inference_dense_4_layer_call_fn_7918в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
ы2ш
A__inference_dense_5_layer_call_and_return_conditional_losses_7929в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
╨2═
&__inference_dense_5_layer_call_fn_7938в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
ы2ш
A__inference_dense_6_layer_call_and_return_conditional_losses_7949в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
╨2═
&__inference_dense_6_layer_call_fn_7958в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
ы2ш
A__inference_dense_7_layer_call_and_return_conditional_losses_7969в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
╨2═
&__inference_dense_7_layer_call_fn_7978в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
ы2ш
A__inference_dense_8_layer_call_and_return_conditional_losses_7989в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
╨2═
&__inference_dense_8_layer_call_fn_7998в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
ы2ш
A__inference_dense_9_layer_call_and_return_conditional_losses_8009в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
╨2═
&__inference_dense_9_layer_call_fn_8018в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
ь2щ
B__inference_dense_10_layer_call_and_return_conditional_losses_8028в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
╤2╬
'__inference_dense_10_layer_call_fn_8037в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
8B6
"__inference_signature_wrapper_7321input_1input_2
░2н
__inference__creator_8042П
З▓Г
FullArgSpec
argsЪ 
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *в 
┤2▒
__inference__initializer_8047П
З▓Г
FullArgSpec
argsЪ 
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *в 
▓2п
__inference__destroyer_8052П
З▓Г
FullArgSpec
argsЪ 
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *в 
	J
Const5
__inference__creator_8042в

в 
к "К 7
__inference__destroyer_8052в

в 
к "К 9
__inference__initializer_8047в

в 
к "К ╨
__inference__wrapped_model_6290мqБ !&'./23<=BCHINOTUZ[`aXвU
NвK
IЪF
!К
input_2         
!К
input_1         
к "3к0
.
dense_10"К
dense_10         ═
E__inference_concatenate_layer_call_and_return_conditional_losses_7892ГZвW
PвM
KЪH
"К
inputs/0         
"К
inputs/1         @
к "%в"
К
0         E
Ъ д
*__inference_concatenate_layer_call_fn_7898vZвW
PвM
KЪH
"К
inputs/0         
"К
inputs/1         @
к "К         Eв
B__inference_dense_10_layer_call_and_return_conditional_losses_8028\`a/в,
%в"
 К
inputs         @
к "%в"
К
0         
Ъ z
'__inference_dense_10_layer_call_fn_8037O`a/в,
%в"
 К
inputs         @
к "К         г
A__inference_dense_1_layer_call_and_return_conditional_losses_7836^ !0в-
&в#
!К
inputs         А
к "&в#
К
0         А
Ъ {
&__inference_dense_1_layer_call_fn_7845Q !0в-
&в#
!К
inputs         А
к "К         Ав
A__inference_dense_2_layer_call_and_return_conditional_losses_7856]&'0в-
&в#
!К
inputs         А
к "%в"
К
0         @
Ъ z
&__inference_dense_2_layer_call_fn_7865P&'0в-
&в#
!К
inputs         А
к "К         @б
A__inference_dense_3_layer_call_and_return_conditional_losses_7876\23/в,
%в"
 К
inputs         @
к "%в"
К
0         @
Ъ y
&__inference_dense_3_layer_call_fn_7885O23/в,
%в"
 К
inputs         @
к "К         @б
A__inference_dense_4_layer_call_and_return_conditional_losses_7909\<=/в,
%в"
 К
inputs         E
к "%в"
К
0         @
Ъ y
&__inference_dense_4_layer_call_fn_7918O<=/в,
%в"
 К
inputs         E
к "К         @б
A__inference_dense_5_layer_call_and_return_conditional_losses_7929\BC/в,
%в"
 К
inputs         @
к "%в"
К
0         @
Ъ y
&__inference_dense_5_layer_call_fn_7938OBC/в,
%в"
 К
inputs         @
к "К         @б
A__inference_dense_6_layer_call_and_return_conditional_losses_7949\HI/в,
%в"
 К
inputs         @
к "%в"
К
0         @
Ъ y
&__inference_dense_6_layer_call_fn_7958OHI/в,
%в"
 К
inputs         @
к "К         @б
A__inference_dense_7_layer_call_and_return_conditional_losses_7969\NO/в,
%в"
 К
inputs         @
к "%в"
К
0         @
Ъ y
&__inference_dense_7_layer_call_fn_7978ONO/в,
%в"
 К
inputs         @
к "К         @б
A__inference_dense_8_layer_call_and_return_conditional_losses_7989\TU/в,
%в"
 К
inputs         @
к "%в"
К
0         @
Ъ y
&__inference_dense_8_layer_call_fn_7998OTU/в,
%в"
 К
inputs         @
к "К         @б
A__inference_dense_9_layer_call_and_return_conditional_losses_8009\Z[/в,
%в"
 К
inputs         @
к "%в"
К
0         @
Ъ y
&__inference_dense_9_layer_call_fn_8018OZ[/в,
%в"
 К
inputs         @
к "К         @б
?__inference_dense_layer_call_and_return_conditional_losses_7816^0в-
&в#
!К
inputs         ▄	
к "&в#
К
0         А
Ъ y
$__inference_dense_layer_call_fn_7825Q0в-
&в#
!К
inputs         ▄	
к "К         Аё
F__inference_functional_1_layer_call_and_return_conditional_losses_6695жqБ !&'./23<=BCHINOTUZ[`a`в]
VвS
IЪF
!К
input_2         
!К
input_1         
p

 
к "%в"
К
0         
Ъ ё
F__inference_functional_1_layer_call_and_return_conditional_losses_6842жqБ !&'./23<=BCHINOTUZ[`a`в]
VвS
IЪF
!К
input_2         
!К
input_1         
p 

 
к "%в"
К
0         
Ъ є
F__inference_functional_1_layer_call_and_return_conditional_losses_7518иqБ !&'./23<=BCHINOTUZ[`abв_
XвU
KЪH
"К
inputs/0         
"К
inputs/1         
p

 
к "%в"
К
0         
Ъ є
F__inference_functional_1_layer_call_and_return_conditional_losses_7688иqБ !&'./23<=BCHINOTUZ[`abв_
XвU
KЪH
"К
inputs/0         
"К
inputs/1         
p 

 
к "%в"
К
0         
Ъ ╔
+__inference_functional_1_layer_call_fn_7048ЩqБ !&'./23<=BCHINOTUZ[`a`в]
VвS
IЪF
!К
input_2         
!К
input_1         
p

 
к "К         ╔
+__inference_functional_1_layer_call_fn_7253ЩqБ !&'./23<=BCHINOTUZ[`a`в]
VвS
IЪF
!К
input_2         
!К
input_1         
p 

 
к "К         ╦
+__inference_functional_1_layer_call_fn_7746ЫqБ !&'./23<=BCHINOTUZ[`abв_
XвU
KЪH
"К
inputs/0         
"К
inputs/1         
p

 
к "К         ╦
+__inference_functional_1_layer_call_fn_7804ЫqБ !&'./23<=BCHINOTUZ[`abв_
XвU
KЪH
"К
inputs/0         
"К
inputs/1         
p 

 
к "К         Г
__inference_restore_fn_8079dqVвS
LвI
(К%
restored_tensors_0         
К
restored_tensors_1	
к "К Ю
__inference_save_fn_8071Бq&в#
в
К
checkpoint_key 
к "╙Ъ╧
kкh

nameК
0/name 
#

slice_specК
0/slice_spec 
(
tensorК
0/tensor         
`к]

nameК
1/name 
#

slice_specК
1/slice_spec 

tensorК
1/tensor	ф
"__inference_signature_wrapper_7321╜qБ !&'./23<=BCHINOTUZ[`aiвf
в 
_к\
,
input_1!К
input_1         
,
input_2!К
input_2         "3к0
.
dense_10"К
dense_10         