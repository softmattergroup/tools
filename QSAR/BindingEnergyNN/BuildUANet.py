import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.layers.experimental import preprocessing
import pandas as pd
from tensorflow.keras.layers.experimental.preprocessing import TextVectorization
from tensorflow.keras.backend import cast 
from tensorflow import strings
from tensorflow.strings import bytes_split

#from tensorflow.keras.layers import Merge


def plot_loss(history):
  plt.plot(history.history['loss'], label='loss')
  plt.plot(history.history['val_loss'], label='val_loss')
  plt.xlabel('Epoch')
  plt.ylabel('Error (mean abs.)')
  plt.legend()
  plt.grid(True)

@tf.keras.utils.register_keras_serializable()
def charsplit(word):
    return tf.strings.bytes_split(word)
    #return strings.split(tf.map_fn(lambda i: i +" ", word)).values



#dataSetRaw  = pd.read_csv("data/ProteinDescriptorsEnergies.csv", usecols=['materialvar1','materialvar2','shape','radius','zp','Residues', 'ProbabilityOfExpressionInInclusionBodies',  'TinyMole', 'SmallMole', 'AliphaticMole', 'AromaticMole', 'PolarMole','BasicMole', 'AcidicMole',   'energy'])

dataSetRaw = pd.read_csv("data/TrainingSet111220.csv")

targetValue="energy"
numericalVarSet =[  'materialvar1', 'materialvar2', 'shape' ,'radius', 'zp']

#log transform radii
dataSetRaw['radius'] = np.where( dataSetRaw.radius==0,0.5   ,dataSetRaw.radius )
dataSetRaw['radius'] = dataSetRaw['radius'].apply(np.log)

#apply the Hamaker normalisation to each energy
dataSetRaw['energy'] = dataSetRaw['energy'].div(dataSetRaw['materialvar3'])


#print(dataSetRaw['radius'])
dataset = dataSetRaw.copy()
print(dataset.tail())




#separate out a testing dataset
train_dataset = dataset.sample(frac=0.9, random_state = 0)
test_dataset = dataset.drop(train_dataset.index)

train_features = train_dataset.copy()
test_features = test_dataset.copy()

train_labels = train_features.pop(targetValue)
test_labels = test_features.pop(targetValue)


#define a normalise and set it up based on the training data
print("Defining numerical normaliser")
normalizer = preprocessing.Normalization()
normalizer.adapt(np.array(train_features[numericalVarSet]))
#normalizer = layers.Input(shape=(1,))
print("Defining text vectorizer")
int_vectorize_layer = TextVectorization(max_tokens = 30, output_mode='int', output_sequence_length = 1500, split=charsplit)
int_vectorize_layer.adapt(np.array(train_features['Sequence']))

print(int_vectorize_layer(train_features['Sequence']))
print(train_features['Sequence'])


print("building model")
stringinput = keras.Input(shape=(1,),dtype="string")
stringparsed = int_vectorize_layer(stringinput)
print(stringparsed)

stringparsednumeric = layers.Dense(128,activation="relu")(stringparsed)
stringparsednumeric = layers.Dense(128,activation="relu")(stringparsednumeric)
stringparsednumeric = layers.Dense(64,activation="relu")(stringparsednumeric)
#stringparsednumeric = layers.Dense(64,activation="relu")(stringparsednumeric)


stringparsednumeric = layers.Dense(64,activation="relu")(stringparsednumeric)

numNumericalInputs = len(numericalVarSet)
numinput = keras.Input(shape=(numNumericalInputs,))
numparsed = normalizer(numinput)


inputs = [numinput,stringinput]

combinedLayer = layers.Concatenate()([numparsed,stringparsednumeric])
x = layers.Dense(128, activation="relu")(combinedLayer)
x = layers.Dense(64, activation="relu")(x)
x = layers.Dense(64, activation="relu")(x)

x = layers.Dense(64, activation="relu")(x)
#x = layers.Dense(64, activation="relu")(x)
#x = layers.Dense(64, activation="relu")(x)
output = layers.Dense(1)(x)
ann_model = keras.Model(inputs = inputs, outputs=output)

print("Compiling model")
ann_model.compile( optimizer = tf.optimizers.Adam(learning_rate=0.0001), loss='mean_absolute_error')

print("Fitting model")
numEpochs = 400

history = ann_model.fit([ train_features[numericalVarSet], train_features['Sequence']  ],train_labels,epochs=numEpochs,verbose=0,validation_split=0.2)


ann_model.save('model_saves/sequence_e'+str(numEpochs)+'_4seq4combined-logr-2p1at128-hamnorm-aa')

hist = pd.DataFrame(history.history)
hist['epoch'] = history.epoch
print(hist.tail())

plot_loss(history)

trainPredictions = ann_model.predict([train_features[numericalVarSet],train_features['Sequence'] ]  )
testPredictions = ann_model.predict([test_features[numericalVarSet],test_features['Sequence'] ]  )

print(train_features['materialvar3'])
print(trainPredictions)

trainPredictionsKBT = train_features['materialvar3'].mul(np.array(trainPredictions).flatten())
testPredictionsKBT =  test_features['materialvar3'].mul(np.array(testPredictions).flatten())
trainLabelKBT = train_features['materialvar3'].mul(train_labels)
testLabelKBT = test_features['materialvar3'].mul(test_labels)


#print(testLabelKBT - testPredictionsKBT)


plt.figure()
plt.scatter(trainPredictions,train_labels)
plt.scatter(testPredictions,test_labels)
plt.xlabel('EAds-ANN [Hamaker]')
plt.ylabel('EAds-UA [Hamaker]]')
#plt.plot(xrange,ypredict,'k-')


#trainPredictionsKBT = trainPredictions*train_features['materialvar3']
#testPredictionsKBT =  testPredictions*test_features['materialvar3']
plt.figure()
plt.scatter( trainPredictionsKBT, trainLabelKBT)
plt.scatter( testPredictionsKBT, testLabelKBT)
plt.xlabel('EAds-ANN [kbT]')
plt.ylabel('EAds-UA [kbT]')

plt.show()
