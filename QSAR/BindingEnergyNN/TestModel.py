import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.layers.experimental import preprocessing
import pandas as pd
from tensorflow.keras.layers.experimental.preprocessing import TextVectorization
from tensorflow.keras.backend import cast 
from tensorflow import strings
from tensorflow.strings import bytes_split
from tensorflow.keras.utils import plot_model


def charsplit(word):
    return bytes_split(word)
    #return strings.split(tf.map_fn(lambda i: i +" ", word)).values

def logradius(datasetin):
    datasetin['radius'] = np.where( datasetin.radius==0,0.5   ,datasetin.radius )
    datasetin['radius'] = datasetin['radius'].apply(np.log)

with tf.keras.utils.custom_object_scope({'Custom>charsplit':charsplit}):
    ann_model = tf.keras.models.load_model('model_saves/sequence_e400_4seq4combined-logr-2p1at128-hamnorm-aa')
#print(ann_model.summary())

dataSetRaw = pd.read_csv("data/TrainingSetDataAA.csv")

#data preprocessing: log-stransform the radius, scale energies by the material average Hamaker constant
logradius(dataSetRaw)
dataSetRaw['energy'] = dataSetRaw['energy'].div(dataSetRaw['materialvar3'])

dataset = dataSetRaw.copy()
dataVals = dataset.pop("energy")

numericalVarSet =[  'materialvar1', 'materialvar2', 'shape' ,'radius', 'zp']

testingValues = pd.read_csv("data/TestDataCarbonBlackSpheres.csv")

#print(testingValues)
print(testingValues['radius'])
logradius(testingValues)
print(testingValues['radius'])
testingValues['energy'] = testingValues['energy'].div(testingValues['materialvar3'])

#print(testingValues)

allRadii=testingValues['radius'].drop_duplicates()
#for radiusVal in allRadii:
#    print(radiusVal)
#    subset = predictionSetTest['radius']==radiusVal
#    print(subset)



plot_model(ann_model, "model_fig.png",dpi=600)

testingEnergy = testingValues.pop("energy")
predictionSetTest =  ann_model.predict(  [testingValues[numericalVarSet],testingValues['Sequence']]  )



#np.savetxt("prediction_output.csv",    predictionSetTest)

radiiSampleRange = [5,10,15,20,25,50,100,150,200]
for radiiTest in radiiSampleRange:
    res = ann_model.predict( [  np.array([      [-7.34161,0.517131, 1, np.log(radiiTest),-25]   ]) ,np.array([["M"]] , dtype="str")      ]  ) 
    print(radiiTest, res[0][0])



predictionSetTestKBT = testingValues['materialvar3'].mul(np.array(predictionSetTest).flatten())
testingEnergyKBT = testingValues['materialvar3'].mul( testingEnergy  )

np.savetxt("prediction_output_uanet.csv",    predictionSetTestKBT)
np.savetxt("prediction_output_ua.csv",    testingEnergyKBT)

dataValsKBT = dataset['materialvar3'].mul(dataVals)

trainingDataPrediction = ann_model.predict([ dataset[numericalVarSet],dataset['Sequence'] ]  )
trainingDataPredictionKBT = dataset['materialvar3'].mul( np.array(trainingDataPrediction).flatten() )

print( "Training correlation: ", np.corrcoef(trainingDataPredictionKBT.to_numpy() , dataValsKBT.to_numpy() ))

print( "Testing correlation: ", np.corrcoef(testingEnergyKBT.to_numpy() , predictionSetTestKBT.to_numpy() ))


#print("Training correlation:" , dataValsKBT.corrwith(trainingDataPredictionKBT))
#print("Testing set correlation: ", testingEnergyKBT.corrwith(predictionSetTestKBT) )

print( (dataValsKBT - trainingDataPredictionKBT).abs().mean())
print(  (testingEnergyKBT - predictionSetTestKBT).abs().mean())

#print( predictionSetTest)

plt.figure()
plt.scatter(trainingDataPredictionKBT,dataValsKBT, label='Training proteins')
#plt.scatter(ann_model.predict([test_features[numericalVarSet],test_features['Sequence'] ]),test_labels)
plt.scatter(predictionSetTestKBT,testingEnergyKBT, label='Testing proteins')
plt.xlabel('EAds-ANN [$k_BT$]')
plt.ylabel('EAds-UA  [$k_BT$]')



plt.figure()
plt.scatter(trainingDataPredictionKBT,dataValsKBT, label='Training proteins')
#plt.scatter(ann_model.predict([test_features[numericalVarSet],test_features['Sequence'] ]),test_labels)
#plt.scatter(predictionSetTestKBT,testingEnergyKBT, label='Testing proteins')
plt.xlabel('EAds-ANN [$k_BT$]')
plt.ylabel('EAds-UA  [$k_BT$]')



#print(testingValues['radius'].drop_duplicates())

plt.figure()
#print allRadii
for radiusVal in allRadii.sort_values():
    subset = testingValues['radius']==radiusVal
    #print(subset)
    plt.scatter(predictionSetTestKBT[subset],testingEnergyKBT[subset], label='R='+str(  np.round(  np.exp(radiusVal))))

#plt.scatter(ann_model.predict([ dataset[numericalVarSet],dataset['Sequence'] ]  ),dataVals, label='Training proteins', alpha=0.0)

#plt.scatter(predictionSetTest,testingEnergy, label='Testing proteins')
plt.xlabel('EAds-ANN [$k_BT$]')
plt.ylabel('EAds-UA  [$k_BT$]')
plt.show()



