#include <cmath>
#include <string>
#include <fstream>
#include <iostream>
#include <string_view>

void leftPadString(std::string& str, const char padChar, const std::size_t desiredLength) {
    std::size_t pos = str.find('.');
    str = str.substr(0, pos + 4);
    str.insert(std::cbegin(str), desiredLength - str.size(), padChar);
}

int main (const int argc, const char * argv[]) {

    if (argc != 4) {
        std::cout << "Error: Invalid number of input arguments\n";
        std::cout << "Usage: ./rotate <pdbfile> <phi> <theta>\n";
        std::cout << "Usage:\n";
        std::cout << "Usage:        angles must be in degrees\n";
        return EXIT_FAILURE;
    }

    std::ifstream handle (argv[1]);

    if (!handle.is_open()) {
        std::cout << "Error: Could not open file '" << argv[1] << "'\n";
        return EXIT_FAILURE;
    }

    double phi, theta;

    try {
        phi   = std::stod(argv[2]);
        theta = std::stod(argv[3]);
    }
    catch (std::invalid_argument& ia) {
        std::cout << "Error: Could not convert either '" << argv[2] << "' or '" << argv[3] << "' to double\n";
        return EXIT_FAILURE;
    }

	phi   = -1.0 * phi  * (M_PI / 180.0);
    theta = (180.0 - theta) * (M_PI / 180.0);
 
 	double rotate[3][3]
  	{
    	{ std::cos(theta) * std::cos(phi)       , -1.0 * std::cos(theta) * std::sin(phi), std::sin(theta) },
    	{ std::sin(phi)                         , std::cos(phi)                         , 0.0             },
   		{ -1.0 * std::sin(theta) * std::cos(phi), std::sin(theta) * std::sin(phi)       , std::cos(theta) }
	};

    std::string line;

    while (std::getline (handle, line)) {
        if (std::string_view(line).substr(0, 4) == "ATOM" || std::string_view(line).substr(0, 6) == "HETATM") {
            
			const double x = std::stod(line.substr(30, 8));
            const double y = std::stod(line.substr(38, 8));
            const double z = std::stod(line.substr(46, 8));

        	const double x2 = x * rotate[0][0] + y * rotate[0][1] + z * rotate[0][2];
          	const double y2 = x * rotate[1][0] + y * rotate[1][1] + z * rotate[1][2];
        	const double z2 = x * rotate[2][0] + y * rotate[2][1] + z * rotate[2][2];

			std::string xstr = std::to_string(x2);	
			std::string ystr = std::to_string(y2);	
			std::string zstr = std::to_string(z2);	

			leftPadString(xstr, ' ', 8); 
			leftPadString(ystr, ' ', 8); 
			leftPadString(zstr, ' ', 8); 

			line.replace(30, 8, xstr);
			line.replace(38, 8, ystr);
			line.replace(46, 8, zstr);
		}       
		
		std::cout << line << "\n";
    }

    handle.close();

    return EXIT_SUCCESS;
}
