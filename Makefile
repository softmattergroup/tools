CXX=g++
CXXFLAGS=-O3 -mtune=native -march=native -std=c++17 -Wall

all : pdb2fasta rotate

pdb2fasta : src/pdb2fasta.cpp
	$(CXX) -o pdb2fasta src/pdb2fasta.cpp $(CXXFLAGS)

rotate : src/rotate_pdb.cpp
	$(CXX) -o rotate src/rotate_pdb.cpp $(CXXFLAGS)

clean :
	rm -f pdb2fasta rotate
