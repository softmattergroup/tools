#! /bin/bash

# Check for curl
if [ "x$(which curl)" == "x" ]
then
    echo "Error: Could not detect curl which is required. Please run the following:"
    echo "Error:"
    echo "Error:        sudo apt install curl"
    echo "Error:"
    exit
fi

# Check for no args
if [ "x${1}" == "x" ]
then
    echo "Usage: ./downloadPDBs <list.txt> [destination]"
    echo "Usage:"
    echo "Usage:    list.txt must be a file containing the PDB names of the desired proteins in a single column e.g."
    echo "Usage:"
    echo "Usage:    1N5U"
    echo "Usage:    2HAV"
    echo "Usage:    3GHG"
    echo "Usage:"
    echo "Usage:    destination an optional parameter and it is the folder where the PDB files will be downloaded to"
    echo "Usage:"
    exit
fi

# Check first arg exists
if [ ! -f "${1}" ]
then
    echo "Error: '${1}' is not a valid file"
    exit
fi

# Check second arg exists
if [ "x${2}" == "x" ]
then
    DESTINATION="."
elif [ ! -d "${2}" ]
then
    echo "Error: '${2}' is not a valid directory"
    exit
else
    DESTINATION=${2}
fi

LIST=$(awk '{print $1}' ${1})

for PDB in ${LIST[@]}
do
    echo "Info: Downloading ${PDB}"

    FILE="${DESTINATION}/${PDB}.pdb"

    curl --silent -L "https://files.rcsb.org/download/${PDB^^}.pdb" > ${FILE}

    # Check and make sure the file downloaded
    if [ ! "x$(grep "404 Not Found" ${FILE})" == "x" ]
    then
        echo "Warning: There was no file named '${PDB}' in the RCSB database"
        rm ${FILE}
    fi

done
