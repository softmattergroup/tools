## Tools

### downloadPDBs.sh

Script to download PDB files from RCSB website.

The script takes two arguments. The first is a text file containing a list of all the PDB names you wish to download in a single column (this argument is mandatory). The second argument is the destination directory where the files will be downloaded (this is optional, the default value is .)

## pdb2fasta

Extract a fasta file from a PDB file.

The accepts a single PDB file as an input and prints an equivalent fasta file to stdout. Use -c argument to print number of amino acids in the PDB file.

pdb2fasta needs to be compiled before first used. Simple run the command 'make' from the tools directory.
