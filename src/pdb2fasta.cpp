#include <vector>
#include <fstream>
#include <iostream>
#include <unordered_map>

// Legal Characters
// ACDEFGHIKLMNPQRSTVWY

const std::unordered_map<std::string, char> amino_acid_to_seq = {
    { "ALA", 'A' },
    { "CYS", 'C' },
    { "ASP", 'D' },
    { "GLU", 'E' },
    { "PHE", 'F' },
    { "GLY", 'G' },
    { "HIS", 'H' },
    { "ILE", 'I' },
    { "LYS", 'K' },
    { "LEU", 'L' },
    { "MET", 'M' },
    { "ASN", 'N' },
    { "PRO", 'P' },
    { "GLN", 'Q' },
    { "ARG", 'R' },
    { "SER", 'S' },
    { "THR", 'T' },
    { "SEC", 'U' },
    { "VAL", 'V' },
    { "TRP", 'W' },
    { "TYR", 'Y' }
};

/*std::size_t split(const std::string& line, std::vector<std::string>& elements) {
    std::size_t first = 0, second;
    do {
        second = line.find(' ', first);
        if (std::isalnum(line[first]))
            elements.emplace_back(line.substr(first, second - first));
        first = second + 1;
    } while (second != std::string::npos);
    return elements.size();
}*/

void process_pdb(const char* filename, bool count) {
    std::ifstream handle(filename);
    if (!handle.is_open()) {
        std::cout << "Could not find file '" << filename << "'\n";
        return;
    }
    std::string seq = "";
    std::string line;
    bool fail = false;
    while (std::getline(handle, line)) {
        if (line.substr(0, 4) == "ATOM" && line.substr(13, 2) == "CA") {
            try {
                seq += amino_acid_to_seq.at(line.substr(17, 3));
            } catch (const std::out_of_range& oor) {
                fail = true;
                std::cout << "Error: Could not find a match for: " << line.substr(17, 3) << "\n";
            }
        }
    }
    if (fail) {
        std::cout << "Fail\n";
        return;
    }
    if (count)
        std::cout << seq.size() << "\n";
    else
    {
        for (std::size_t i = 80; i < seq.size(); i += 81)
            seq.insert(i, 1, '\n');
        std::cout << ">pdb|parse-from-pdb\n";
        std::cout << seq << "\n";
    }
}

int main(const int argc, const char* argv[]) {
    bool count = false;

    if (argc == 1) { 
        std::cout << "Usage: pdb2fasta [-c] <pdbfile>\n";
        std::cout << "Usage:\n";
        std::cout << "Usage:    Extract a fasta file from a pdb file. The data will be written to stdout\n";
        std::cout << "Usage:\n";
        std::cout << "Usage:    Optional: -c    Print the number of amino acids within the PDB file\n";
        std::cout << "Usage:\n";
    }
    else if (std::string(argv[1]) == "-c")
        count = true;

    for (int i = 1 + count; i < argc; ++i)
        process_pdb(argv[i], count);
}
