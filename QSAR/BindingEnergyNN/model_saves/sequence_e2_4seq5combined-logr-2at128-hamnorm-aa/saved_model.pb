В¤
юЫ
8
Const
output"dtype"
valuetensor"
dtypetype
l
LookupTableExportV2
table_handle
keys"Tkeys
values"Tvalues"
Tkeystype"
Tvaluestypeѕ
е
MutableHashTableV2
table_handle"
	containerstring "
shared_namestring "!
use_node_name_sharingbool( "
	key_dtypetype"
value_dtypetypeѕ

NoOp
│
PartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetypeѕ
Й
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring ѕ
ќ
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 ѕ"serve*2.3.12v2.3.0-54-gfcc4b966f18я░
v
dense/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
▄ђ*
shared_namedense/kernel
o
 dense/kernel/Read/ReadVariableOpReadVariableOpdense/kernel* 
_output_shapes
:
▄ђ*
dtype0
m

dense/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:ђ*
shared_name
dense/bias
f
dense/bias/Read/ReadVariableOpReadVariableOp
dense/bias*
_output_shapes	
:ђ*
dtype0
z
dense_1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
ђђ*
shared_namedense_1/kernel
s
"dense_1/kernel/Read/ReadVariableOpReadVariableOpdense_1/kernel* 
_output_shapes
:
ђђ*
dtype0
q
dense_1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:ђ*
shared_namedense_1/bias
j
 dense_1/bias/Read/ReadVariableOpReadVariableOpdense_1/bias*
_output_shapes	
:ђ*
dtype0
y
dense_2/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	ђ@*
shared_namedense_2/kernel
r
"dense_2/kernel/Read/ReadVariableOpReadVariableOpdense_2/kernel*
_output_shapes
:	ђ@*
dtype0
p
dense_2/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*
shared_namedense_2/bias
i
 dense_2/bias/Read/ReadVariableOpReadVariableOpdense_2/bias*
_output_shapes
:@*
dtype0
`
meanVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_namemean
Y
mean/Read/ReadVariableOpReadVariableOpmean*
_output_shapes
:*
dtype0
h
varianceVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_name
variance
a
variance/Read/ReadVariableOpReadVariableOpvariance*
_output_shapes
:*
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0	
x
dense_3/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*
shared_namedense_3/kernel
q
"dense_3/kernel/Read/ReadVariableOpReadVariableOpdense_3/kernel*
_output_shapes

:@@*
dtype0
p
dense_3/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*
shared_namedense_3/bias
i
 dense_3/bias/Read/ReadVariableOpReadVariableOpdense_3/bias*
_output_shapes
:@*
dtype0
x
dense_4/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:E@*
shared_namedense_4/kernel
q
"dense_4/kernel/Read/ReadVariableOpReadVariableOpdense_4/kernel*
_output_shapes

:E@*
dtype0
p
dense_4/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*
shared_namedense_4/bias
i
 dense_4/bias/Read/ReadVariableOpReadVariableOpdense_4/bias*
_output_shapes
:@*
dtype0
x
dense_5/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*
shared_namedense_5/kernel
q
"dense_5/kernel/Read/ReadVariableOpReadVariableOpdense_5/kernel*
_output_shapes

:@@*
dtype0
p
dense_5/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*
shared_namedense_5/bias
i
 dense_5/bias/Read/ReadVariableOpReadVariableOpdense_5/bias*
_output_shapes
:@*
dtype0
x
dense_6/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*
shared_namedense_6/kernel
q
"dense_6/kernel/Read/ReadVariableOpReadVariableOpdense_6/kernel*
_output_shapes

:@@*
dtype0
p
dense_6/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*
shared_namedense_6/bias
i
 dense_6/bias/Read/ReadVariableOpReadVariableOpdense_6/bias*
_output_shapes
:@*
dtype0
x
dense_7/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*
shared_namedense_7/kernel
q
"dense_7/kernel/Read/ReadVariableOpReadVariableOpdense_7/kernel*
_output_shapes

:@@*
dtype0
p
dense_7/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*
shared_namedense_7/bias
i
 dense_7/bias/Read/ReadVariableOpReadVariableOpdense_7/bias*
_output_shapes
:@*
dtype0
x
dense_8/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*
shared_namedense_8/kernel
q
"dense_8/kernel/Read/ReadVariableOpReadVariableOpdense_8/kernel*
_output_shapes

:@@*
dtype0
p
dense_8/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*
shared_namedense_8/bias
i
 dense_8/bias/Read/ReadVariableOpReadVariableOpdense_8/bias*
_output_shapes
:@*
dtype0
x
dense_9/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@*
shared_namedense_9/kernel
q
"dense_9/kernel/Read/ReadVariableOpReadVariableOpdense_9/kernel*
_output_shapes

:@*
dtype0
p
dense_9/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_namedense_9/bias
i
 dense_9/bias/Read/ReadVariableOpReadVariableOpdense_9/bias*
_output_shapes
:*
dtype0
f
	Adam/iterVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	Adam/iter
_
Adam/iter/Read/ReadVariableOpReadVariableOp	Adam/iter*
_output_shapes
: *
dtype0	
j
Adam/beta_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_1
c
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_output_shapes
: *
dtype0
j
Adam/beta_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_2
c
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
_output_shapes
: *
dtype0
h

Adam/decayVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name
Adam/decay
a
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_output_shapes
: *
dtype0
x
Adam/learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameAdam/learning_rate
q
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*
_output_shapes
: *
dtype0
є
string_lookup_index_tableMutableHashTableV2*
_output_shapes
: *
	key_dtype0*
shared_name
table_26*
value_dtype0	
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
b
count_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	count_1
[
count_1/Read/ReadVariableOpReadVariableOpcount_1*
_output_shapes
: *
dtype0
ё
Adam/dense/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:
▄ђ*$
shared_nameAdam/dense/kernel/m
}
'Adam/dense/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense/kernel/m* 
_output_shapes
:
▄ђ*
dtype0
{
Adam/dense/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:ђ*"
shared_nameAdam/dense/bias/m
t
%Adam/dense/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense/bias/m*
_output_shapes	
:ђ*
dtype0
ѕ
Adam/dense_1/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:
ђђ*&
shared_nameAdam/dense_1/kernel/m
Ђ
)Adam/dense_1/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_1/kernel/m* 
_output_shapes
:
ђђ*
dtype0

Adam/dense_1/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:ђ*$
shared_nameAdam/dense_1/bias/m
x
'Adam/dense_1/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_1/bias/m*
_output_shapes	
:ђ*
dtype0
Є
Adam/dense_2/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:	ђ@*&
shared_nameAdam/dense_2/kernel/m
ђ
)Adam/dense_2/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_2/kernel/m*
_output_shapes
:	ђ@*
dtype0
~
Adam/dense_2/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_2/bias/m
w
'Adam/dense_2/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_2/bias/m*
_output_shapes
:@*
dtype0
є
Adam/dense_3/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*&
shared_nameAdam/dense_3/kernel/m

)Adam/dense_3/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_3/kernel/m*
_output_shapes

:@@*
dtype0
~
Adam/dense_3/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_3/bias/m
w
'Adam/dense_3/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_3/bias/m*
_output_shapes
:@*
dtype0
є
Adam/dense_4/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:E@*&
shared_nameAdam/dense_4/kernel/m

)Adam/dense_4/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_4/kernel/m*
_output_shapes

:E@*
dtype0
~
Adam/dense_4/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_4/bias/m
w
'Adam/dense_4/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_4/bias/m*
_output_shapes
:@*
dtype0
є
Adam/dense_5/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*&
shared_nameAdam/dense_5/kernel/m

)Adam/dense_5/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_5/kernel/m*
_output_shapes

:@@*
dtype0
~
Adam/dense_5/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_5/bias/m
w
'Adam/dense_5/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_5/bias/m*
_output_shapes
:@*
dtype0
є
Adam/dense_6/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*&
shared_nameAdam/dense_6/kernel/m

)Adam/dense_6/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_6/kernel/m*
_output_shapes

:@@*
dtype0
~
Adam/dense_6/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_6/bias/m
w
'Adam/dense_6/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_6/bias/m*
_output_shapes
:@*
dtype0
є
Adam/dense_7/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*&
shared_nameAdam/dense_7/kernel/m

)Adam/dense_7/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_7/kernel/m*
_output_shapes

:@@*
dtype0
~
Adam/dense_7/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_7/bias/m
w
'Adam/dense_7/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_7/bias/m*
_output_shapes
:@*
dtype0
є
Adam/dense_8/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*&
shared_nameAdam/dense_8/kernel/m

)Adam/dense_8/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_8/kernel/m*
_output_shapes

:@@*
dtype0
~
Adam/dense_8/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_8/bias/m
w
'Adam/dense_8/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_8/bias/m*
_output_shapes
:@*
dtype0
є
Adam/dense_9/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@*&
shared_nameAdam/dense_9/kernel/m

)Adam/dense_9/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_9/kernel/m*
_output_shapes

:@*
dtype0
~
Adam/dense_9/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*$
shared_nameAdam/dense_9/bias/m
w
'Adam/dense_9/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_9/bias/m*
_output_shapes
:*
dtype0
ё
Adam/dense/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:
▄ђ*$
shared_nameAdam/dense/kernel/v
}
'Adam/dense/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense/kernel/v* 
_output_shapes
:
▄ђ*
dtype0
{
Adam/dense/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:ђ*"
shared_nameAdam/dense/bias/v
t
%Adam/dense/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense/bias/v*
_output_shapes	
:ђ*
dtype0
ѕ
Adam/dense_1/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:
ђђ*&
shared_nameAdam/dense_1/kernel/v
Ђ
)Adam/dense_1/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_1/kernel/v* 
_output_shapes
:
ђђ*
dtype0

Adam/dense_1/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:ђ*$
shared_nameAdam/dense_1/bias/v
x
'Adam/dense_1/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_1/bias/v*
_output_shapes	
:ђ*
dtype0
Є
Adam/dense_2/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:	ђ@*&
shared_nameAdam/dense_2/kernel/v
ђ
)Adam/dense_2/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_2/kernel/v*
_output_shapes
:	ђ@*
dtype0
~
Adam/dense_2/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_2/bias/v
w
'Adam/dense_2/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_2/bias/v*
_output_shapes
:@*
dtype0
є
Adam/dense_3/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*&
shared_nameAdam/dense_3/kernel/v

)Adam/dense_3/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_3/kernel/v*
_output_shapes

:@@*
dtype0
~
Adam/dense_3/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_3/bias/v
w
'Adam/dense_3/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_3/bias/v*
_output_shapes
:@*
dtype0
є
Adam/dense_4/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:E@*&
shared_nameAdam/dense_4/kernel/v

)Adam/dense_4/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_4/kernel/v*
_output_shapes

:E@*
dtype0
~
Adam/dense_4/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_4/bias/v
w
'Adam/dense_4/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_4/bias/v*
_output_shapes
:@*
dtype0
є
Adam/dense_5/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*&
shared_nameAdam/dense_5/kernel/v

)Adam/dense_5/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_5/kernel/v*
_output_shapes

:@@*
dtype0
~
Adam/dense_5/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_5/bias/v
w
'Adam/dense_5/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_5/bias/v*
_output_shapes
:@*
dtype0
є
Adam/dense_6/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*&
shared_nameAdam/dense_6/kernel/v

)Adam/dense_6/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_6/kernel/v*
_output_shapes

:@@*
dtype0
~
Adam/dense_6/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_6/bias/v
w
'Adam/dense_6/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_6/bias/v*
_output_shapes
:@*
dtype0
є
Adam/dense_7/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*&
shared_nameAdam/dense_7/kernel/v

)Adam/dense_7/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_7/kernel/v*
_output_shapes

:@@*
dtype0
~
Adam/dense_7/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_7/bias/v
w
'Adam/dense_7/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_7/bias/v*
_output_shapes
:@*
dtype0
є
Adam/dense_8/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*&
shared_nameAdam/dense_8/kernel/v

)Adam/dense_8/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_8/kernel/v*
_output_shapes

:@@*
dtype0
~
Adam/dense_8/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_8/bias/v
w
'Adam/dense_8/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_8/bias/v*
_output_shapes
:@*
dtype0
є
Adam/dense_9/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@*&
shared_nameAdam/dense_9/kernel/v

)Adam/dense_9/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_9/kernel/v*
_output_shapes

:@*
dtype0
~
Adam/dense_9/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*$
shared_nameAdam/dense_9/bias/v
w
'Adam/dense_9/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_9/bias/v*
_output_shapes
:*
dtype0
G
ConstConst*
_output_shapes
: *
dtype0	*
value	B	 R
У
PartitionedCallPartitionedCall*	
Tin
 *
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8ѓ *"
fR
__inference_<lambda>_7887

NoOpNoOp^PartitionedCall
Э
Hstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2LookupTableExportV2string_lookup_index_table*
Tkeys0*
Tvalues0	*,
_class"
 loc:@string_lookup_index_table*2
_output_shapes 
:         :         
ўi
Const_1Const"/device:CPU:0*
_output_shapes
: *
dtype0*Лh
valueКhB─h Bйh
з
layer-0
layer_with_weights-0
layer-1
layer_with_weights-1
layer-2
layer_with_weights-2
layer-3
layer-4
layer_with_weights-3
layer-5
layer_with_weights-4
layer-6
layer_with_weights-5
layer-7
	layer-8

layer_with_weights-6

layer-9
layer_with_weights-7
layer-10
layer_with_weights-8
layer-11
layer_with_weights-9
layer-12
layer_with_weights-10
layer-13
layer_with_weights-11
layer-14
	optimizer
regularization_losses
trainable_variables
	variables
	keras_api

signatures
 
=
state_variables
_index_lookup_layer
	keras_api
h

kernel
bias
regularization_losses
trainable_variables
	variables
	keras_api
h

kernel
 bias
!regularization_losses
"trainable_variables
#	variables
$	keras_api
 
h

%kernel
&bias
'regularization_losses
(trainable_variables
)	variables
*	keras_api
]
+state_variables
,_broadcast_shape
-mean
.variance
	/count
0	keras_api
h

1kernel
2bias
3regularization_losses
4trainable_variables
5	variables
6	keras_api
R
7regularization_losses
8trainable_variables
9	variables
:	keras_api
h

;kernel
<bias
=regularization_losses
>trainable_variables
?	variables
@	keras_api
h

Akernel
Bbias
Cregularization_losses
Dtrainable_variables
E	variables
F	keras_api
h

Gkernel
Hbias
Iregularization_losses
Jtrainable_variables
K	variables
L	keras_api
h

Mkernel
Nbias
Oregularization_losses
Ptrainable_variables
Q	variables
R	keras_api
h

Skernel
Tbias
Uregularization_losses
Vtrainable_variables
W	variables
X	keras_api
h

Ykernel
Zbias
[regularization_losses
\trainable_variables
]	variables
^	keras_api
л
_iter

`beta_1

abeta_2
	bdecay
clearning_ratemеmЕmф mФ%mг&mГ1m«2m»;m░<m▒Am▓Bm│Gm┤HmхMmХNmиSmИTm╣Ym║Zm╗v╝vйvЙ v┐%v└&v┴1v┬2v├;v─<v┼AvкBvКGv╚Hv╔Mv╩Nv╦Sv╠Tv═Yv╬Zv¤
 
ќ
0
1
2
 3
%4
&5
16
27
;8
<9
A10
B11
G12
H13
M14
N15
S16
T17
Y18
Z19
»
1
2
3
 4
%5
&6
-7
.8
/9
110
211
;12
<13
A14
B15
G16
H17
M18
N19
S20
T21
Y22
Z23
Г

dlayers
regularization_losses
trainable_variables
elayer_regularization_losses
fnon_trainable_variables
glayer_metrics
	variables
hmetrics
 
 
0
istate_variables

j_table
k	keras_api
 
XV
VARIABLE_VALUEdense/kernel6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUE
TR
VARIABLE_VALUE
dense/bias4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUE
 

0
1

0
1
Г

llayers
regularization_losses
trainable_variables
mlayer_regularization_losses
nnon_trainable_variables
olayer_metrics
	variables
pmetrics
ZX
VARIABLE_VALUEdense_1/kernel6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEdense_1/bias4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUE
 

0
 1

0
 1
Г

qlayers
!regularization_losses
"trainable_variables
rlayer_regularization_losses
snon_trainable_variables
tlayer_metrics
#	variables
umetrics
ZX
VARIABLE_VALUEdense_2/kernel6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEdense_2/bias4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUE
 

%0
&1

%0
&1
Г

vlayers
'regularization_losses
(trainable_variables
wlayer_regularization_losses
xnon_trainable_variables
ylayer_metrics
)	variables
zmetrics
#
-mean
.variance
	/count
 
NL
VARIABLE_VALUEmean4layer_with_weights-4/mean/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEvariance8layer_with_weights-4/variance/.ATTRIBUTES/VARIABLE_VALUE
PN
VARIABLE_VALUEcount5layer_with_weights-4/count/.ATTRIBUTES/VARIABLE_VALUE
 
ZX
VARIABLE_VALUEdense_3/kernel6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEdense_3/bias4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUE
 

10
21

10
21
Г

{layers
3regularization_losses
4trainable_variables
|layer_regularization_losses
}non_trainable_variables
~layer_metrics
5	variables
metrics
 
 
 
▓
ђlayers
7regularization_losses
8trainable_variables
 Ђlayer_regularization_losses
ѓnon_trainable_variables
Ѓlayer_metrics
9	variables
ёmetrics
ZX
VARIABLE_VALUEdense_4/kernel6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEdense_4/bias4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUE
 

;0
<1

;0
<1
▓
Ёlayers
=regularization_losses
>trainable_variables
 єlayer_regularization_losses
Єnon_trainable_variables
ѕlayer_metrics
?	variables
Ѕmetrics
ZX
VARIABLE_VALUEdense_5/kernel6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEdense_5/bias4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUE
 

A0
B1

A0
B1
▓
іlayers
Cregularization_losses
Dtrainable_variables
 Іlayer_regularization_losses
їnon_trainable_variables
Їlayer_metrics
E	variables
јmetrics
ZX
VARIABLE_VALUEdense_6/kernel6layer_with_weights-8/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEdense_6/bias4layer_with_weights-8/bias/.ATTRIBUTES/VARIABLE_VALUE
 

G0
H1

G0
H1
▓
Јlayers
Iregularization_losses
Jtrainable_variables
 љlayer_regularization_losses
Љnon_trainable_variables
њlayer_metrics
K	variables
Њmetrics
ZX
VARIABLE_VALUEdense_7/kernel6layer_with_weights-9/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEdense_7/bias4layer_with_weights-9/bias/.ATTRIBUTES/VARIABLE_VALUE
 

M0
N1

M0
N1
▓
ћlayers
Oregularization_losses
Ptrainable_variables
 Ћlayer_regularization_losses
ќnon_trainable_variables
Ќlayer_metrics
Q	variables
ўmetrics
[Y
VARIABLE_VALUEdense_8/kernel7layer_with_weights-10/kernel/.ATTRIBUTES/VARIABLE_VALUE
WU
VARIABLE_VALUEdense_8/bias5layer_with_weights-10/bias/.ATTRIBUTES/VARIABLE_VALUE
 

S0
T1

S0
T1
▓
Ўlayers
Uregularization_losses
Vtrainable_variables
 џlayer_regularization_losses
Џnon_trainable_variables
юlayer_metrics
W	variables
Юmetrics
[Y
VARIABLE_VALUEdense_9/kernel7layer_with_weights-11/kernel/.ATTRIBUTES/VARIABLE_VALUE
WU
VARIABLE_VALUEdense_9/bias5layer_with_weights-11/bias/.ATTRIBUTES/VARIABLE_VALUE
 

Y0
Z1

Y0
Z1
▓
ъlayers
[regularization_losses
\trainable_variables
 Ъlayer_regularization_losses
аnon_trainable_variables
Аlayer_metrics
]	variables
бmetrics
HF
VARIABLE_VALUE	Adam/iter)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_1+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_2+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUE
JH
VARIABLE_VALUE
Adam/decay*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEAdam/learning_rate2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUE
n
0
1
2
3
4
5
6
7
	8

9
10
11
12
13
14
 

-1
.2
/3
 

Б0
 
LJ
tableAlayer_with_weights-0/_index_lookup_layer/_table/.ATTRIBUTES/table
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
8

цtotal

Цcount
д	variables
Д	keras_api
OM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE
QO
VARIABLE_VALUEcount_14keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE

ц0
Ц1

д	variables
{y
VARIABLE_VALUEAdam/dense/kernel/mRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
wu
VARIABLE_VALUEAdam/dense/bias/mPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_1/kernel/mRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_1/bias/mPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_2/kernel/mRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_2/bias/mPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_3/kernel/mRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_3/bias/mPlayer_with_weights-5/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_4/kernel/mRlayer_with_weights-6/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_4/bias/mPlayer_with_weights-6/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_5/kernel/mRlayer_with_weights-7/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_5/bias/mPlayer_with_weights-7/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_6/kernel/mRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_6/bias/mPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_7/kernel/mRlayer_with_weights-9/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_7/bias/mPlayer_with_weights-9/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_8/kernel/mSlayer_with_weights-10/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_8/bias/mQlayer_with_weights-10/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_9/kernel/mSlayer_with_weights-11/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_9/bias/mQlayer_with_weights-11/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
{y
VARIABLE_VALUEAdam/dense/kernel/vRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
wu
VARIABLE_VALUEAdam/dense/bias/vPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_1/kernel/vRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_1/bias/vPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_2/kernel/vRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_2/bias/vPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_3/kernel/vRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_3/bias/vPlayer_with_weights-5/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_4/kernel/vRlayer_with_weights-6/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_4/bias/vPlayer_with_weights-6/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_5/kernel/vRlayer_with_weights-7/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_5/bias/vPlayer_with_weights-7/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_6/kernel/vRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_6/bias/vPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_7/kernel/vRlayer_with_weights-9/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_7/bias/vPlayer_with_weights-9/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_8/kernel/vSlayer_with_weights-10/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_8/bias/vQlayer_with_weights-10/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_9/kernel/vSlayer_with_weights-11/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_9/bias/vQlayer_with_weights-11/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
z
serving_default_input_1Placeholder*'
_output_shapes
:         *
dtype0*
shape:         
z
serving_default_input_2Placeholder*'
_output_shapes
:         *
dtype0*
shape:         
м
StatefulPartitionedCallStatefulPartitionedCallserving_default_input_1serving_default_input_2string_lookup_index_tableConstdense/kernel
dense/biasdense_1/kerneldense_1/biasdense_2/kerneldense_2/biasmeanvariancedense_3/kerneldense_3/biasdense_4/kerneldense_4/biasdense_5/kerneldense_5/biasdense_6/kerneldense_6/biasdense_7/kerneldense_7/biasdense_8/kerneldense_8/biasdense_9/kerneldense_9/bias*%
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *8
_read_only_resource_inputs
	
*-
config_proto

CPU

GPU 2J 8ѓ *+
f&R$
"__inference_signature_wrapper_7166
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
ь
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename dense/kernel/Read/ReadVariableOpdense/bias/Read/ReadVariableOp"dense_1/kernel/Read/ReadVariableOp dense_1/bias/Read/ReadVariableOp"dense_2/kernel/Read/ReadVariableOp dense_2/bias/Read/ReadVariableOpmean/Read/ReadVariableOpvariance/Read/ReadVariableOpcount/Read/ReadVariableOp"dense_3/kernel/Read/ReadVariableOp dense_3/bias/Read/ReadVariableOp"dense_4/kernel/Read/ReadVariableOp dense_4/bias/Read/ReadVariableOp"dense_5/kernel/Read/ReadVariableOp dense_5/bias/Read/ReadVariableOp"dense_6/kernel/Read/ReadVariableOp dense_6/bias/Read/ReadVariableOp"dense_7/kernel/Read/ReadVariableOp dense_7/bias/Read/ReadVariableOp"dense_8/kernel/Read/ReadVariableOp dense_8/bias/Read/ReadVariableOp"dense_9/kernel/Read/ReadVariableOp dense_9/bias/Read/ReadVariableOpAdam/iter/Read/ReadVariableOpAdam/beta_1/Read/ReadVariableOpAdam/beta_2/Read/ReadVariableOpAdam/decay/Read/ReadVariableOp&Adam/learning_rate/Read/ReadVariableOpHstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2Jstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2:1total/Read/ReadVariableOpcount_1/Read/ReadVariableOp'Adam/dense/kernel/m/Read/ReadVariableOp%Adam/dense/bias/m/Read/ReadVariableOp)Adam/dense_1/kernel/m/Read/ReadVariableOp'Adam/dense_1/bias/m/Read/ReadVariableOp)Adam/dense_2/kernel/m/Read/ReadVariableOp'Adam/dense_2/bias/m/Read/ReadVariableOp)Adam/dense_3/kernel/m/Read/ReadVariableOp'Adam/dense_3/bias/m/Read/ReadVariableOp)Adam/dense_4/kernel/m/Read/ReadVariableOp'Adam/dense_4/bias/m/Read/ReadVariableOp)Adam/dense_5/kernel/m/Read/ReadVariableOp'Adam/dense_5/bias/m/Read/ReadVariableOp)Adam/dense_6/kernel/m/Read/ReadVariableOp'Adam/dense_6/bias/m/Read/ReadVariableOp)Adam/dense_7/kernel/m/Read/ReadVariableOp'Adam/dense_7/bias/m/Read/ReadVariableOp)Adam/dense_8/kernel/m/Read/ReadVariableOp'Adam/dense_8/bias/m/Read/ReadVariableOp)Adam/dense_9/kernel/m/Read/ReadVariableOp'Adam/dense_9/bias/m/Read/ReadVariableOp'Adam/dense/kernel/v/Read/ReadVariableOp%Adam/dense/bias/v/Read/ReadVariableOp)Adam/dense_1/kernel/v/Read/ReadVariableOp'Adam/dense_1/bias/v/Read/ReadVariableOp)Adam/dense_2/kernel/v/Read/ReadVariableOp'Adam/dense_2/bias/v/Read/ReadVariableOp)Adam/dense_3/kernel/v/Read/ReadVariableOp'Adam/dense_3/bias/v/Read/ReadVariableOp)Adam/dense_4/kernel/v/Read/ReadVariableOp'Adam/dense_4/bias/v/Read/ReadVariableOp)Adam/dense_5/kernel/v/Read/ReadVariableOp'Adam/dense_5/bias/v/Read/ReadVariableOp)Adam/dense_6/kernel/v/Read/ReadVariableOp'Adam/dense_6/bias/v/Read/ReadVariableOp)Adam/dense_7/kernel/v/Read/ReadVariableOp'Adam/dense_7/bias/v/Read/ReadVariableOp)Adam/dense_8/kernel/v/Read/ReadVariableOp'Adam/dense_8/bias/v/Read/ReadVariableOp)Adam/dense_9/kernel/v/Read/ReadVariableOp'Adam/dense_9/bias/v/Read/ReadVariableOpConst_1*U
TinN
L2J			*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8ѓ *&
f!R
__inference__traced_save_8128
Ы
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenamedense/kernel
dense/biasdense_1/kerneldense_1/biasdense_2/kerneldense_2/biasmeanvariancecountdense_3/kerneldense_3/biasdense_4/kerneldense_4/biasdense_5/kerneldense_5/biasdense_6/kerneldense_6/biasdense_7/kerneldense_7/biasdense_8/kerneldense_8/biasdense_9/kerneldense_9/bias	Adam/iterAdam/beta_1Adam/beta_2
Adam/decayAdam/learning_ratestring_lookup_index_tabletotalcount_1Adam/dense/kernel/mAdam/dense/bias/mAdam/dense_1/kernel/mAdam/dense_1/bias/mAdam/dense_2/kernel/mAdam/dense_2/bias/mAdam/dense_3/kernel/mAdam/dense_3/bias/mAdam/dense_4/kernel/mAdam/dense_4/bias/mAdam/dense_5/kernel/mAdam/dense_5/bias/mAdam/dense_6/kernel/mAdam/dense_6/bias/mAdam/dense_7/kernel/mAdam/dense_7/bias/mAdam/dense_8/kernel/mAdam/dense_8/bias/mAdam/dense_9/kernel/mAdam/dense_9/bias/mAdam/dense/kernel/vAdam/dense/bias/vAdam/dense_1/kernel/vAdam/dense_1/bias/vAdam/dense_2/kernel/vAdam/dense_2/bias/vAdam/dense_3/kernel/vAdam/dense_3/bias/vAdam/dense_4/kernel/vAdam/dense_4/bias/vAdam/dense_5/kernel/vAdam/dense_5/bias/vAdam/dense_6/kernel/vAdam/dense_6/bias/vAdam/dense_7/kernel/vAdam/dense_7/bias/vAdam/dense_8/kernel/vAdam/dense_8/bias/vAdam/dense_9/kernel/vAdam/dense_9/bias/v*S
TinL
J2H*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8ѓ *)
f$R"
 __inference__traced_restore_8351ПЈ
Н
х
__inference_save_fn_7874
checkpoint_keyY
Ustring_lookup_index_table_lookup_table_export_values_lookuptableexportv2_table_handle
identity

identity_1

identity_2

identity_3

identity_4

identity_5	ѕбHstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2ш
Hstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2LookupTableExportV2Ustring_lookup_index_table_lookup_table_export_values_lookuptableexportv2_table_handle",/job:localhost/replica:0/task:0/device:CPU:0*
Tkeys0*
Tvalues0	*'
_output_shapes
:         :2J
Hstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2T
add/yConst*
_output_shapes
: *
dtype0*
valueB B-keys2
add/yR
addAddcheckpoint_keyadd/y:output:0*
T0*
_output_shapes
: 2
addZ
add_1/yConst*
_output_shapes
: *
dtype0*
valueB B-values2	
add_1/yX
add_1Addcheckpoint_keyadd_1/y:output:0*
T0*
_output_shapes
: 2
add_1Ћ
IdentityIdentityadd:z:0I^string_lookup_index_table_lookup_table_export_values/LookupTableExportV2*
T0*
_output_shapes
: 2

IdentityO
ConstConst*
_output_shapes
: *
dtype0*
valueB B 2
Constа

Identity_1IdentityConst:output:0I^string_lookup_index_table_lookup_table_export_values/LookupTableExportV2*
T0*
_output_shapes
: 2

Identity_1Ь

Identity_2IdentityOstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2:keys:0I^string_lookup_index_table_lookup_table_export_values/LookupTableExportV2*
T0*#
_output_shapes
:         2

Identity_2Џ

Identity_3Identity	add_1:z:0I^string_lookup_index_table_lookup_table_export_values/LookupTableExportV2*
T0*
_output_shapes
: 2

Identity_3S
Const_1Const*
_output_shapes
: *
dtype0*
valueB B 2	
Const_1б

Identity_4IdentityConst_1:output:0I^string_lookup_index_table_lookup_table_export_values/LookupTableExportV2*
T0*
_output_shapes
: 2

Identity_4т

Identity_5IdentityQstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2:values:0I^string_lookup_index_table_lookup_table_export_values/LookupTableExportV2*
T0	*
_output_shapes
:2

Identity_5"
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0"!

identity_3Identity_3:output:0"!

identity_4Identity_4:output:0"!

identity_5Identity_5:output:0*
_input_shapes
: :2ћ
Hstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2Hstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2:F B

_output_shapes
: 
(
_user_specified_namecheckpoint_key
ж┬
ќ	
F__inference_functional_1_layer_call_and_return_conditional_losses_6709
input_2
input_1Z
Vtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handle[
Wtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value	

dense_6646

dense_6648
dense_1_6651
dense_1_6653
dense_2_6656
dense_2_66581
-normalization_reshape_readvariableop_resource3
/normalization_reshape_1_readvariableop_resource
dense_3_6672
dense_3_6674
dense_4_6678
dense_4_6680
dense_5_6683
dense_5_6685
dense_6_6688
dense_6_6690
dense_7_6693
dense_7_6695
dense_8_6698
dense_8_6700
dense_9_6703
dense_9_6705
identityѕбdense/StatefulPartitionedCallбdense_1/StatefulPartitionedCallбdense_2/StatefulPartitionedCallбdense_3/StatefulPartitionedCallбdense_4/StatefulPartitionedCallбdense_5/StatefulPartitionedCallбdense_6/StatefulPartitionedCallбdense_7/StatefulPartitionedCallбdense_8/StatefulPartitionedCallбdense_9/StatefulPartitionedCallбItext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2Ђ
text_vectorization/StringLowerStringLowerinput_1*'
_output_shapes
:         2 
text_vectorization/StringLower§
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*'
_output_shapes
:         *6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite 2'
%text_vectorization/StaticRegexReplace┴
text_vectorization/SqueezeSqueeze.text_vectorization/StaticRegexReplace:output:0*
T0*#
_output_shapes
:         *
squeeze_dims

         2
text_vectorization/Squeezeи
9text_vectorization/StringsByteSplit/StringSplit/delimiterConst*
_output_shapes
: *
dtype0*
valueB B 2;
9text_vectorization/StringsByteSplit/StringSplit/delimiterф
/text_vectorization/StringsByteSplit/StringSplitStringSplit#text_vectorization/Squeeze:output:0Btext_vectorization/StringsByteSplit/StringSplit/delimiter:output:0*<
_output_shapes*
(:         :         :*

skip_empty( 21
/text_vectorization/StringsByteSplit/StringSplit├
7text_vectorization/StringsByteSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        29
7text_vectorization/StringsByteSplit/strided_slice/stackК
9text_vectorization/StringsByteSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       2;
9text_vectorization/StringsByteSplit/strided_slice/stack_1К
9text_vectorization/StringsByteSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2;
9text_vectorization/StringsByteSplit/strided_slice/stack_2­
1text_vectorization/StringsByteSplit/strided_sliceStridedSlice9text_vectorization/StringsByteSplit/StringSplit:indices:0@text_vectorization/StringsByteSplit/strided_slice/stack:output:0Btext_vectorization/StringsByteSplit/strided_slice/stack_1:output:0Btext_vectorization/StringsByteSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:         *

begin_mask*
end_mask*
shrink_axis_mask23
1text_vectorization/StringsByteSplit/strided_slice└
9text_vectorization/StringsByteSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2;
9text_vectorization/StringsByteSplit/strided_slice_1/stack─
;text_vectorization/StringsByteSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2=
;text_vectorization/StringsByteSplit/strided_slice_1/stack_1─
;text_vectorization/StringsByteSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2=
;text_vectorization/StringsByteSplit/strided_slice_1/stack_2╔
3text_vectorization/StringsByteSplit/strided_slice_1StridedSlice7text_vectorization/StringsByteSplit/StringSplit:shape:0Btext_vectorization/StringsByteSplit/strided_slice_1/stack:output:0Dtext_vectorization/StringsByteSplit/strided_slice_1/stack_1:output:0Dtext_vectorization/StringsByteSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask25
3text_vectorization/StringsByteSplit/strided_slice_1╣
Ztext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast:text_vectorization/StringsByteSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:         2\
Ztext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast▓
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast<text_vectorization/StringsByteSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: 2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1┌
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShape^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shapeќ
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstЛ
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdmtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0mtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: 2e
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prodќ
htext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : 2j
htext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yП
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreaterltext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0qtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterЬ
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastjtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: 2e
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Castџ
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1┴
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMax^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0otext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maxј
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y╬
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ktext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0mtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add┴
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulgtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulк
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum`text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum╩
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum`text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumЊ
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2╔
gtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincount^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0otext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:         2i
gtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincountѕ
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : 2c
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisл
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumntext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:         2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsumў
etext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R 2g
etext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0ѕ
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2c
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis┐
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2ntext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:         2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatђ
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Vtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handle8text_vectorization/StringsByteSplit/StringSplit:values:0Wtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*#
_output_shapes
:         2K
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2є
2text_vectorization/string_lookup/assert_equal/NoOpNoOp*
_output_shapes
 24
2text_vectorization/string_lookup/assert_equal/NoOpС
)text_vectorization/string_lookup/IdentityIdentityRtext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:         2+
)text_vectorization/string_lookup/Identityч
+text_vectorization/string_lookup/Identity_1Identityetext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat:output:0*
T0	*#
_output_shapes
:         2-
+text_vectorization/string_lookup/Identity_1ц
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R 21
/text_vectorization/RaggedToTensor/default_valueЮ
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
: *
dtype0	*
valueB	 R
         2)
'text_vectorization/RaggedToTensor/Const§
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:02text_vectorization/string_lookup/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:04text_vectorization/string_lookup/Identity_1:output:0*
T0	*
Tindex0	*
Tshape0	*0
_output_shapes
:                  *
num_row_partition_tensors*%
row_partition_types

ROW_SPLITS28
6text_vectorization/RaggedToTensor/RaggedTensorToTensorБ
text_vectorization/ShapeShape?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
T0	*
_output_shapes
:2
text_vectorization/Shapeџ
&text_vectorization/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&text_vectorization/strided_slice/stackъ
(text_vectorization/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(text_vectorization/strided_slice/stack_1ъ
(text_vectorization/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(text_vectorization/strided_slice/stack_2н
 text_vectorization/strided_sliceStridedSlice!text_vectorization/Shape:output:0/text_vectorization/strided_slice/stack:output:01text_vectorization/strided_slice/stack_1:output:01text_vectorization/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 text_vectorization/strided_slicew
text_vectorization/sub/xConst*
_output_shapes
: *
dtype0*
value
B :▄2
text_vectorization/sub/xд
text_vectorization/subSub!text_vectorization/sub/x:output:0)text_vectorization/strided_slice:output:0*
T0*
_output_shapes
: 2
text_vectorization/suby
text_vectorization/Less/yConst*
_output_shapes
: *
dtype0*
value
B :▄2
text_vectorization/Less/yф
text_vectorization/LessLess)text_vectorization/strided_slice:output:0"text_vectorization/Less/y:output:0*
T0*
_output_shapes
: 2
text_vectorization/Lessщ
text_vectorization/condStatelessIftext_vectorization/Less:z:0text_vectorization/sub:z:0?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
Tcond0
*
Tin
2	*
Tout
2	*
_lower_using_switch_merge(*0
_output_shapes
:                  * 
_read_only_resource_inputs
 *5
else_branch&R$
"text_vectorization_cond_false_6626*/
output_shapes
:                  *4
then_branch%R#
!text_vectorization_cond_true_66252
text_vectorization/condЦ
 text_vectorization/cond/IdentityIdentity text_vectorization/cond:output:0*
T0	*(
_output_shapes
:         ▄2"
 text_vectorization/cond/IdentityБ
dense/StatefulPartitionedCallStatefulPartitionedCall)text_vectorization/cond/Identity:output:0
dense_6646
dense_6648*
Tin
2	*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:         ђ*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *H
fCRA
?__inference_dense_layer_call_and_return_conditional_losses_62812
dense/StatefulPartitionedCallф
dense_1/StatefulPartitionedCallStatefulPartitionedCall&dense/StatefulPartitionedCall:output:0dense_1_6651dense_1_6653*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:         ђ*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_1_layer_call_and_return_conditional_losses_63082!
dense_1/StatefulPartitionedCallФ
dense_2/StatefulPartitionedCallStatefulPartitionedCall(dense_1/StatefulPartitionedCall:output:0dense_2_6656dense_2_6658*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_2_layer_call_and_return_conditional_losses_63352!
dense_2/StatefulPartitionedCallХ
$normalization/Reshape/ReadVariableOpReadVariableOp-normalization_reshape_readvariableop_resource*
_output_shapes
:*
dtype02&
$normalization/Reshape/ReadVariableOpІ
normalization/Reshape/shapeConst*
_output_shapes
:*
dtype0*
valueB"      2
normalization/Reshape/shapeХ
normalization/ReshapeReshape,normalization/Reshape/ReadVariableOp:value:0$normalization/Reshape/shape:output:0*
T0*
_output_shapes

:2
normalization/Reshape╝
&normalization/Reshape_1/ReadVariableOpReadVariableOp/normalization_reshape_1_readvariableop_resource*
_output_shapes
:*
dtype02(
&normalization/Reshape_1/ReadVariableOpЈ
normalization/Reshape_1/shapeConst*
_output_shapes
:*
dtype0*
valueB"      2
normalization/Reshape_1/shapeЙ
normalization/Reshape_1Reshape.normalization/Reshape_1/ReadVariableOp:value:0&normalization/Reshape_1/shape:output:0*
T0*
_output_shapes

:2
normalization/Reshape_1ѕ
normalization/subSubinput_2normalization/Reshape:output:0*
T0*'
_output_shapes
:         2
normalization/sub{
normalization/SqrtSqrt normalization/Reshape_1:output:0*
T0*
_output_shapes

:2
normalization/Sqrtџ
normalization/truedivRealDivnormalization/sub:z:0normalization/Sqrt:y:0*
T0*'
_output_shapes
:         2
normalization/truedivФ
dense_3/StatefulPartitionedCallStatefulPartitionedCall(dense_2/StatefulPartitionedCall:output:0dense_3_6672dense_3_6674*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_3_layer_call_and_return_conditional_losses_63732!
dense_3/StatefulPartitionedCallЎ
concatenate/PartitionedCallPartitionedCallnormalization/truediv:z:0(dense_3/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         E* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8ѓ *N
fIRG
E__inference_concatenate_layer_call_and_return_conditional_losses_63962
concatenate/PartitionedCallД
dense_4/StatefulPartitionedCallStatefulPartitionedCall$concatenate/PartitionedCall:output:0dense_4_6678dense_4_6680*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_4_layer_call_and_return_conditional_losses_64162!
dense_4/StatefulPartitionedCallФ
dense_5/StatefulPartitionedCallStatefulPartitionedCall(dense_4/StatefulPartitionedCall:output:0dense_5_6683dense_5_6685*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_5_layer_call_and_return_conditional_losses_64432!
dense_5/StatefulPartitionedCallФ
dense_6/StatefulPartitionedCallStatefulPartitionedCall(dense_5/StatefulPartitionedCall:output:0dense_6_6688dense_6_6690*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_6_layer_call_and_return_conditional_losses_64702!
dense_6/StatefulPartitionedCallФ
dense_7/StatefulPartitionedCallStatefulPartitionedCall(dense_6/StatefulPartitionedCall:output:0dense_7_6693dense_7_6695*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_7_layer_call_and_return_conditional_losses_64972!
dense_7/StatefulPartitionedCallФ
dense_8/StatefulPartitionedCallStatefulPartitionedCall(dense_7/StatefulPartitionedCall:output:0dense_8_6698dense_8_6700*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_8_layer_call_and_return_conditional_losses_65242!
dense_8/StatefulPartitionedCallФ
dense_9/StatefulPartitionedCallStatefulPartitionedCall(dense_8/StatefulPartitionedCall:output:0dense_9_6703dense_9_6705*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_9_layer_call_and_return_conditional_losses_65502!
dense_9/StatefulPartitionedCallџ
IdentityIdentity(dense_9/StatefulPartitionedCall:output:0^dense/StatefulPartitionedCall ^dense_1/StatefulPartitionedCall ^dense_2/StatefulPartitionedCall ^dense_3/StatefulPartitionedCall ^dense_4/StatefulPartitionedCall ^dense_5/StatefulPartitionedCall ^dense_6/StatefulPartitionedCall ^dense_7/StatefulPartitionedCall ^dense_8/StatefulPartitionedCall ^dense_9/StatefulPartitionedCallJ^text_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*Ў
_input_shapesЄ
ё:         :         :: ::::::::::::::::::::::2>
dense/StatefulPartitionedCalldense/StatefulPartitionedCall2B
dense_1/StatefulPartitionedCalldense_1/StatefulPartitionedCall2B
dense_2/StatefulPartitionedCalldense_2/StatefulPartitionedCall2B
dense_3/StatefulPartitionedCalldense_3/StatefulPartitionedCall2B
dense_4/StatefulPartitionedCalldense_4/StatefulPartitionedCall2B
dense_5/StatefulPartitionedCalldense_5/StatefulPartitionedCall2B
dense_6/StatefulPartitionedCalldense_6/StatefulPartitionedCall2B
dense_7/StatefulPartitionedCalldense_7/StatefulPartitionedCall2B
dense_8/StatefulPartitionedCalldense_8/StatefulPartitionedCall2B
dense_9/StatefulPartitionedCalldense_9/StatefulPartitionedCall2ќ
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2:P L
'
_output_shapes
:         
!
_user_specified_name	input_2:PL
'
_output_shapes
:         
!
_user_specified_name	input_1
ъ
С
!text_vectorization_cond_true_6967A
=text_vectorization_cond_pad_paddings_1_text_vectorization_subV
Rtext_vectorization_cond_pad_text_vectorization_raggedtotensor_raggedtensortotensor	$
 text_vectorization_cond_identity	ќ
(text_vectorization/cond/Pad/paddings/1/0Const*
_output_shapes
: *
dtype0*
value	B : 2*
(text_vectorization/cond/Pad/paddings/1/0Э
&text_vectorization/cond/Pad/paddings/1Pack1text_vectorization/cond/Pad/paddings/1/0:output:0=text_vectorization_cond_pad_paddings_1_text_vectorization_sub*
N*
T0*
_output_shapes
:2(
&text_vectorization/cond/Pad/paddings/1Ц
(text_vectorization/cond/Pad/paddings/0_1Const*
_output_shapes
:*
dtype0*
valueB"        2*
(text_vectorization/cond/Pad/paddings/0_1Ж
$text_vectorization/cond/Pad/paddingsPack1text_vectorization/cond/Pad/paddings/0_1:output:0/text_vectorization/cond/Pad/paddings/1:output:0*
N*
T0*
_output_shapes

:2&
$text_vectorization/cond/Pad/paddings 
text_vectorization/cond/PadPadRtext_vectorization_cond_pad_text_vectorization_raggedtotensor_raggedtensortotensor-text_vectorization/cond/Pad/paddings:output:0*
T0	*0
_output_shapes
:                  2
text_vectorization/cond/Pad▒
 text_vectorization/cond/IdentityIdentity$text_vectorization/cond/Pad:output:0*
T0	*0
_output_shapes
:                  2"
 text_vectorization/cond/Identity"M
 text_vectorization_cond_identity)text_vectorization/cond/Identity:output:0*1
_input_shapes 
: :                  : 

_output_shapes
: :62
0
_output_shapes
:                  
┐
у
+__inference_functional_1_layer_call_fn_7573
inputs_0
inputs_1
unknown
	unknown_0	
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
	unknown_7
	unknown_8
	unknown_9

unknown_10

unknown_11

unknown_12

unknown_13

unknown_14

unknown_15

unknown_16

unknown_17

unknown_18

unknown_19

unknown_20

unknown_21

unknown_22
identityѕбStatefulPartitionedCallг
StatefulPartitionedCallStatefulPartitionedCallinputs_0inputs_1unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22*%
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *8
_read_only_resource_inputs
	
*-
config_proto

CPU

GPU 2J 8ѓ *O
fJRH
F__inference_functional_1_layer_call_and_return_conditional_losses_68552
StatefulPartitionedCallј
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*Ў
_input_shapesЄ
ё:         :         :: ::::::::::::::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:Q M
'
_output_shapes
:         
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:         
"
_user_specified_name
inputs/1
о
{
&__inference_dense_3_layer_call_fn_7708

inputs
unknown
	unknown_0
identityѕбStatefulPartitionedCallы
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_3_layer_call_and_return_conditional_losses_63732
StatefulPartitionedCallј
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
ЌІ
ј
__inference__traced_save_8128
file_prefix+
'savev2_dense_kernel_read_readvariableop)
%savev2_dense_bias_read_readvariableop-
)savev2_dense_1_kernel_read_readvariableop+
'savev2_dense_1_bias_read_readvariableop-
)savev2_dense_2_kernel_read_readvariableop+
'savev2_dense_2_bias_read_readvariableop#
savev2_mean_read_readvariableop'
#savev2_variance_read_readvariableop$
 savev2_count_read_readvariableop	-
)savev2_dense_3_kernel_read_readvariableop+
'savev2_dense_3_bias_read_readvariableop-
)savev2_dense_4_kernel_read_readvariableop+
'savev2_dense_4_bias_read_readvariableop-
)savev2_dense_5_kernel_read_readvariableop+
'savev2_dense_5_bias_read_readvariableop-
)savev2_dense_6_kernel_read_readvariableop+
'savev2_dense_6_bias_read_readvariableop-
)savev2_dense_7_kernel_read_readvariableop+
'savev2_dense_7_bias_read_readvariableop-
)savev2_dense_8_kernel_read_readvariableop+
'savev2_dense_8_bias_read_readvariableop-
)savev2_dense_9_kernel_read_readvariableop+
'savev2_dense_9_bias_read_readvariableop(
$savev2_adam_iter_read_readvariableop	*
&savev2_adam_beta_1_read_readvariableop*
&savev2_adam_beta_2_read_readvariableop)
%savev2_adam_decay_read_readvariableop1
-savev2_adam_learning_rate_read_readvariableopS
Osavev2_string_lookup_index_table_lookup_table_export_values_lookuptableexportv2U
Qsavev2_string_lookup_index_table_lookup_table_export_values_lookuptableexportv2_1	$
 savev2_total_read_readvariableop&
"savev2_count_1_read_readvariableop2
.savev2_adam_dense_kernel_m_read_readvariableop0
,savev2_adam_dense_bias_m_read_readvariableop4
0savev2_adam_dense_1_kernel_m_read_readvariableop2
.savev2_adam_dense_1_bias_m_read_readvariableop4
0savev2_adam_dense_2_kernel_m_read_readvariableop2
.savev2_adam_dense_2_bias_m_read_readvariableop4
0savev2_adam_dense_3_kernel_m_read_readvariableop2
.savev2_adam_dense_3_bias_m_read_readvariableop4
0savev2_adam_dense_4_kernel_m_read_readvariableop2
.savev2_adam_dense_4_bias_m_read_readvariableop4
0savev2_adam_dense_5_kernel_m_read_readvariableop2
.savev2_adam_dense_5_bias_m_read_readvariableop4
0savev2_adam_dense_6_kernel_m_read_readvariableop2
.savev2_adam_dense_6_bias_m_read_readvariableop4
0savev2_adam_dense_7_kernel_m_read_readvariableop2
.savev2_adam_dense_7_bias_m_read_readvariableop4
0savev2_adam_dense_8_kernel_m_read_readvariableop2
.savev2_adam_dense_8_bias_m_read_readvariableop4
0savev2_adam_dense_9_kernel_m_read_readvariableop2
.savev2_adam_dense_9_bias_m_read_readvariableop2
.savev2_adam_dense_kernel_v_read_readvariableop0
,savev2_adam_dense_bias_v_read_readvariableop4
0savev2_adam_dense_1_kernel_v_read_readvariableop2
.savev2_adam_dense_1_bias_v_read_readvariableop4
0savev2_adam_dense_2_kernel_v_read_readvariableop2
.savev2_adam_dense_2_bias_v_read_readvariableop4
0savev2_adam_dense_3_kernel_v_read_readvariableop2
.savev2_adam_dense_3_bias_v_read_readvariableop4
0savev2_adam_dense_4_kernel_v_read_readvariableop2
.savev2_adam_dense_4_bias_v_read_readvariableop4
0savev2_adam_dense_5_kernel_v_read_readvariableop2
.savev2_adam_dense_5_bias_v_read_readvariableop4
0savev2_adam_dense_6_kernel_v_read_readvariableop2
.savev2_adam_dense_6_bias_v_read_readvariableop4
0savev2_adam_dense_7_kernel_v_read_readvariableop2
.savev2_adam_dense_7_bias_v_read_readvariableop4
0savev2_adam_dense_8_kernel_v_read_readvariableop2
.savev2_adam_dense_8_bias_v_read_readvariableop4
0savev2_adam_dense_9_kernel_v_read_readvariableop2
.savev2_adam_dense_9_bias_v_read_readvariableop
savev2_const_1

identity_1ѕбMergeV2CheckpointsЈ
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*2
StaticRegexFullMatchc
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.part2
ConstЇ
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*<
value3B1 B+_temp_88d68137ced04f62af05da1888d0fa06/part2	
Const_1І
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: 2
Selectt

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shardд
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilenameч(
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:I*
dtype0*Ї(
valueЃ(Bђ(IB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/mean/.ATTRIBUTES/VARIABLE_VALUEB8layer_with_weights-4/variance/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-4/count/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-8/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-8/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-9/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-9/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-10/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-10/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-11/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-11/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEBFlayer_with_weights-0/_index_lookup_layer/_table/.ATTRIBUTES/table-keysBHlayer_with_weights-0/_index_lookup_layer/_table/.ATTRIBUTES/table-valuesB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-5/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-6/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-6/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-7/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-7/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-9/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-9/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-10/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-10/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-11/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-11/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-5/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-6/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-6/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-7/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-7/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-9/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-9/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-10/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-10/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-11/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-11/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2/tensor_namesЮ
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:I*
dtype0*Д
valueЮBџIB B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
SaveV2/shape_and_slicesє
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0'savev2_dense_kernel_read_readvariableop%savev2_dense_bias_read_readvariableop)savev2_dense_1_kernel_read_readvariableop'savev2_dense_1_bias_read_readvariableop)savev2_dense_2_kernel_read_readvariableop'savev2_dense_2_bias_read_readvariableopsavev2_mean_read_readvariableop#savev2_variance_read_readvariableop savev2_count_read_readvariableop)savev2_dense_3_kernel_read_readvariableop'savev2_dense_3_bias_read_readvariableop)savev2_dense_4_kernel_read_readvariableop'savev2_dense_4_bias_read_readvariableop)savev2_dense_5_kernel_read_readvariableop'savev2_dense_5_bias_read_readvariableop)savev2_dense_6_kernel_read_readvariableop'savev2_dense_6_bias_read_readvariableop)savev2_dense_7_kernel_read_readvariableop'savev2_dense_7_bias_read_readvariableop)savev2_dense_8_kernel_read_readvariableop'savev2_dense_8_bias_read_readvariableop)savev2_dense_9_kernel_read_readvariableop'savev2_dense_9_bias_read_readvariableop$savev2_adam_iter_read_readvariableop&savev2_adam_beta_1_read_readvariableop&savev2_adam_beta_2_read_readvariableop%savev2_adam_decay_read_readvariableop-savev2_adam_learning_rate_read_readvariableopOsavev2_string_lookup_index_table_lookup_table_export_values_lookuptableexportv2Qsavev2_string_lookup_index_table_lookup_table_export_values_lookuptableexportv2_1 savev2_total_read_readvariableop"savev2_count_1_read_readvariableop.savev2_adam_dense_kernel_m_read_readvariableop,savev2_adam_dense_bias_m_read_readvariableop0savev2_adam_dense_1_kernel_m_read_readvariableop.savev2_adam_dense_1_bias_m_read_readvariableop0savev2_adam_dense_2_kernel_m_read_readvariableop.savev2_adam_dense_2_bias_m_read_readvariableop0savev2_adam_dense_3_kernel_m_read_readvariableop.savev2_adam_dense_3_bias_m_read_readvariableop0savev2_adam_dense_4_kernel_m_read_readvariableop.savev2_adam_dense_4_bias_m_read_readvariableop0savev2_adam_dense_5_kernel_m_read_readvariableop.savev2_adam_dense_5_bias_m_read_readvariableop0savev2_adam_dense_6_kernel_m_read_readvariableop.savev2_adam_dense_6_bias_m_read_readvariableop0savev2_adam_dense_7_kernel_m_read_readvariableop.savev2_adam_dense_7_bias_m_read_readvariableop0savev2_adam_dense_8_kernel_m_read_readvariableop.savev2_adam_dense_8_bias_m_read_readvariableop0savev2_adam_dense_9_kernel_m_read_readvariableop.savev2_adam_dense_9_bias_m_read_readvariableop.savev2_adam_dense_kernel_v_read_readvariableop,savev2_adam_dense_bias_v_read_readvariableop0savev2_adam_dense_1_kernel_v_read_readvariableop.savev2_adam_dense_1_bias_v_read_readvariableop0savev2_adam_dense_2_kernel_v_read_readvariableop.savev2_adam_dense_2_bias_v_read_readvariableop0savev2_adam_dense_3_kernel_v_read_readvariableop.savev2_adam_dense_3_bias_v_read_readvariableop0savev2_adam_dense_4_kernel_v_read_readvariableop.savev2_adam_dense_4_bias_v_read_readvariableop0savev2_adam_dense_5_kernel_v_read_readvariableop.savev2_adam_dense_5_bias_v_read_readvariableop0savev2_adam_dense_6_kernel_v_read_readvariableop.savev2_adam_dense_6_bias_v_read_readvariableop0savev2_adam_dense_7_kernel_v_read_readvariableop.savev2_adam_dense_7_bias_v_read_readvariableop0savev2_adam_dense_8_kernel_v_read_readvariableop.savev2_adam_dense_8_bias_v_read_readvariableop0savev2_adam_dense_9_kernel_v_read_readvariableop.savev2_adam_dense_9_bias_v_read_readvariableopsavev2_const_1"/device:CPU:0*
_output_shapes
 *W
dtypesM
K2I			2
SaveV2║
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixesА
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identitym

Identity_1IdentityIdentity:output:0^MergeV2Checkpoints*
T0*
_output_shapes
: 2

Identity_1"!

identity_1Identity_1:output:0*╚
_input_shapesХ
│: :
▄ђ:ђ:
ђђ:ђ:	ђ@:@::: :@@:@:E@:@:@@:@:@@:@:@@:@:@@:@:@:: : : : : :         :         : : :
▄ђ:ђ:
ђђ:ђ:	ђ@:@:@@:@:E@:@:@@:@:@@:@:@@:@:@@:@:@::
▄ђ:ђ:
ђђ:ђ:	ђ@:@:@@:@:E@:@:@@:@:@@:@:@@:@:@@:@:@:: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:&"
 
_output_shapes
:
▄ђ:!

_output_shapes	
:ђ:&"
 
_output_shapes
:
ђђ:!

_output_shapes	
:ђ:%!

_output_shapes
:	ђ@: 

_output_shapes
:@: 

_output_shapes
:: 

_output_shapes
::	

_output_shapes
: :$
 

_output_shapes

:@@: 

_output_shapes
:@:$ 

_output_shapes

:E@: 

_output_shapes
:@:$ 

_output_shapes

:@@: 

_output_shapes
:@:$ 

_output_shapes

:@@: 

_output_shapes
:@:$ 

_output_shapes

:@@: 

_output_shapes
:@:$ 

_output_shapes

:@@: 

_output_shapes
:@:$ 

_output_shapes

:@: 

_output_shapes
::

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :)%
#
_output_shapes
:         :)%
#
_output_shapes
:         :

_output_shapes
: : 

_output_shapes
: :&!"
 
_output_shapes
:
▄ђ:!"

_output_shapes	
:ђ:&#"
 
_output_shapes
:
ђђ:!$

_output_shapes	
:ђ:%%!

_output_shapes
:	ђ@: &

_output_shapes
:@:$' 

_output_shapes

:@@: (

_output_shapes
:@:$) 

_output_shapes

:E@: *

_output_shapes
:@:$+ 

_output_shapes

:@@: ,

_output_shapes
:@:$- 

_output_shapes

:@@: .

_output_shapes
:@:$/ 

_output_shapes

:@@: 0

_output_shapes
:@:$1 

_output_shapes

:@@: 2

_output_shapes
:@:$3 

_output_shapes

:@: 4

_output_shapes
::&5"
 
_output_shapes
:
▄ђ:!6

_output_shapes	
:ђ:&7"
 
_output_shapes
:
ђђ:!8

_output_shapes	
:ђ:%9!

_output_shapes
:	ђ@: :

_output_shapes
:@:$; 

_output_shapes

:@@: <

_output_shapes
:@:$= 

_output_shapes

:E@: >

_output_shapes
:@:$? 

_output_shapes

:@@: @

_output_shapes
:@:$A 

_output_shapes

:@@: B

_output_shapes
:@:$C 

_output_shapes

:@@: D

_output_shapes
:@:$E 

_output_shapes

:@@: F

_output_shapes
:@:$G 

_output_shapes

:@: H

_output_shapes
::I

_output_shapes
: 
у┬
ќ	
F__inference_functional_1_layer_call_and_return_conditional_losses_7051

inputs
inputs_1Z
Vtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handle[
Wtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value	

dense_6988

dense_6990
dense_1_6993
dense_1_6995
dense_2_6998
dense_2_70001
-normalization_reshape_readvariableop_resource3
/normalization_reshape_1_readvariableop_resource
dense_3_7014
dense_3_7016
dense_4_7020
dense_4_7022
dense_5_7025
dense_5_7027
dense_6_7030
dense_6_7032
dense_7_7035
dense_7_7037
dense_8_7040
dense_8_7042
dense_9_7045
dense_9_7047
identityѕбdense/StatefulPartitionedCallбdense_1/StatefulPartitionedCallбdense_2/StatefulPartitionedCallбdense_3/StatefulPartitionedCallбdense_4/StatefulPartitionedCallбdense_5/StatefulPartitionedCallбdense_6/StatefulPartitionedCallбdense_7/StatefulPartitionedCallбdense_8/StatefulPartitionedCallбdense_9/StatefulPartitionedCallбItext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2ѓ
text_vectorization/StringLowerStringLowerinputs_1*'
_output_shapes
:         2 
text_vectorization/StringLower§
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*'
_output_shapes
:         *6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite 2'
%text_vectorization/StaticRegexReplace┴
text_vectorization/SqueezeSqueeze.text_vectorization/StaticRegexReplace:output:0*
T0*#
_output_shapes
:         *
squeeze_dims

         2
text_vectorization/Squeezeи
9text_vectorization/StringsByteSplit/StringSplit/delimiterConst*
_output_shapes
: *
dtype0*
valueB B 2;
9text_vectorization/StringsByteSplit/StringSplit/delimiterф
/text_vectorization/StringsByteSplit/StringSplitStringSplit#text_vectorization/Squeeze:output:0Btext_vectorization/StringsByteSplit/StringSplit/delimiter:output:0*<
_output_shapes*
(:         :         :*

skip_empty( 21
/text_vectorization/StringsByteSplit/StringSplit├
7text_vectorization/StringsByteSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        29
7text_vectorization/StringsByteSplit/strided_slice/stackК
9text_vectorization/StringsByteSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       2;
9text_vectorization/StringsByteSplit/strided_slice/stack_1К
9text_vectorization/StringsByteSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2;
9text_vectorization/StringsByteSplit/strided_slice/stack_2­
1text_vectorization/StringsByteSplit/strided_sliceStridedSlice9text_vectorization/StringsByteSplit/StringSplit:indices:0@text_vectorization/StringsByteSplit/strided_slice/stack:output:0Btext_vectorization/StringsByteSplit/strided_slice/stack_1:output:0Btext_vectorization/StringsByteSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:         *

begin_mask*
end_mask*
shrink_axis_mask23
1text_vectorization/StringsByteSplit/strided_slice└
9text_vectorization/StringsByteSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2;
9text_vectorization/StringsByteSplit/strided_slice_1/stack─
;text_vectorization/StringsByteSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2=
;text_vectorization/StringsByteSplit/strided_slice_1/stack_1─
;text_vectorization/StringsByteSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2=
;text_vectorization/StringsByteSplit/strided_slice_1/stack_2╔
3text_vectorization/StringsByteSplit/strided_slice_1StridedSlice7text_vectorization/StringsByteSplit/StringSplit:shape:0Btext_vectorization/StringsByteSplit/strided_slice_1/stack:output:0Dtext_vectorization/StringsByteSplit/strided_slice_1/stack_1:output:0Dtext_vectorization/StringsByteSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask25
3text_vectorization/StringsByteSplit/strided_slice_1╣
Ztext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast:text_vectorization/StringsByteSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:         2\
Ztext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast▓
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast<text_vectorization/StringsByteSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: 2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1┌
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShape^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shapeќ
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstЛ
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdmtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0mtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: 2e
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prodќ
htext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : 2j
htext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yП
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreaterltext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0qtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterЬ
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastjtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: 2e
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Castџ
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1┴
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMax^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0otext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maxј
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y╬
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ktext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0mtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add┴
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulgtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulк
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum`text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum╩
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum`text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumЊ
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2╔
gtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincount^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0otext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:         2i
gtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincountѕ
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : 2c
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisл
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumntext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:         2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsumў
etext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R 2g
etext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0ѕ
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2c
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis┐
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2ntext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:         2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatђ
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Vtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handle8text_vectorization/StringsByteSplit/StringSplit:values:0Wtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*#
_output_shapes
:         2K
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2є
2text_vectorization/string_lookup/assert_equal/NoOpNoOp*
_output_shapes
 24
2text_vectorization/string_lookup/assert_equal/NoOpС
)text_vectorization/string_lookup/IdentityIdentityRtext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:         2+
)text_vectorization/string_lookup/Identityч
+text_vectorization/string_lookup/Identity_1Identityetext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat:output:0*
T0	*#
_output_shapes
:         2-
+text_vectorization/string_lookup/Identity_1ц
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R 21
/text_vectorization/RaggedToTensor/default_valueЮ
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
: *
dtype0	*
valueB	 R
         2)
'text_vectorization/RaggedToTensor/Const§
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:02text_vectorization/string_lookup/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:04text_vectorization/string_lookup/Identity_1:output:0*
T0	*
Tindex0	*
Tshape0	*0
_output_shapes
:                  *
num_row_partition_tensors*%
row_partition_types

ROW_SPLITS28
6text_vectorization/RaggedToTensor/RaggedTensorToTensorБ
text_vectorization/ShapeShape?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
T0	*
_output_shapes
:2
text_vectorization/Shapeџ
&text_vectorization/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&text_vectorization/strided_slice/stackъ
(text_vectorization/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(text_vectorization/strided_slice/stack_1ъ
(text_vectorization/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(text_vectorization/strided_slice/stack_2н
 text_vectorization/strided_sliceStridedSlice!text_vectorization/Shape:output:0/text_vectorization/strided_slice/stack:output:01text_vectorization/strided_slice/stack_1:output:01text_vectorization/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 text_vectorization/strided_slicew
text_vectorization/sub/xConst*
_output_shapes
: *
dtype0*
value
B :▄2
text_vectorization/sub/xд
text_vectorization/subSub!text_vectorization/sub/x:output:0)text_vectorization/strided_slice:output:0*
T0*
_output_shapes
: 2
text_vectorization/suby
text_vectorization/Less/yConst*
_output_shapes
: *
dtype0*
value
B :▄2
text_vectorization/Less/yф
text_vectorization/LessLess)text_vectorization/strided_slice:output:0"text_vectorization/Less/y:output:0*
T0*
_output_shapes
: 2
text_vectorization/Lessщ
text_vectorization/condStatelessIftext_vectorization/Less:z:0text_vectorization/sub:z:0?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
Tcond0
*
Tin
2	*
Tout
2	*
_lower_using_switch_merge(*0
_output_shapes
:                  * 
_read_only_resource_inputs
 *5
else_branch&R$
"text_vectorization_cond_false_6968*/
output_shapes
:                  *4
then_branch%R#
!text_vectorization_cond_true_69672
text_vectorization/condЦ
 text_vectorization/cond/IdentityIdentity text_vectorization/cond:output:0*
T0	*(
_output_shapes
:         ▄2"
 text_vectorization/cond/IdentityБ
dense/StatefulPartitionedCallStatefulPartitionedCall)text_vectorization/cond/Identity:output:0
dense_6988
dense_6990*
Tin
2	*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:         ђ*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *H
fCRA
?__inference_dense_layer_call_and_return_conditional_losses_62812
dense/StatefulPartitionedCallф
dense_1/StatefulPartitionedCallStatefulPartitionedCall&dense/StatefulPartitionedCall:output:0dense_1_6993dense_1_6995*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:         ђ*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_1_layer_call_and_return_conditional_losses_63082!
dense_1/StatefulPartitionedCallФ
dense_2/StatefulPartitionedCallStatefulPartitionedCall(dense_1/StatefulPartitionedCall:output:0dense_2_6998dense_2_7000*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_2_layer_call_and_return_conditional_losses_63352!
dense_2/StatefulPartitionedCallХ
$normalization/Reshape/ReadVariableOpReadVariableOp-normalization_reshape_readvariableop_resource*
_output_shapes
:*
dtype02&
$normalization/Reshape/ReadVariableOpІ
normalization/Reshape/shapeConst*
_output_shapes
:*
dtype0*
valueB"      2
normalization/Reshape/shapeХ
normalization/ReshapeReshape,normalization/Reshape/ReadVariableOp:value:0$normalization/Reshape/shape:output:0*
T0*
_output_shapes

:2
normalization/Reshape╝
&normalization/Reshape_1/ReadVariableOpReadVariableOp/normalization_reshape_1_readvariableop_resource*
_output_shapes
:*
dtype02(
&normalization/Reshape_1/ReadVariableOpЈ
normalization/Reshape_1/shapeConst*
_output_shapes
:*
dtype0*
valueB"      2
normalization/Reshape_1/shapeЙ
normalization/Reshape_1Reshape.normalization/Reshape_1/ReadVariableOp:value:0&normalization/Reshape_1/shape:output:0*
T0*
_output_shapes

:2
normalization/Reshape_1Є
normalization/subSubinputsnormalization/Reshape:output:0*
T0*'
_output_shapes
:         2
normalization/sub{
normalization/SqrtSqrt normalization/Reshape_1:output:0*
T0*
_output_shapes

:2
normalization/Sqrtџ
normalization/truedivRealDivnormalization/sub:z:0normalization/Sqrt:y:0*
T0*'
_output_shapes
:         2
normalization/truedivФ
dense_3/StatefulPartitionedCallStatefulPartitionedCall(dense_2/StatefulPartitionedCall:output:0dense_3_7014dense_3_7016*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_3_layer_call_and_return_conditional_losses_63732!
dense_3/StatefulPartitionedCallЎ
concatenate/PartitionedCallPartitionedCallnormalization/truediv:z:0(dense_3/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         E* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8ѓ *N
fIRG
E__inference_concatenate_layer_call_and_return_conditional_losses_63962
concatenate/PartitionedCallД
dense_4/StatefulPartitionedCallStatefulPartitionedCall$concatenate/PartitionedCall:output:0dense_4_7020dense_4_7022*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_4_layer_call_and_return_conditional_losses_64162!
dense_4/StatefulPartitionedCallФ
dense_5/StatefulPartitionedCallStatefulPartitionedCall(dense_4/StatefulPartitionedCall:output:0dense_5_7025dense_5_7027*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_5_layer_call_and_return_conditional_losses_64432!
dense_5/StatefulPartitionedCallФ
dense_6/StatefulPartitionedCallStatefulPartitionedCall(dense_5/StatefulPartitionedCall:output:0dense_6_7030dense_6_7032*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_6_layer_call_and_return_conditional_losses_64702!
dense_6/StatefulPartitionedCallФ
dense_7/StatefulPartitionedCallStatefulPartitionedCall(dense_6/StatefulPartitionedCall:output:0dense_7_7035dense_7_7037*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_7_layer_call_and_return_conditional_losses_64972!
dense_7/StatefulPartitionedCallФ
dense_8/StatefulPartitionedCallStatefulPartitionedCall(dense_7/StatefulPartitionedCall:output:0dense_8_7040dense_8_7042*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_8_layer_call_and_return_conditional_losses_65242!
dense_8/StatefulPartitionedCallФ
dense_9/StatefulPartitionedCallStatefulPartitionedCall(dense_8/StatefulPartitionedCall:output:0dense_9_7045dense_9_7047*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_9_layer_call_and_return_conditional_losses_65502!
dense_9/StatefulPartitionedCallџ
IdentityIdentity(dense_9/StatefulPartitionedCall:output:0^dense/StatefulPartitionedCall ^dense_1/StatefulPartitionedCall ^dense_2/StatefulPartitionedCall ^dense_3/StatefulPartitionedCall ^dense_4/StatefulPartitionedCall ^dense_5/StatefulPartitionedCall ^dense_6/StatefulPartitionedCall ^dense_7/StatefulPartitionedCall ^dense_8/StatefulPartitionedCall ^dense_9/StatefulPartitionedCallJ^text_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*Ў
_input_shapesЄ
ё:         :         :: ::::::::::::::::::::::2>
dense/StatefulPartitionedCalldense/StatefulPartitionedCall2B
dense_1/StatefulPartitionedCalldense_1/StatefulPartitionedCall2B
dense_2/StatefulPartitionedCalldense_2/StatefulPartitionedCall2B
dense_3/StatefulPartitionedCalldense_3/StatefulPartitionedCall2B
dense_4/StatefulPartitionedCalldense_4/StatefulPartitionedCall2B
dense_5/StatefulPartitionedCalldense_5/StatefulPartitionedCall2B
dense_6/StatefulPartitionedCalldense_6/StatefulPartitionedCall2B
dense_7/StatefulPartitionedCalldense_7/StatefulPartitionedCall2B
dense_8/StatefulPartitionedCalldense_8/StatefulPartitionedCall2B
dense_9/StatefulPartitionedCalldense_9/StatefulPartitionedCall2ќ
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2:O K
'
_output_shapes
:         
 
_user_specified_nameinputs:OK
'
_output_shapes
:         
 
_user_specified_nameinputs
Ќ
▓
.functional_1_text_vectorization_cond_true_6084[
Wfunctional_1_text_vectorization_cond_pad_paddings_1_functional_1_text_vectorization_subp
lfunctional_1_text_vectorization_cond_pad_functional_1_text_vectorization_raggedtotensor_raggedtensortotensor	1
-functional_1_text_vectorization_cond_identity	░
5functional_1/text_vectorization/cond/Pad/paddings/1/0Const*
_output_shapes
: *
dtype0*
value	B : 27
5functional_1/text_vectorization/cond/Pad/paddings/1/0╣
3functional_1/text_vectorization/cond/Pad/paddings/1Pack>functional_1/text_vectorization/cond/Pad/paddings/1/0:output:0Wfunctional_1_text_vectorization_cond_pad_paddings_1_functional_1_text_vectorization_sub*
N*
T0*
_output_shapes
:25
3functional_1/text_vectorization/cond/Pad/paddings/1┐
5functional_1/text_vectorization/cond/Pad/paddings/0_1Const*
_output_shapes
:*
dtype0*
valueB"        27
5functional_1/text_vectorization/cond/Pad/paddings/0_1ъ
1functional_1/text_vectorization/cond/Pad/paddingsPack>functional_1/text_vectorization/cond/Pad/paddings/0_1:output:0<functional_1/text_vectorization/cond/Pad/paddings/1:output:0*
N*
T0*
_output_shapes

:23
1functional_1/text_vectorization/cond/Pad/paddings└
(functional_1/text_vectorization/cond/PadPadlfunctional_1_text_vectorization_cond_pad_functional_1_text_vectorization_raggedtotensor_raggedtensortotensor:functional_1/text_vectorization/cond/Pad/paddings:output:0*
T0	*0
_output_shapes
:                  2*
(functional_1/text_vectorization/cond/Padп
-functional_1/text_vectorization/cond/IdentityIdentity1functional_1/text_vectorization/cond/Pad:output:0*
T0	*0
_output_shapes
:                  2/
-functional_1/text_vectorization/cond/Identity"g
-functional_1_text_vectorization_cond_identity6functional_1/text_vectorization/cond/Identity:output:0*1
_input_shapes 
: :                  : 

_output_shapes
: :62
0
_output_shapes
:                  
Ј	
Д
?__inference_dense_layer_call_and_return_conditional_losses_6281

inputs	"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityѕ^
CastCastinputs*

DstT0*

SrcT0	*(
_output_shapes
:         ▄2
CastЈ
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
▄ђ*
dtype02
MatMul/ReadVariableOpv
MatMulMatMulCast:y:0MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2
MatMulЇ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:ђ*
dtype02
BiasAdd/ReadVariableOpѓ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:         ђ2
Relug
IdentityIdentityRelu:activations:0*
T0*(
_output_shapes
:         ђ2

Identity"
identityIdentity:output:0*/
_input_shapes
:         ▄:::P L
(
_output_shapes
:         ▄
 
_user_specified_nameinputs
Ѕ
▄
"__inference_signature_wrapper_7166
input_1
input_2
unknown
	unknown_0	
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
	unknown_7
	unknown_8
	unknown_9

unknown_10

unknown_11

unknown_12

unknown_13

unknown_14

unknown_15

unknown_16

unknown_17

unknown_18

unknown_19

unknown_20

unknown_21

unknown_22
identityѕбStatefulPartitionedCallЃ
StatefulPartitionedCallStatefulPartitionedCallinput_2input_1unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22*%
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *8
_read_only_resource_inputs
	
*-
config_proto

CPU

GPU 2J 8ѓ *(
f#R!
__inference__wrapped_model_61892
StatefulPartitionedCallј
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*Ў
_input_shapesЄ
ё:         :         :: ::::::::::::::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:P L
'
_output_shapes
:         
!
_user_specified_name	input_1:PL
'
_output_shapes
:         
!
_user_specified_name	input_2
┌
{
&__inference_dense_1_layer_call_fn_7668

inputs
unknown
	unknown_0
identityѕбStatefulPartitionedCallЫ
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:         ђ*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_1_layer_call_and_return_conditional_losses_63082
StatefulPartitionedCallЈ
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*(
_output_shapes
:         ђ2

Identity"
identityIdentity:output:0*/
_input_shapes
:         ђ::22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:         ђ
 
_user_specified_nameinputs
╣
т
+__inference_functional_1_layer_call_fn_7102
input_2
input_1
unknown
	unknown_0	
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
	unknown_7
	unknown_8
	unknown_9

unknown_10

unknown_11

unknown_12

unknown_13

unknown_14

unknown_15

unknown_16

unknown_17

unknown_18

unknown_19

unknown_20

unknown_21

unknown_22
identityѕбStatefulPartitionedCallф
StatefulPartitionedCallStatefulPartitionedCallinput_2input_1unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22*%
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *8
_read_only_resource_inputs
	
*-
config_proto

CPU

GPU 2J 8ѓ *O
fJRH
F__inference_functional_1_layer_call_and_return_conditional_losses_70512
StatefulPartitionedCallј
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*Ў
_input_shapesЄ
ё:         :         :: ::::::::::::::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:P L
'
_output_shapes
:         
!
_user_specified_name	input_2:PL
'
_output_shapes
:         
!
_user_specified_name	input_1
║
q
E__inference_concatenate_layer_call_and_return_conditional_losses_7715
inputs_0
inputs_1
identity\
concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concat/axisЂ
concatConcatV2inputs_0inputs_1concat/axis:output:0*
N*
T0*'
_output_shapes
:         E2
concatc
IdentityIdentityconcat:output:0*
T0*'
_output_shapes
:         E2

Identity"
identityIdentity:output:0*9
_input_shapes(
&:         :         @:Q M
'
_output_shapes
:         
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:         @
"
_user_specified_name
inputs/1
д
Е
A__inference_dense_6_layer_call_and_return_conditional_losses_6470

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityѕЇ
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulї
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpЂ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @:::O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
п
{
&__inference_dense_2_layer_call_fn_7688

inputs
unknown
	unknown_0
identityѕбStatefulPartitionedCallы
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_2_layer_call_and_return_conditional_losses_63352
StatefulPartitionedCallј
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*/
_input_shapes
:         ђ::22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:         ђ
 
_user_specified_nameinputs
╣
т
+__inference_functional_1_layer_call_fn_6906
input_2
input_1
unknown
	unknown_0	
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
	unknown_7
	unknown_8
	unknown_9

unknown_10

unknown_11

unknown_12

unknown_13

unknown_14

unknown_15

unknown_16

unknown_17

unknown_18

unknown_19

unknown_20

unknown_21

unknown_22
identityѕбStatefulPartitionedCallф
StatefulPartitionedCallStatefulPartitionedCallinput_2input_1unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22*%
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *8
_read_only_resource_inputs
	
*-
config_proto

CPU

GPU 2J 8ѓ *O
fJRH
F__inference_functional_1_layer_call_and_return_conditional_losses_68552
StatefulPartitionedCallј
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*Ў
_input_shapesЄ
ё:         :         :: ::::::::::::::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:P L
'
_output_shapes
:         
!
_user_specified_name	input_2:PL
'
_output_shapes
:         
!
_user_specified_name	input_1
¤
Н
"text_vectorization_cond_false_6968'
#text_vectorization_cond_placeholder`
\text_vectorization_cond_strided_slice_text_vectorization_raggedtotensor_raggedtensortotensor	$
 text_vectorization_cond_identity	Ф
+text_vectorization/cond/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2-
+text_vectorization/cond/strided_slice/stack»
-text_vectorization/cond/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ▄  2/
-text_vectorization/cond/strided_slice/stack_1»
-text_vectorization/cond/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2/
-text_vectorization/cond/strided_slice/stack_2╠
%text_vectorization/cond/strided_sliceStridedSlice\text_vectorization_cond_strided_slice_text_vectorization_raggedtotensor_raggedtensortotensor4text_vectorization/cond/strided_slice/stack:output:06text_vectorization/cond/strided_slice/stack_1:output:06text_vectorization/cond/strided_slice/stack_2:output:0*
Index0*
T0	*0
_output_shapes
:                  *

begin_mask*
end_mask2'
%text_vectorization/cond/strided_slice╗
 text_vectorization/cond/IdentityIdentity.text_vectorization/cond/strided_slice:output:0*
T0	*0
_output_shapes
:                  2"
 text_vectorization/cond/Identity"M
 text_vectorization_cond_identity)text_vectorization/cond/Identity:output:0*1
_input_shapes 
: :                  : 

_output_shapes
: :62
0
_output_shapes
:                  
д
Е
A__inference_dense_4_layer_call_and_return_conditional_losses_7732

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityѕЇ
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:E@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulї
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpЂ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         E:::O K
'
_output_shapes
:         E
 
_user_specified_nameinputs
Е
Е
A__inference_dense_2_layer_call_and_return_conditional_losses_7679

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityѕј
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	ђ@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulї
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpЂ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*/
_input_shapes
:         ђ:::P L
(
_output_shapes
:         ђ
 
_user_specified_nameinputs
ъ
С
!text_vectorization_cond_true_7414A
=text_vectorization_cond_pad_paddings_1_text_vectorization_subV
Rtext_vectorization_cond_pad_text_vectorization_raggedtotensor_raggedtensortotensor	$
 text_vectorization_cond_identity	ќ
(text_vectorization/cond/Pad/paddings/1/0Const*
_output_shapes
: *
dtype0*
value	B : 2*
(text_vectorization/cond/Pad/paddings/1/0Э
&text_vectorization/cond/Pad/paddings/1Pack1text_vectorization/cond/Pad/paddings/1/0:output:0=text_vectorization_cond_pad_paddings_1_text_vectorization_sub*
N*
T0*
_output_shapes
:2(
&text_vectorization/cond/Pad/paddings/1Ц
(text_vectorization/cond/Pad/paddings/0_1Const*
_output_shapes
:*
dtype0*
valueB"        2*
(text_vectorization/cond/Pad/paddings/0_1Ж
$text_vectorization/cond/Pad/paddingsPack1text_vectorization/cond/Pad/paddings/0_1:output:0/text_vectorization/cond/Pad/paddings/1:output:0*
N*
T0*
_output_shapes

:2&
$text_vectorization/cond/Pad/paddings 
text_vectorization/cond/PadPadRtext_vectorization_cond_pad_text_vectorization_raggedtotensor_raggedtensortotensor-text_vectorization/cond/Pad/paddings:output:0*
T0	*0
_output_shapes
:                  2
text_vectorization/cond/Pad▒
 text_vectorization/cond/IdentityIdentity$text_vectorization/cond/Pad:output:0*
T0	*0
_output_shapes
:                  2"
 text_vectorization/cond/Identity"M
 text_vectorization_cond_identity)text_vectorization/cond/Identity:output:0*1
_input_shapes 
: :                  : 

_output_shapes
: :62
0
_output_shapes
:                  
╩
Е
A__inference_dense_9_layer_call_and_return_conditional_losses_6550

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityѕЇ
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2
MatMulї
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOpЂ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2	
BiasAddd
IdentityIdentityBiasAdd:output:0*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @:::O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
д
Е
A__inference_dense_7_layer_call_and_return_conditional_losses_6497

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityѕЇ
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulї
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpЂ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @:::O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
о
y
$__inference_dense_layer_call_fn_7648

inputs	
unknown
	unknown_0
identityѕбStatefulPartitionedCall­
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2	*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:         ђ*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *H
fCRA
?__inference_dense_layer_call_and_return_conditional_losses_62812
StatefulPartitionedCallЈ
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*(
_output_shapes
:         ђ2

Identity"
identityIdentity:output:0*/
_input_shapes
:         ▄::22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:         ▄
 
_user_specified_nameinputs
д
Е
A__inference_dense_5_layer_call_and_return_conditional_losses_7752

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityѕЇ
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulї
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpЂ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @:::O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
¤
Н
"text_vectorization_cond_false_7252'
#text_vectorization_cond_placeholder`
\text_vectorization_cond_strided_slice_text_vectorization_raggedtotensor_raggedtensortotensor	$
 text_vectorization_cond_identity	Ф
+text_vectorization/cond/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2-
+text_vectorization/cond/strided_slice/stack»
-text_vectorization/cond/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ▄  2/
-text_vectorization/cond/strided_slice/stack_1»
-text_vectorization/cond/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2/
-text_vectorization/cond/strided_slice/stack_2╠
%text_vectorization/cond/strided_sliceStridedSlice\text_vectorization_cond_strided_slice_text_vectorization_raggedtotensor_raggedtensortotensor4text_vectorization/cond/strided_slice/stack:output:06text_vectorization/cond/strided_slice/stack_1:output:06text_vectorization/cond/strided_slice/stack_2:output:0*
Index0*
T0	*0
_output_shapes
:                  *

begin_mask*
end_mask2'
%text_vectorization/cond/strided_slice╗
 text_vectorization/cond/IdentityIdentity.text_vectorization/cond/strided_slice:output:0*
T0	*0
_output_shapes
:                  2"
 text_vectorization/cond/Identity"M
 text_vectorization_cond_identity)text_vectorization/cond/Identity:output:0*1
_input_shapes 
: :                  : 

_output_shapes
: :62
0
_output_shapes
:                  
д
Е
A__inference_dense_6_layer_call_and_return_conditional_losses_7772

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityѕЇ
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulї
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpЂ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @:::O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
Ю 
З
__inference__wrapped_model_6189
input_2
input_1g
cfunctional_1_text_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handleh
dfunctional_1_text_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value	5
1functional_1_dense_matmul_readvariableop_resource6
2functional_1_dense_biasadd_readvariableop_resource7
3functional_1_dense_1_matmul_readvariableop_resource8
4functional_1_dense_1_biasadd_readvariableop_resource7
3functional_1_dense_2_matmul_readvariableop_resource8
4functional_1_dense_2_biasadd_readvariableop_resource>
:functional_1_normalization_reshape_readvariableop_resource@
<functional_1_normalization_reshape_1_readvariableop_resource7
3functional_1_dense_3_matmul_readvariableop_resource8
4functional_1_dense_3_biasadd_readvariableop_resource7
3functional_1_dense_4_matmul_readvariableop_resource8
4functional_1_dense_4_biasadd_readvariableop_resource7
3functional_1_dense_5_matmul_readvariableop_resource8
4functional_1_dense_5_biasadd_readvariableop_resource7
3functional_1_dense_6_matmul_readvariableop_resource8
4functional_1_dense_6_biasadd_readvariableop_resource7
3functional_1_dense_7_matmul_readvariableop_resource8
4functional_1_dense_7_biasadd_readvariableop_resource7
3functional_1_dense_8_matmul_readvariableop_resource8
4functional_1_dense_8_biasadd_readvariableop_resource7
3functional_1_dense_9_matmul_readvariableop_resource8
4functional_1_dense_9_biasadd_readvariableop_resource
identityѕбVfunctional_1/text_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2Џ
+functional_1/text_vectorization/StringLowerStringLowerinput_1*'
_output_shapes
:         2-
+functional_1/text_vectorization/StringLowerц
2functional_1/text_vectorization/StaticRegexReplaceStaticRegexReplace4functional_1/text_vectorization/StringLower:output:0*'
_output_shapes
:         *6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite 24
2functional_1/text_vectorization/StaticRegexReplaceУ
'functional_1/text_vectorization/SqueezeSqueeze;functional_1/text_vectorization/StaticRegexReplace:output:0*
T0*#
_output_shapes
:         *
squeeze_dims

         2)
'functional_1/text_vectorization/SqueezeЛ
Ffunctional_1/text_vectorization/StringsByteSplit/StringSplit/delimiterConst*
_output_shapes
: *
dtype0*
valueB B 2H
Ffunctional_1/text_vectorization/StringsByteSplit/StringSplit/delimiterя
<functional_1/text_vectorization/StringsByteSplit/StringSplitStringSplit0functional_1/text_vectorization/Squeeze:output:0Ofunctional_1/text_vectorization/StringsByteSplit/StringSplit/delimiter:output:0*<
_output_shapes*
(:         :         :*

skip_empty( 2>
<functional_1/text_vectorization/StringsByteSplit/StringSplitП
Dfunctional_1/text_vectorization/StringsByteSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2F
Dfunctional_1/text_vectorization/StringsByteSplit/strided_slice/stackр
Ffunctional_1/text_vectorization/StringsByteSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       2H
Ffunctional_1/text_vectorization/StringsByteSplit/strided_slice/stack_1р
Ffunctional_1/text_vectorization/StringsByteSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2H
Ffunctional_1/text_vectorization/StringsByteSplit/strided_slice/stack_2Й
>functional_1/text_vectorization/StringsByteSplit/strided_sliceStridedSliceFfunctional_1/text_vectorization/StringsByteSplit/StringSplit:indices:0Mfunctional_1/text_vectorization/StringsByteSplit/strided_slice/stack:output:0Ofunctional_1/text_vectorization/StringsByteSplit/strided_slice/stack_1:output:0Ofunctional_1/text_vectorization/StringsByteSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:         *

begin_mask*
end_mask*
shrink_axis_mask2@
>functional_1/text_vectorization/StringsByteSplit/strided_slice┌
Ffunctional_1/text_vectorization/StringsByteSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2H
Ffunctional_1/text_vectorization/StringsByteSplit/strided_slice_1/stackя
Hfunctional_1/text_vectorization/StringsByteSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2J
Hfunctional_1/text_vectorization/StringsByteSplit/strided_slice_1/stack_1я
Hfunctional_1/text_vectorization/StringsByteSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2J
Hfunctional_1/text_vectorization/StringsByteSplit/strided_slice_1/stack_2Ќ
@functional_1/text_vectorization/StringsByteSplit/strided_slice_1StridedSliceDfunctional_1/text_vectorization/StringsByteSplit/StringSplit:shape:0Ofunctional_1/text_vectorization/StringsByteSplit/strided_slice_1/stack:output:0Qfunctional_1/text_vectorization/StringsByteSplit/strided_slice_1/stack_1:output:0Qfunctional_1/text_vectorization/StringsByteSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask2B
@functional_1/text_vectorization/StringsByteSplit/strided_slice_1Я
gfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCastGfunctional_1/text_vectorization/StringsByteSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:         2i
gfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast┘
ifunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1CastIfunctional_1/text_vectorization/StringsByteSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: 2k
ifunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Ђ
qfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShapekfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:2s
qfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape░
qfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2s
qfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstЁ
pfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdzfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0zfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: 2r
pfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod░
ufunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : 2w
ufunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yЉ
sfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreateryfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0~functional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: 2u
sfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterЋ
pfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastwfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: 2r
pfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast┤
sfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2u
sfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1ш
ofunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMaxkfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0|functional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: 2q
ofunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maxе
qfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :2s
qfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yѓ
ofunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2xfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0zfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: 2q
ofunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addш
ofunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMultfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0sfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: 2q
ofunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulЩ
sfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximummfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0sfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: 2u
sfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum■
sfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimummfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0wfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: 2u
sfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumГ
sfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 2u
sfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2і
tfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincountkfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0wfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0|functional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:         2v
tfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincountб
nfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : 2p
nfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisё
ifunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsum{functional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0wfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:         2k
ifunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum▓
rfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R 2t
rfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0б
nfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2p
nfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisђ
ifunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2{functional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0ofunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0wfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:         2k
ifunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat┴
Vfunctional_1/text_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2LookupTableFindV2cfunctional_1_text_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handleEfunctional_1/text_vectorization/StringsByteSplit/StringSplit:values:0dfunctional_1_text_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*#
_output_shapes
:         2X
Vfunctional_1/text_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2а
?functional_1/text_vectorization/string_lookup/assert_equal/NoOpNoOp*
_output_shapes
 2A
?functional_1/text_vectorization/string_lookup/assert_equal/NoOpІ
6functional_1/text_vectorization/string_lookup/IdentityIdentity_functional_1/text_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:         28
6functional_1/text_vectorization/string_lookup/Identityб
8functional_1/text_vectorization/string_lookup/Identity_1Identityrfunctional_1/text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat:output:0*
T0	*#
_output_shapes
:         2:
8functional_1/text_vectorization/string_lookup/Identity_1Й
<functional_1/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R 2>
<functional_1/text_vectorization/RaggedToTensor/default_valueи
4functional_1/text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
: *
dtype0	*
valueB	 R
         26
4functional_1/text_vectorization/RaggedToTensor/Const╦
Cfunctional_1/text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor=functional_1/text_vectorization/RaggedToTensor/Const:output:0?functional_1/text_vectorization/string_lookup/Identity:output:0Efunctional_1/text_vectorization/RaggedToTensor/default_value:output:0Afunctional_1/text_vectorization/string_lookup/Identity_1:output:0*
T0	*
Tindex0	*
Tshape0	*0
_output_shapes
:                  *
num_row_partition_tensors*%
row_partition_types

ROW_SPLITS2E
Cfunctional_1/text_vectorization/RaggedToTensor/RaggedTensorToTensor╩
%functional_1/text_vectorization/ShapeShapeLfunctional_1/text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
T0	*
_output_shapes
:2'
%functional_1/text_vectorization/Shape┤
3functional_1/text_vectorization/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:25
3functional_1/text_vectorization/strided_slice/stackИ
5functional_1/text_vectorization/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:27
5functional_1/text_vectorization/strided_slice/stack_1И
5functional_1/text_vectorization/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:27
5functional_1/text_vectorization/strided_slice/stack_2б
-functional_1/text_vectorization/strided_sliceStridedSlice.functional_1/text_vectorization/Shape:output:0<functional_1/text_vectorization/strided_slice/stack:output:0>functional_1/text_vectorization/strided_slice/stack_1:output:0>functional_1/text_vectorization/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2/
-functional_1/text_vectorization/strided_sliceЉ
%functional_1/text_vectorization/sub/xConst*
_output_shapes
: *
dtype0*
value
B :▄2'
%functional_1/text_vectorization/sub/x┌
#functional_1/text_vectorization/subSub.functional_1/text_vectorization/sub/x:output:06functional_1/text_vectorization/strided_slice:output:0*
T0*
_output_shapes
: 2%
#functional_1/text_vectorization/subЊ
&functional_1/text_vectorization/Less/yConst*
_output_shapes
: *
dtype0*
value
B :▄2(
&functional_1/text_vectorization/Less/yя
$functional_1/text_vectorization/LessLess6functional_1/text_vectorization/strided_slice:output:0/functional_1/text_vectorization/Less/y:output:0*
T0*
_output_shapes
: 2&
$functional_1/text_vectorization/Lessн
$functional_1/text_vectorization/condStatelessIf(functional_1/text_vectorization/Less:z:0'functional_1/text_vectorization/sub:z:0Lfunctional_1/text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
Tcond0
*
Tin
2	*
Tout
2	*
_lower_using_switch_merge(*0
_output_shapes
:                  * 
_read_only_resource_inputs
 *B
else_branch3R1
/functional_1_text_vectorization_cond_false_6085*/
output_shapes
:                  *A
then_branch2R0
.functional_1_text_vectorization_cond_true_60842&
$functional_1/text_vectorization/cond╠
-functional_1/text_vectorization/cond/IdentityIdentity-functional_1/text_vectorization/cond:output:0*
T0	*(
_output_shapes
:         ▄2/
-functional_1/text_vectorization/cond/Identity┤
functional_1/dense/CastCast6functional_1/text_vectorization/cond/Identity:output:0*

DstT0*

SrcT0	*(
_output_shapes
:         ▄2
functional_1/dense/Cast╚
(functional_1/dense/MatMul/ReadVariableOpReadVariableOp1functional_1_dense_matmul_readvariableop_resource* 
_output_shapes
:
▄ђ*
dtype02*
(functional_1/dense/MatMul/ReadVariableOp┬
functional_1/dense/MatMulMatMulfunctional_1/dense/Cast:y:00functional_1/dense/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2
functional_1/dense/MatMulк
)functional_1/dense/BiasAdd/ReadVariableOpReadVariableOp2functional_1_dense_biasadd_readvariableop_resource*
_output_shapes	
:ђ*
dtype02+
)functional_1/dense/BiasAdd/ReadVariableOp╬
functional_1/dense/BiasAddBiasAdd#functional_1/dense/MatMul:product:01functional_1/dense/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2
functional_1/dense/BiasAddњ
functional_1/dense/ReluRelu#functional_1/dense/BiasAdd:output:0*
T0*(
_output_shapes
:         ђ2
functional_1/dense/Relu╬
*functional_1/dense_1/MatMul/ReadVariableOpReadVariableOp3functional_1_dense_1_matmul_readvariableop_resource* 
_output_shapes
:
ђђ*
dtype02,
*functional_1/dense_1/MatMul/ReadVariableOpм
functional_1/dense_1/MatMulMatMul%functional_1/dense/Relu:activations:02functional_1/dense_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2
functional_1/dense_1/MatMul╠
+functional_1/dense_1/BiasAdd/ReadVariableOpReadVariableOp4functional_1_dense_1_biasadd_readvariableop_resource*
_output_shapes	
:ђ*
dtype02-
+functional_1/dense_1/BiasAdd/ReadVariableOpо
functional_1/dense_1/BiasAddBiasAdd%functional_1/dense_1/MatMul:product:03functional_1/dense_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2
functional_1/dense_1/BiasAddў
functional_1/dense_1/ReluRelu%functional_1/dense_1/BiasAdd:output:0*
T0*(
_output_shapes
:         ђ2
functional_1/dense_1/Relu═
*functional_1/dense_2/MatMul/ReadVariableOpReadVariableOp3functional_1_dense_2_matmul_readvariableop_resource*
_output_shapes
:	ђ@*
dtype02,
*functional_1/dense_2/MatMul/ReadVariableOpМ
functional_1/dense_2/MatMulMatMul'functional_1/dense_1/Relu:activations:02functional_1/dense_2/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_2/MatMul╦
+functional_1/dense_2/BiasAdd/ReadVariableOpReadVariableOp4functional_1_dense_2_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02-
+functional_1/dense_2/BiasAdd/ReadVariableOpН
functional_1/dense_2/BiasAddBiasAdd%functional_1/dense_2/MatMul:product:03functional_1/dense_2/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_2/BiasAddЌ
functional_1/dense_2/ReluRelu%functional_1/dense_2/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
functional_1/dense_2/ReluП
1functional_1/normalization/Reshape/ReadVariableOpReadVariableOp:functional_1_normalization_reshape_readvariableop_resource*
_output_shapes
:*
dtype023
1functional_1/normalization/Reshape/ReadVariableOpЦ
(functional_1/normalization/Reshape/shapeConst*
_output_shapes
:*
dtype0*
valueB"      2*
(functional_1/normalization/Reshape/shapeЖ
"functional_1/normalization/ReshapeReshape9functional_1/normalization/Reshape/ReadVariableOp:value:01functional_1/normalization/Reshape/shape:output:0*
T0*
_output_shapes

:2$
"functional_1/normalization/Reshapeс
3functional_1/normalization/Reshape_1/ReadVariableOpReadVariableOp<functional_1_normalization_reshape_1_readvariableop_resource*
_output_shapes
:*
dtype025
3functional_1/normalization/Reshape_1/ReadVariableOpЕ
*functional_1/normalization/Reshape_1/shapeConst*
_output_shapes
:*
dtype0*
valueB"      2,
*functional_1/normalization/Reshape_1/shapeЫ
$functional_1/normalization/Reshape_1Reshape;functional_1/normalization/Reshape_1/ReadVariableOp:value:03functional_1/normalization/Reshape_1/shape:output:0*
T0*
_output_shapes

:2&
$functional_1/normalization/Reshape_1»
functional_1/normalization/subSubinput_2+functional_1/normalization/Reshape:output:0*
T0*'
_output_shapes
:         2 
functional_1/normalization/subб
functional_1/normalization/SqrtSqrt-functional_1/normalization/Reshape_1:output:0*
T0*
_output_shapes

:2!
functional_1/normalization/Sqrt╬
"functional_1/normalization/truedivRealDiv"functional_1/normalization/sub:z:0#functional_1/normalization/Sqrt:y:0*
T0*'
_output_shapes
:         2$
"functional_1/normalization/truediv╠
*functional_1/dense_3/MatMul/ReadVariableOpReadVariableOp3functional_1_dense_3_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02,
*functional_1/dense_3/MatMul/ReadVariableOpМ
functional_1/dense_3/MatMulMatMul'functional_1/dense_2/Relu:activations:02functional_1/dense_3/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_3/MatMul╦
+functional_1/dense_3/BiasAdd/ReadVariableOpReadVariableOp4functional_1_dense_3_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02-
+functional_1/dense_3/BiasAdd/ReadVariableOpН
functional_1/dense_3/BiasAddBiasAdd%functional_1/dense_3/MatMul:product:03functional_1/dense_3/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_3/BiasAddЌ
functional_1/dense_3/ReluRelu%functional_1/dense_3/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
functional_1/dense_3/Reluј
$functional_1/concatenate/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2&
$functional_1/concatenate/concat/axisЅ
functional_1/concatenate/concatConcatV2&functional_1/normalization/truediv:z:0'functional_1/dense_3/Relu:activations:0-functional_1/concatenate/concat/axis:output:0*
N*
T0*'
_output_shapes
:         E2!
functional_1/concatenate/concat╠
*functional_1/dense_4/MatMul/ReadVariableOpReadVariableOp3functional_1_dense_4_matmul_readvariableop_resource*
_output_shapes

:E@*
dtype02,
*functional_1/dense_4/MatMul/ReadVariableOpн
functional_1/dense_4/MatMulMatMul(functional_1/concatenate/concat:output:02functional_1/dense_4/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_4/MatMul╦
+functional_1/dense_4/BiasAdd/ReadVariableOpReadVariableOp4functional_1_dense_4_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02-
+functional_1/dense_4/BiasAdd/ReadVariableOpН
functional_1/dense_4/BiasAddBiasAdd%functional_1/dense_4/MatMul:product:03functional_1/dense_4/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_4/BiasAddЌ
functional_1/dense_4/ReluRelu%functional_1/dense_4/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
functional_1/dense_4/Relu╠
*functional_1/dense_5/MatMul/ReadVariableOpReadVariableOp3functional_1_dense_5_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02,
*functional_1/dense_5/MatMul/ReadVariableOpМ
functional_1/dense_5/MatMulMatMul'functional_1/dense_4/Relu:activations:02functional_1/dense_5/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_5/MatMul╦
+functional_1/dense_5/BiasAdd/ReadVariableOpReadVariableOp4functional_1_dense_5_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02-
+functional_1/dense_5/BiasAdd/ReadVariableOpН
functional_1/dense_5/BiasAddBiasAdd%functional_1/dense_5/MatMul:product:03functional_1/dense_5/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_5/BiasAddЌ
functional_1/dense_5/ReluRelu%functional_1/dense_5/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
functional_1/dense_5/Relu╠
*functional_1/dense_6/MatMul/ReadVariableOpReadVariableOp3functional_1_dense_6_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02,
*functional_1/dense_6/MatMul/ReadVariableOpМ
functional_1/dense_6/MatMulMatMul'functional_1/dense_5/Relu:activations:02functional_1/dense_6/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_6/MatMul╦
+functional_1/dense_6/BiasAdd/ReadVariableOpReadVariableOp4functional_1_dense_6_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02-
+functional_1/dense_6/BiasAdd/ReadVariableOpН
functional_1/dense_6/BiasAddBiasAdd%functional_1/dense_6/MatMul:product:03functional_1/dense_6/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_6/BiasAddЌ
functional_1/dense_6/ReluRelu%functional_1/dense_6/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
functional_1/dense_6/Relu╠
*functional_1/dense_7/MatMul/ReadVariableOpReadVariableOp3functional_1_dense_7_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02,
*functional_1/dense_7/MatMul/ReadVariableOpМ
functional_1/dense_7/MatMulMatMul'functional_1/dense_6/Relu:activations:02functional_1/dense_7/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_7/MatMul╦
+functional_1/dense_7/BiasAdd/ReadVariableOpReadVariableOp4functional_1_dense_7_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02-
+functional_1/dense_7/BiasAdd/ReadVariableOpН
functional_1/dense_7/BiasAddBiasAdd%functional_1/dense_7/MatMul:product:03functional_1/dense_7/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_7/BiasAddЌ
functional_1/dense_7/ReluRelu%functional_1/dense_7/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
functional_1/dense_7/Relu╠
*functional_1/dense_8/MatMul/ReadVariableOpReadVariableOp3functional_1_dense_8_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02,
*functional_1/dense_8/MatMul/ReadVariableOpМ
functional_1/dense_8/MatMulMatMul'functional_1/dense_7/Relu:activations:02functional_1/dense_8/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_8/MatMul╦
+functional_1/dense_8/BiasAdd/ReadVariableOpReadVariableOp4functional_1_dense_8_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02-
+functional_1/dense_8/BiasAdd/ReadVariableOpН
functional_1/dense_8/BiasAddBiasAdd%functional_1/dense_8/MatMul:product:03functional_1/dense_8/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
functional_1/dense_8/BiasAddЌ
functional_1/dense_8/ReluRelu%functional_1/dense_8/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
functional_1/dense_8/Relu╠
*functional_1/dense_9/MatMul/ReadVariableOpReadVariableOp3functional_1_dense_9_matmul_readvariableop_resource*
_output_shapes

:@*
dtype02,
*functional_1/dense_9/MatMul/ReadVariableOpМ
functional_1/dense_9/MatMulMatMul'functional_1/dense_8/Relu:activations:02functional_1/dense_9/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2
functional_1/dense_9/MatMul╦
+functional_1/dense_9/BiasAdd/ReadVariableOpReadVariableOp4functional_1_dense_9_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02-
+functional_1/dense_9/BiasAdd/ReadVariableOpН
functional_1/dense_9/BiasAddBiasAdd%functional_1/dense_9/MatMul:product:03functional_1/dense_9/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2
functional_1/dense_9/BiasAddм
IdentityIdentity%functional_1/dense_9/BiasAdd:output:0W^functional_1/text_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*Ў
_input_shapesЄ
ё:         :         :: ::::::::::::::::::::::2░
Vfunctional_1/text_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2Vfunctional_1/text_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2:P L
'
_output_shapes
:         
!
_user_specified_name	input_2:PL
'
_output_shapes
:         
!
_user_specified_name	input_1
о
{
&__inference_dense_4_layer_call_fn_7741

inputs
unknown
	unknown_0
identityѕбStatefulPartitionedCallы
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_4_layer_call_and_return_conditional_losses_64162
StatefulPartitionedCallј
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         E::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:         E
 
_user_specified_nameinputs
д
Е
A__inference_dense_5_layer_call_and_return_conditional_losses_6443

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityѕЇ
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulї
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpЂ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @:::O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
а	
Ж
__inference_restore_fn_7882
restored_tensors_0
restored_tensors_1	L
Hstring_lookup_index_table_table_restore_lookuptableimportv2_table_handle
identityѕб;string_lookup_index_table_table_restore/LookupTableImportV2я
;string_lookup_index_table_table_restore/LookupTableImportV2LookupTableImportV2Hstring_lookup_index_table_table_restore_lookuptableimportv2_table_handlerestored_tensors_0restored_tensors_1",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*
_output_shapes
 2=
;string_lookup_index_table_table_restore/LookupTableImportV2P
ConstConst*
_output_shapes
: *
dtype0*
value	B :2
ConstЈ
IdentityIdentityConst:output:0<^string_lookup_index_table_table_restore/LookupTableImportV2*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0**
_input_shapes
:         ::2z
;string_lookup_index_table_table_restore/LookupTableImportV2;string_lookup_index_table_table_restore/LookupTableImportV2:W S
#
_output_shapes
:         
,
_user_specified_namerestored_tensors_0:LH

_output_shapes
:
,
_user_specified_namerestored_tensors_1
о
{
&__inference_dense_5_layer_call_fn_7761

inputs
unknown
	unknown_0
identityѕбStatefulPartitionedCallы
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_5_layer_call_and_return_conditional_losses_64432
StatefulPartitionedCallј
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
Ј	
Д
?__inference_dense_layer_call_and_return_conditional_losses_7639

inputs	"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityѕ^
CastCastinputs*

DstT0*

SrcT0	*(
_output_shapes
:         ▄2
CastЈ
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
▄ђ*
dtype02
MatMul/ReadVariableOpv
MatMulMatMulCast:y:0MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2
MatMulЇ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:ђ*
dtype02
BiasAdd/ReadVariableOpѓ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:         ђ2
Relug
IdentityIdentityRelu:activations:0*
T0*(
_output_shapes
:         ђ2

Identity"
identityIdentity:output:0*/
_input_shapes
:         ▄:::P L
(
_output_shapes
:         ▄
 
_user_specified_nameinputs
Мо
п

F__inference_functional_1_layer_call_and_return_conditional_losses_7356
inputs_0
inputs_1Z
Vtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handle[
Wtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value	(
$dense_matmul_readvariableop_resource)
%dense_biasadd_readvariableop_resource*
&dense_1_matmul_readvariableop_resource+
'dense_1_biasadd_readvariableop_resource*
&dense_2_matmul_readvariableop_resource+
'dense_2_biasadd_readvariableop_resource1
-normalization_reshape_readvariableop_resource3
/normalization_reshape_1_readvariableop_resource*
&dense_3_matmul_readvariableop_resource+
'dense_3_biasadd_readvariableop_resource*
&dense_4_matmul_readvariableop_resource+
'dense_4_biasadd_readvariableop_resource*
&dense_5_matmul_readvariableop_resource+
'dense_5_biasadd_readvariableop_resource*
&dense_6_matmul_readvariableop_resource+
'dense_6_biasadd_readvariableop_resource*
&dense_7_matmul_readvariableop_resource+
'dense_7_biasadd_readvariableop_resource*
&dense_8_matmul_readvariableop_resource+
'dense_8_biasadd_readvariableop_resource*
&dense_9_matmul_readvariableop_resource+
'dense_9_biasadd_readvariableop_resource
identityѕбItext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2ѓ
text_vectorization/StringLowerStringLowerinputs_1*'
_output_shapes
:         2 
text_vectorization/StringLower§
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*'
_output_shapes
:         *6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite 2'
%text_vectorization/StaticRegexReplace┴
text_vectorization/SqueezeSqueeze.text_vectorization/StaticRegexReplace:output:0*
T0*#
_output_shapes
:         *
squeeze_dims

         2
text_vectorization/Squeezeи
9text_vectorization/StringsByteSplit/StringSplit/delimiterConst*
_output_shapes
: *
dtype0*
valueB B 2;
9text_vectorization/StringsByteSplit/StringSplit/delimiterф
/text_vectorization/StringsByteSplit/StringSplitStringSplit#text_vectorization/Squeeze:output:0Btext_vectorization/StringsByteSplit/StringSplit/delimiter:output:0*<
_output_shapes*
(:         :         :*

skip_empty( 21
/text_vectorization/StringsByteSplit/StringSplit├
7text_vectorization/StringsByteSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        29
7text_vectorization/StringsByteSplit/strided_slice/stackК
9text_vectorization/StringsByteSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       2;
9text_vectorization/StringsByteSplit/strided_slice/stack_1К
9text_vectorization/StringsByteSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2;
9text_vectorization/StringsByteSplit/strided_slice/stack_2­
1text_vectorization/StringsByteSplit/strided_sliceStridedSlice9text_vectorization/StringsByteSplit/StringSplit:indices:0@text_vectorization/StringsByteSplit/strided_slice/stack:output:0Btext_vectorization/StringsByteSplit/strided_slice/stack_1:output:0Btext_vectorization/StringsByteSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:         *

begin_mask*
end_mask*
shrink_axis_mask23
1text_vectorization/StringsByteSplit/strided_slice└
9text_vectorization/StringsByteSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2;
9text_vectorization/StringsByteSplit/strided_slice_1/stack─
;text_vectorization/StringsByteSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2=
;text_vectorization/StringsByteSplit/strided_slice_1/stack_1─
;text_vectorization/StringsByteSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2=
;text_vectorization/StringsByteSplit/strided_slice_1/stack_2╔
3text_vectorization/StringsByteSplit/strided_slice_1StridedSlice7text_vectorization/StringsByteSplit/StringSplit:shape:0Btext_vectorization/StringsByteSplit/strided_slice_1/stack:output:0Dtext_vectorization/StringsByteSplit/strided_slice_1/stack_1:output:0Dtext_vectorization/StringsByteSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask25
3text_vectorization/StringsByteSplit/strided_slice_1╣
Ztext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast:text_vectorization/StringsByteSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:         2\
Ztext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast▓
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast<text_vectorization/StringsByteSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: 2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1┌
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShape^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shapeќ
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstЛ
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdmtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0mtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: 2e
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prodќ
htext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : 2j
htext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yП
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreaterltext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0qtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterЬ
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastjtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: 2e
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Castџ
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1┴
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMax^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0otext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maxј
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y╬
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ktext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0mtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add┴
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulgtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulк
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum`text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum╩
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum`text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumЊ
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2╔
gtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincount^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0otext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:         2i
gtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincountѕ
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : 2c
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisл
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumntext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:         2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsumў
etext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R 2g
etext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0ѕ
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2c
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis┐
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2ntext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:         2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatђ
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Vtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handle8text_vectorization/StringsByteSplit/StringSplit:values:0Wtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*#
_output_shapes
:         2K
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2є
2text_vectorization/string_lookup/assert_equal/NoOpNoOp*
_output_shapes
 24
2text_vectorization/string_lookup/assert_equal/NoOpС
)text_vectorization/string_lookup/IdentityIdentityRtext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:         2+
)text_vectorization/string_lookup/Identityч
+text_vectorization/string_lookup/Identity_1Identityetext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat:output:0*
T0	*#
_output_shapes
:         2-
+text_vectorization/string_lookup/Identity_1ц
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R 21
/text_vectorization/RaggedToTensor/default_valueЮ
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
: *
dtype0	*
valueB	 R
         2)
'text_vectorization/RaggedToTensor/Const§
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:02text_vectorization/string_lookup/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:04text_vectorization/string_lookup/Identity_1:output:0*
T0	*
Tindex0	*
Tshape0	*0
_output_shapes
:                  *
num_row_partition_tensors*%
row_partition_types

ROW_SPLITS28
6text_vectorization/RaggedToTensor/RaggedTensorToTensorБ
text_vectorization/ShapeShape?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
T0	*
_output_shapes
:2
text_vectorization/Shapeџ
&text_vectorization/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&text_vectorization/strided_slice/stackъ
(text_vectorization/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(text_vectorization/strided_slice/stack_1ъ
(text_vectorization/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(text_vectorization/strided_slice/stack_2н
 text_vectorization/strided_sliceStridedSlice!text_vectorization/Shape:output:0/text_vectorization/strided_slice/stack:output:01text_vectorization/strided_slice/stack_1:output:01text_vectorization/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 text_vectorization/strided_slicew
text_vectorization/sub/xConst*
_output_shapes
: *
dtype0*
value
B :▄2
text_vectorization/sub/xд
text_vectorization/subSub!text_vectorization/sub/x:output:0)text_vectorization/strided_slice:output:0*
T0*
_output_shapes
: 2
text_vectorization/suby
text_vectorization/Less/yConst*
_output_shapes
: *
dtype0*
value
B :▄2
text_vectorization/Less/yф
text_vectorization/LessLess)text_vectorization/strided_slice:output:0"text_vectorization/Less/y:output:0*
T0*
_output_shapes
: 2
text_vectorization/Lessщ
text_vectorization/condStatelessIftext_vectorization/Less:z:0text_vectorization/sub:z:0?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
Tcond0
*
Tin
2	*
Tout
2	*
_lower_using_switch_merge(*0
_output_shapes
:                  * 
_read_only_resource_inputs
 *5
else_branch&R$
"text_vectorization_cond_false_7252*/
output_shapes
:                  *4
then_branch%R#
!text_vectorization_cond_true_72512
text_vectorization/condЦ
 text_vectorization/cond/IdentityIdentity text_vectorization/cond:output:0*
T0	*(
_output_shapes
:         ▄2"
 text_vectorization/cond/IdentityЇ

dense/CastCast)text_vectorization/cond/Identity:output:0*

DstT0*

SrcT0	*(
_output_shapes
:         ▄2

dense/CastА
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource* 
_output_shapes
:
▄ђ*
dtype02
dense/MatMul/ReadVariableOpј
dense/MatMulMatMuldense/Cast:y:0#dense/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2
dense/MatMulЪ
dense/BiasAdd/ReadVariableOpReadVariableOp%dense_biasadd_readvariableop_resource*
_output_shapes	
:ђ*
dtype02
dense/BiasAdd/ReadVariableOpџ
dense/BiasAddBiasAdddense/MatMul:product:0$dense/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2
dense/BiasAddk

dense/ReluReludense/BiasAdd:output:0*
T0*(
_output_shapes
:         ђ2

dense/ReluД
dense_1/MatMul/ReadVariableOpReadVariableOp&dense_1_matmul_readvariableop_resource* 
_output_shapes
:
ђђ*
dtype02
dense_1/MatMul/ReadVariableOpъ
dense_1/MatMulMatMuldense/Relu:activations:0%dense_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2
dense_1/MatMulЦ
dense_1/BiasAdd/ReadVariableOpReadVariableOp'dense_1_biasadd_readvariableop_resource*
_output_shapes	
:ђ*
dtype02 
dense_1/BiasAdd/ReadVariableOpб
dense_1/BiasAddBiasAdddense_1/MatMul:product:0&dense_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2
dense_1/BiasAddq
dense_1/ReluReludense_1/BiasAdd:output:0*
T0*(
_output_shapes
:         ђ2
dense_1/Reluд
dense_2/MatMul/ReadVariableOpReadVariableOp&dense_2_matmul_readvariableop_resource*
_output_shapes
:	ђ@*
dtype02
dense_2/MatMul/ReadVariableOpЪ
dense_2/MatMulMatMuldense_1/Relu:activations:0%dense_2/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_2/MatMulц
dense_2/BiasAdd/ReadVariableOpReadVariableOp'dense_2_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_2/BiasAdd/ReadVariableOpА
dense_2/BiasAddBiasAdddense_2/MatMul:product:0&dense_2/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_2/BiasAddp
dense_2/ReluReludense_2/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_2/ReluХ
$normalization/Reshape/ReadVariableOpReadVariableOp-normalization_reshape_readvariableop_resource*
_output_shapes
:*
dtype02&
$normalization/Reshape/ReadVariableOpІ
normalization/Reshape/shapeConst*
_output_shapes
:*
dtype0*
valueB"      2
normalization/Reshape/shapeХ
normalization/ReshapeReshape,normalization/Reshape/ReadVariableOp:value:0$normalization/Reshape/shape:output:0*
T0*
_output_shapes

:2
normalization/Reshape╝
&normalization/Reshape_1/ReadVariableOpReadVariableOp/normalization_reshape_1_readvariableop_resource*
_output_shapes
:*
dtype02(
&normalization/Reshape_1/ReadVariableOpЈ
normalization/Reshape_1/shapeConst*
_output_shapes
:*
dtype0*
valueB"      2
normalization/Reshape_1/shapeЙ
normalization/Reshape_1Reshape.normalization/Reshape_1/ReadVariableOp:value:0&normalization/Reshape_1/shape:output:0*
T0*
_output_shapes

:2
normalization/Reshape_1Ѕ
normalization/subSubinputs_0normalization/Reshape:output:0*
T0*'
_output_shapes
:         2
normalization/sub{
normalization/SqrtSqrt normalization/Reshape_1:output:0*
T0*
_output_shapes

:2
normalization/Sqrtџ
normalization/truedivRealDivnormalization/sub:z:0normalization/Sqrt:y:0*
T0*'
_output_shapes
:         2
normalization/truedivЦ
dense_3/MatMul/ReadVariableOpReadVariableOp&dense_3_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
dense_3/MatMul/ReadVariableOpЪ
dense_3/MatMulMatMuldense_2/Relu:activations:0%dense_3/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_3/MatMulц
dense_3/BiasAdd/ReadVariableOpReadVariableOp'dense_3_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_3/BiasAdd/ReadVariableOpА
dense_3/BiasAddBiasAdddense_3/MatMul:product:0&dense_3/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_3/BiasAddp
dense_3/ReluReludense_3/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_3/Relut
concatenate/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concatenate/concat/axis╚
concatenate/concatConcatV2normalization/truediv:z:0dense_3/Relu:activations:0 concatenate/concat/axis:output:0*
N*
T0*'
_output_shapes
:         E2
concatenate/concatЦ
dense_4/MatMul/ReadVariableOpReadVariableOp&dense_4_matmul_readvariableop_resource*
_output_shapes

:E@*
dtype02
dense_4/MatMul/ReadVariableOpа
dense_4/MatMulMatMulconcatenate/concat:output:0%dense_4/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_4/MatMulц
dense_4/BiasAdd/ReadVariableOpReadVariableOp'dense_4_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_4/BiasAdd/ReadVariableOpА
dense_4/BiasAddBiasAdddense_4/MatMul:product:0&dense_4/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_4/BiasAddp
dense_4/ReluReludense_4/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_4/ReluЦ
dense_5/MatMul/ReadVariableOpReadVariableOp&dense_5_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
dense_5/MatMul/ReadVariableOpЪ
dense_5/MatMulMatMuldense_4/Relu:activations:0%dense_5/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_5/MatMulц
dense_5/BiasAdd/ReadVariableOpReadVariableOp'dense_5_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_5/BiasAdd/ReadVariableOpА
dense_5/BiasAddBiasAdddense_5/MatMul:product:0&dense_5/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_5/BiasAddp
dense_5/ReluReludense_5/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_5/ReluЦ
dense_6/MatMul/ReadVariableOpReadVariableOp&dense_6_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
dense_6/MatMul/ReadVariableOpЪ
dense_6/MatMulMatMuldense_5/Relu:activations:0%dense_6/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_6/MatMulц
dense_6/BiasAdd/ReadVariableOpReadVariableOp'dense_6_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_6/BiasAdd/ReadVariableOpА
dense_6/BiasAddBiasAdddense_6/MatMul:product:0&dense_6/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_6/BiasAddp
dense_6/ReluReludense_6/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_6/ReluЦ
dense_7/MatMul/ReadVariableOpReadVariableOp&dense_7_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
dense_7/MatMul/ReadVariableOpЪ
dense_7/MatMulMatMuldense_6/Relu:activations:0%dense_7/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_7/MatMulц
dense_7/BiasAdd/ReadVariableOpReadVariableOp'dense_7_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_7/BiasAdd/ReadVariableOpА
dense_7/BiasAddBiasAdddense_7/MatMul:product:0&dense_7/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_7/BiasAddp
dense_7/ReluReludense_7/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_7/ReluЦ
dense_8/MatMul/ReadVariableOpReadVariableOp&dense_8_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
dense_8/MatMul/ReadVariableOpЪ
dense_8/MatMulMatMuldense_7/Relu:activations:0%dense_8/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_8/MatMulц
dense_8/BiasAdd/ReadVariableOpReadVariableOp'dense_8_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_8/BiasAdd/ReadVariableOpА
dense_8/BiasAddBiasAdddense_8/MatMul:product:0&dense_8/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_8/BiasAddp
dense_8/ReluReludense_8/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_8/ReluЦ
dense_9/MatMul/ReadVariableOpReadVariableOp&dense_9_matmul_readvariableop_resource*
_output_shapes

:@*
dtype02
dense_9/MatMul/ReadVariableOpЪ
dense_9/MatMulMatMuldense_8/Relu:activations:0%dense_9/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2
dense_9/MatMulц
dense_9/BiasAdd/ReadVariableOpReadVariableOp'dense_9_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02 
dense_9/BiasAdd/ReadVariableOpА
dense_9/BiasAddBiasAdddense_9/MatMul:product:0&dense_9/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2
dense_9/BiasAddИ
IdentityIdentitydense_9/BiasAdd:output:0J^text_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*Ў
_input_shapesЄ
ё:         :         :: ::::::::::::::::::::::2ќ
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2:Q M
'
_output_shapes
:         
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:         
"
_user_specified_name
inputs/1
Мо
п

F__inference_functional_1_layer_call_and_return_conditional_losses_7519
inputs_0
inputs_1Z
Vtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handle[
Wtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value	(
$dense_matmul_readvariableop_resource)
%dense_biasadd_readvariableop_resource*
&dense_1_matmul_readvariableop_resource+
'dense_1_biasadd_readvariableop_resource*
&dense_2_matmul_readvariableop_resource+
'dense_2_biasadd_readvariableop_resource1
-normalization_reshape_readvariableop_resource3
/normalization_reshape_1_readvariableop_resource*
&dense_3_matmul_readvariableop_resource+
'dense_3_biasadd_readvariableop_resource*
&dense_4_matmul_readvariableop_resource+
'dense_4_biasadd_readvariableop_resource*
&dense_5_matmul_readvariableop_resource+
'dense_5_biasadd_readvariableop_resource*
&dense_6_matmul_readvariableop_resource+
'dense_6_biasadd_readvariableop_resource*
&dense_7_matmul_readvariableop_resource+
'dense_7_biasadd_readvariableop_resource*
&dense_8_matmul_readvariableop_resource+
'dense_8_biasadd_readvariableop_resource*
&dense_9_matmul_readvariableop_resource+
'dense_9_biasadd_readvariableop_resource
identityѕбItext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2ѓ
text_vectorization/StringLowerStringLowerinputs_1*'
_output_shapes
:         2 
text_vectorization/StringLower§
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*'
_output_shapes
:         *6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite 2'
%text_vectorization/StaticRegexReplace┴
text_vectorization/SqueezeSqueeze.text_vectorization/StaticRegexReplace:output:0*
T0*#
_output_shapes
:         *
squeeze_dims

         2
text_vectorization/Squeezeи
9text_vectorization/StringsByteSplit/StringSplit/delimiterConst*
_output_shapes
: *
dtype0*
valueB B 2;
9text_vectorization/StringsByteSplit/StringSplit/delimiterф
/text_vectorization/StringsByteSplit/StringSplitStringSplit#text_vectorization/Squeeze:output:0Btext_vectorization/StringsByteSplit/StringSplit/delimiter:output:0*<
_output_shapes*
(:         :         :*

skip_empty( 21
/text_vectorization/StringsByteSplit/StringSplit├
7text_vectorization/StringsByteSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        29
7text_vectorization/StringsByteSplit/strided_slice/stackК
9text_vectorization/StringsByteSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       2;
9text_vectorization/StringsByteSplit/strided_slice/stack_1К
9text_vectorization/StringsByteSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2;
9text_vectorization/StringsByteSplit/strided_slice/stack_2­
1text_vectorization/StringsByteSplit/strided_sliceStridedSlice9text_vectorization/StringsByteSplit/StringSplit:indices:0@text_vectorization/StringsByteSplit/strided_slice/stack:output:0Btext_vectorization/StringsByteSplit/strided_slice/stack_1:output:0Btext_vectorization/StringsByteSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:         *

begin_mask*
end_mask*
shrink_axis_mask23
1text_vectorization/StringsByteSplit/strided_slice└
9text_vectorization/StringsByteSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2;
9text_vectorization/StringsByteSplit/strided_slice_1/stack─
;text_vectorization/StringsByteSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2=
;text_vectorization/StringsByteSplit/strided_slice_1/stack_1─
;text_vectorization/StringsByteSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2=
;text_vectorization/StringsByteSplit/strided_slice_1/stack_2╔
3text_vectorization/StringsByteSplit/strided_slice_1StridedSlice7text_vectorization/StringsByteSplit/StringSplit:shape:0Btext_vectorization/StringsByteSplit/strided_slice_1/stack:output:0Dtext_vectorization/StringsByteSplit/strided_slice_1/stack_1:output:0Dtext_vectorization/StringsByteSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask25
3text_vectorization/StringsByteSplit/strided_slice_1╣
Ztext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast:text_vectorization/StringsByteSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:         2\
Ztext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast▓
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast<text_vectorization/StringsByteSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: 2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1┌
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShape^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shapeќ
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstЛ
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdmtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0mtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: 2e
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prodќ
htext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : 2j
htext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yП
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreaterltext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0qtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterЬ
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastjtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: 2e
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Castџ
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1┴
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMax^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0otext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maxј
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y╬
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ktext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0mtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add┴
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulgtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulк
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum`text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum╩
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum`text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumЊ
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2╔
gtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincount^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0otext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:         2i
gtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincountѕ
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : 2c
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisл
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumntext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:         2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsumў
etext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R 2g
etext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0ѕ
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2c
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis┐
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2ntext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:         2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatђ
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Vtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handle8text_vectorization/StringsByteSplit/StringSplit:values:0Wtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*#
_output_shapes
:         2K
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2є
2text_vectorization/string_lookup/assert_equal/NoOpNoOp*
_output_shapes
 24
2text_vectorization/string_lookup/assert_equal/NoOpС
)text_vectorization/string_lookup/IdentityIdentityRtext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:         2+
)text_vectorization/string_lookup/Identityч
+text_vectorization/string_lookup/Identity_1Identityetext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat:output:0*
T0	*#
_output_shapes
:         2-
+text_vectorization/string_lookup/Identity_1ц
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R 21
/text_vectorization/RaggedToTensor/default_valueЮ
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
: *
dtype0	*
valueB	 R
         2)
'text_vectorization/RaggedToTensor/Const§
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:02text_vectorization/string_lookup/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:04text_vectorization/string_lookup/Identity_1:output:0*
T0	*
Tindex0	*
Tshape0	*0
_output_shapes
:                  *
num_row_partition_tensors*%
row_partition_types

ROW_SPLITS28
6text_vectorization/RaggedToTensor/RaggedTensorToTensorБ
text_vectorization/ShapeShape?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
T0	*
_output_shapes
:2
text_vectorization/Shapeџ
&text_vectorization/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&text_vectorization/strided_slice/stackъ
(text_vectorization/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(text_vectorization/strided_slice/stack_1ъ
(text_vectorization/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(text_vectorization/strided_slice/stack_2н
 text_vectorization/strided_sliceStridedSlice!text_vectorization/Shape:output:0/text_vectorization/strided_slice/stack:output:01text_vectorization/strided_slice/stack_1:output:01text_vectorization/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 text_vectorization/strided_slicew
text_vectorization/sub/xConst*
_output_shapes
: *
dtype0*
value
B :▄2
text_vectorization/sub/xд
text_vectorization/subSub!text_vectorization/sub/x:output:0)text_vectorization/strided_slice:output:0*
T0*
_output_shapes
: 2
text_vectorization/suby
text_vectorization/Less/yConst*
_output_shapes
: *
dtype0*
value
B :▄2
text_vectorization/Less/yф
text_vectorization/LessLess)text_vectorization/strided_slice:output:0"text_vectorization/Less/y:output:0*
T0*
_output_shapes
: 2
text_vectorization/Lessщ
text_vectorization/condStatelessIftext_vectorization/Less:z:0text_vectorization/sub:z:0?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
Tcond0
*
Tin
2	*
Tout
2	*
_lower_using_switch_merge(*0
_output_shapes
:                  * 
_read_only_resource_inputs
 *5
else_branch&R$
"text_vectorization_cond_false_7415*/
output_shapes
:                  *4
then_branch%R#
!text_vectorization_cond_true_74142
text_vectorization/condЦ
 text_vectorization/cond/IdentityIdentity text_vectorization/cond:output:0*
T0	*(
_output_shapes
:         ▄2"
 text_vectorization/cond/IdentityЇ

dense/CastCast)text_vectorization/cond/Identity:output:0*

DstT0*

SrcT0	*(
_output_shapes
:         ▄2

dense/CastА
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource* 
_output_shapes
:
▄ђ*
dtype02
dense/MatMul/ReadVariableOpј
dense/MatMulMatMuldense/Cast:y:0#dense/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2
dense/MatMulЪ
dense/BiasAdd/ReadVariableOpReadVariableOp%dense_biasadd_readvariableop_resource*
_output_shapes	
:ђ*
dtype02
dense/BiasAdd/ReadVariableOpџ
dense/BiasAddBiasAdddense/MatMul:product:0$dense/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2
dense/BiasAddk

dense/ReluReludense/BiasAdd:output:0*
T0*(
_output_shapes
:         ђ2

dense/ReluД
dense_1/MatMul/ReadVariableOpReadVariableOp&dense_1_matmul_readvariableop_resource* 
_output_shapes
:
ђђ*
dtype02
dense_1/MatMul/ReadVariableOpъ
dense_1/MatMulMatMuldense/Relu:activations:0%dense_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2
dense_1/MatMulЦ
dense_1/BiasAdd/ReadVariableOpReadVariableOp'dense_1_biasadd_readvariableop_resource*
_output_shapes	
:ђ*
dtype02 
dense_1/BiasAdd/ReadVariableOpб
dense_1/BiasAddBiasAdddense_1/MatMul:product:0&dense_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2
dense_1/BiasAddq
dense_1/ReluReludense_1/BiasAdd:output:0*
T0*(
_output_shapes
:         ђ2
dense_1/Reluд
dense_2/MatMul/ReadVariableOpReadVariableOp&dense_2_matmul_readvariableop_resource*
_output_shapes
:	ђ@*
dtype02
dense_2/MatMul/ReadVariableOpЪ
dense_2/MatMulMatMuldense_1/Relu:activations:0%dense_2/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_2/MatMulц
dense_2/BiasAdd/ReadVariableOpReadVariableOp'dense_2_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_2/BiasAdd/ReadVariableOpА
dense_2/BiasAddBiasAdddense_2/MatMul:product:0&dense_2/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_2/BiasAddp
dense_2/ReluReludense_2/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_2/ReluХ
$normalization/Reshape/ReadVariableOpReadVariableOp-normalization_reshape_readvariableop_resource*
_output_shapes
:*
dtype02&
$normalization/Reshape/ReadVariableOpІ
normalization/Reshape/shapeConst*
_output_shapes
:*
dtype0*
valueB"      2
normalization/Reshape/shapeХ
normalization/ReshapeReshape,normalization/Reshape/ReadVariableOp:value:0$normalization/Reshape/shape:output:0*
T0*
_output_shapes

:2
normalization/Reshape╝
&normalization/Reshape_1/ReadVariableOpReadVariableOp/normalization_reshape_1_readvariableop_resource*
_output_shapes
:*
dtype02(
&normalization/Reshape_1/ReadVariableOpЈ
normalization/Reshape_1/shapeConst*
_output_shapes
:*
dtype0*
valueB"      2
normalization/Reshape_1/shapeЙ
normalization/Reshape_1Reshape.normalization/Reshape_1/ReadVariableOp:value:0&normalization/Reshape_1/shape:output:0*
T0*
_output_shapes

:2
normalization/Reshape_1Ѕ
normalization/subSubinputs_0normalization/Reshape:output:0*
T0*'
_output_shapes
:         2
normalization/sub{
normalization/SqrtSqrt normalization/Reshape_1:output:0*
T0*
_output_shapes

:2
normalization/Sqrtџ
normalization/truedivRealDivnormalization/sub:z:0normalization/Sqrt:y:0*
T0*'
_output_shapes
:         2
normalization/truedivЦ
dense_3/MatMul/ReadVariableOpReadVariableOp&dense_3_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
dense_3/MatMul/ReadVariableOpЪ
dense_3/MatMulMatMuldense_2/Relu:activations:0%dense_3/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_3/MatMulц
dense_3/BiasAdd/ReadVariableOpReadVariableOp'dense_3_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_3/BiasAdd/ReadVariableOpА
dense_3/BiasAddBiasAdddense_3/MatMul:product:0&dense_3/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_3/BiasAddp
dense_3/ReluReludense_3/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_3/Relut
concatenate/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concatenate/concat/axis╚
concatenate/concatConcatV2normalization/truediv:z:0dense_3/Relu:activations:0 concatenate/concat/axis:output:0*
N*
T0*'
_output_shapes
:         E2
concatenate/concatЦ
dense_4/MatMul/ReadVariableOpReadVariableOp&dense_4_matmul_readvariableop_resource*
_output_shapes

:E@*
dtype02
dense_4/MatMul/ReadVariableOpа
dense_4/MatMulMatMulconcatenate/concat:output:0%dense_4/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_4/MatMulц
dense_4/BiasAdd/ReadVariableOpReadVariableOp'dense_4_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_4/BiasAdd/ReadVariableOpА
dense_4/BiasAddBiasAdddense_4/MatMul:product:0&dense_4/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_4/BiasAddp
dense_4/ReluReludense_4/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_4/ReluЦ
dense_5/MatMul/ReadVariableOpReadVariableOp&dense_5_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
dense_5/MatMul/ReadVariableOpЪ
dense_5/MatMulMatMuldense_4/Relu:activations:0%dense_5/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_5/MatMulц
dense_5/BiasAdd/ReadVariableOpReadVariableOp'dense_5_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_5/BiasAdd/ReadVariableOpА
dense_5/BiasAddBiasAdddense_5/MatMul:product:0&dense_5/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_5/BiasAddp
dense_5/ReluReludense_5/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_5/ReluЦ
dense_6/MatMul/ReadVariableOpReadVariableOp&dense_6_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
dense_6/MatMul/ReadVariableOpЪ
dense_6/MatMulMatMuldense_5/Relu:activations:0%dense_6/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_6/MatMulц
dense_6/BiasAdd/ReadVariableOpReadVariableOp'dense_6_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_6/BiasAdd/ReadVariableOpА
dense_6/BiasAddBiasAdddense_6/MatMul:product:0&dense_6/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_6/BiasAddp
dense_6/ReluReludense_6/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_6/ReluЦ
dense_7/MatMul/ReadVariableOpReadVariableOp&dense_7_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
dense_7/MatMul/ReadVariableOpЪ
dense_7/MatMulMatMuldense_6/Relu:activations:0%dense_7/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_7/MatMulц
dense_7/BiasAdd/ReadVariableOpReadVariableOp'dense_7_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_7/BiasAdd/ReadVariableOpА
dense_7/BiasAddBiasAdddense_7/MatMul:product:0&dense_7/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_7/BiasAddp
dense_7/ReluReludense_7/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_7/ReluЦ
dense_8/MatMul/ReadVariableOpReadVariableOp&dense_8_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
dense_8/MatMul/ReadVariableOpЪ
dense_8/MatMulMatMuldense_7/Relu:activations:0%dense_8/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_8/MatMulц
dense_8/BiasAdd/ReadVariableOpReadVariableOp'dense_8_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_8/BiasAdd/ReadVariableOpА
dense_8/BiasAddBiasAdddense_8/MatMul:product:0&dense_8/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
dense_8/BiasAddp
dense_8/ReluReludense_8/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
dense_8/ReluЦ
dense_9/MatMul/ReadVariableOpReadVariableOp&dense_9_matmul_readvariableop_resource*
_output_shapes

:@*
dtype02
dense_9/MatMul/ReadVariableOpЪ
dense_9/MatMulMatMuldense_8/Relu:activations:0%dense_9/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2
dense_9/MatMulц
dense_9/BiasAdd/ReadVariableOpReadVariableOp'dense_9_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02 
dense_9/BiasAdd/ReadVariableOpА
dense_9/BiasAddBiasAdddense_9/MatMul:product:0&dense_9/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2
dense_9/BiasAddИ
IdentityIdentitydense_9/BiasAdd:output:0J^text_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*Ў
_input_shapesЄ
ё:         :         :: ::::::::::::::::::::::2ќ
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2:Q M
'
_output_shapes
:         
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:         
"
_user_specified_name
inputs/1
о
{
&__inference_dense_6_layer_call_fn_7781

inputs
unknown
	unknown_0
identityѕбStatefulPartitionedCallы
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_6_layer_call_and_return_conditional_losses_64702
StatefulPartitionedCallј
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
ъ
С
!text_vectorization_cond_true_6771A
=text_vectorization_cond_pad_paddings_1_text_vectorization_subV
Rtext_vectorization_cond_pad_text_vectorization_raggedtotensor_raggedtensortotensor	$
 text_vectorization_cond_identity	ќ
(text_vectorization/cond/Pad/paddings/1/0Const*
_output_shapes
: *
dtype0*
value	B : 2*
(text_vectorization/cond/Pad/paddings/1/0Э
&text_vectorization/cond/Pad/paddings/1Pack1text_vectorization/cond/Pad/paddings/1/0:output:0=text_vectorization_cond_pad_paddings_1_text_vectorization_sub*
N*
T0*
_output_shapes
:2(
&text_vectorization/cond/Pad/paddings/1Ц
(text_vectorization/cond/Pad/paddings/0_1Const*
_output_shapes
:*
dtype0*
valueB"        2*
(text_vectorization/cond/Pad/paddings/0_1Ж
$text_vectorization/cond/Pad/paddingsPack1text_vectorization/cond/Pad/paddings/0_1:output:0/text_vectorization/cond/Pad/paddings/1:output:0*
N*
T0*
_output_shapes

:2&
$text_vectorization/cond/Pad/paddings 
text_vectorization/cond/PadPadRtext_vectorization_cond_pad_text_vectorization_raggedtotensor_raggedtensortotensor-text_vectorization/cond/Pad/paddings:output:0*
T0	*0
_output_shapes
:                  2
text_vectorization/cond/Pad▒
 text_vectorization/cond/IdentityIdentity$text_vectorization/cond/Pad:output:0*
T0	*0
_output_shapes
:                  2"
 text_vectorization/cond/Identity"M
 text_vectorization_cond_identity)text_vectorization/cond/Identity:output:0*1
_input_shapes 
: :                  : 

_output_shapes
: :62
0
_output_shapes
:                  
»
Е
A__inference_dense_1_layer_call_and_return_conditional_losses_6308

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityѕЈ
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
ђђ*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2
MatMulЇ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:ђ*
dtype02
BiasAdd/ReadVariableOpѓ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:         ђ2
Relug
IdentityIdentityRelu:activations:0*
T0*(
_output_shapes
:         ђ2

Identity"
identityIdentity:output:0*/
_input_shapes
:         ђ:::P L
(
_output_shapes
:         ђ
 
_user_specified_nameinputs
ж┬
ќ	
F__inference_functional_1_layer_call_and_return_conditional_losses_6567
input_2
input_1Z
Vtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handle[
Wtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value	

dense_6292

dense_6294
dense_1_6319
dense_1_6321
dense_2_6346
dense_2_63481
-normalization_reshape_readvariableop_resource3
/normalization_reshape_1_readvariableop_resource
dense_3_6384
dense_3_6386
dense_4_6427
dense_4_6429
dense_5_6454
dense_5_6456
dense_6_6481
dense_6_6483
dense_7_6508
dense_7_6510
dense_8_6535
dense_8_6537
dense_9_6561
dense_9_6563
identityѕбdense/StatefulPartitionedCallбdense_1/StatefulPartitionedCallбdense_2/StatefulPartitionedCallбdense_3/StatefulPartitionedCallбdense_4/StatefulPartitionedCallбdense_5/StatefulPartitionedCallбdense_6/StatefulPartitionedCallбdense_7/StatefulPartitionedCallбdense_8/StatefulPartitionedCallбdense_9/StatefulPartitionedCallбItext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2Ђ
text_vectorization/StringLowerStringLowerinput_1*'
_output_shapes
:         2 
text_vectorization/StringLower§
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*'
_output_shapes
:         *6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite 2'
%text_vectorization/StaticRegexReplace┴
text_vectorization/SqueezeSqueeze.text_vectorization/StaticRegexReplace:output:0*
T0*#
_output_shapes
:         *
squeeze_dims

         2
text_vectorization/Squeezeи
9text_vectorization/StringsByteSplit/StringSplit/delimiterConst*
_output_shapes
: *
dtype0*
valueB B 2;
9text_vectorization/StringsByteSplit/StringSplit/delimiterф
/text_vectorization/StringsByteSplit/StringSplitStringSplit#text_vectorization/Squeeze:output:0Btext_vectorization/StringsByteSplit/StringSplit/delimiter:output:0*<
_output_shapes*
(:         :         :*

skip_empty( 21
/text_vectorization/StringsByteSplit/StringSplit├
7text_vectorization/StringsByteSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        29
7text_vectorization/StringsByteSplit/strided_slice/stackК
9text_vectorization/StringsByteSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       2;
9text_vectorization/StringsByteSplit/strided_slice/stack_1К
9text_vectorization/StringsByteSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2;
9text_vectorization/StringsByteSplit/strided_slice/stack_2­
1text_vectorization/StringsByteSplit/strided_sliceStridedSlice9text_vectorization/StringsByteSplit/StringSplit:indices:0@text_vectorization/StringsByteSplit/strided_slice/stack:output:0Btext_vectorization/StringsByteSplit/strided_slice/stack_1:output:0Btext_vectorization/StringsByteSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:         *

begin_mask*
end_mask*
shrink_axis_mask23
1text_vectorization/StringsByteSplit/strided_slice└
9text_vectorization/StringsByteSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2;
9text_vectorization/StringsByteSplit/strided_slice_1/stack─
;text_vectorization/StringsByteSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2=
;text_vectorization/StringsByteSplit/strided_slice_1/stack_1─
;text_vectorization/StringsByteSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2=
;text_vectorization/StringsByteSplit/strided_slice_1/stack_2╔
3text_vectorization/StringsByteSplit/strided_slice_1StridedSlice7text_vectorization/StringsByteSplit/StringSplit:shape:0Btext_vectorization/StringsByteSplit/strided_slice_1/stack:output:0Dtext_vectorization/StringsByteSplit/strided_slice_1/stack_1:output:0Dtext_vectorization/StringsByteSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask25
3text_vectorization/StringsByteSplit/strided_slice_1╣
Ztext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast:text_vectorization/StringsByteSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:         2\
Ztext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast▓
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast<text_vectorization/StringsByteSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: 2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1┌
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShape^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shapeќ
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstЛ
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdmtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0mtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: 2e
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prodќ
htext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : 2j
htext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yП
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreaterltext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0qtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterЬ
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastjtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: 2e
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Castџ
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1┴
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMax^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0otext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maxј
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y╬
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ktext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0mtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add┴
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulgtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulк
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum`text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum╩
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum`text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumЊ
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2╔
gtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincount^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0otext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:         2i
gtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincountѕ
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : 2c
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisл
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumntext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:         2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsumў
etext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R 2g
etext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0ѕ
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2c
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis┐
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2ntext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:         2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatђ
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Vtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handle8text_vectorization/StringsByteSplit/StringSplit:values:0Wtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*#
_output_shapes
:         2K
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2є
2text_vectorization/string_lookup/assert_equal/NoOpNoOp*
_output_shapes
 24
2text_vectorization/string_lookup/assert_equal/NoOpС
)text_vectorization/string_lookup/IdentityIdentityRtext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:         2+
)text_vectorization/string_lookup/Identityч
+text_vectorization/string_lookup/Identity_1Identityetext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat:output:0*
T0	*#
_output_shapes
:         2-
+text_vectorization/string_lookup/Identity_1ц
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R 21
/text_vectorization/RaggedToTensor/default_valueЮ
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
: *
dtype0	*
valueB	 R
         2)
'text_vectorization/RaggedToTensor/Const§
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:02text_vectorization/string_lookup/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:04text_vectorization/string_lookup/Identity_1:output:0*
T0	*
Tindex0	*
Tshape0	*0
_output_shapes
:                  *
num_row_partition_tensors*%
row_partition_types

ROW_SPLITS28
6text_vectorization/RaggedToTensor/RaggedTensorToTensorБ
text_vectorization/ShapeShape?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
T0	*
_output_shapes
:2
text_vectorization/Shapeџ
&text_vectorization/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&text_vectorization/strided_slice/stackъ
(text_vectorization/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(text_vectorization/strided_slice/stack_1ъ
(text_vectorization/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(text_vectorization/strided_slice/stack_2н
 text_vectorization/strided_sliceStridedSlice!text_vectorization/Shape:output:0/text_vectorization/strided_slice/stack:output:01text_vectorization/strided_slice/stack_1:output:01text_vectorization/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 text_vectorization/strided_slicew
text_vectorization/sub/xConst*
_output_shapes
: *
dtype0*
value
B :▄2
text_vectorization/sub/xд
text_vectorization/subSub!text_vectorization/sub/x:output:0)text_vectorization/strided_slice:output:0*
T0*
_output_shapes
: 2
text_vectorization/suby
text_vectorization/Less/yConst*
_output_shapes
: *
dtype0*
value
B :▄2
text_vectorization/Less/yф
text_vectorization/LessLess)text_vectorization/strided_slice:output:0"text_vectorization/Less/y:output:0*
T0*
_output_shapes
: 2
text_vectorization/Lessщ
text_vectorization/condStatelessIftext_vectorization/Less:z:0text_vectorization/sub:z:0?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
Tcond0
*
Tin
2	*
Tout
2	*
_lower_using_switch_merge(*0
_output_shapes
:                  * 
_read_only_resource_inputs
 *5
else_branch&R$
"text_vectorization_cond_false_6249*/
output_shapes
:                  *4
then_branch%R#
!text_vectorization_cond_true_62482
text_vectorization/condЦ
 text_vectorization/cond/IdentityIdentity text_vectorization/cond:output:0*
T0	*(
_output_shapes
:         ▄2"
 text_vectorization/cond/IdentityБ
dense/StatefulPartitionedCallStatefulPartitionedCall)text_vectorization/cond/Identity:output:0
dense_6292
dense_6294*
Tin
2	*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:         ђ*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *H
fCRA
?__inference_dense_layer_call_and_return_conditional_losses_62812
dense/StatefulPartitionedCallф
dense_1/StatefulPartitionedCallStatefulPartitionedCall&dense/StatefulPartitionedCall:output:0dense_1_6319dense_1_6321*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:         ђ*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_1_layer_call_and_return_conditional_losses_63082!
dense_1/StatefulPartitionedCallФ
dense_2/StatefulPartitionedCallStatefulPartitionedCall(dense_1/StatefulPartitionedCall:output:0dense_2_6346dense_2_6348*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_2_layer_call_and_return_conditional_losses_63352!
dense_2/StatefulPartitionedCallХ
$normalization/Reshape/ReadVariableOpReadVariableOp-normalization_reshape_readvariableop_resource*
_output_shapes
:*
dtype02&
$normalization/Reshape/ReadVariableOpІ
normalization/Reshape/shapeConst*
_output_shapes
:*
dtype0*
valueB"      2
normalization/Reshape/shapeХ
normalization/ReshapeReshape,normalization/Reshape/ReadVariableOp:value:0$normalization/Reshape/shape:output:0*
T0*
_output_shapes

:2
normalization/Reshape╝
&normalization/Reshape_1/ReadVariableOpReadVariableOp/normalization_reshape_1_readvariableop_resource*
_output_shapes
:*
dtype02(
&normalization/Reshape_1/ReadVariableOpЈ
normalization/Reshape_1/shapeConst*
_output_shapes
:*
dtype0*
valueB"      2
normalization/Reshape_1/shapeЙ
normalization/Reshape_1Reshape.normalization/Reshape_1/ReadVariableOp:value:0&normalization/Reshape_1/shape:output:0*
T0*
_output_shapes

:2
normalization/Reshape_1ѕ
normalization/subSubinput_2normalization/Reshape:output:0*
T0*'
_output_shapes
:         2
normalization/sub{
normalization/SqrtSqrt normalization/Reshape_1:output:0*
T0*
_output_shapes

:2
normalization/Sqrtџ
normalization/truedivRealDivnormalization/sub:z:0normalization/Sqrt:y:0*
T0*'
_output_shapes
:         2
normalization/truedivФ
dense_3/StatefulPartitionedCallStatefulPartitionedCall(dense_2/StatefulPartitionedCall:output:0dense_3_6384dense_3_6386*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_3_layer_call_and_return_conditional_losses_63732!
dense_3/StatefulPartitionedCallЎ
concatenate/PartitionedCallPartitionedCallnormalization/truediv:z:0(dense_3/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         E* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8ѓ *N
fIRG
E__inference_concatenate_layer_call_and_return_conditional_losses_63962
concatenate/PartitionedCallД
dense_4/StatefulPartitionedCallStatefulPartitionedCall$concatenate/PartitionedCall:output:0dense_4_6427dense_4_6429*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_4_layer_call_and_return_conditional_losses_64162!
dense_4/StatefulPartitionedCallФ
dense_5/StatefulPartitionedCallStatefulPartitionedCall(dense_4/StatefulPartitionedCall:output:0dense_5_6454dense_5_6456*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_5_layer_call_and_return_conditional_losses_64432!
dense_5/StatefulPartitionedCallФ
dense_6/StatefulPartitionedCallStatefulPartitionedCall(dense_5/StatefulPartitionedCall:output:0dense_6_6481dense_6_6483*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_6_layer_call_and_return_conditional_losses_64702!
dense_6/StatefulPartitionedCallФ
dense_7/StatefulPartitionedCallStatefulPartitionedCall(dense_6/StatefulPartitionedCall:output:0dense_7_6508dense_7_6510*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_7_layer_call_and_return_conditional_losses_64972!
dense_7/StatefulPartitionedCallФ
dense_8/StatefulPartitionedCallStatefulPartitionedCall(dense_7/StatefulPartitionedCall:output:0dense_8_6535dense_8_6537*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_8_layer_call_and_return_conditional_losses_65242!
dense_8/StatefulPartitionedCallФ
dense_9/StatefulPartitionedCallStatefulPartitionedCall(dense_8/StatefulPartitionedCall:output:0dense_9_6561dense_9_6563*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_9_layer_call_and_return_conditional_losses_65502!
dense_9/StatefulPartitionedCallџ
IdentityIdentity(dense_9/StatefulPartitionedCall:output:0^dense/StatefulPartitionedCall ^dense_1/StatefulPartitionedCall ^dense_2/StatefulPartitionedCall ^dense_3/StatefulPartitionedCall ^dense_4/StatefulPartitionedCall ^dense_5/StatefulPartitionedCall ^dense_6/StatefulPartitionedCall ^dense_7/StatefulPartitionedCall ^dense_8/StatefulPartitionedCall ^dense_9/StatefulPartitionedCallJ^text_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*Ў
_input_shapesЄ
ё:         :         :: ::::::::::::::::::::::2>
dense/StatefulPartitionedCalldense/StatefulPartitionedCall2B
dense_1/StatefulPartitionedCalldense_1/StatefulPartitionedCall2B
dense_2/StatefulPartitionedCalldense_2/StatefulPartitionedCall2B
dense_3/StatefulPartitionedCalldense_3/StatefulPartitionedCall2B
dense_4/StatefulPartitionedCalldense_4/StatefulPartitionedCall2B
dense_5/StatefulPartitionedCalldense_5/StatefulPartitionedCall2B
dense_6/StatefulPartitionedCalldense_6/StatefulPartitionedCall2B
dense_7/StatefulPartitionedCalldense_7/StatefulPartitionedCall2B
dense_8/StatefulPartitionedCalldense_8/StatefulPartitionedCall2B
dense_9/StatefulPartitionedCalldense_9/StatefulPartitionedCall2ќ
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2:P L
'
_output_shapes
:         
!
_user_specified_name	input_2:PL
'
_output_shapes
:         
!
_user_specified_name	input_1
д
Е
A__inference_dense_4_layer_call_and_return_conditional_losses_6416

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityѕЇ
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:E@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulї
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpЂ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         E:::O K
'
_output_shapes
:         E
 
_user_specified_nameinputs
▒
o
E__inference_concatenate_layer_call_and_return_conditional_losses_6396

inputs
inputs_1
identity\
concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concat/axis
concatConcatV2inputsinputs_1concat/axis:output:0*
N*
T0*'
_output_shapes
:         E2
concatc
IdentityIdentityconcat:output:0*
T0*'
_output_shapes
:         E2

Identity"
identityIdentity:output:0*9
_input_shapes(
&:         :         @:O K
'
_output_shapes
:         
 
_user_specified_nameinputs:OK
'
_output_shapes
:         @
 
_user_specified_nameinputs
о
{
&__inference_dense_8_layer_call_fn_7821

inputs
unknown
	unknown_0
identityѕбStatefulPartitionedCallы
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_8_layer_call_and_return_conditional_losses_65242
StatefulPartitionedCallј
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
ё
+
__inference__destroyer_7855
identityP
ConstConst*
_output_shapes
: *
dtype0*
value	B :2
ConstQ
IdentityIdentityConst:output:0*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*
_input_shapes 
¤
Н
"text_vectorization_cond_false_6626'
#text_vectorization_cond_placeholder`
\text_vectorization_cond_strided_slice_text_vectorization_raggedtotensor_raggedtensortotensor	$
 text_vectorization_cond_identity	Ф
+text_vectorization/cond/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2-
+text_vectorization/cond/strided_slice/stack»
-text_vectorization/cond/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ▄  2/
-text_vectorization/cond/strided_slice/stack_1»
-text_vectorization/cond/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2/
-text_vectorization/cond/strided_slice/stack_2╠
%text_vectorization/cond/strided_sliceStridedSlice\text_vectorization_cond_strided_slice_text_vectorization_raggedtotensor_raggedtensortotensor4text_vectorization/cond/strided_slice/stack:output:06text_vectorization/cond/strided_slice/stack_1:output:06text_vectorization/cond/strided_slice/stack_2:output:0*
Index0*
T0	*0
_output_shapes
:                  *

begin_mask*
end_mask2'
%text_vectorization/cond/strided_slice╗
 text_vectorization/cond/IdentityIdentity.text_vectorization/cond/strided_slice:output:0*
T0	*0
_output_shapes
:                  2"
 text_vectorization/cond/Identity"M
 text_vectorization_cond_identity)text_vectorization/cond/Identity:output:0*1
_input_shapes 
: :                  : 

_output_shapes
: :62
0
_output_shapes
:                  
ъ
С
!text_vectorization_cond_true_7251A
=text_vectorization_cond_pad_paddings_1_text_vectorization_subV
Rtext_vectorization_cond_pad_text_vectorization_raggedtotensor_raggedtensortotensor	$
 text_vectorization_cond_identity	ќ
(text_vectorization/cond/Pad/paddings/1/0Const*
_output_shapes
: *
dtype0*
value	B : 2*
(text_vectorization/cond/Pad/paddings/1/0Э
&text_vectorization/cond/Pad/paddings/1Pack1text_vectorization/cond/Pad/paddings/1/0:output:0=text_vectorization_cond_pad_paddings_1_text_vectorization_sub*
N*
T0*
_output_shapes
:2(
&text_vectorization/cond/Pad/paddings/1Ц
(text_vectorization/cond/Pad/paddings/0_1Const*
_output_shapes
:*
dtype0*
valueB"        2*
(text_vectorization/cond/Pad/paddings/0_1Ж
$text_vectorization/cond/Pad/paddingsPack1text_vectorization/cond/Pad/paddings/0_1:output:0/text_vectorization/cond/Pad/paddings/1:output:0*
N*
T0*
_output_shapes

:2&
$text_vectorization/cond/Pad/paddings 
text_vectorization/cond/PadPadRtext_vectorization_cond_pad_text_vectorization_raggedtotensor_raggedtensortotensor-text_vectorization/cond/Pad/paddings:output:0*
T0	*0
_output_shapes
:                  2
text_vectorization/cond/Pad▒
 text_vectorization/cond/IdentityIdentity$text_vectorization/cond/Pad:output:0*
T0	*0
_output_shapes
:                  2"
 text_vectorization/cond/Identity"M
 text_vectorization_cond_identity)text_vectorization/cond/Identity:output:0*1
_input_shapes 
: :                  : 

_output_shapes
: :62
0
_output_shapes
:                  
Ў
V
*__inference_concatenate_layer_call_fn_7721
inputs_0
inputs_1
identityл
PartitionedCallPartitionedCallinputs_0inputs_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         E* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8ѓ *N
fIRG
E__inference_concatenate_layer_call_and_return_conditional_losses_63962
PartitionedCalll
IdentityIdentityPartitionedCall:output:0*
T0*'
_output_shapes
:         E2

Identity"
identityIdentity:output:0*9
_input_shapes(
&:         :         @:Q M
'
_output_shapes
:         
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:         @
"
_user_specified_name
inputs/1
¤
Н
"text_vectorization_cond_false_6249'
#text_vectorization_cond_placeholder`
\text_vectorization_cond_strided_slice_text_vectorization_raggedtotensor_raggedtensortotensor	$
 text_vectorization_cond_identity	Ф
+text_vectorization/cond/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2-
+text_vectorization/cond/strided_slice/stack»
-text_vectorization/cond/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ▄  2/
-text_vectorization/cond/strided_slice/stack_1»
-text_vectorization/cond/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2/
-text_vectorization/cond/strided_slice/stack_2╠
%text_vectorization/cond/strided_sliceStridedSlice\text_vectorization_cond_strided_slice_text_vectorization_raggedtotensor_raggedtensortotensor4text_vectorization/cond/strided_slice/stack:output:06text_vectorization/cond/strided_slice/stack_1:output:06text_vectorization/cond/strided_slice/stack_2:output:0*
Index0*
T0	*0
_output_shapes
:                  *

begin_mask*
end_mask2'
%text_vectorization/cond/strided_slice╗
 text_vectorization/cond/IdentityIdentity.text_vectorization/cond/strided_slice:output:0*
T0	*0
_output_shapes
:                  2"
 text_vectorization/cond/Identity"M
 text_vectorization_cond_identity)text_vectorization/cond/Identity:output:0*1
_input_shapes 
: :                  : 

_output_shapes
: :62
0
_output_shapes
:                  
д
Е
A__inference_dense_8_layer_call_and_return_conditional_losses_7812

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityѕЇ
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulї
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpЂ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @:::O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
у┬
ќ	
F__inference_functional_1_layer_call_and_return_conditional_losses_6855

inputs
inputs_1Z
Vtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handle[
Wtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value	

dense_6792

dense_6794
dense_1_6797
dense_1_6799
dense_2_6802
dense_2_68041
-normalization_reshape_readvariableop_resource3
/normalization_reshape_1_readvariableop_resource
dense_3_6818
dense_3_6820
dense_4_6824
dense_4_6826
dense_5_6829
dense_5_6831
dense_6_6834
dense_6_6836
dense_7_6839
dense_7_6841
dense_8_6844
dense_8_6846
dense_9_6849
dense_9_6851
identityѕбdense/StatefulPartitionedCallбdense_1/StatefulPartitionedCallбdense_2/StatefulPartitionedCallбdense_3/StatefulPartitionedCallбdense_4/StatefulPartitionedCallбdense_5/StatefulPartitionedCallбdense_6/StatefulPartitionedCallбdense_7/StatefulPartitionedCallбdense_8/StatefulPartitionedCallбdense_9/StatefulPartitionedCallбItext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2ѓ
text_vectorization/StringLowerStringLowerinputs_1*'
_output_shapes
:         2 
text_vectorization/StringLower§
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*'
_output_shapes
:         *6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite 2'
%text_vectorization/StaticRegexReplace┴
text_vectorization/SqueezeSqueeze.text_vectorization/StaticRegexReplace:output:0*
T0*#
_output_shapes
:         *
squeeze_dims

         2
text_vectorization/Squeezeи
9text_vectorization/StringsByteSplit/StringSplit/delimiterConst*
_output_shapes
: *
dtype0*
valueB B 2;
9text_vectorization/StringsByteSplit/StringSplit/delimiterф
/text_vectorization/StringsByteSplit/StringSplitStringSplit#text_vectorization/Squeeze:output:0Btext_vectorization/StringsByteSplit/StringSplit/delimiter:output:0*<
_output_shapes*
(:         :         :*

skip_empty( 21
/text_vectorization/StringsByteSplit/StringSplit├
7text_vectorization/StringsByteSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        29
7text_vectorization/StringsByteSplit/strided_slice/stackК
9text_vectorization/StringsByteSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       2;
9text_vectorization/StringsByteSplit/strided_slice/stack_1К
9text_vectorization/StringsByteSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2;
9text_vectorization/StringsByteSplit/strided_slice/stack_2­
1text_vectorization/StringsByteSplit/strided_sliceStridedSlice9text_vectorization/StringsByteSplit/StringSplit:indices:0@text_vectorization/StringsByteSplit/strided_slice/stack:output:0Btext_vectorization/StringsByteSplit/strided_slice/stack_1:output:0Btext_vectorization/StringsByteSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:         *

begin_mask*
end_mask*
shrink_axis_mask23
1text_vectorization/StringsByteSplit/strided_slice└
9text_vectorization/StringsByteSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2;
9text_vectorization/StringsByteSplit/strided_slice_1/stack─
;text_vectorization/StringsByteSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2=
;text_vectorization/StringsByteSplit/strided_slice_1/stack_1─
;text_vectorization/StringsByteSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2=
;text_vectorization/StringsByteSplit/strided_slice_1/stack_2╔
3text_vectorization/StringsByteSplit/strided_slice_1StridedSlice7text_vectorization/StringsByteSplit/StringSplit:shape:0Btext_vectorization/StringsByteSplit/strided_slice_1/stack:output:0Dtext_vectorization/StringsByteSplit/strided_slice_1/stack_1:output:0Dtext_vectorization/StringsByteSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask25
3text_vectorization/StringsByteSplit/strided_slice_1╣
Ztext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast:text_vectorization/StringsByteSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:         2\
Ztext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast▓
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast<text_vectorization/StringsByteSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: 2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1┌
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShape^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shapeќ
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstЛ
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdmtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0mtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: 2e
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prodќ
htext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : 2j
htext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yП
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreaterltext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0qtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterЬ
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastjtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: 2e
ctext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Castџ
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1┴
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMax^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0otext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maxј
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :2f
dtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y╬
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ktext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0mtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add┴
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulgtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: 2d
btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulк
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum`text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum╩
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum`text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumЊ
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 2h
ftext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2╔
gtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincount^text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0otext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:         2i
gtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincountѕ
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : 2c
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisл
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumntext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:         2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsumў
etext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R 2g
etext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0ѕ
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2c
atext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis┐
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2ntext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0btext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0jtext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:         2^
\text_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatђ
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Vtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handle8text_vectorization/StringsByteSplit/StringSplit:values:0Wtext_vectorization_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*#
_output_shapes
:         2K
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2є
2text_vectorization/string_lookup/assert_equal/NoOpNoOp*
_output_shapes
 24
2text_vectorization/string_lookup/assert_equal/NoOpС
)text_vectorization/string_lookup/IdentityIdentityRtext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:         2+
)text_vectorization/string_lookup/Identityч
+text_vectorization/string_lookup/Identity_1Identityetext_vectorization/StringsByteSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat:output:0*
T0	*#
_output_shapes
:         2-
+text_vectorization/string_lookup/Identity_1ц
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R 21
/text_vectorization/RaggedToTensor/default_valueЮ
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
: *
dtype0	*
valueB	 R
         2)
'text_vectorization/RaggedToTensor/Const§
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:02text_vectorization/string_lookup/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:04text_vectorization/string_lookup/Identity_1:output:0*
T0	*
Tindex0	*
Tshape0	*0
_output_shapes
:                  *
num_row_partition_tensors*%
row_partition_types

ROW_SPLITS28
6text_vectorization/RaggedToTensor/RaggedTensorToTensorБ
text_vectorization/ShapeShape?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
T0	*
_output_shapes
:2
text_vectorization/Shapeџ
&text_vectorization/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&text_vectorization/strided_slice/stackъ
(text_vectorization/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(text_vectorization/strided_slice/stack_1ъ
(text_vectorization/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(text_vectorization/strided_slice/stack_2н
 text_vectorization/strided_sliceStridedSlice!text_vectorization/Shape:output:0/text_vectorization/strided_slice/stack:output:01text_vectorization/strided_slice/stack_1:output:01text_vectorization/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 text_vectorization/strided_slicew
text_vectorization/sub/xConst*
_output_shapes
: *
dtype0*
value
B :▄2
text_vectorization/sub/xд
text_vectorization/subSub!text_vectorization/sub/x:output:0)text_vectorization/strided_slice:output:0*
T0*
_output_shapes
: 2
text_vectorization/suby
text_vectorization/Less/yConst*
_output_shapes
: *
dtype0*
value
B :▄2
text_vectorization/Less/yф
text_vectorization/LessLess)text_vectorization/strided_slice:output:0"text_vectorization/Less/y:output:0*
T0*
_output_shapes
: 2
text_vectorization/Lessщ
text_vectorization/condStatelessIftext_vectorization/Less:z:0text_vectorization/sub:z:0?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*
Tcond0
*
Tin
2	*
Tout
2	*
_lower_using_switch_merge(*0
_output_shapes
:                  * 
_read_only_resource_inputs
 *5
else_branch&R$
"text_vectorization_cond_false_6772*/
output_shapes
:                  *4
then_branch%R#
!text_vectorization_cond_true_67712
text_vectorization/condЦ
 text_vectorization/cond/IdentityIdentity text_vectorization/cond:output:0*
T0	*(
_output_shapes
:         ▄2"
 text_vectorization/cond/IdentityБ
dense/StatefulPartitionedCallStatefulPartitionedCall)text_vectorization/cond/Identity:output:0
dense_6792
dense_6794*
Tin
2	*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:         ђ*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *H
fCRA
?__inference_dense_layer_call_and_return_conditional_losses_62812
dense/StatefulPartitionedCallф
dense_1/StatefulPartitionedCallStatefulPartitionedCall&dense/StatefulPartitionedCall:output:0dense_1_6797dense_1_6799*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:         ђ*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_1_layer_call_and_return_conditional_losses_63082!
dense_1/StatefulPartitionedCallФ
dense_2/StatefulPartitionedCallStatefulPartitionedCall(dense_1/StatefulPartitionedCall:output:0dense_2_6802dense_2_6804*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_2_layer_call_and_return_conditional_losses_63352!
dense_2/StatefulPartitionedCallХ
$normalization/Reshape/ReadVariableOpReadVariableOp-normalization_reshape_readvariableop_resource*
_output_shapes
:*
dtype02&
$normalization/Reshape/ReadVariableOpІ
normalization/Reshape/shapeConst*
_output_shapes
:*
dtype0*
valueB"      2
normalization/Reshape/shapeХ
normalization/ReshapeReshape,normalization/Reshape/ReadVariableOp:value:0$normalization/Reshape/shape:output:0*
T0*
_output_shapes

:2
normalization/Reshape╝
&normalization/Reshape_1/ReadVariableOpReadVariableOp/normalization_reshape_1_readvariableop_resource*
_output_shapes
:*
dtype02(
&normalization/Reshape_1/ReadVariableOpЈ
normalization/Reshape_1/shapeConst*
_output_shapes
:*
dtype0*
valueB"      2
normalization/Reshape_1/shapeЙ
normalization/Reshape_1Reshape.normalization/Reshape_1/ReadVariableOp:value:0&normalization/Reshape_1/shape:output:0*
T0*
_output_shapes

:2
normalization/Reshape_1Є
normalization/subSubinputsnormalization/Reshape:output:0*
T0*'
_output_shapes
:         2
normalization/sub{
normalization/SqrtSqrt normalization/Reshape_1:output:0*
T0*
_output_shapes

:2
normalization/Sqrtџ
normalization/truedivRealDivnormalization/sub:z:0normalization/Sqrt:y:0*
T0*'
_output_shapes
:         2
normalization/truedivФ
dense_3/StatefulPartitionedCallStatefulPartitionedCall(dense_2/StatefulPartitionedCall:output:0dense_3_6818dense_3_6820*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_3_layer_call_and_return_conditional_losses_63732!
dense_3/StatefulPartitionedCallЎ
concatenate/PartitionedCallPartitionedCallnormalization/truediv:z:0(dense_3/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         E* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8ѓ *N
fIRG
E__inference_concatenate_layer_call_and_return_conditional_losses_63962
concatenate/PartitionedCallД
dense_4/StatefulPartitionedCallStatefulPartitionedCall$concatenate/PartitionedCall:output:0dense_4_6824dense_4_6826*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_4_layer_call_and_return_conditional_losses_64162!
dense_4/StatefulPartitionedCallФ
dense_5/StatefulPartitionedCallStatefulPartitionedCall(dense_4/StatefulPartitionedCall:output:0dense_5_6829dense_5_6831*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_5_layer_call_and_return_conditional_losses_64432!
dense_5/StatefulPartitionedCallФ
dense_6/StatefulPartitionedCallStatefulPartitionedCall(dense_5/StatefulPartitionedCall:output:0dense_6_6834dense_6_6836*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_6_layer_call_and_return_conditional_losses_64702!
dense_6/StatefulPartitionedCallФ
dense_7/StatefulPartitionedCallStatefulPartitionedCall(dense_6/StatefulPartitionedCall:output:0dense_7_6839dense_7_6841*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_7_layer_call_and_return_conditional_losses_64972!
dense_7/StatefulPartitionedCallФ
dense_8/StatefulPartitionedCallStatefulPartitionedCall(dense_7/StatefulPartitionedCall:output:0dense_8_6844dense_8_6846*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_8_layer_call_and_return_conditional_losses_65242!
dense_8/StatefulPartitionedCallФ
dense_9/StatefulPartitionedCallStatefulPartitionedCall(dense_8/StatefulPartitionedCall:output:0dense_9_6849dense_9_6851*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_9_layer_call_and_return_conditional_losses_65502!
dense_9/StatefulPartitionedCallџ
IdentityIdentity(dense_9/StatefulPartitionedCall:output:0^dense/StatefulPartitionedCall ^dense_1/StatefulPartitionedCall ^dense_2/StatefulPartitionedCall ^dense_3/StatefulPartitionedCall ^dense_4/StatefulPartitionedCall ^dense_5/StatefulPartitionedCall ^dense_6/StatefulPartitionedCall ^dense_7/StatefulPartitionedCall ^dense_8/StatefulPartitionedCall ^dense_9/StatefulPartitionedCallJ^text_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*Ў
_input_shapesЄ
ё:         :         :: ::::::::::::::::::::::2>
dense/StatefulPartitionedCalldense/StatefulPartitionedCall2B
dense_1/StatefulPartitionedCalldense_1/StatefulPartitionedCall2B
dense_2/StatefulPartitionedCalldense_2/StatefulPartitionedCall2B
dense_3/StatefulPartitionedCalldense_3/StatefulPartitionedCall2B
dense_4/StatefulPartitionedCalldense_4/StatefulPartitionedCall2B
dense_5/StatefulPartitionedCalldense_5/StatefulPartitionedCall2B
dense_6/StatefulPartitionedCalldense_6/StatefulPartitionedCall2B
dense_7/StatefulPartitionedCalldense_7/StatefulPartitionedCall2B
dense_8/StatefulPartitionedCalldense_8/StatefulPartitionedCall2B
dense_9/StatefulPartitionedCalldense_9/StatefulPartitionedCall2ќ
Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2Itext_vectorization/string_lookup/None_lookup_table_find/LookupTableFindV2:O K
'
_output_shapes
:         
 
_user_specified_nameinputs:OK
'
_output_shapes
:         
 
_user_specified_nameinputs
Ё
)
__inference_<lambda>_7887
identityS
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?2
ConstQ
IdentityIdentityConst:output:0*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*
_input_shapes 
╩
Е
A__inference_dense_9_layer_call_and_return_conditional_losses_7831

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityѕЇ
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2
MatMulї
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOpЂ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2	
BiasAddd
IdentityIdentityBiasAdd:output:0*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @:::O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
є
-
__inference__initializer_7850
identityP
ConstConst*
_output_shapes
: *
dtype0*
value	B :2
ConstQ
IdentityIdentityConst:output:0*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*
_input_shapes 
¤
Н
"text_vectorization_cond_false_6772'
#text_vectorization_cond_placeholder`
\text_vectorization_cond_strided_slice_text_vectorization_raggedtotensor_raggedtensortotensor	$
 text_vectorization_cond_identity	Ф
+text_vectorization/cond/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2-
+text_vectorization/cond/strided_slice/stack»
-text_vectorization/cond/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ▄  2/
-text_vectorization/cond/strided_slice/stack_1»
-text_vectorization/cond/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2/
-text_vectorization/cond/strided_slice/stack_2╠
%text_vectorization/cond/strided_sliceStridedSlice\text_vectorization_cond_strided_slice_text_vectorization_raggedtotensor_raggedtensortotensor4text_vectorization/cond/strided_slice/stack:output:06text_vectorization/cond/strided_slice/stack_1:output:06text_vectorization/cond/strided_slice/stack_2:output:0*
Index0*
T0	*0
_output_shapes
:                  *

begin_mask*
end_mask2'
%text_vectorization/cond/strided_slice╗
 text_vectorization/cond/IdentityIdentity.text_vectorization/cond/strided_slice:output:0*
T0	*0
_output_shapes
:                  2"
 text_vectorization/cond/Identity"M
 text_vectorization_cond_identity)text_vectorization/cond/Identity:output:0*1
_input_shapes 
: :                  : 

_output_shapes
: :62
0
_output_shapes
:                  
д
Е
A__inference_dense_7_layer_call_and_return_conditional_losses_7792

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityѕЇ
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulї
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpЂ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @:::O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
о
{
&__inference_dense_7_layer_call_fn_7801

inputs
unknown
	unknown_0
identityѕбStatefulPartitionedCallы
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_7_layer_call_and_return_conditional_losses_64972
StatefulPartitionedCallј
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
д
Е
A__inference_dense_3_layer_call_and_return_conditional_losses_6373

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityѕЇ
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulї
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpЂ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @:::O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
ъ
С
!text_vectorization_cond_true_6248A
=text_vectorization_cond_pad_paddings_1_text_vectorization_subV
Rtext_vectorization_cond_pad_text_vectorization_raggedtotensor_raggedtensortotensor	$
 text_vectorization_cond_identity	ќ
(text_vectorization/cond/Pad/paddings/1/0Const*
_output_shapes
: *
dtype0*
value	B : 2*
(text_vectorization/cond/Pad/paddings/1/0Э
&text_vectorization/cond/Pad/paddings/1Pack1text_vectorization/cond/Pad/paddings/1/0:output:0=text_vectorization_cond_pad_paddings_1_text_vectorization_sub*
N*
T0*
_output_shapes
:2(
&text_vectorization/cond/Pad/paddings/1Ц
(text_vectorization/cond/Pad/paddings/0_1Const*
_output_shapes
:*
dtype0*
valueB"        2*
(text_vectorization/cond/Pad/paddings/0_1Ж
$text_vectorization/cond/Pad/paddingsPack1text_vectorization/cond/Pad/paddings/0_1:output:0/text_vectorization/cond/Pad/paddings/1:output:0*
N*
T0*
_output_shapes

:2&
$text_vectorization/cond/Pad/paddings 
text_vectorization/cond/PadPadRtext_vectorization_cond_pad_text_vectorization_raggedtotensor_raggedtensortotensor-text_vectorization/cond/Pad/paddings:output:0*
T0	*0
_output_shapes
:                  2
text_vectorization/cond/Pad▒
 text_vectorization/cond/IdentityIdentity$text_vectorization/cond/Pad:output:0*
T0	*0
_output_shapes
:                  2"
 text_vectorization/cond/Identity"M
 text_vectorization_cond_identity)text_vectorization/cond/Identity:output:0*1
_input_shapes 
: :                  : 

_output_shapes
: :62
0
_output_shapes
:                  
Щ
ќ
/functional_1_text_vectorization_cond_false_60854
0functional_1_text_vectorization_cond_placeholderz
vfunctional_1_text_vectorization_cond_strided_slice_functional_1_text_vectorization_raggedtotensor_raggedtensortotensor	1
-functional_1_text_vectorization_cond_identity	┼
8functional_1/text_vectorization/cond/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2:
8functional_1/text_vectorization/cond/strided_slice/stack╔
:functional_1/text_vectorization/cond/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ▄  2<
:functional_1/text_vectorization/cond/strided_slice/stack_1╔
:functional_1/text_vectorization/cond/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2<
:functional_1/text_vectorization/cond/strided_slice/stack_2Д
2functional_1/text_vectorization/cond/strided_sliceStridedSlicevfunctional_1_text_vectorization_cond_strided_slice_functional_1_text_vectorization_raggedtotensor_raggedtensortotensorAfunctional_1/text_vectorization/cond/strided_slice/stack:output:0Cfunctional_1/text_vectorization/cond/strided_slice/stack_1:output:0Cfunctional_1/text_vectorization/cond/strided_slice/stack_2:output:0*
Index0*
T0	*0
_output_shapes
:                  *

begin_mask*
end_mask24
2functional_1/text_vectorization/cond/strided_sliceР
-functional_1/text_vectorization/cond/IdentityIdentity;functional_1/text_vectorization/cond/strided_slice:output:0*
T0	*0
_output_shapes
:                  2/
-functional_1/text_vectorization/cond/Identity"g
-functional_1_text_vectorization_cond_identity6functional_1/text_vectorization/cond/Identity:output:0*1
_input_shapes 
: :                  : 

_output_shapes
: :62
0
_output_shapes
:                  
д
Е
A__inference_dense_3_layer_call_and_return_conditional_losses_7699

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityѕЇ
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulї
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpЂ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @:::O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
ЂФ
п$
 __inference__traced_restore_8351
file_prefix!
assignvariableop_dense_kernel!
assignvariableop_1_dense_bias%
!assignvariableop_2_dense_1_kernel#
assignvariableop_3_dense_1_bias%
!assignvariableop_4_dense_2_kernel#
assignvariableop_5_dense_2_bias
assignvariableop_6_mean
assignvariableop_7_variance
assignvariableop_8_count%
!assignvariableop_9_dense_3_kernel$
 assignvariableop_10_dense_3_bias&
"assignvariableop_11_dense_4_kernel$
 assignvariableop_12_dense_4_bias&
"assignvariableop_13_dense_5_kernel$
 assignvariableop_14_dense_5_bias&
"assignvariableop_15_dense_6_kernel$
 assignvariableop_16_dense_6_bias&
"assignvariableop_17_dense_7_kernel$
 assignvariableop_18_dense_7_bias&
"assignvariableop_19_dense_8_kernel$
 assignvariableop_20_dense_8_bias&
"assignvariableop_21_dense_9_kernel$
 assignvariableop_22_dense_9_bias!
assignvariableop_23_adam_iter#
assignvariableop_24_adam_beta_1#
assignvariableop_25_adam_beta_2"
assignvariableop_26_adam_decay*
&assignvariableop_27_adam_learning_rateY
Ustring_lookup_index_table_table_restore_lookuptableimportv2_string_lookup_index_table
assignvariableop_28_total
assignvariableop_29_count_1+
'assignvariableop_30_adam_dense_kernel_m)
%assignvariableop_31_adam_dense_bias_m-
)assignvariableop_32_adam_dense_1_kernel_m+
'assignvariableop_33_adam_dense_1_bias_m-
)assignvariableop_34_adam_dense_2_kernel_m+
'assignvariableop_35_adam_dense_2_bias_m-
)assignvariableop_36_adam_dense_3_kernel_m+
'assignvariableop_37_adam_dense_3_bias_m-
)assignvariableop_38_adam_dense_4_kernel_m+
'assignvariableop_39_adam_dense_4_bias_m-
)assignvariableop_40_adam_dense_5_kernel_m+
'assignvariableop_41_adam_dense_5_bias_m-
)assignvariableop_42_adam_dense_6_kernel_m+
'assignvariableop_43_adam_dense_6_bias_m-
)assignvariableop_44_adam_dense_7_kernel_m+
'assignvariableop_45_adam_dense_7_bias_m-
)assignvariableop_46_adam_dense_8_kernel_m+
'assignvariableop_47_adam_dense_8_bias_m-
)assignvariableop_48_adam_dense_9_kernel_m+
'assignvariableop_49_adam_dense_9_bias_m+
'assignvariableop_50_adam_dense_kernel_v)
%assignvariableop_51_adam_dense_bias_v-
)assignvariableop_52_adam_dense_1_kernel_v+
'assignvariableop_53_adam_dense_1_bias_v-
)assignvariableop_54_adam_dense_2_kernel_v+
'assignvariableop_55_adam_dense_2_bias_v-
)assignvariableop_56_adam_dense_3_kernel_v+
'assignvariableop_57_adam_dense_3_bias_v-
)assignvariableop_58_adam_dense_4_kernel_v+
'assignvariableop_59_adam_dense_4_bias_v-
)assignvariableop_60_adam_dense_5_kernel_v+
'assignvariableop_61_adam_dense_5_bias_v-
)assignvariableop_62_adam_dense_6_kernel_v+
'assignvariableop_63_adam_dense_6_bias_v-
)assignvariableop_64_adam_dense_7_kernel_v+
'assignvariableop_65_adam_dense_7_bias_v-
)assignvariableop_66_adam_dense_8_kernel_v+
'assignvariableop_67_adam_dense_8_bias_v-
)assignvariableop_68_adam_dense_9_kernel_v+
'assignvariableop_69_adam_dense_9_bias_v
identity_71ѕбAssignVariableOpбAssignVariableOp_1бAssignVariableOp_10бAssignVariableOp_11бAssignVariableOp_12бAssignVariableOp_13бAssignVariableOp_14бAssignVariableOp_15бAssignVariableOp_16бAssignVariableOp_17бAssignVariableOp_18бAssignVariableOp_19бAssignVariableOp_2бAssignVariableOp_20бAssignVariableOp_21бAssignVariableOp_22бAssignVariableOp_23бAssignVariableOp_24бAssignVariableOp_25бAssignVariableOp_26бAssignVariableOp_27бAssignVariableOp_28бAssignVariableOp_29бAssignVariableOp_3бAssignVariableOp_30бAssignVariableOp_31бAssignVariableOp_32бAssignVariableOp_33бAssignVariableOp_34бAssignVariableOp_35бAssignVariableOp_36бAssignVariableOp_37бAssignVariableOp_38бAssignVariableOp_39бAssignVariableOp_4бAssignVariableOp_40бAssignVariableOp_41бAssignVariableOp_42бAssignVariableOp_43бAssignVariableOp_44бAssignVariableOp_45бAssignVariableOp_46бAssignVariableOp_47бAssignVariableOp_48бAssignVariableOp_49бAssignVariableOp_5бAssignVariableOp_50бAssignVariableOp_51бAssignVariableOp_52бAssignVariableOp_53бAssignVariableOp_54бAssignVariableOp_55бAssignVariableOp_56бAssignVariableOp_57бAssignVariableOp_58бAssignVariableOp_59бAssignVariableOp_6бAssignVariableOp_60бAssignVariableOp_61бAssignVariableOp_62бAssignVariableOp_63бAssignVariableOp_64бAssignVariableOp_65бAssignVariableOp_66бAssignVariableOp_67бAssignVariableOp_68бAssignVariableOp_69бAssignVariableOp_7бAssignVariableOp_8бAssignVariableOp_9б;string_lookup_index_table_table_restore/LookupTableImportV2Ђ)
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:I*
dtype0*Ї(
valueЃ(Bђ(IB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/mean/.ATTRIBUTES/VARIABLE_VALUEB8layer_with_weights-4/variance/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-4/count/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-8/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-8/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-9/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-9/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-10/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-10/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-11/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-11/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEBFlayer_with_weights-0/_index_lookup_layer/_table/.ATTRIBUTES/table-keysBHlayer_with_weights-0/_index_lookup_layer/_table/.ATTRIBUTES/table-valuesB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-5/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-6/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-6/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-7/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-7/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-9/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-9/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-10/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-10/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-11/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-11/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-5/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-6/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-6/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-7/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-7/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-9/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-9/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-10/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-10/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-11/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-11/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2/tensor_namesБ
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:I*
dtype0*Д
valueЮBџIB B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
RestoreV2/shape_and_slicesЏ
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*║
_output_shapesД
ц:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*W
dtypesM
K2I			2
	RestoreV2g
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:2

Identityю
AssignVariableOpAssignVariableOpassignvariableop_dense_kernelIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOpk

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:2

Identity_1б
AssignVariableOp_1AssignVariableOpassignvariableop_1_dense_biasIdentity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_1k

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:2

Identity_2д
AssignVariableOp_2AssignVariableOp!assignvariableop_2_dense_1_kernelIdentity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_2k

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:2

Identity_3ц
AssignVariableOp_3AssignVariableOpassignvariableop_3_dense_1_biasIdentity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_3k

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0*
_output_shapes
:2

Identity_4д
AssignVariableOp_4AssignVariableOp!assignvariableop_4_dense_2_kernelIdentity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_4k

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:2

Identity_5ц
AssignVariableOp_5AssignVariableOpassignvariableop_5_dense_2_biasIdentity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_5k

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:2

Identity_6ю
AssignVariableOp_6AssignVariableOpassignvariableop_6_meanIdentity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_6k

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:2

Identity_7а
AssignVariableOp_7AssignVariableOpassignvariableop_7_varianceIdentity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_7k

Identity_8IdentityRestoreV2:tensors:8"/device:CPU:0*
T0	*
_output_shapes
:2

Identity_8Ю
AssignVariableOp_8AssignVariableOpassignvariableop_8_countIdentity_8:output:0"/device:CPU:0*
_output_shapes
 *
dtype0	2
AssignVariableOp_8k

Identity_9IdentityRestoreV2:tensors:9"/device:CPU:0*
T0*
_output_shapes
:2

Identity_9д
AssignVariableOp_9AssignVariableOp!assignvariableop_9_dense_3_kernelIdentity_9:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_9n
Identity_10IdentityRestoreV2:tensors:10"/device:CPU:0*
T0*
_output_shapes
:2
Identity_10е
AssignVariableOp_10AssignVariableOp assignvariableop_10_dense_3_biasIdentity_10:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_10n
Identity_11IdentityRestoreV2:tensors:11"/device:CPU:0*
T0*
_output_shapes
:2
Identity_11ф
AssignVariableOp_11AssignVariableOp"assignvariableop_11_dense_4_kernelIdentity_11:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_11n
Identity_12IdentityRestoreV2:tensors:12"/device:CPU:0*
T0*
_output_shapes
:2
Identity_12е
AssignVariableOp_12AssignVariableOp assignvariableop_12_dense_4_biasIdentity_12:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_12n
Identity_13IdentityRestoreV2:tensors:13"/device:CPU:0*
T0*
_output_shapes
:2
Identity_13ф
AssignVariableOp_13AssignVariableOp"assignvariableop_13_dense_5_kernelIdentity_13:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_13n
Identity_14IdentityRestoreV2:tensors:14"/device:CPU:0*
T0*
_output_shapes
:2
Identity_14е
AssignVariableOp_14AssignVariableOp assignvariableop_14_dense_5_biasIdentity_14:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_14n
Identity_15IdentityRestoreV2:tensors:15"/device:CPU:0*
T0*
_output_shapes
:2
Identity_15ф
AssignVariableOp_15AssignVariableOp"assignvariableop_15_dense_6_kernelIdentity_15:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_15n
Identity_16IdentityRestoreV2:tensors:16"/device:CPU:0*
T0*
_output_shapes
:2
Identity_16е
AssignVariableOp_16AssignVariableOp assignvariableop_16_dense_6_biasIdentity_16:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_16n
Identity_17IdentityRestoreV2:tensors:17"/device:CPU:0*
T0*
_output_shapes
:2
Identity_17ф
AssignVariableOp_17AssignVariableOp"assignvariableop_17_dense_7_kernelIdentity_17:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_17n
Identity_18IdentityRestoreV2:tensors:18"/device:CPU:0*
T0*
_output_shapes
:2
Identity_18е
AssignVariableOp_18AssignVariableOp assignvariableop_18_dense_7_biasIdentity_18:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_18n
Identity_19IdentityRestoreV2:tensors:19"/device:CPU:0*
T0*
_output_shapes
:2
Identity_19ф
AssignVariableOp_19AssignVariableOp"assignvariableop_19_dense_8_kernelIdentity_19:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_19n
Identity_20IdentityRestoreV2:tensors:20"/device:CPU:0*
T0*
_output_shapes
:2
Identity_20е
AssignVariableOp_20AssignVariableOp assignvariableop_20_dense_8_biasIdentity_20:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_20n
Identity_21IdentityRestoreV2:tensors:21"/device:CPU:0*
T0*
_output_shapes
:2
Identity_21ф
AssignVariableOp_21AssignVariableOp"assignvariableop_21_dense_9_kernelIdentity_21:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_21n
Identity_22IdentityRestoreV2:tensors:22"/device:CPU:0*
T0*
_output_shapes
:2
Identity_22е
AssignVariableOp_22AssignVariableOp assignvariableop_22_dense_9_biasIdentity_22:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_22n
Identity_23IdentityRestoreV2:tensors:23"/device:CPU:0*
T0	*
_output_shapes
:2
Identity_23Ц
AssignVariableOp_23AssignVariableOpassignvariableop_23_adam_iterIdentity_23:output:0"/device:CPU:0*
_output_shapes
 *
dtype0	2
AssignVariableOp_23n
Identity_24IdentityRestoreV2:tensors:24"/device:CPU:0*
T0*
_output_shapes
:2
Identity_24Д
AssignVariableOp_24AssignVariableOpassignvariableop_24_adam_beta_1Identity_24:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_24n
Identity_25IdentityRestoreV2:tensors:25"/device:CPU:0*
T0*
_output_shapes
:2
Identity_25Д
AssignVariableOp_25AssignVariableOpassignvariableop_25_adam_beta_2Identity_25:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_25n
Identity_26IdentityRestoreV2:tensors:26"/device:CPU:0*
T0*
_output_shapes
:2
Identity_26д
AssignVariableOp_26AssignVariableOpassignvariableop_26_adam_decayIdentity_26:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_26n
Identity_27IdentityRestoreV2:tensors:27"/device:CPU:0*
T0*
_output_shapes
:2
Identity_27«
AssignVariableOp_27AssignVariableOp&assignvariableop_27_adam_learning_rateIdentity_27:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_27№
;string_lookup_index_table_table_restore/LookupTableImportV2LookupTableImportV2Ustring_lookup_index_table_table_restore_lookuptableimportv2_string_lookup_index_tableRestoreV2:tensors:28RestoreV2:tensors:29*	
Tin0*

Tout0	*,
_class"
 loc:@string_lookup_index_table*
_output_shapes
 2=
;string_lookup_index_table_table_restore/LookupTableImportV2n
Identity_28IdentityRestoreV2:tensors:30"/device:CPU:0*
T0*
_output_shapes
:2
Identity_28А
AssignVariableOp_28AssignVariableOpassignvariableop_28_totalIdentity_28:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_28n
Identity_29IdentityRestoreV2:tensors:31"/device:CPU:0*
T0*
_output_shapes
:2
Identity_29Б
AssignVariableOp_29AssignVariableOpassignvariableop_29_count_1Identity_29:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_29n
Identity_30IdentityRestoreV2:tensors:32"/device:CPU:0*
T0*
_output_shapes
:2
Identity_30»
AssignVariableOp_30AssignVariableOp'assignvariableop_30_adam_dense_kernel_mIdentity_30:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_30n
Identity_31IdentityRestoreV2:tensors:33"/device:CPU:0*
T0*
_output_shapes
:2
Identity_31Г
AssignVariableOp_31AssignVariableOp%assignvariableop_31_adam_dense_bias_mIdentity_31:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_31n
Identity_32IdentityRestoreV2:tensors:34"/device:CPU:0*
T0*
_output_shapes
:2
Identity_32▒
AssignVariableOp_32AssignVariableOp)assignvariableop_32_adam_dense_1_kernel_mIdentity_32:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_32n
Identity_33IdentityRestoreV2:tensors:35"/device:CPU:0*
T0*
_output_shapes
:2
Identity_33»
AssignVariableOp_33AssignVariableOp'assignvariableop_33_adam_dense_1_bias_mIdentity_33:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_33n
Identity_34IdentityRestoreV2:tensors:36"/device:CPU:0*
T0*
_output_shapes
:2
Identity_34▒
AssignVariableOp_34AssignVariableOp)assignvariableop_34_adam_dense_2_kernel_mIdentity_34:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_34n
Identity_35IdentityRestoreV2:tensors:37"/device:CPU:0*
T0*
_output_shapes
:2
Identity_35»
AssignVariableOp_35AssignVariableOp'assignvariableop_35_adam_dense_2_bias_mIdentity_35:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_35n
Identity_36IdentityRestoreV2:tensors:38"/device:CPU:0*
T0*
_output_shapes
:2
Identity_36▒
AssignVariableOp_36AssignVariableOp)assignvariableop_36_adam_dense_3_kernel_mIdentity_36:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_36n
Identity_37IdentityRestoreV2:tensors:39"/device:CPU:0*
T0*
_output_shapes
:2
Identity_37»
AssignVariableOp_37AssignVariableOp'assignvariableop_37_adam_dense_3_bias_mIdentity_37:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_37n
Identity_38IdentityRestoreV2:tensors:40"/device:CPU:0*
T0*
_output_shapes
:2
Identity_38▒
AssignVariableOp_38AssignVariableOp)assignvariableop_38_adam_dense_4_kernel_mIdentity_38:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_38n
Identity_39IdentityRestoreV2:tensors:41"/device:CPU:0*
T0*
_output_shapes
:2
Identity_39»
AssignVariableOp_39AssignVariableOp'assignvariableop_39_adam_dense_4_bias_mIdentity_39:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_39n
Identity_40IdentityRestoreV2:tensors:42"/device:CPU:0*
T0*
_output_shapes
:2
Identity_40▒
AssignVariableOp_40AssignVariableOp)assignvariableop_40_adam_dense_5_kernel_mIdentity_40:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_40n
Identity_41IdentityRestoreV2:tensors:43"/device:CPU:0*
T0*
_output_shapes
:2
Identity_41»
AssignVariableOp_41AssignVariableOp'assignvariableop_41_adam_dense_5_bias_mIdentity_41:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_41n
Identity_42IdentityRestoreV2:tensors:44"/device:CPU:0*
T0*
_output_shapes
:2
Identity_42▒
AssignVariableOp_42AssignVariableOp)assignvariableop_42_adam_dense_6_kernel_mIdentity_42:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_42n
Identity_43IdentityRestoreV2:tensors:45"/device:CPU:0*
T0*
_output_shapes
:2
Identity_43»
AssignVariableOp_43AssignVariableOp'assignvariableop_43_adam_dense_6_bias_mIdentity_43:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_43n
Identity_44IdentityRestoreV2:tensors:46"/device:CPU:0*
T0*
_output_shapes
:2
Identity_44▒
AssignVariableOp_44AssignVariableOp)assignvariableop_44_adam_dense_7_kernel_mIdentity_44:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_44n
Identity_45IdentityRestoreV2:tensors:47"/device:CPU:0*
T0*
_output_shapes
:2
Identity_45»
AssignVariableOp_45AssignVariableOp'assignvariableop_45_adam_dense_7_bias_mIdentity_45:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_45n
Identity_46IdentityRestoreV2:tensors:48"/device:CPU:0*
T0*
_output_shapes
:2
Identity_46▒
AssignVariableOp_46AssignVariableOp)assignvariableop_46_adam_dense_8_kernel_mIdentity_46:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_46n
Identity_47IdentityRestoreV2:tensors:49"/device:CPU:0*
T0*
_output_shapes
:2
Identity_47»
AssignVariableOp_47AssignVariableOp'assignvariableop_47_adam_dense_8_bias_mIdentity_47:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_47n
Identity_48IdentityRestoreV2:tensors:50"/device:CPU:0*
T0*
_output_shapes
:2
Identity_48▒
AssignVariableOp_48AssignVariableOp)assignvariableop_48_adam_dense_9_kernel_mIdentity_48:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_48n
Identity_49IdentityRestoreV2:tensors:51"/device:CPU:0*
T0*
_output_shapes
:2
Identity_49»
AssignVariableOp_49AssignVariableOp'assignvariableop_49_adam_dense_9_bias_mIdentity_49:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_49n
Identity_50IdentityRestoreV2:tensors:52"/device:CPU:0*
T0*
_output_shapes
:2
Identity_50»
AssignVariableOp_50AssignVariableOp'assignvariableop_50_adam_dense_kernel_vIdentity_50:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_50n
Identity_51IdentityRestoreV2:tensors:53"/device:CPU:0*
T0*
_output_shapes
:2
Identity_51Г
AssignVariableOp_51AssignVariableOp%assignvariableop_51_adam_dense_bias_vIdentity_51:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_51n
Identity_52IdentityRestoreV2:tensors:54"/device:CPU:0*
T0*
_output_shapes
:2
Identity_52▒
AssignVariableOp_52AssignVariableOp)assignvariableop_52_adam_dense_1_kernel_vIdentity_52:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_52n
Identity_53IdentityRestoreV2:tensors:55"/device:CPU:0*
T0*
_output_shapes
:2
Identity_53»
AssignVariableOp_53AssignVariableOp'assignvariableop_53_adam_dense_1_bias_vIdentity_53:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_53n
Identity_54IdentityRestoreV2:tensors:56"/device:CPU:0*
T0*
_output_shapes
:2
Identity_54▒
AssignVariableOp_54AssignVariableOp)assignvariableop_54_adam_dense_2_kernel_vIdentity_54:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_54n
Identity_55IdentityRestoreV2:tensors:57"/device:CPU:0*
T0*
_output_shapes
:2
Identity_55»
AssignVariableOp_55AssignVariableOp'assignvariableop_55_adam_dense_2_bias_vIdentity_55:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_55n
Identity_56IdentityRestoreV2:tensors:58"/device:CPU:0*
T0*
_output_shapes
:2
Identity_56▒
AssignVariableOp_56AssignVariableOp)assignvariableop_56_adam_dense_3_kernel_vIdentity_56:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_56n
Identity_57IdentityRestoreV2:tensors:59"/device:CPU:0*
T0*
_output_shapes
:2
Identity_57»
AssignVariableOp_57AssignVariableOp'assignvariableop_57_adam_dense_3_bias_vIdentity_57:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_57n
Identity_58IdentityRestoreV2:tensors:60"/device:CPU:0*
T0*
_output_shapes
:2
Identity_58▒
AssignVariableOp_58AssignVariableOp)assignvariableop_58_adam_dense_4_kernel_vIdentity_58:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_58n
Identity_59IdentityRestoreV2:tensors:61"/device:CPU:0*
T0*
_output_shapes
:2
Identity_59»
AssignVariableOp_59AssignVariableOp'assignvariableop_59_adam_dense_4_bias_vIdentity_59:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_59n
Identity_60IdentityRestoreV2:tensors:62"/device:CPU:0*
T0*
_output_shapes
:2
Identity_60▒
AssignVariableOp_60AssignVariableOp)assignvariableop_60_adam_dense_5_kernel_vIdentity_60:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_60n
Identity_61IdentityRestoreV2:tensors:63"/device:CPU:0*
T0*
_output_shapes
:2
Identity_61»
AssignVariableOp_61AssignVariableOp'assignvariableop_61_adam_dense_5_bias_vIdentity_61:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_61n
Identity_62IdentityRestoreV2:tensors:64"/device:CPU:0*
T0*
_output_shapes
:2
Identity_62▒
AssignVariableOp_62AssignVariableOp)assignvariableop_62_adam_dense_6_kernel_vIdentity_62:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_62n
Identity_63IdentityRestoreV2:tensors:65"/device:CPU:0*
T0*
_output_shapes
:2
Identity_63»
AssignVariableOp_63AssignVariableOp'assignvariableop_63_adam_dense_6_bias_vIdentity_63:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_63n
Identity_64IdentityRestoreV2:tensors:66"/device:CPU:0*
T0*
_output_shapes
:2
Identity_64▒
AssignVariableOp_64AssignVariableOp)assignvariableop_64_adam_dense_7_kernel_vIdentity_64:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_64n
Identity_65IdentityRestoreV2:tensors:67"/device:CPU:0*
T0*
_output_shapes
:2
Identity_65»
AssignVariableOp_65AssignVariableOp'assignvariableop_65_adam_dense_7_bias_vIdentity_65:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_65n
Identity_66IdentityRestoreV2:tensors:68"/device:CPU:0*
T0*
_output_shapes
:2
Identity_66▒
AssignVariableOp_66AssignVariableOp)assignvariableop_66_adam_dense_8_kernel_vIdentity_66:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_66n
Identity_67IdentityRestoreV2:tensors:69"/device:CPU:0*
T0*
_output_shapes
:2
Identity_67»
AssignVariableOp_67AssignVariableOp'assignvariableop_67_adam_dense_8_bias_vIdentity_67:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_67n
Identity_68IdentityRestoreV2:tensors:70"/device:CPU:0*
T0*
_output_shapes
:2
Identity_68▒
AssignVariableOp_68AssignVariableOp)assignvariableop_68_adam_dense_9_kernel_vIdentity_68:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_68n
Identity_69IdentityRestoreV2:tensors:71"/device:CPU:0*
T0*
_output_shapes
:2
Identity_69»
AssignVariableOp_69AssignVariableOp'assignvariableop_69_adam_dense_9_bias_vIdentity_69:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_699
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOpа
Identity_70Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_40^AssignVariableOp_41^AssignVariableOp_42^AssignVariableOp_43^AssignVariableOp_44^AssignVariableOp_45^AssignVariableOp_46^AssignVariableOp_47^AssignVariableOp_48^AssignVariableOp_49^AssignVariableOp_5^AssignVariableOp_50^AssignVariableOp_51^AssignVariableOp_52^AssignVariableOp_53^AssignVariableOp_54^AssignVariableOp_55^AssignVariableOp_56^AssignVariableOp_57^AssignVariableOp_58^AssignVariableOp_59^AssignVariableOp_6^AssignVariableOp_60^AssignVariableOp_61^AssignVariableOp_62^AssignVariableOp_63^AssignVariableOp_64^AssignVariableOp_65^AssignVariableOp_66^AssignVariableOp_67^AssignVariableOp_68^AssignVariableOp_69^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp<^string_lookup_index_table_table_restore/LookupTableImportV2"/device:CPU:0*
T0*
_output_shapes
: 2
Identity_70Њ
Identity_71IdentityIdentity_70:output:0^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_40^AssignVariableOp_41^AssignVariableOp_42^AssignVariableOp_43^AssignVariableOp_44^AssignVariableOp_45^AssignVariableOp_46^AssignVariableOp_47^AssignVariableOp_48^AssignVariableOp_49^AssignVariableOp_5^AssignVariableOp_50^AssignVariableOp_51^AssignVariableOp_52^AssignVariableOp_53^AssignVariableOp_54^AssignVariableOp_55^AssignVariableOp_56^AssignVariableOp_57^AssignVariableOp_58^AssignVariableOp_59^AssignVariableOp_6^AssignVariableOp_60^AssignVariableOp_61^AssignVariableOp_62^AssignVariableOp_63^AssignVariableOp_64^AssignVariableOp_65^AssignVariableOp_66^AssignVariableOp_67^AssignVariableOp_68^AssignVariableOp_69^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9<^string_lookup_index_table_table_restore/LookupTableImportV2*
T0*
_output_shapes
: 2
Identity_71"#
identity_71Identity_71:output:0*│
_input_shapesА
ъ: :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212*
AssignVariableOp_22AssignVariableOp_222*
AssignVariableOp_23AssignVariableOp_232*
AssignVariableOp_24AssignVariableOp_242*
AssignVariableOp_25AssignVariableOp_252*
AssignVariableOp_26AssignVariableOp_262*
AssignVariableOp_27AssignVariableOp_272*
AssignVariableOp_28AssignVariableOp_282*
AssignVariableOp_29AssignVariableOp_292(
AssignVariableOp_3AssignVariableOp_32*
AssignVariableOp_30AssignVariableOp_302*
AssignVariableOp_31AssignVariableOp_312*
AssignVariableOp_32AssignVariableOp_322*
AssignVariableOp_33AssignVariableOp_332*
AssignVariableOp_34AssignVariableOp_342*
AssignVariableOp_35AssignVariableOp_352*
AssignVariableOp_36AssignVariableOp_362*
AssignVariableOp_37AssignVariableOp_372*
AssignVariableOp_38AssignVariableOp_382*
AssignVariableOp_39AssignVariableOp_392(
AssignVariableOp_4AssignVariableOp_42*
AssignVariableOp_40AssignVariableOp_402*
AssignVariableOp_41AssignVariableOp_412*
AssignVariableOp_42AssignVariableOp_422*
AssignVariableOp_43AssignVariableOp_432*
AssignVariableOp_44AssignVariableOp_442*
AssignVariableOp_45AssignVariableOp_452*
AssignVariableOp_46AssignVariableOp_462*
AssignVariableOp_47AssignVariableOp_472*
AssignVariableOp_48AssignVariableOp_482*
AssignVariableOp_49AssignVariableOp_492(
AssignVariableOp_5AssignVariableOp_52*
AssignVariableOp_50AssignVariableOp_502*
AssignVariableOp_51AssignVariableOp_512*
AssignVariableOp_52AssignVariableOp_522*
AssignVariableOp_53AssignVariableOp_532*
AssignVariableOp_54AssignVariableOp_542*
AssignVariableOp_55AssignVariableOp_552*
AssignVariableOp_56AssignVariableOp_562*
AssignVariableOp_57AssignVariableOp_572*
AssignVariableOp_58AssignVariableOp_582*
AssignVariableOp_59AssignVariableOp_592(
AssignVariableOp_6AssignVariableOp_62*
AssignVariableOp_60AssignVariableOp_602*
AssignVariableOp_61AssignVariableOp_612*
AssignVariableOp_62AssignVariableOp_622*
AssignVariableOp_63AssignVariableOp_632*
AssignVariableOp_64AssignVariableOp_642*
AssignVariableOp_65AssignVariableOp_652*
AssignVariableOp_66AssignVariableOp_662*
AssignVariableOp_67AssignVariableOp_672*
AssignVariableOp_68AssignVariableOp_682*
AssignVariableOp_69AssignVariableOp_692(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_92z
;string_lookup_index_table_table_restore/LookupTableImportV2;string_lookup_index_table_table_restore/LookupTableImportV2:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:2.
,
_class"
 loc:@string_lookup_index_table
ъ
С
!text_vectorization_cond_true_6625A
=text_vectorization_cond_pad_paddings_1_text_vectorization_subV
Rtext_vectorization_cond_pad_text_vectorization_raggedtotensor_raggedtensortotensor	$
 text_vectorization_cond_identity	ќ
(text_vectorization/cond/Pad/paddings/1/0Const*
_output_shapes
: *
dtype0*
value	B : 2*
(text_vectorization/cond/Pad/paddings/1/0Э
&text_vectorization/cond/Pad/paddings/1Pack1text_vectorization/cond/Pad/paddings/1/0:output:0=text_vectorization_cond_pad_paddings_1_text_vectorization_sub*
N*
T0*
_output_shapes
:2(
&text_vectorization/cond/Pad/paddings/1Ц
(text_vectorization/cond/Pad/paddings/0_1Const*
_output_shapes
:*
dtype0*
valueB"        2*
(text_vectorization/cond/Pad/paddings/0_1Ж
$text_vectorization/cond/Pad/paddingsPack1text_vectorization/cond/Pad/paddings/0_1:output:0/text_vectorization/cond/Pad/paddings/1:output:0*
N*
T0*
_output_shapes

:2&
$text_vectorization/cond/Pad/paddings 
text_vectorization/cond/PadPadRtext_vectorization_cond_pad_text_vectorization_raggedtotensor_raggedtensortotensor-text_vectorization/cond/Pad/paddings:output:0*
T0	*0
_output_shapes
:                  2
text_vectorization/cond/Pad▒
 text_vectorization/cond/IdentityIdentity$text_vectorization/cond/Pad:output:0*
T0	*0
_output_shapes
:                  2"
 text_vectorization/cond/Identity"M
 text_vectorization_cond_identity)text_vectorization/cond/Identity:output:0*1
_input_shapes 
: :                  : 

_output_shapes
: :62
0
_output_shapes
:                  
о
{
&__inference_dense_9_layer_call_fn_7840

inputs
unknown
	unknown_0
identityѕбStatefulPartitionedCallы
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *J
fERC
A__inference_dense_9_layer_call_and_return_conditional_losses_65502
StatefulPartitionedCallј
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
┐
у
+__inference_functional_1_layer_call_fn_7627
inputs_0
inputs_1
unknown
	unknown_0	
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
	unknown_7
	unknown_8
	unknown_9

unknown_10

unknown_11

unknown_12

unknown_13

unknown_14

unknown_15

unknown_16

unknown_17

unknown_18

unknown_19

unknown_20

unknown_21

unknown_22
identityѕбStatefulPartitionedCallг
StatefulPartitionedCallStatefulPartitionedCallinputs_0inputs_1unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22*%
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *8
_read_only_resource_inputs
	
*-
config_proto

CPU

GPU 2J 8ѓ *O
fJRH
F__inference_functional_1_layer_call_and_return_conditional_losses_70512
StatefulPartitionedCallј
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*Ў
_input_shapesЄ
ё:         :         :: ::::::::::::::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:Q M
'
_output_shapes
:         
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:         
"
_user_specified_name
inputs/1
Е
Е
A__inference_dense_2_layer_call_and_return_conditional_losses_6335

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityѕј
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	ђ@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulї
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpЂ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*/
_input_shapes
:         ђ:::P L
(
_output_shapes
:         ђ
 
_user_specified_nameinputs
¤
Н
"text_vectorization_cond_false_7415'
#text_vectorization_cond_placeholder`
\text_vectorization_cond_strided_slice_text_vectorization_raggedtotensor_raggedtensortotensor	$
 text_vectorization_cond_identity	Ф
+text_vectorization/cond/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2-
+text_vectorization/cond/strided_slice/stack»
-text_vectorization/cond/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"    ▄  2/
-text_vectorization/cond/strided_slice/stack_1»
-text_vectorization/cond/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2/
-text_vectorization/cond/strided_slice/stack_2╠
%text_vectorization/cond/strided_sliceStridedSlice\text_vectorization_cond_strided_slice_text_vectorization_raggedtotensor_raggedtensortotensor4text_vectorization/cond/strided_slice/stack:output:06text_vectorization/cond/strided_slice/stack_1:output:06text_vectorization/cond/strided_slice/stack_2:output:0*
Index0*
T0	*0
_output_shapes
:                  *

begin_mask*
end_mask2'
%text_vectorization/cond/strided_slice╗
 text_vectorization/cond/IdentityIdentity.text_vectorization/cond/strided_slice:output:0*
T0	*0
_output_shapes
:                  2"
 text_vectorization/cond/Identity"M
 text_vectorization_cond_identity)text_vectorization/cond/Identity:output:0*1
_input_shapes 
: :                  : 

_output_shapes
: :62
0
_output_shapes
:                  
д
Е
A__inference_dense_8_layer_call_and_return_conditional_losses_6524

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityѕЇ
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulї
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpЂ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
Reluf
IdentityIdentityRelu:activations:0*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @:::O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
С
H
__inference__creator_7845
identityѕбstring_lookup_index_tableБ
string_lookup_index_tableMutableHashTableV2*
_output_shapes
: *
	key_dtype0*
shared_name
table_26*
value_dtype0	2
string_lookup_index_tableЄ
IdentityIdentity(string_lookup_index_table:table_handle:0^string_lookup_index_table*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*
_input_shapes 26
string_lookup_index_tablestring_lookup_index_table
»
Е
A__inference_dense_1_layer_call_and_return_conditional_losses_7659

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityѕЈ
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
ђђ*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2
MatMulЇ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:ђ*
dtype02
BiasAdd/ReadVariableOpѓ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:         ђ2
Relug
IdentityIdentityRelu:activations:0*
T0*(
_output_shapes
:         ђ2

Identity"
identityIdentity:output:0*/
_input_shapes
:         ђ:::P L
(
_output_shapes
:         ђ
 
_user_specified_nameinputs"ИL
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*у
serving_defaultМ
;
input_10
serving_default_input_1:0         
;
input_20
serving_default_input_2:0         ;
dense_90
StatefulPartitionedCall:0         tensorflow/serving/predict:╬Њ
▓o
layer-0
layer_with_weights-0
layer-1
layer_with_weights-1
layer-2
layer_with_weights-2
layer-3
layer-4
layer_with_weights-3
layer-5
layer_with_weights-4
layer-6
layer_with_weights-5
layer-7
	layer-8

layer_with_weights-6

layer-9
layer_with_weights-7
layer-10
layer_with_weights-8
layer-11
layer_with_weights-9
layer-12
layer_with_weights-10
layer-13
layer_with_weights-11
layer-14
	optimizer
regularization_losses
trainable_variables
	variables
	keras_api

signatures
л_default_save_signature
+Л&call_and_return_all_conditional_losses
м__call__"Рi
_tf_keras_networkкi{"class_name": "Functional", "name": "functional_1", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "must_restore_from_config": false, "config": {"name": "functional_1", "layers": [{"class_name": "InputLayer", "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 1]}, "dtype": "string", "sparse": false, "ragged": false, "name": "input_1"}, "name": "input_1", "inbound_nodes": []}, {"class_name": "TextVectorization", "config": {"name": "text_vectorization", "trainable": true, "dtype": "string", "max_tokens": 30, "standardize": "lower_and_strip_punctuation", "split": "Custom>charsplit", "ngrams": null, "output_mode": "int", "output_sequence_length": 1500, "pad_to_max_tokens": true}, "name": "text_vectorization", "inbound_nodes": [[["input_1", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense", "trainable": true, "dtype": "float32", "units": 128, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense", "inbound_nodes": [[["text_vectorization", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_1", "trainable": true, "dtype": "float32", "units": 128, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_1", "inbound_nodes": [[["dense", 0, 0, {}]]]}, {"class_name": "InputLayer", "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 5]}, "dtype": "float32", "sparse": false, "ragged": false, "name": "input_2"}, "name": "input_2", "inbound_nodes": []}, {"class_name": "Dense", "config": {"name": "dense_2", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_2", "inbound_nodes": [[["dense_1", 0, 0, {}]]]}, {"class_name": "Normalization", "config": {"name": "normalization", "trainable": true, "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "dtype": "float32", "axis": {"class_name": "__tuple__", "items": [-1]}}, "name": "normalization", "inbound_nodes": [[["input_2", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_3", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_3", "inbound_nodes": [[["dense_2", 0, 0, {}]]]}, {"class_name": "Concatenate", "config": {"name": "concatenate", "trainable": true, "dtype": "float32", "axis": -1}, "name": "concatenate", "inbound_nodes": [[["normalization", 0, 0, {}], ["dense_3", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_4", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_4", "inbound_nodes": [[["concatenate", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_5", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_5", "inbound_nodes": [[["dense_4", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_6", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_6", "inbound_nodes": [[["dense_5", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_7", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_7", "inbound_nodes": [[["dense_6", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_8", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_8", "inbound_nodes": [[["dense_7", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_9", "trainable": true, "dtype": "float32", "units": 1, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_9", "inbound_nodes": [[["dense_8", 0, 0, {}]]]}], "input_layers": [["input_2", 0, 0], ["input_1", 0, 0]], "output_layers": [["dense_9", 0, 0]]}, "build_input_shape": [{"class_name": "TensorShape", "items": [null, 5]}, {"class_name": "TensorShape", "items": [null, 1]}], "is_graph_network": true, "keras_version": "2.4.0", "backend": "tensorflow", "model_config": {"class_name": "Functional", "config": {"name": "functional_1", "layers": [{"class_name": "InputLayer", "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 1]}, "dtype": "string", "sparse": false, "ragged": false, "name": "input_1"}, "name": "input_1", "inbound_nodes": []}, {"class_name": "TextVectorization", "config": {"name": "text_vectorization", "trainable": true, "dtype": "string", "max_tokens": 30, "standardize": "lower_and_strip_punctuation", "split": "Custom>charsplit", "ngrams": null, "output_mode": "int", "output_sequence_length": 1500, "pad_to_max_tokens": true}, "name": "text_vectorization", "inbound_nodes": [[["input_1", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense", "trainable": true, "dtype": "float32", "units": 128, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense", "inbound_nodes": [[["text_vectorization", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_1", "trainable": true, "dtype": "float32", "units": 128, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_1", "inbound_nodes": [[["dense", 0, 0, {}]]]}, {"class_name": "InputLayer", "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 5]}, "dtype": "float32", "sparse": false, "ragged": false, "name": "input_2"}, "name": "input_2", "inbound_nodes": []}, {"class_name": "Dense", "config": {"name": "dense_2", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_2", "inbound_nodes": [[["dense_1", 0, 0, {}]]]}, {"class_name": "Normalization", "config": {"name": "normalization", "trainable": true, "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "dtype": "float32", "axis": {"class_name": "__tuple__", "items": [-1]}}, "name": "normalization", "inbound_nodes": [[["input_2", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_3", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_3", "inbound_nodes": [[["dense_2", 0, 0, {}]]]}, {"class_name": "Concatenate", "config": {"name": "concatenate", "trainable": true, "dtype": "float32", "axis": -1}, "name": "concatenate", "inbound_nodes": [[["normalization", 0, 0, {}], ["dense_3", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_4", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_4", "inbound_nodes": [[["concatenate", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_5", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_5", "inbound_nodes": [[["dense_4", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_6", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_6", "inbound_nodes": [[["dense_5", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_7", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_7", "inbound_nodes": [[["dense_6", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_8", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_8", "inbound_nodes": [[["dense_7", 0, 0, {}]]]}, {"class_name": "Dense", "config": {"name": "dense_9", "trainable": true, "dtype": "float32", "units": 1, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_9", "inbound_nodes": [[["dense_8", 0, 0, {}]]]}], "input_layers": [["input_2", 0, 0], ["input_1", 0, 0]], "output_layers": [["dense_9", 0, 0]]}}, "training_config": {"loss": "mean_absolute_error", "metrics": null, "weighted_metrics": null, "loss_weights": null, "optimizer_config": {"class_name": "Adam", "config": {"name": "Adam", "learning_rate": 0.0006000000284984708, "decay": 0.0, "beta_1": 0.8999999761581421, "beta_2": 0.9990000128746033, "epsilon": 1e-07, "amsgrad": false}}}}
у"С
_tf_keras_input_layer─{"class_name": "InputLayer", "name": "input_1", "dtype": "string", "sparse": false, "ragged": false, "batch_input_shape": {"class_name": "__tuple__", "items": [null, 1]}, "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 1]}, "dtype": "string", "sparse": false, "ragged": false, "name": "input_1"}}
Є
state_variables
_index_lookup_layer
	keras_api"К
_tf_keras_layerГ{"class_name": "TextVectorization", "name": "text_vectorization", "trainable": true, "expects_training_arg": false, "dtype": "string", "batch_input_shape": null, "stateful": false, "must_restore_from_config": true, "config": {"name": "text_vectorization", "trainable": true, "dtype": "string", "max_tokens": 30, "standardize": "lower_and_strip_punctuation", "split": "Custom>charsplit", "ngrams": null, "output_mode": "int", "output_sequence_length": 1500, "pad_to_max_tokens": true}, "build_input_shape": {"class_name": "TensorShape", "items": [59125, 1]}}
з

kernel
bias
regularization_losses
trainable_variables
	variables
	keras_api
+Н&call_and_return_all_conditional_losses
о__call__"╠
_tf_keras_layer▓{"class_name": "Dense", "name": "dense", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "dense", "trainable": true, "dtype": "float32", "units": 128, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 1500}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 1500]}}
ш

kernel
 bias
!regularization_losses
"trainable_variables
#	variables
$	keras_api
+О&call_and_return_all_conditional_losses
п__call__"╬
_tf_keras_layer┤{"class_name": "Dense", "name": "dense_1", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "dense_1", "trainable": true, "dtype": "float32", "units": 128, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 128}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 128]}}
ж"Т
_tf_keras_input_layerк{"class_name": "InputLayer", "name": "input_2", "dtype": "float32", "sparse": false, "ragged": false, "batch_input_shape": {"class_name": "__tuple__", "items": [null, 5]}, "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 5]}, "dtype": "float32", "sparse": false, "ragged": false, "name": "input_2"}}
З

%kernel
&bias
'regularization_losses
(trainable_variables
)	variables
*	keras_api
+┘&call_and_return_all_conditional_losses
┌__call__"═
_tf_keras_layer│{"class_name": "Dense", "name": "dense_2", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "dense_2", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 128}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 128]}}
я
+state_variables
,_broadcast_shape
-mean
.variance
	/count
0	keras_api"■
_tf_keras_layerС{"class_name": "Normalization", "name": "normalization", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "stateful": false, "must_restore_from_config": true, "config": {"name": "normalization", "trainable": true, "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "dtype": "float32", "axis": {"class_name": "__tuple__", "items": [-1]}}, "build_input_shape": [512, 5]}
Ы

1kernel
2bias
3regularization_losses
4trainable_variables
5	variables
6	keras_api
+█&call_and_return_all_conditional_losses
▄__call__"╦
_tf_keras_layer▒{"class_name": "Dense", "name": "dense_3", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "dense_3", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 64]}}
╩
7regularization_losses
8trainable_variables
9	variables
:	keras_api
+П&call_and_return_all_conditional_losses
я__call__"╣
_tf_keras_layerЪ{"class_name": "Concatenate", "name": "concatenate", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "concatenate", "trainable": true, "dtype": "float32", "axis": -1}, "build_input_shape": [{"class_name": "TensorShape", "items": [null, 5]}, {"class_name": "TensorShape", "items": [null, 64]}]}
Ы

;kernel
<bias
=regularization_losses
>trainable_variables
?	variables
@	keras_api
+▀&call_and_return_all_conditional_losses
Я__call__"╦
_tf_keras_layer▒{"class_name": "Dense", "name": "dense_4", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "dense_4", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 69}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 69]}}
Ы

Akernel
Bbias
Cregularization_losses
Dtrainable_variables
E	variables
F	keras_api
+р&call_and_return_all_conditional_losses
Р__call__"╦
_tf_keras_layer▒{"class_name": "Dense", "name": "dense_5", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "dense_5", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 64]}}
Ы

Gkernel
Hbias
Iregularization_losses
Jtrainable_variables
K	variables
L	keras_api
+с&call_and_return_all_conditional_losses
С__call__"╦
_tf_keras_layer▒{"class_name": "Dense", "name": "dense_6", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "dense_6", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 64]}}
Ы

Mkernel
Nbias
Oregularization_losses
Ptrainable_variables
Q	variables
R	keras_api
+т&call_and_return_all_conditional_losses
Т__call__"╦
_tf_keras_layer▒{"class_name": "Dense", "name": "dense_7", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "dense_7", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 64]}}
Ы

Skernel
Tbias
Uregularization_losses
Vtrainable_variables
W	variables
X	keras_api
+у&call_and_return_all_conditional_losses
У__call__"╦
_tf_keras_layer▒{"class_name": "Dense", "name": "dense_8", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "dense_8", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 64]}}
з

Ykernel
Zbias
[regularization_losses
\trainable_variables
]	variables
^	keras_api
+ж&call_and_return_all_conditional_losses
Ж__call__"╠
_tf_keras_layer▓{"class_name": "Dense", "name": "dense_9", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "dense_9", "trainable": true, "dtype": "float32", "units": 1, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 64]}}
с
_iter

`beta_1

abeta_2
	bdecay
clearning_ratemеmЕmф mФ%mг&mГ1m«2m»;m░<m▒Am▓Bm│Gm┤HmхMmХNmиSmИTm╣Ym║Zm╗v╝vйvЙ v┐%v└&v┴1v┬2v├;v─<v┼AvкBvКGv╚Hv╔Mv╩Nv╦Sv╠Tv═Yv╬Zv¤"
	optimizer
 "
trackable_list_wrapper
Х
0
1
2
 3
%4
&5
16
27
;8
<9
A10
B11
G12
H13
M14
N15
S16
T17
Y18
Z19"
trackable_list_wrapper
¤
1
2
3
 4
%5
&6
-7
.8
/9
110
211
;12
<13
A14
B15
G16
H17
M18
N19
S20
T21
Y22
Z23"
trackable_list_wrapper
╬

dlayers
regularization_losses
trainable_variables
elayer_regularization_losses
fnon_trainable_variables
glayer_metrics
	variables
hmetrics
м__call__
л_default_save_signature
+Л&call_and_return_all_conditional_losses
'Л"call_and_return_conditional_losses"
_generic_user_object
-
вserving_default"
signature_map
 "
trackable_dict_wrapper
Л
istate_variables

j_table
k	keras_api"ъ
_tf_keras_layerё{"class_name": "StringLookup", "name": "string_lookup", "trainable": true, "expects_training_arg": false, "dtype": "string", "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "stateful": false, "must_restore_from_config": true, "config": {"name": "string_lookup", "trainable": true, "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "dtype": "string", "invert": false, "max_tokens": 30, "num_oov_indices": 1, "oov_token": "[UNK]", "mask_token": "", "encoding": "utf-8"}}
"
_generic_user_object
 :
▄ђ2dense/kernel
:ђ2
dense/bias
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
░

llayers
regularization_losses
trainable_variables
mlayer_regularization_losses
nnon_trainable_variables
olayer_metrics
	variables
pmetrics
о__call__
+Н&call_and_return_all_conditional_losses
'Н"call_and_return_conditional_losses"
_generic_user_object
": 
ђђ2dense_1/kernel
:ђ2dense_1/bias
 "
trackable_list_wrapper
.
0
 1"
trackable_list_wrapper
.
0
 1"
trackable_list_wrapper
░

qlayers
!regularization_losses
"trainable_variables
rlayer_regularization_losses
snon_trainable_variables
tlayer_metrics
#	variables
umetrics
п__call__
+О&call_and_return_all_conditional_losses
'О"call_and_return_conditional_losses"
_generic_user_object
!:	ђ@2dense_2/kernel
:@2dense_2/bias
 "
trackable_list_wrapper
.
%0
&1"
trackable_list_wrapper
.
%0
&1"
trackable_list_wrapper
░

vlayers
'regularization_losses
(trainable_variables
wlayer_regularization_losses
xnon_trainable_variables
ylayer_metrics
)	variables
zmetrics
┌__call__
+┘&call_and_return_all_conditional_losses
'┘"call_and_return_conditional_losses"
_generic_user_object
C
-mean
.variance
	/count"
trackable_dict_wrapper
 "
trackable_list_wrapper
:2mean
:2variance
:	 2count
"
_generic_user_object
 :@@2dense_3/kernel
:@2dense_3/bias
 "
trackable_list_wrapper
.
10
21"
trackable_list_wrapper
.
10
21"
trackable_list_wrapper
░

{layers
3regularization_losses
4trainable_variables
|layer_regularization_losses
}non_trainable_variables
~layer_metrics
5	variables
metrics
▄__call__
+█&call_and_return_all_conditional_losses
'█"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
х
ђlayers
7regularization_losses
8trainable_variables
 Ђlayer_regularization_losses
ѓnon_trainable_variables
Ѓlayer_metrics
9	variables
ёmetrics
я__call__
+П&call_and_return_all_conditional_losses
'П"call_and_return_conditional_losses"
_generic_user_object
 :E@2dense_4/kernel
:@2dense_4/bias
 "
trackable_list_wrapper
.
;0
<1"
trackable_list_wrapper
.
;0
<1"
trackable_list_wrapper
х
Ёlayers
=regularization_losses
>trainable_variables
 єlayer_regularization_losses
Єnon_trainable_variables
ѕlayer_metrics
?	variables
Ѕmetrics
Я__call__
+▀&call_and_return_all_conditional_losses
'▀"call_and_return_conditional_losses"
_generic_user_object
 :@@2dense_5/kernel
:@2dense_5/bias
 "
trackable_list_wrapper
.
A0
B1"
trackable_list_wrapper
.
A0
B1"
trackable_list_wrapper
х
іlayers
Cregularization_losses
Dtrainable_variables
 Іlayer_regularization_losses
їnon_trainable_variables
Їlayer_metrics
E	variables
јmetrics
Р__call__
+р&call_and_return_all_conditional_losses
'р"call_and_return_conditional_losses"
_generic_user_object
 :@@2dense_6/kernel
:@2dense_6/bias
 "
trackable_list_wrapper
.
G0
H1"
trackable_list_wrapper
.
G0
H1"
trackable_list_wrapper
х
Јlayers
Iregularization_losses
Jtrainable_variables
 љlayer_regularization_losses
Љnon_trainable_variables
њlayer_metrics
K	variables
Њmetrics
С__call__
+с&call_and_return_all_conditional_losses
'с"call_and_return_conditional_losses"
_generic_user_object
 :@@2dense_7/kernel
:@2dense_7/bias
 "
trackable_list_wrapper
.
M0
N1"
trackable_list_wrapper
.
M0
N1"
trackable_list_wrapper
х
ћlayers
Oregularization_losses
Ptrainable_variables
 Ћlayer_regularization_losses
ќnon_trainable_variables
Ќlayer_metrics
Q	variables
ўmetrics
Т__call__
+т&call_and_return_all_conditional_losses
'т"call_and_return_conditional_losses"
_generic_user_object
 :@@2dense_8/kernel
:@2dense_8/bias
 "
trackable_list_wrapper
.
S0
T1"
trackable_list_wrapper
.
S0
T1"
trackable_list_wrapper
х
Ўlayers
Uregularization_losses
Vtrainable_variables
 џlayer_regularization_losses
Џnon_trainable_variables
юlayer_metrics
W	variables
Юmetrics
У__call__
+у&call_and_return_all_conditional_losses
'у"call_and_return_conditional_losses"
_generic_user_object
 :@2dense_9/kernel
:2dense_9/bias
 "
trackable_list_wrapper
.
Y0
Z1"
trackable_list_wrapper
.
Y0
Z1"
trackable_list_wrapper
х
ъlayers
[regularization_losses
\trainable_variables
 Ъlayer_regularization_losses
аnon_trainable_variables
Аlayer_metrics
]	variables
бmetrics
Ж__call__
+ж&call_and_return_all_conditional_losses
'ж"call_and_return_conditional_losses"
_generic_user_object
:	 (2	Adam/iter
: (2Adam/beta_1
: (2Adam/beta_2
: (2
Adam/decay
: (2Adam/learning_rate
ј
0
1
2
3
4
5
6
7
	8

9
10
11
12
13
14"
trackable_list_wrapper
 "
trackable_list_wrapper
5
-1
.2
/3"
trackable_list_wrapper
 "
trackable_dict_wrapper
(
Б0"
trackable_list_wrapper
 "
trackable_dict_wrapper
T
В_create_resource
ь_initialize
Ь_destroy_resourceR Z
tableМн
"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
┐

цtotal

Цcount
д	variables
Д	keras_api"ё
_tf_keras_metricj{"class_name": "Mean", "name": "loss", "dtype": "float32", "config": {"name": "loss", "dtype": "float32"}}
:  (2total
:  (2count
0
ц0
Ц1"
trackable_list_wrapper
.
д	variables"
_generic_user_object
%:#
▄ђ2Adam/dense/kernel/m
:ђ2Adam/dense/bias/m
':%
ђђ2Adam/dense_1/kernel/m
 :ђ2Adam/dense_1/bias/m
&:$	ђ@2Adam/dense_2/kernel/m
:@2Adam/dense_2/bias/m
%:#@@2Adam/dense_3/kernel/m
:@2Adam/dense_3/bias/m
%:#E@2Adam/dense_4/kernel/m
:@2Adam/dense_4/bias/m
%:#@@2Adam/dense_5/kernel/m
:@2Adam/dense_5/bias/m
%:#@@2Adam/dense_6/kernel/m
:@2Adam/dense_6/bias/m
%:#@@2Adam/dense_7/kernel/m
:@2Adam/dense_7/bias/m
%:#@@2Adam/dense_8/kernel/m
:@2Adam/dense_8/bias/m
%:#@2Adam/dense_9/kernel/m
:2Adam/dense_9/bias/m
%:#
▄ђ2Adam/dense/kernel/v
:ђ2Adam/dense/bias/v
':%
ђђ2Adam/dense_1/kernel/v
 :ђ2Adam/dense_1/bias/v
&:$	ђ@2Adam/dense_2/kernel/v
:@2Adam/dense_2/bias/v
%:#@@2Adam/dense_3/kernel/v
:@2Adam/dense_3/bias/v
%:#E@2Adam/dense_4/kernel/v
:@2Adam/dense_4/bias/v
%:#@@2Adam/dense_5/kernel/v
:@2Adam/dense_5/bias/v
%:#@@2Adam/dense_6/kernel/v
:@2Adam/dense_6/bias/v
%:#@@2Adam/dense_7/kernel/v
:@2Adam/dense_7/bias/v
%:#@@2Adam/dense_8/kernel/v
:@2Adam/dense_8/bias/v
%:#@2Adam/dense_9/kernel/v
:2Adam/dense_9/bias/v
Ё2ѓ
__inference__wrapped_model_6189я
І▓Є
FullArgSpec
argsџ 
varargsjargs
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *NбK
IџF
!і
input_2         
!і
input_1         
Т2с
F__inference_functional_1_layer_call_and_return_conditional_losses_6567
F__inference_functional_1_layer_call_and_return_conditional_losses_7356
F__inference_functional_1_layer_call_and_return_conditional_losses_7519
F__inference_functional_1_layer_call_and_return_conditional_losses_6709└
и▓│
FullArgSpec1
args)џ&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaultsџ
p 

 

kwonlyargsџ 
kwonlydefaultsф 
annotationsф *
 
Щ2э
+__inference_functional_1_layer_call_fn_7627
+__inference_functional_1_layer_call_fn_6906
+__inference_functional_1_layer_call_fn_7102
+__inference_functional_1_layer_call_fn_7573└
и▓│
FullArgSpec1
args)џ&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaultsџ
p 

 

kwonlyargsџ 
kwonlydefaultsф 
annotationsф *
 
.B,
__inference_save_fn_7874checkpoint_key
IBG
__inference_restore_fn_7882restored_tensors_0restored_tensors_1
ж2Т
?__inference_dense_layer_call_and_return_conditional_losses_7639б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
╬2╦
$__inference_dense_layer_call_fn_7648б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
в2У
A__inference_dense_1_layer_call_and_return_conditional_losses_7659б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
л2═
&__inference_dense_1_layer_call_fn_7668б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
в2У
A__inference_dense_2_layer_call_and_return_conditional_losses_7679б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
л2═
&__inference_dense_2_layer_call_fn_7688б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
в2У
A__inference_dense_3_layer_call_and_return_conditional_losses_7699б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
л2═
&__inference_dense_3_layer_call_fn_7708б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
№2В
E__inference_concatenate_layer_call_and_return_conditional_losses_7715б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
н2Л
*__inference_concatenate_layer_call_fn_7721б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
в2У
A__inference_dense_4_layer_call_and_return_conditional_losses_7732б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
л2═
&__inference_dense_4_layer_call_fn_7741б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
в2У
A__inference_dense_5_layer_call_and_return_conditional_losses_7752б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
л2═
&__inference_dense_5_layer_call_fn_7761б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
в2У
A__inference_dense_6_layer_call_and_return_conditional_losses_7772б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
л2═
&__inference_dense_6_layer_call_fn_7781б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
в2У
A__inference_dense_7_layer_call_and_return_conditional_losses_7792б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
л2═
&__inference_dense_7_layer_call_fn_7801б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
в2У
A__inference_dense_8_layer_call_and_return_conditional_losses_7812б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
л2═
&__inference_dense_8_layer_call_fn_7821б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
в2У
A__inference_dense_9_layer_call_and_return_conditional_losses_7831б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
л2═
&__inference_dense_9_layer_call_fn_7840б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
8B6
"__inference_signature_wrapper_7166input_1input_2
░2Г
__inference__creator_7845Ј
Є▓Ѓ
FullArgSpec
argsџ 
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *б 
┤2▒
__inference__initializer_7850Ј
Є▓Ѓ
FullArgSpec
argsџ 
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *б 
▓2»
__inference__destroyer_7855Ј
Є▓Ѓ
FullArgSpec
argsџ 
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *б 
	J
Const5
__inference__creator_7845б

б 
ф "і 7
__inference__destroyer_7855б

б 
ф "і 9
__inference__initializer_7850б

б 
ф "і ╠
__inference__wrapped_model_6189еj№ %&-.12;<ABGHMNSTYZXбU
NбK
IџF
!і
input_2         
!і
input_1         
ф "1ф.
,
dense_9!і
dense_9         ═
E__inference_concatenate_layer_call_and_return_conditional_losses_7715ЃZбW
PбM
KџH
"і
inputs/0         
"і
inputs/1         @
ф "%б"
і
0         E
џ ц
*__inference_concatenate_layer_call_fn_7721vZбW
PбM
KџH
"і
inputs/0         
"і
inputs/1         @
ф "і         EБ
A__inference_dense_1_layer_call_and_return_conditional_losses_7659^ 0б-
&б#
!і
inputs         ђ
ф "&б#
і
0         ђ
џ {
&__inference_dense_1_layer_call_fn_7668Q 0б-
&б#
!і
inputs         ђ
ф "і         ђб
A__inference_dense_2_layer_call_and_return_conditional_losses_7679]%&0б-
&б#
!і
inputs         ђ
ф "%б"
і
0         @
џ z
&__inference_dense_2_layer_call_fn_7688P%&0б-
&б#
!і
inputs         ђ
ф "і         @А
A__inference_dense_3_layer_call_and_return_conditional_losses_7699\12/б,
%б"
 і
inputs         @
ф "%б"
і
0         @
џ y
&__inference_dense_3_layer_call_fn_7708O12/б,
%б"
 і
inputs         @
ф "і         @А
A__inference_dense_4_layer_call_and_return_conditional_losses_7732\;</б,
%б"
 і
inputs         E
ф "%б"
і
0         @
џ y
&__inference_dense_4_layer_call_fn_7741O;</б,
%б"
 і
inputs         E
ф "і         @А
A__inference_dense_5_layer_call_and_return_conditional_losses_7752\AB/б,
%б"
 і
inputs         @
ф "%б"
і
0         @
џ y
&__inference_dense_5_layer_call_fn_7761OAB/б,
%б"
 і
inputs         @
ф "і         @А
A__inference_dense_6_layer_call_and_return_conditional_losses_7772\GH/б,
%б"
 і
inputs         @
ф "%б"
і
0         @
џ y
&__inference_dense_6_layer_call_fn_7781OGH/б,
%б"
 і
inputs         @
ф "і         @А
A__inference_dense_7_layer_call_and_return_conditional_losses_7792\MN/б,
%б"
 і
inputs         @
ф "%б"
і
0         @
џ y
&__inference_dense_7_layer_call_fn_7801OMN/б,
%б"
 і
inputs         @
ф "і         @А
A__inference_dense_8_layer_call_and_return_conditional_losses_7812\ST/б,
%б"
 і
inputs         @
ф "%б"
і
0         @
џ y
&__inference_dense_8_layer_call_fn_7821OST/б,
%б"
 і
inputs         @
ф "і         @А
A__inference_dense_9_layer_call_and_return_conditional_losses_7831\YZ/б,
%б"
 і
inputs         @
ф "%б"
і
0         
џ y
&__inference_dense_9_layer_call_fn_7840OYZ/б,
%б"
 і
inputs         @
ф "і         А
?__inference_dense_layer_call_and_return_conditional_losses_7639^0б-
&б#
!і
inputs         ▄	
ф "&б#
і
0         ђ
џ y
$__inference_dense_layer_call_fn_7648Q0б-
&б#
!і
inputs         ▄	
ф "і         ђ№
F__inference_functional_1_layer_call_and_return_conditional_losses_6567цj№ %&-.12;<ABGHMNSTYZ`б]
VбS
IџF
!і
input_2         
!і
input_1         
p

 
ф "%б"
і
0         
џ №
F__inference_functional_1_layer_call_and_return_conditional_losses_6709цj№ %&-.12;<ABGHMNSTYZ`б]
VбS
IџF
!і
input_2         
!і
input_1         
p 

 
ф "%б"
і
0         
џ ы
F__inference_functional_1_layer_call_and_return_conditional_losses_7356дj№ %&-.12;<ABGHMNSTYZbб_
XбU
KџH
"і
inputs/0         
"і
inputs/1         
p

 
ф "%б"
і
0         
џ ы
F__inference_functional_1_layer_call_and_return_conditional_losses_7519дj№ %&-.12;<ABGHMNSTYZbб_
XбU
KџH
"і
inputs/0         
"і
inputs/1         
p 

 
ф "%б"
і
0         
џ К
+__inference_functional_1_layer_call_fn_6906Ќj№ %&-.12;<ABGHMNSTYZ`б]
VбS
IџF
!і
input_2         
!і
input_1         
p

 
ф "і         К
+__inference_functional_1_layer_call_fn_7102Ќj№ %&-.12;<ABGHMNSTYZ`б]
VбS
IџF
!і
input_2         
!і
input_1         
p 

 
ф "і         ╔
+__inference_functional_1_layer_call_fn_7573Ўj№ %&-.12;<ABGHMNSTYZbб_
XбU
KџH
"і
inputs/0         
"і
inputs/1         
p

 
ф "і         ╔
+__inference_functional_1_layer_call_fn_7627Ўj№ %&-.12;<ABGHMNSTYZbб_
XбU
KџH
"і
inputs/0         
"і
inputs/1         
p 

 
ф "і         Ѓ
__inference_restore_fn_7882djVбS
LбI
(і%
restored_tensors_0         
і
restored_tensors_1	
ф "і ъ
__inference_save_fn_7874Ђj&б#
б
і
checkpoint_key 
ф "Мџ¤
kфh

nameі
0/name 
#

slice_specі
0/slice_spec 
(
tensorі
0/tensor         
`ф]

nameі
1/name 
#

slice_specі
1/slice_spec 

tensorі
1/tensor	Я
"__inference_signature_wrapper_7166╣j№ %&-.12;<ABGHMNSTYZiбf
б 
_ф\
,
input_1!і
input_1         
,
input_2!і
input_2         "1ф.
,
dense_9!і
dense_9         